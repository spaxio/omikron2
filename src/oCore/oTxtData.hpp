﻿/*
 * Copyright © 2013 Karol Pałka
 */

#pragma once
#include "oBinData.hpp"

namespace oo
{

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////// Logowanie do pliku //////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	class CFileLogger : public IOutputDevice
	{
		private:
			CFile File;

		public:
			CFileLogger(const tstring& filename);
			~CFileLogger();

			void Write(const tstring& message) override;
			void Writeln(const tstring& message) override;

	};

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// Zapis i odczyt plikow tekstowych ///////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Zapisuje tekst do wybranego pliku (kodowanie UTF-8)
	bool SaveTextToFile(const tstring& filename, const tstring& str);
	// Odczytuje tekst z wybranego pliku (TYLKO UTF-8)
	bool LoadTextFromFile(const tstring& filename, tstring& str);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////// Parser sx;2 //////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	/*
	 *		sx;2
	 *
	 *		Komentarz - Rozpoczyna sie od znaku 'Hash' <#> konczy znakiem nowej linii
	 *		Sekcja    - Otwarcie sekcji - <[> zamkniecie sekcji - <]>
	 *		Operator  - Dwokropek <:>
	 *		Klucze    - Moze skladac sie jedynie ze znakow [a-z] [A-Z] [0-9] [_] | Klucz nie moze zaczynac sie od cyfry
	 *		lancuch   - Rozpoczyna sie od <"> a konczy na <">. Pomiedzy tymi znakami moga byc dowolne znaki UTF-8
	 *		            Moga byc takze uzywane sekwencje ucieczki: [&n] [&r] [&t] [&&] [&"]
	 *		Wartosc   - 32 bitowa wartosc calkowita ze znakiem albo liczba zmiennoprzecinkowa pojedynczej prezycji (bez wsparcia dla notacji naukowej)
	 */

	// Znaki specialne
	enum { S_OPERATOR		= ':'  };
	enum { S_OPENSECTION	= '['  };
	enum { S_CLOSESECTION	= ']'  };
	enum { S_QUOTE			= '"'  };
	enum { S_SPECIALSIG		= '\\' };
	enum { S_LINECOMMENT	= '#'  };

	// Klasa pozwalajaca na zapis kodu w jezyku sx;2
	class CSxWriter
	{
		public:
			// Stale flagi formatowania
			enum { NONE = 0 };		// Nic :P
			enum { L_SPACE = 1 };	// Lewa Spaca
			enum { L_EOL = 2 };		// Lewy koniec lini
			enum { L_TAB = 4 };		// Lewy tab
			enum { R_SPACE = 8 };	// Prawa spacja
			enum { R_EOL = 16 };	// Prawy koniec lini
			enum { R_TAB = 32 };	// Prawy tab

		private:
			tstring Code;	// Tekst
			msize Depth;	// Glebokosc

		private:
			// Wpisuje odpowiednia liczbe tabulatorow
			void SetTab(msize num) { while(num--) Code += '\t'; }
			// Wpisuje odpowiednie biale znaki po lewej stronie elementow
			void WsLeft(u32 n);
			// Wpisuje odpowiednie biale znaki po prawej stronie elementow
			void WsRight(u32 n);

		public:
			// Sprawdza czy dany lancuch znakow mozna zapisac w postaci klucza
			static bool CanWriteAsKey(const tstring& str);
			// Poprawia lancuch znakow
			static tstring CorectString(const tstring& str);

		public:
			// Domyslny konstruktor (nie robi nic konkretnego ;p)
			CSxWriter() : Depth(0) {}
			// Zapisje cala tresc do wybranego pliku (jesli istnieje to go nadmienia)
			bool SaveToFile(const tstring& filename) { return SaveTextToFile(filename, Code); }
			// Zwraca referencje na kod
			tstring& GetCodeRef() { return Code; }
			// Zwraca kod
			tstring GetCode() { return Code; }

		public:
			// Wpisuje lancuch znakow
			void WriteString(const tstring& text, u32 ws = NONE) { WsLeft(ws); Code += CorectString(text); WsRight(ws); }
			// Wpisuje liczbe calkowita
			void WriteInteger(int v, u32 ws = NONE) { WsLeft(ws); Code += ToString(v); WsRight(ws); }
			// Wpisuje liczbe zmiennoprzecinkowa
			void WriteFloat(float v, u32 ws = NONE) { WsLeft(ws); Code += ToString(v); WsRight(ws); }
			// Wpisuje znak poczatku sekcji
			void WriteOpenSection(u32 ws = R_EOL) { Depth++; WsLeft(ws); Code += S_OPENSECTION; WsRight(ws); }
			// Wpisuje znak konca sekcji
			void WriteCloseSection(u32 ws = L_EOL) { Depth--; WsLeft(ws); Code += S_CLOSESECTION; WsRight(ws); }
			// Wpisuje operator
			void WriteOperator(u32 ws = R_SPACE) { WsLeft(ws); Code += S_OPERATOR; WsRight(ws); }
			// Wpisuje komentarz
			void WriteComment(const tstring& comment, u32 ws = R_EOL) { WsLeft(ws); Code += S_LINECOMMENT; Code += comment; WsRight(ws); }
			// Dowolne
			void WriteFree(const tstring& str, u32 ws = NONE) { WsLeft(ws); Code += str; WsRight(ws); }

	};

	// Klasa pozwalajaca na odczyt kodu w jezyku sx;2
	class CSxReader
	{
		private:
			const tstring* Code;	// Staly wskaznik na tekst ktory ma byc parsowany
			msize Index;			// Aktualny index

		public:
			// Sprawdza na podstawie indexu pozycje kursora w tekscie
			void GetPos(msize& ln, msize& col);
			tstring GetPosAsString();

		public:
			// Konstruktory
			CSxReader(const tstring* code) : Code(code), Index(0) { assert(code); }

		private:
			// Odczytuje dowolny znak (ignorujac przy tym komentarze)
			// c - Tutaj zostanie zwrocony znak
			bool Parse_ch(msize& index, char& c);
			// Sprawdza czy wystepuje z gory okreslony znak
			// c - Oczekiwany znak
			bool Parse_pch(msize& index, char c);
			// Parsuje bialy znak
			bool Parse_w(msize& index);
			// Jedzie przez wszystkie biale znaki
			void Parse_W(msize& index);
			// Parsuje wartosc liczbowa
			bool Parse_value(msize& index, tstring& outval);
			// Parsuje wartosc tekstowo-hexagonalna
			// size - ile znakow ma przetworzyc (np. dla lancucha "FFFF" size wynosi 4)
			bool Parse_char_code(msize& index, u32& out, u32 size);

		public:
			// Odczytuje lancuch znakow
			bool ReadString(tstring& str);
			// Odczytuje liczbe zmiennoprzecinkowa
			bool ReadFloat(float &v);
			// Odczytuje liczbe calkowita
			bool ReadInteger(int &v);
			// Odczytuje operator
			bool ReadOperator();
			// Odczytuje znak otwarcia sekcji
			bool ReadOpenSection();
			// Odczytuje znak zamkniecia sekcji
			bool ReadCloseSection();

		public:
			// Odczyt z wyjatakim
			void MustReadString(tstring& element) { if (!ReadString(element)) throw omikron_exception("Nieprawidlowy lancuch znakow"); }
			void MustReadFloat(float &v) { if (!ReadFloat(v)) throw omikron_exception("Nieprawidlowa wartosc zmiennoprzecinkowa"); }
			void MustReadInteger(int &v) { if (!ReadInteger(v)) throw omikron_exception("Nieprawidlowa wartosc calkowita"); }
			void MustReadOperator() { if (!ReadOperator()) throw omikron_exception("Brakuje ':'"); }
			void MustReadOpenSection() { if (!ReadOpenSection()) throw omikron_exception("Brakuje '['"); }
			void MustReadCloseSection() { if (!ReadCloseSection()) throw omikron_exception("Brakuje ']'"); }

	};

	inline bool StringToBoolThrow(const tstring& str, const tstring& errormsg)
	{
		tstring val = appToLowerCopy(str);
		if (val == "yes" || val == "true" || val == "1") return true;
		else if (val == "no" || val == "false" || val == "0") return false;
		else throw omikron_exception(errormsg);
	}

	enum ESxValType { VT_FLOAT, VT_INT, VT_STRING };

	// Pojedyncza wartosc w jezyku Sx;2 (zmiennoprzecinkowa, calkowita, lanncuch znakow, klucz)
	class CSxVal
	{
		private:
			union
			{
				int		IntVal;
				float	FloatVal;
				char*	StringVal;
			};

			int SxValType;	// Typ jaki przechowujemy w tej klasie

		private:
			void Copy(const CSxVal& other);

			void Delete() { delete [] StringVal; };
			void Allocate(msize length) { StringVal = new char[length]; };
			void Check() { if (SxValType == VT_STRING) Delete(); }
			void SetString(const tstring& str)
			{
				Allocate(str.size() + 1);
				memcpy(StringVal, str.c_str(), (str.size() + 1) * sizeof(char));
			}
			void CheckAndSetString(const tstring& str) { Check(); SetString(str); }

		public:
			CSxVal() : SxValType(VT_INT) {}
			~CSxVal() { Check(); }

			CSxVal(const CSxVal& other)
			{
				Copy(other);
			}
			CSxVal& operator=(const CSxVal& other)
			{
				if (this != &other) { Check(); Copy(other); }
				return *this;
			}

			CSxVal(CSxVal&& other) :
				StringVal(other.StringVal),
				SxValType(other.SxValType)
			{
				other.SxValType = VT_INT;
				other.StringVal = nullptr;
			}
			CSxVal& operator=(CSxVal&& other)
			{
				if (this != &other)
				{
					Check();
					SxValType = other.SxValType;
					StringVal = other.StringVal;
					other.SxValType = VT_INT;
					other.StringVal = nullptr;
				}

				return *this;
			}

			CSxVal& operator=(int value) { Check(); IntVal = value; SxValType = VT_INT; return *this; }
			CSxVal& operator=(float value) { Check(); FloatVal = value; SxValType = VT_FLOAT; return *this; }
			CSxVal& operator=(const tstring& value) { CheckAndSetString(value); SxValType = VT_STRING; return *this; }

			CSxVal(int value) : IntVal(value), SxValType(VT_INT) {}
			CSxVal(float value) : FloatVal(value), SxValType(VT_FLOAT) {}
			CSxVal(const tstring& value) : SxValType(VT_STRING) { SetString(value); }

			bool CanGetAsInt() const { return SxValType == VT_INT || SxValType == VT_FLOAT; }
			bool CanGetAsFloat() const { return SxValType == VT_FLOAT || SxValType == VT_INT; }
			bool CanGetAsString() const { return SxValType == VT_STRING; }

			int GetAsInt() const
			{
				assert(SxValType == VT_INT || SxValType == VT_FLOAT);
				if (SxValType == VT_INT) return IntVal;
				else return static_cast<int>(FloatVal);
			}

			float GetAsFloat() const
			{
				assert(SxValType == VT_FLOAT || SxValType == VT_INT);
				if (SxValType == VT_FLOAT) return FloatVal;
				else return static_cast<float>(IntVal);
			}

			tstring GetAsString() const { assert(SxValType == VT_STRING); return StringVal; }

			ESxValType GetType() const { return (ESxValType)SxValType; }

			const char* GetAsCStr() const { assert(SxValType == VT_STRING); return StringVal; }

		public:
			tstring ToString() const
			{
				switch (SxValType)
				{
					case VT_INT: return ::oo::ToString(IntVal);
					case VT_FLOAT: return ::oo::ToString(FloatVal);
					default: return StringVal;
				}
			}

		public:
			void Serialize(ISerializer& archive);
	};

	typedef CSxVal SxVal_t;

	// Pojedynczy wezel
	class CSxNode
	{
		public:
			typedef std::list<CSxNode> Elements_t;
			typedef std::vector<SxVal_t> Values_t;

			tstring Key;			// Klucz tego wezla
			u32 KeyHash;			// Skrot klucza
			Values_t Values;		// Wartosci wezla
			Elements_t Elements;	// Lista nastepnych wezlow

	};

	class CSxHigWriter
	{
		private:
			CSxNode* MainNode;		// Wskaznik do punktu zaczepnego bazy danych
			CSxNode* ActiveNode;	// Wskaznik do naszego aktywnego elementu

		public:
			// Szuka odpowiedniego elementu w korzeniu n (jesli go nie znajduje to tworzy go)
			static CSxNode* FindElement(const tstring& dir, CSxNode* n);
			// Zapisuje baze danych w formacie sx;2 do pliku
			static bool SaveNodeToFile(CSxNode* n, const tstring& filename);
			// Zapisuje baze danych w formacie sx;2 do stringa
			static void SaveNodeToString(CSxNode* n, tstring& code);

		public:
			// Konstruktor
			CSxHigWriter() : MainNode(nullptr), ActiveNode(nullptr) {}

			// Ustawia zewnetrzna baze danych
			void SetDataBase(CSxNode* node) { assert(node); MainNode = node; ActiveNode = node; }

			// Szuka wybranego elementu i ustawia go jako aktywny
			CSxHigWriter& operator[](const char* dir) 		{ ActiveNode = FindElement(dir, MainNode); return *this; }
			CSxHigWriter& operator<<(int value)		        { ActiveNode->Values.push_back(value); return *this; }
			CSxHigWriter& operator<<(float value)	        { ActiveNode->Values.push_back(value); return *this; }
			CSxHigWriter& operator<<(const tstring& value)	{ ActiveNode->Values.push_back(value); return *this; }
			CSxHigWriter& operator<<(bool value)	        { ActiveNode->Values.push_back(value?1:0); return *this; }

	};

	class CSxHigReader
	{
		private:
			CSxNode* MainNode;		// Punkt zaczepny bazy danych
			CSxNode* ActiveNode;    // Wskaznik do naszego aktywnego elementu
			msize Index;			// Aktualny numer wartosci (patrz, operatory)

			// Sprawdza poprawnosc indeksa, jesli jest bledny to rzuca wyjatek
			void ValidIndex();

		public:
			// Szuka odpowiedniego elementu w korzeniu n (jesli go nie znajduje zwraca nullptr)
			static CSxNode* FindElement(const tstring& dir, CSxNode* n);
			// Odczytuje baze danych w formacie sx;2 z pliku
			static void LoadNodeFromFile(CSxNode* n, const tstring& filename);
			// Odczytuje baze danych w formacie sx;2 ze stringa
			static void LoadNodeFromString(CSxNode* n, const tstring& code);

		public:
			// Konstruktor
			CSxHigReader() : MainNode(nullptr), ActiveNode(nullptr), Index(0) {}
			CSxHigReader(CSxNode* node) { SetDataBase(node); }

			// Ustawia baze danych na ktorej CSxHigReader bedzie operowac
			void SetDataBase(CSxNode* node) { assert(node); MainNode = node; ActiveNode = node; Index =0; }

		public:
			// Podaje liczbe wartosci aktualnie wybranego elementu
			msize GetNumValues() const { return ActiveNode->Values.size(); }
			// Podaje index wartosci aktualnie wybranego elementu
			msize GetIndex() const { return Index; }
			// Podaje liczbe elementow aktualnie wybranego elementu
			msize GetNumElements() const { return ActiveNode->Elements.size(); }
			// Zwraca bezposrednio stala referencje do liczby elementow
			const CSxNode::Elements_t& GetElements() const { return ActiveNode->Elements; }

		public:
			CSxHigReader& At(const tstring& dir);

			// Sprawdza istnienie danego klucza (bool set okresla czy po znalezieniu klucza ma go ustawic jako aktywny)
			// Metoda nie rzuca wyjatkow (metoda nie wspolpracuje z At i operatorem[])
			bool Exist(const char* dir, bool set = false);

			// Operatory
			CSxHigReader& operator[](const char* dir);
			CSxHigReader& operator>>(int& c);
			CSxHigReader& operator>>(float& c);
			CSxHigReader& operator>>(tstring& c);
			CSxHigReader& operator>>(SxVal_t& c);
			CSxHigReader& operator>>(bool& c);

	};

	/////////////////////////////////////////////////////////////////////

	// Funkcja mapujaca pliki tekstowe na krotke
	//
	// Powiedzmy, ze masz plik tekstowy z danymi o takiej postaci:
	// obiekt1 = 34 2221 "test" klucz;
	// obiekt2 = 12  344 "test" innyklucz;
	// obiekt3 = 3     0 "test" klucz;
	// obiekt4 = 3    34 "test" klucz;
	//
	// Zakladajac ze caly plik zawiera linie o jednakowym foramcie (linia w rozmieniu zawartosc miedzy srednikami ;)
	// To mozemy wykorzystac funkcje ponizej do zmapowania ja na wektor krotek
	//
	// std::vector< boost::tuple<kstring, int, int, tstring, kstring> > Dane;
	// MapScript("mojedane.txt", Dane);
	//

	inline bool ReadValByType(int& val, CSxReader& reader) { return reader.ReadInteger(val); }
	inline bool ReadValByType(float& val, CSxReader& reader) { return reader.ReadFloat(val); }
	inline bool ReadValByType(tstring& val, CSxReader& reader) { return reader.ReadString(val); }

	// Niestandardowe rozszerzenia
	inline bool ReadValByType(recti& val, CSxReader& reader)
	{
		if ( reader.ReadInteger(val.left) &&
			 reader.ReadInteger(val.top) &&
			 reader.ReadInteger(val.right) &&
			 reader.ReadInteger(val.bottom) ) return true;
		else return false;
	}

	// jak bedzie potrzeba to dodamy dla vect2f rectf colorf itp...

	inline void ReadBySymbol(CSxReader&, boost::tuples::null_type) {}

	template <typename T>
	inline void ReadBySymbol(CSxReader& reader, T& pack)
	{
		if (!ReadValByType(pack.head, reader))
		{
			throw omikron_exception("Zla wartosc : " + reader.GetPosAsString());
		}
		ReadBySymbol(reader, pack.get_tail());
	}

	template <typename T>
	inline bool MapScript(const tstring& filename, T& pack)
	{
		try
		{
			tstring code;
			if (!LoadTextFromFile(filename, code))
			{
				throw omikron_exception("Nie mozna wczytac pliku : " + filename);
			}

			CSxReader reader(&code);

			typename T::value_type tuple;
			while (ReadValByType(tuple.head, reader))
			{
				reader.MustReadOperator();
				ReadBySymbol(reader, tuple.get_tail());
				pack.push_back(tuple);
			}
		}
		catch (omikron_exception& e)
		{
			LogMsg("Nie udalo sie zmapowac pliku na krotke : " + e.str());
			return false;
		}

		return true;
	}

} // End oo
