﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oBinData.hpp"

#include <zlib.h>

namespace oo
{

	/////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// Strumienie /////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	CFile::CFile() : File(nullptr)
	{
	}

	CFile::CFile(const tstring& filename, EOpenMode om) : File(nullptr)
	{
		Open(filename, om);
	}

	CFile::~CFile()
	{
		Close();
	}

	bool CFile::Open(const tstring& filename, EOpenMode om)
	{
		Close();

		if (om == OM_WRITE) File = fopen(filename.c_str(), "wb");
		else File = fopen(filename.c_str(), "rb");

		OpenMode = om;

		return File != nullptr;
	}

	void CFile::Close()
	{
		if (File != nullptr)
		{
			fclose(File);
			File = nullptr;
		}
	}

	void CFile::Flush()
	{
		if (File != nullptr)
		{
			fflush(File);
		}
	}

	msize CFile::GetSize()
	{
		if (File != nullptr)
		{
			msize p = ftell(File);
			fseek(File, 0, SEEK_END);
			msize r = ftell(File);
			fseek(File, p, SEEK_SET);
			return r;
		}
		else return 0;
	}

	msize CFile::GetPos()
	{
		if (File != nullptr) return ftell(File);
		else return 0;
	}

	void CFile::SetPos(msize p, EStreamPos s)
	{
		if (File != nullptr)
		{
			switch (s)
			{
				case SP_BEG: fseek(File, p, SEEK_SET); break;
				case SP_END: fseek(File, p, SEEK_END); break;
				case SP_CUR: fseek(File, p, SEEK_CUR); break;
			}
		}
	}

	bool CFile::IsOK()
	{
		return File != nullptr;
	}

	bool CFile::EndOfStreame()
	{
		return feof(File) != 0;
	}

	bool CFile::CanRead()
	{
		return OpenMode == OM_READ;
	}

	bool CFile::CanWrite()
	{
		return OpenMode == OM_WRITE;
	}

	msize CFile::Read(void* data, msize size)
	{
		assert(data);

		if ((File == nullptr) || (size == 0)) return 0;
		return fread(data, 1, size, File);
	}

	msize CFile::Write(const void* data, msize size)
	{
		assert(data);

		if ((File == nullptr) || (size == 0)) return 0;
		return fwrite(data, 1, size, File);
	}

	u8* CBuffer::GetPtr()
	{
		return Data.data();
	}

	msize CBuffer::GetSize()
	{
		return Data.size();
	}

	msize CBuffer::GetPos()
	{
		return Pos;
	}

	void CBuffer::SetPos(msize p, EStreamPos s)
	{
		switch(s)
		{
			case  SP_BEG:
				if (p >  Data.size()) Pos =  Data.size();
				else Pos = p;
			break;

			case  SP_END:
				if (p > Data.size()) Pos = 0;
				else Pos = Data.size() - p;
			break;

			case  SP_CUR:
				if ((Pos + p) > Data.size()) Pos = Data.size();
				else Pos +=p;
			break;
		}
	}

	bool CBuffer::IsOK()
	{
		return true;
	}

	bool CBuffer::EndOfStreame()
	{
		return Data.size() == Pos;
	}

	bool CBuffer::CanRead()
	{
		return true;
	}

	bool CBuffer::CanWrite()
	{
		return true;
	}

	msize CBuffer::Read(void* data, msize size)
	{
		if (size == 0) return 0;
		assert(data != nullptr);

		if ( (Pos + size) > Data.size() ) size = Data.size() - Pos;
		memmove(data, Data.data() + Pos, size);
		Pos += size;

		return size;
	}

	msize CBuffer::Write(const void* data, msize size)
	{
		if (size == 0) return 0;
		assert(data != nullptr);

		if ( (Pos + size) > Data.size() ) size = Data.size() - Pos;
		memmove(Data.data() + Pos, data, size);
		Pos += size;

		return size;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// Rozne fajne struktury ////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Rejestr wszystkich CMemoryPool
	typedef std::vector<CMemoryPool*> MemoryPoolReg_t;

	// Takie dziwadlo
	MemoryPoolReg_t& GetMemoryPoolRegInstance()
	{
		static MemoryPoolReg_t MemoryPoolReg;
		return MemoryPoolReg;
	}

	void MemoryPoolWriteStat(IOutputDevice& out)
	{
		const MemoryPoolReg_t& mp = GetMemoryPoolRegInstance();
		for (MemoryPoolReg_t::const_iterator it = mp.begin(); it != mp.end(); ++it)
		{
			out.Writeln( (*it)->GetTextStats() );
		}
	}

	CMemoryPool::CMemoryPool(const char* name, msize chunksize, msize chunkcount) : Name(name), ChunkSize(chunksize), ChunkCount(chunkcount), PoolSize(chunksize*chunkcount), Allocated(0)
	{
		assert(chunksize >= 4);

		Pool = malloc(PoolSize);
		assert(Pool);

		#ifdef _DEBUG
		memset(Pool, 0xEF, PoolSize);
		#endif

		FreeChunk = Pool;

		msize offset = (msize)Pool + PoolSize - ChunkSize;
		msize rawlastptr = 0;

		while ((void*)offset != Pool)
		{
			*((msize*)offset) = rawlastptr;
			rawlastptr = offset;
			offset -= ChunkSize;

		}

		*((msize*)Pool) = rawlastptr;

		GetMemoryPoolRegInstance().push_back(this);
	}

	CMemoryPool::~CMemoryPool()
	{
		// Potencjalny wyciek
		assert(Allocated == 0);

		free(Pool);

		MemoryPoolReg_t& mp = GetMemoryPoolRegInstance();
		auto it = std::find(mp.begin(), mp.end(), this);
		if (it != mp.end()) mp.erase(it);
		else assert(0);
	}

	tstring CMemoryPool::GetTextStats() const
	{
		return format("%s %i/%i x %i B -> %i/%i kB",
					   GetName(), GetAllocated(), GetChunkCount(), GetChunkSize(),
					   ((GetAllocated() * GetChunkSize()) / 1024), (GetPoolSize() / 1024) );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////// Sumy kontrolne CRC32 /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Algorytm generujacy tablice CRC
	std::vector<u32> MakeCrc32Table()
	{
		const u32 polynomial = 0xEDB88320; // Jest to oficjalny wielomian CRC32
		std::vector<u32> table(256);
		int i, j;

		u32 crc;
		for (i = 0; i < 256; i++)
		{
			crc = i;
			for (j = 8; j > 0; j--)
			{
				if (crc & 1) crc = (crc >> 1) ^ polynomial;
				else crc >>= 1;
			}
			table[i] = crc;
		}
		return table;
	}

	// Tablica CRC32
	const std::vector<u32> Crc32Table = MakeCrc32Table();

	// Startowa suma dla CRC_INIT
	const u32 CRC_INIT = 0xFFFFFFFF;

	// Funkcja generuje CRC dla danych
	inline void CRC32_Update(u8 *data, msize size, u32 &crc)
	{
		while (size--) crc = (crc >> 8) ^ Crc32Table[(*data++) ^ (crc & 0xFF)];
	}

	// Generuje sume CRC z danych
	u32 GetCRC(u8* data, msize size)
	{
		assert(data);
		u32 crc = CRC_INIT;
		CRC32_Update(data, size, crc);
		return ~crc;
	}

	// Generuje sume CRC ze strumienia
	u32 GetCRC(IStream& stream)
	{
		u32 crc = CRC_INIT;

		if (stream.IsOK() && stream.CanRead())
		{
			u8 buffer[1024];
			msize len =0;
			msize pos =0;

			pos = stream.GetPos();
			stream.SetPos(0, IStream::SP_BEG);
			while (!stream.EndOfStreame())
			{
				len = stream.Read(buffer, 1024);
				CRC32_Update(buffer, len, crc);
			}

			stream.SetPos(pos, IStream::SP_BEG);
		}

	  return ~crc;
	}

	u32 GetCRC(const tstring& str)
	{
		u32 crc = CRC_INIT;
		CRC32_Update((u8*)str.c_str(), (u32)str.size(), crc);
		return ~crc;
	}

	u32 CombinationTwoCRC(u32 crc1, u32 crc2)
	{
		CRC32_Update((u8*)&crc2, sizeof(crc2), crc1);
		return crc1;
	}

	void GenCRC(u8 *data, msize size, u32 &crc)
	{
		assert(data);
		CRC32_Update(data, size, crc);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// Engine File System //////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Kompresuje dane
	bool CompressData(const u8* data, msize datasize, u8* buffer, msize& buffersize)
	{
		assert(data);
		assert(buffer);

		uLong bs=buffersize;
		int result = compress(buffer, &bs, data, datasize);
		if (result != Z_OK) return false;

		buffersize = bs;
		return true;
	}

	// Dekompresuje
	bool DecompressData(const u8* data, msize datasize, u8* buffer, msize& buffersize)
	{
		assert(data);
		assert(buffer);

		uLong bs=buffersize;
		int result = uncompress(buffer, &bs, data, datasize);
		if (result != Z_OK) return false;

		buffersize = bs;
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// MIX Virtual File System ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	enum { MIX_ID		= 0x55525552 };			// Identyfikator formatu (tutaj 'RURU')
	enum { MIX_VERSION	= 0x00000003 };			// Wersja formatu
	enum { MIX_MAX_FS	= 0x01000000 };			// Maxymalny rozmiar pliku w archiwum MIX (tutaj 16MB)

	enum { FF_EMPTY_FILE	= 0x00 };	// Uszkodzony/Pusty
	enum { FF_NORMAL_FILE	= 0x01 };	// Zwykly plik
	enum { FF_COMPRESS_FILE = 0x02 };	// Skompresowany plik

	const char MIX_NAME_SEPARATOR = '\n';		// Odziela nazwy w tablicy nazw

	// Naglowek pliku MIX (vfs) sizeof - 28 bajtow
	#pragma pack(1)
	struct MIXHEADER
	{
		u32 Id;					// Identyfikator formatu
		u32 Version;			// Wersja archiwum MIX

		u32 FileHeadersOffset;	// Przesuniecie informacji o poszczegolnych plikach
		u32 NumFiles;			// Liczba plikow

		u32 DataOffset;			// Przesuniecie danych wlasciwych

		u32 NameTableOffset;	// Przesuniecie tablicy nazw
		u32 NameTableSize;		// Rozmiar tablicy nazw

	};
	#pragma pack()

	// Naglowek pojedynczego wewnetrznego pliku (w archiwum MIX) sizeof = 24 bajty
	#pragma pack(1)
	struct MIXFILEHEADER
	{
		u32 FileType;			// Typ pliku

		u32 FileSize;			// Rozmiar
		u32 FileSizeCompress;	// Skompresowany
		u32 FileOffset;			// Poczatek pliku

		u32 DataChecksum;		// Suma kontrolna pliku CRC32

		u32 FileNameHash;		// Skrot nazwy

	};
	#pragma pack()

	bool Vfs_WriteCompressDataToMix(const tstring& filename, CBuffer& data, CFile& file, u32 fileindex, u32& offset)
	{
		// Wskaznik na bufor ktory bedziemy zapisywac (aktywny do zapisu jako nie skompresowany)
		CBuffer dataC(data.GetSize() * 2 + 512);
		msize dataC_size = dataC.GetSize();

		if (!CompressData((const u8*)data.GetPtr(), data.GetSize(), (u8*)dataC.GetPtr(), dataC_size))
		{
			LogMsg( "[ERR] [MIX] Nie mozna skompresowac danych z pliku : " + filename );
			return false;
		}

		MIXFILEHEADER mixfileheader;


		// Wypelniamy naglowek pliku
		mixfileheader.FileType = FF_COMPRESS_FILE;
		mixfileheader.FileOffset = offset;
		mixfileheader.FileSize = data.GetSize();
		mixfileheader.FileSizeCompress = dataC_size;
		mixfileheader.DataChecksum = GetCRC(data);
		mixfileheader.FileNameHash = HashString(filename);

		// Zapisujemy go w odpowiednim rekordzie
		file.SetPos(sizeof(MIXHEADER) + (sizeof(MIXFILEHEADER)*fileindex), CFile::SP_BEG);
		file.Write(&mixfileheader, sizeof(mixfileheader));

		// Powracamy do poprzedniej pozycji (tam gdzie sa dane plikow)
		file.SetPos(offset, CFile::SP_BEG);

		// Zapisujemy buforek
		file.Write(dataC.GetPtr(), dataC_size);

		// Dodajemy rozmiar do offsetu
		offset += dataC_size;

		return true;
	}

	bool Vfs_WriteDataToMix(const tstring& filename, CBuffer& data, CFile& file, u32 fileindex, u32& offset)
	{
		MIXFILEHEADER mixfileheader;

		// Wypelniamy naglowek pliku
		mixfileheader.FileType = FF_NORMAL_FILE;
		mixfileheader.FileOffset = offset;
		mixfileheader.FileSize = data.GetSize();
		mixfileheader.FileSizeCompress = data.GetSize();
		mixfileheader.DataChecksum = GetCRC(data);
		mixfileheader.FileNameHash = HashString(filename);

		// Zapisujemy go w odpowiednim rekordzie
		file.SetPos(sizeof(MIXHEADER) + (sizeof(MIXFILEHEADER)*fileindex), CFile::SP_BEG);
		file.Write(&mixfileheader, sizeof(mixfileheader));

		// Powracamy do poprzedniej pozycji (tam gdzie sa dane plikow)
		file.SetPos(offset, CFile::SP_BEG);

		// Zapisujemy buforek
		file.Write(data.GetPtr(), data.GetSize());

		// Dodajemy rozmiar do offsetu
		offset += data.GetSize();

		return true;
	}

	// Generuje plik mix
	bool Vfs_Pack(const tstring& folder, const tstring& mixfile, const tstring& exceptions, const tstring& compress)
	{
		// Listujemy wszystkie pliki
		std::vector<tstring> strtab = sysListFiles(folder.c_str(), true, false);

		if (strtab.empty())
		{
			LogMsg( "[ERR] [MIX] Folder jest pusty albo nie istnieje : " + folder );
			return false;
		}

		tstring strtable;
		strtable.reserve(strtab.size()*20);

		// Tworzymy plik
		CFile file(mixfile, CFile::OM_WRITE);

		if (!file.IsOK())
		{
			LogMsg( "[ERR] [MIX] Nie mozna utworzyc archiwum mix : " + mixfile );
			return false;
		}

		// Naglowek
		MIXHEADER mix;
		memset(&mix, 0, sizeof(mix));

		// Zapisujemy czysty naglowek
		file.Write(&mix, sizeof(mix));

		// Szykujemy puste rekordy
		MIXFILEHEADER mixfileheader;
		memset(&mixfileheader, 0, sizeof(mixfileheader));
		for (msize a = 0; a < strtab.size(); a++)
		{
			file.Write(&mixfileheader, sizeof(mixfileheader));
		}

		// Aktualnie przerabiany plik
		u32 fileindex = 0;
		// Poczatek danych (za naglowkami plikow)
		const u32 dataoffset = sizeof(MIXHEADER) + (sizeof(MIXFILEHEADER)*strtab.size());
		// Aktualna pozycja
		u32 offset = dataoffset;

		// Zapisujemy pliki
		for (msize a=0; a<strtab.size(); a++)
		{
			// Sprawdzamy czy plik ma byc pominiety
			if (CheckFileExtension(strtab[a], exceptions))
			{
				LogMsg( "[MIX] [ADD] Ignoruje (plik jest na liscie wyjatkow) : " + strtab[a] );
				continue;
			}

			// Otwieramy plik
			CFile part(strtab[a], CFile::OM_READ);
			if (!part.IsOK())
			{
				LogMsg( "[MIX] [ADD] Ignoruje (nie mozna otworzyc pliku) : " + strtab[a] );
				continue;
			}

			// Sprawdzamy czy rozmiar jest prawidlowy
			msize part_size = part.GetSize();
			if ( (part_size==0) || (part_size>MIX_MAX_FS))
			{
				LogMsg( "[MIX] [ADD] Ignoruje (plik jest za duzy albo pusty) : " + strtab[a] );
				continue;
			}

			// Buforek na pliczek
			CBuffer part_buffer(part.GetSize());

			// Usuwamy z nazwy zbedne elementy
			strtab[a].erase(0, folder.size());
			strtab[a] = SetTypeSeparator(strtab[a], '/');

			// Sprawdzamy czy kompresowac czy nie
			bool file_compress = CheckFileExtension(strtab[a], compress);
			bool write_result = false;

			LogMsg( "[MIX] [ADD] " + strtab[a] + (file_compress?" <C>":"") );

			// Czytamy dane z pliku do bufora
			if (part.Read(part_buffer.GetPtr(), part_buffer.GetSize()) != part_buffer.GetSize())
			{
				LogMsg( "[ERR] [MIX] Nie mozna odczytac danych z pliku : " + strtab[a] );
				return false;
			}

			// Zapisujemy odpowiednio (uwaga offset idze jako referencja - zmienia sie!)
			if (file_compress) write_result = Vfs_WriteCompressDataToMix(strtab[a], part_buffer, file, fileindex, offset);
			else write_result = Vfs_WriteDataToMix(strtab[a], part_buffer, file, fileindex, offset);

			if (!write_result) continue;

			fileindex++;									// Pliczek zaliczony
			strtable += strtab[a];							// Dodajemy nazwe do tablicy nazw
			strtable += MIX_NAME_SEPARATOR;					// Dodajemy separator \0
		}

		// Zapisujemy tablice nazw
		file.Write(strtable.data(), strtable.size());

		// Zapisujemy naglowek
		file.SetPos(0, CFile::SP_BEG);
		mix.Id = MIX_ID;
		mix.Version = MIX_VERSION;
		mix.FileHeadersOffset = sizeof(MIXHEADER);
		mix.NumFiles = fileindex;
		mix.DataOffset = dataoffset;
		mix.NameTableOffset = offset;
		mix.NameTableSize = strtable.size();
		file.Write(&mix, sizeof(mix));

		return true;
	}

	bool Vfs_ReadCompressDataFromMix(CBuffer& data, CFile& file, msize size)
	{
		CBuffer dataC(size);
		msize data_size = data.GetSize();

		if (file.Read(dataC.GetPtr(), size) != size)
		{
			LogMsg( "[ERR] [MIX] Niespodziewany koniec pliku" );
			return false;
		}

		if (!DecompressData((const u8*)dataC.GetPtr(), dataC.GetSize(), (u8*)data.GetPtr(), data_size))
		{
			LogMsg( "[ERR] [MIX] Nie mozna zdekompresowac danych z pliku : " );
			return false;
		}

		return true;
	}

	bool Vfs_ReadDataFromMix(CBuffer& data, CFile& file, msize size)
	{
		if (file.Read(data.GetPtr(), size) != size)
		{
			LogMsg( "[ERR] [MIX] Niespodziewany koniec pliku" );
			return false;
		}

		return true;
	}

	bool Vfs_Unpack(const tstring& mixfile, const tstring& folder)
	{
		// Odtwarzamy foldery
		std::vector<tstring> files;
		if (!Vfs_ListFiles(mixfile, files))
		{
			LogMsg( "[ERR] [MIX] Nie mozna rozpakowac archiwum, blad w czasie wyliczania plikow : " + mixfile );
			return false;
		}

		for (auto& rec : files)
		{
			rec = SetTypeSeparator(rec, PATH_SEP);
			msize pos = rec.find_last_of(PATH_SEP);
			tstring foldername = folder;

			if (pos != tstring::npos) foldername += rec.substr(0, pos);
			else continue;

			sysMakeDirectory(foldername);
		}

		// Pliki...
		CFile file(mixfile.c_str(), CFile::OM_READ);

		if (!file.IsOK())
		{
			LogMsg( "[ERR] [MIX] Nie mozna otworzyc pliku archiwum : " + mixfile );
			return false;
		}

		MIXHEADER header;

		// Naglowek
		file.Read(&header, sizeof(header));

		if ( (header.Id!=MIX_ID) || (header.Version!=MIX_VERSION) )
		{
			LogMsg( "[ERR] [MIX] Niepoprawny format archiwum MIX v2 (zly naglowek) : " + mixfile );
			return false;
		}

		if (header.NumFiles != files.size())
		{
			LogMsg("[ERR] [MIX] Liczba wyliczonych nazw (%i) jest rozna od liczby plikow w archiwum MIX (%i) : %s", files.size(), header.NumFiles, mixfile);
			return false;
		}

		for (msize i = 0; i < header.NumFiles; i++)
		{
			file.SetPos(header.FileHeadersOffset + (sizeof(MIXFILEHEADER)*i), CFile::SP_BEG);

			// Czytamy naglowek pliku
			MIXFILEHEADER fileheader;
			file.Read(&fileheader, sizeof(fileheader));

			// Sprawdzamy czy rekord nie jest pusty
			if (fileheader.FileType == FF_EMPTY_FILE) continue;
			if (fileheader.FileSize ==0 || fileheader.FileSize > MIX_MAX_FS) continue;

			// Poczatek pliku
			file.SetPos(fileheader.FileOffset, CFile::SP_BEG);
			// Tworzymy buffor na plik
			CBuffer file_data(fileheader.FileSize);

			// Czytamy odpowiednio
			if (fileheader.FileType == FF_NORMAL_FILE) Vfs_ReadDataFromMix(file_data, file, fileheader.FileSize);
			else if (fileheader.FileType == FF_COMPRESS_FILE) Vfs_ReadCompressDataFromMix(file_data, file, fileheader.FileSizeCompress);
				 else continue;

			CFile subfile(folder + files[i], CFile::OM_WRITE);
			subfile.Write(file_data.GetPtr(), file_data.GetSize());
		}

		return true;
	}

	bool Vfs_ListFiles(const tstring& mixfile, std::vector<tstring>& filesout)
	{
		filesout.clear();
		CFile file(mixfile.c_str(), CFile::OM_READ);

		if (!file.IsOK())
		{
			LogMsg( "[ERR] [MIX] Nie mozna otworzyc pliku archiwum : " + mixfile );
			return false;
		}

		MIXHEADER header;

		file.Read(&header, sizeof(header));

		if ( (header.Id!=MIX_ID) || (header.Version!=MIX_VERSION) )
		{
			LogMsg( "[ERR] [MIX] Niepoprawny format archiwum MIX v3 (zly naglowek) : " + mixfile);
			return false;
		}

		// Odczytujemy tablice nazw do bufora
		std::vector<char> tablename_buf(header.NameTableSize);
		file.SetPos(header.NameTableOffset, CFile::SP_BEG);
		if (file.Read(&tablename_buf[0], tablename_buf.size()) != tablename_buf.size())
		{
			LogMsg( "[ERR] [MIX] Niespodziewany koniec archiwum przy odczytywaniu tablicy nazw : " + mixfile );
			return false;
		}

		// Brak plikow do wyliczenia
		if (tablename_buf.empty()) return true;

		// Nazwy sa w UTF-8 sprowadzamy do formatu wewnetrznego (UTF-16/32)
		tstring tablename(tablename_buf.begin(), tablename_buf.end());

		filesout = Split(tablename, MIX_NAME_SEPARATOR);
		if (filesout.size() != header.NumFiles)
		{
			filesout.clear();
			LogMsg( "[ERR] [MIX] Nie udalo sie poprawnie zinterpretowac tablicy nazw : " + mixfile );
			return false;
		}

		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// Engine File System //////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Pojedynczy plik w liscie
	struct SMixFile
	{
		u32 FileOffset;			// Przesuniecie w pliku MIX
		u32 FileSize;			// Rozmiar pliku
		u32 FileSizeCompress;	// Rozmiar skompresowanego pliku
		u32 ParentMix;			// Klucz do mix'a rodzica ktory zawiera dany plik

		tstring FilenameVFS;	// Nazwa pliku w VFS
		tstring FilenameFS;		// Nazwa pliku w FS
	};

	std::map<u32, SMixFile> GFileList;
	std::map<u32, tstring> GMixFileList;
	tstring GEngineDirectory;
	tstring GAPathBegin;

	RefFile GetCompressFile(CFile& pack, const SMixFile& mf)
	{
		// Tymczasowy buffor na skompresowane dane
		CBuffer bufferC(mf.FileSizeCompress);
		// Plik w postaci bufora jaki zwrocimy
		auto buffer_s = std::make_shared<CBuffer>(mf.FileSize);
		// Castujemy buforek co by miec dostep do interfejsu CBuffer
		CBuffer* buffer_ptr = static_cast<CBuffer*>(buffer_s.get());

		// Odczytujemy skompresowane dane
		pack.SetPos(mf.FileOffset, CFile::SP_BEG);
		if (pack.Read(bufferC.GetPtr(), bufferC.GetSize()) != bufferC.GetSize())
		{
			LogMsg( "[ERR] [MIX] Niespodziewany koniec archiwum : " + GMixFileList[mf.ParentMix]);
			return std::make_shared<CFile>(); // Jest to niepoprawny plik
		}

		// Dekompresujemy dane
		msize buffer_size = buffer_ptr->GetSize();
		if (!DecompressData((const u8*)bufferC.GetPtr(), bufferC.GetSize(), (u8*)buffer_ptr->GetPtr(), buffer_size))
		{
			LogMsg( "[ERR] [MIX] Nie mozna zdekompresowac danych z pliku : " + GMixFileList[mf.ParentMix] );
			return std::make_shared<CFile>(); // Jest to niepoprawny plik
		}

		return buffer_s;
	}

	RefFile GetNonCompressFile(CFile& pack, const SMixFile& mf)
	{
		// Plik w postaci bufora jaki zwrocimy
		auto buffer_s = std::make_shared<CBuffer>(mf.FileSize);
		// Castujemy buforek co by miec dostep do interfejsu CBuffer
		CBuffer* buffer_ptr = static_cast<CBuffer*>(buffer_s.get());

		// Odczytujemy skompresowane dane
		pack.SetPos(mf.FileOffset, CFile::SP_BEG);
		if (pack.Read(buffer_ptr->GetPtr(), buffer_ptr->GetSize()) != buffer_ptr->GetSize())
		{
			LogMsg( "[ERR] [MIX] Niespodziewany koniec archiwum : " + GMixFileList[mf.ParentMix] );
			return std::make_shared<CFile>(); // Jest to niepoprawny plik
		}

		return buffer_s;
	}

	RefFile GetFileFromMix(u32 hash, const tstring& filename)
	{
		// Szukamy plik w VFS
		auto fi = GFileList.find(hash);

		// Jesli nie ma to...
		if (fi==GFileList.end())
		{
			LogMsg( "[ERR] [VFS] Nie odnaleziono pliku w rejestrze VFS : " + filename );
			return std::make_shared<CFile>(); // Jest to niepoprawny plik
		}

		SMixFile mf = (*fi).second;

		CFile mixfile(GMixFileList[mf.ParentMix], CFile::OM_READ);

		if (!mixfile.IsOK())
		{
			LogMsg("[ERR] [VFS] Plik %s odwoluje sie do archiwum %s ktorego nie mozna otworzyc", filename, GMixFileList[mf.ParentMix]);
			return std::make_shared<CFile>(); // Jest to niepoprawny plik
		}

		// Jesli te rozmiary sa rozne to plik jest skompresowany
		if (mf.FileSize != mf.FileSizeCompress) return GetCompressFile(mixfile, mf);
		else return GetNonCompressFile(mixfile, mf);
	}

	// Zapisuje do listy wszystkie pliki i podfoldery z wybranego pliku RUR
	bool RegisterMixFile(const tstring& filename)
	{
		std::vector<tstring> filename_list;
		if (!Vfs_ListFiles(filename, filename_list))
		{
			LogMsg( "[ERR] [MIX] Nie udalo sie wyliczyc plikow w archiwum : " + filename );
			return false;
		}

		// Otwieramy plik
		CFile file(filename.c_str(), CFile::OM_READ);
		if (!file.IsOK()) return false;	// Nie dajemy tutaj loga bo brak tego pliku tez jest w sumie poprawny -> zobacz RefreshList

		// Odczytujemy naglowek
		MIXHEADER mixheader;
		file.Read(&mixheader, sizeof(mixheader));

		if ( (mixheader.Id != MIX_ID) || (mixheader.Version != MIX_VERSION) )
		{
			LogMsg( "[ERR] [MIX] Niepoprawny format pliku MIX v3 (zly naglowek) : " + filename );
			return false;
		}

		// Odczytujemy naglowki plikow
		file.SetPos(mixheader.FileHeadersOffset, CFile::SP_BEG);

		u32 mixnamehash = HashString(filename);

		if (filename_list.size() != mixheader.NumFiles)
		{
			LogMsg( "[ERR] [MIX] Liczba wyliczonych plikow jest rozna od liczby podanej w naglowku : " + filename );
			return false;
		}

		for (u32 i = 0; i < mixheader.NumFiles; i++)
		{
			MIXFILEHEADER mixfile;		// Naglowek pliku, ktory jest w pliku mix
			SMixFile vfsfile;			// Naglowek sygnatury, ktory jest przechowywany w rejestrze

			file.Read(&mixfile, sizeof(mixfile));

			if (mixfile.FileType == FF_EMPTY_FILE) continue;

			vfsfile.FileSize = mixfile.FileSize;
			vfsfile.FileSizeCompress = mixfile.FileSizeCompress;
			vfsfile.FileOffset = mixfile.FileOffset;
			vfsfile.ParentMix = mixnamehash;
			vfsfile.FilenameVFS = filename_list[i];
			vfsfile.FilenameFS = "";

			// Umieszczamy plik w rejestrze
			GFileList[mixfile.FileNameHash] = vfsfile;
		}

		// Umieszczamy skrot sciezki do mix
		GMixFileList[mixnamehash] = filename;

		return true;
	}

	void ScanFolder(const tstring& folder)
	{
		tstring path_to_user_folder = GEngineDirectory + PATH_SEP + folder + PATH_SEP;
		std::vector<tstring> files = sysListFiles(path_to_user_folder.c_str(), true, false);

		tstring sep(1, PATH_SEP);

		for (const tstring& f : files)
		{
			tstring relative_filename = SetTypeSeparator(f.substr(path_to_user_folder.size()), '/');
			u32 hash = HashString(relative_filename);

			// Nazwa moze byc pusta (bo uzytkownik i tak wpisuje wlasna nazwe)
			auto& file = GFileList[hash];
			file.FilenameVFS = relative_filename;
			file.FilenameFS = sep + folder + sep + SetTypeSeparator(relative_filename, PATH_SEP);
			file.ParentMix = 0;
			file.FileOffset = 0;
			file.FileSize = 0;
			file.FileSizeCompress = 0;
		}
	}

	void vfsRefreshList()
	{
		assert(GFileList.empty());
		assert(GMixFileList.empty());

		GEngineDirectory = GetExeDirectory();
		GAPathBegin = appToLowerCopy(GEngineDirectory.substr(0, 3));

		LogMsg( "[VFS] RefreshList() - Skanowanie folderu user" );
		ScanFolder("user");

		LogMsg( "[VFS] RefreshList() - Skanowanie plikow mix" );
		for (u32 i = 0; i < 10; i++)
		{
			tstring mixfilename = GEngineDirectory + PATH_SEP + "data" + ToString(i) + ".mix";
			if (sysFileExists(mixfilename))
			{
				if (RegisterMixFile(mixfilename)) LogMsg( "[VFS] [REG] " + mixfilename);
			}
		}

		LogMsg( "[VFS] RefreshList() - Skanowanie folderu data" );
		ScanFolder("data");
	}

	std::vector<tstring> vfsSearch(const tstring& wildcard)
	{
		std::vector<tstring> result;
		for (const auto& files : GFileList)
		{
			if (CompareByWildcard(wildcard, files.second.FilenameVFS))
			{
				result.push_back(files.second.FilenameVFS);
			}
		}

		return result;
	}

	std::map<tstring, RefFile> GMountTempStream;
	std::mutex GMountMutex;
	int GUidname = 0;

	CFileMounter::CFileMounter(const RefFile& stream)
	{
		assert(stream != nullptr);
		std::lock_guard<std::mutex> lock(GMountMutex);

		Filename = "?" + ToString(GUidname);
		GUidname++;

		GMountTempStream[Filename] = stream;
	}

	CFileMounter::~CFileMounter()
	{
		std::lock_guard<std::mutex> lock(GMountMutex);
		GMountTempStream.erase(Filename);
	}

	RefFile GetMountTempFile(const tstring& filename)
	{
		std::lock_guard<std::mutex> lock(GMountMutex);
		return GMountTempStream[filename];
	}

	RefFile vfsGetFile(const tstring& filename)
	{
		assert(filename.empty() == false);

		// Plik zaczynajacy sie od znaku ? oznacza strumien tymczasowy
		if ((*filename.begin()) == '?')
		{
			return GetMountTempFile(filename);
		}

		// Sprawdzamy czy 3 pierwsze znaki sa takie same jak w GEngineDirectory
		// jesli tak, oznacza to ze jest to sciezka bezwzgledna i powinna zostac otworzona
		// z pominieciem VFS
		if (appToLowerCopy(filename.substr(0, 3)) == GAPathBegin)
		{
			return std::make_shared<CFile>(filename, CFile::OM_READ);
		}

		// Poprawiamy separatory
		tstring fixed_path = SetTypeSeparator(filename, '/');

		// Wyciagamy hasha
		u32 hash = HashString(fixed_path);

		// Sprawdzamy czy plik ten byl rejestrowany
		auto box = GFileList.find(hash);
		if (box != GFileList.end())
		{
			if (box->second.FilenameFS.empty() == false)
			{
				// Jesli tak to go otwieramy
				return std::make_shared<CFile>(GEngineDirectory + box->second.FilenameFS, CFile::OM_READ);
			}
			else
			{
				// Jesli nie to szukamy go w MIXach
				return GetFileFromMix(hash, fixed_path);
			}
		}
		else
		{
			return std::make_shared<CFile>();
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// Data i czas systemowy /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Pobiera aktualny czas systemowy
	tstring GetTimeString()
	{
		time_t date_temp;
		struct tm *date_format;
		char date_out[16];

		time(&date_temp);
		date_format = localtime(&date_temp);
		strftime(date_out, 16, "%H:%M:%S", date_format);

		return date_out;
	}

	// Pobiera aktualna date
	tstring GetDataString()
	{
		time_t date_temp;
		struct tm *date_format;
		char date_out[16];

		time(&date_temp);
		date_format = localtime(&date_temp);
		strftime(date_out, 16, "%d/%m/%y", date_format);

		return date_out;
	}

} // End oo
