﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkowe
#pragma once
#include "oCommon.hpp"

namespace oo
{

	/////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// Konsty ;p //////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	const float HALFPI      = 1.57079632f;	// 90o
	const float PI          = 3.14159265f;	// 180o
	const float TWOPI       = 6.28318530f;	// 360o

	const float RADTODEG    = 57.2957795f;
	const float DEGTORAD    = 0.01745329f;

	/////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// Cala ta geometria //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	struct FVector2
	{
		float x;
		float y;

		FVector2() {}
		FVector2(float nx, float ny) : x(nx), y(ny) {}

		FVector2 operator-() const { return FVector2(-x, -y);   }

		FVector2 operator+(const FVector2& other) const { return FVector2(x + other.x, y + other.y);	}
		FVector2& operator+=(const FVector2& other) { x+=other.x; y+=other.y; return *this; }

		FVector2 operator-(const FVector2& other) const { return FVector2(x - other.x, y - other.y);	}
		FVector2& operator-=(const FVector2& other) { x-=other.x; y-=other.y; return *this; }

		FVector2 operator*(const FVector2& other) const { return FVector2(x * other.x, y * other.y);	}
		FVector2& operator*=(const FVector2& other) { x*=other.x; y*=other.y; return *this; }
		FVector2 operator*(const float v) const { return FVector2(x * v, y * v); }
		FVector2& operator*=(const float v) { x*=v; y*=v; return *this; }

		FVector2 operator/(const FVector2& other) const { return FVector2(x / other.x, y / other.y);	}
		FVector2& operator/=(const FVector2& other) { x/=other.x; y/=other.y; return *this; }
		FVector2 operator/(const float v) const { return FVector2(x / v, y / v);	}
		FVector2& operator/=(const float v) { x/=v; y/=v; return *this; }

		void Set(const float nx, const float ny) { x=nx; y=ny; }
		void Set(const FVector2& p) { x=p.x; y=p.y; }

	};

	struct FVector3
	{
		float x;
		float y;
		float z;

		FVector3() {}
		FVector3(float nx, float ny, float nz) : x(nx), y(ny), z(nz) {}

		FVector3 operator-() const { return FVector3(-x, -y, -z);   }

		FVector3 operator+(const FVector3& other) const { return FVector3(x + other.x, y + other.y, z + other.z);	}
		FVector3& operator+=(const FVector3& other)	{ x+=other.x; y+=other.y; z+=other.z; return *this; }

		FVector3 operator-(const FVector3& other) const { return FVector3(x - other.x, y - other.y, z - other.z);	}
		FVector3& operator-=(const FVector3& other)	{ x-=other.x; y-=other.y; z-=other.z; return *this; }

		FVector3 operator*(const FVector3& other) const { return FVector3(x * other.x, y * other.y,  z * other.z);	}
		FVector3& operator*=(const FVector3& other)	{ x*=other.x; y*=other.y; z*=other.z; return *this; }
		FVector3 operator*(const float v) const { return FVector3(x * v, y * v, z * v);	}
		FVector3& operator*=(const float v) { x*=v; y*=v; z*=v; return *this; }

		FVector3 operator/(const FVector3& other) const { return FVector3(x / other.x, y / other.y, z / other.z);	}
		FVector3& operator/=(const FVector3& other)	{ x/=other.x; y/=other.y; z/=other.z; return *this; }
		FVector3 operator/(const float v) const { return FVector3(x / v, y / v, z / v);	}
		FVector3& operator/=(const float v) { x/=v; y/=v; z/=v; return *this; }

		void Set(const float nx, const float ny, float nz) { x=nx; y=ny; z=nz; }
		void Set(const FVector3& p) { x=p.x; y=p.y; z=p.z; }

	};

	struct FCircle
	{
		FVector2 pos;
		float r;

		FCircle() {}
		FCircle(float x, float y, float rr) : r(rr) { pos.Set(x,y); }
		FCircle(const FVector2 &p, float rr): pos(p),r(rr) {}

		void Set(float x, float y, float rr) { pos.Set(x, y); r = rr; }
		void Set(const FVector2 &p, float rr) { pos = p; r = rr; }

		bool IsValid() const { return ( r<0.0f ); }

		bool IsCircleCollided(const FCircle& c) const
		{
			// Kod z http://www.gamasutra.com/features/20020118/vandenhuevel_01.htm
			float deltaXSquared = pos.x - c.pos.x;
			deltaXSquared *= deltaXSquared;

			float deltaYSquared = pos.y - c.pos.y;
			deltaYSquared *= deltaYSquared;

			float sumRadiiSquared = r + c.r;
			sumRadiiSquared *= sumRadiiSquared;

			return (deltaXSquared + deltaYSquared <= sumRadiiSquared);
		}
		
		bool IsPointInside(const FVector2& p) const
		{
			return IsCircleCollided(FCircle(p, 0.0f));
		}

	};

	struct FRect
	{
		float  left;   // Lewy X
		float  top;    // Gorny Y
		float  right;  // Prwy X
		float  bottom; // Dolny Y

		// Konstruktory
		FRect() {}
		FRect(const FVector2 &a, float width, float height): left(a.x), top(a.y), right(a.x+width), bottom(a.y+height) {}
		FRect(float x1, float y1, float x2, float y2): left(x1), top(y1), right(x2), bottom(y2) {}

		// Setery
		void Set(const FVector2 &a, float width,float height) { left = a.x; top = a.y; right = a.x+width; bottom = a.y+height; }
		void Set(float x1, float y1, float x2, float y2) { left = x1; top = y1; right = x2; bottom = y2; }

		// Pobiera szerokosc
		float GetWidth() const { return (right-left); }
		// Pobiera wysokosc
		float GetHeight() const { return (bottom-top); }
		// Pobiera szerokosc i wysokosc
		FVector2 GetSize() const { return FVector2(right-left, bottom-top); }
		// Pobiera pole powierzchni
		float Field() const { return GetWidth()*GetHeight(); }
		// Pobiera lewy gorny punkt
		FVector2 LeftTop() const { return FVector2(left, top); }
		// Pobiera prawy dolny punkt
		FVector2 RightBottom() const { return FVector2(right, bottom); }
		// Pobiera lewy dolny punkt
		FVector2 LeftBottom() const { return FVector2(left, bottom); }
		// Pobiera Prawy gorny punkt
		FVector2 RightTop() const { return FVector2(right, top); }

		// Pobiera srodkowy punkt
		FVector2 GetCenter() const { return FVector2((left+right)*0.5f, (top+bottom)*0.5f); }

		// Rozszerza prostokat we wszystkich kierunkach
		void Expand(float e) { left-=e; top-=e; right+=e; bottom+=e; }
		// Rozszerza prostokat w poziomie
		void ExpandH(float e) { left-=e; right+=e; }
		// Rozszerza prostokat w pionie
		void ExpandV(float e) { top-=e; bottom+=e; }
		// Ustawia szerokosc
		void SetWidth(float width) { right = left + width; }
		// Ustawia wysokosc
		void SetHeight(float height) { bottom = top + height; }
		// Ustawia szerokosc i wysokosc
		void SetSize(const FVector2& v) { SetWidth(v.x); SetHeight(v.y); }
		// Zeruje
		void Zero() { left = top = right = bottom = 0.0f; }
		// Przesowa prostokat
		void Move(float x, float y) { left+=x; top+=y; right+=x; bottom+=y; }

		// Sprawdza czy prostokat nadaje sie do poprawnego wyswietlenia
		bool IsValid() const { return ((left<right) && (top<bottom)); }

		// Okresla pozycje porostokata
		void SetLocation(const FVector2& e)
		{
			right -= left;
			left = e.x;
			right += left;

			bottom -= top;
			top = e.y;
			bottom += top;
		}

		// Okresla centralna pozycje prostokata
		void SetCenter(const FVector2& e)
		{
			right -= left;
			left = e.x-right*0.5f;
			right += left;

			bottom -= top;
			top = e.y-bottom*0.5f;
			bottom += top;
		}

		// Przycina prostokat w odrebie prostokata r
		void Crop(const FRect& r)
		{
			if (left < r.left) left = r.left;
			if (right > r.right) right = r.right;

			if (top < r.top) top = r.top;
			if (bottom > r.bottom) bottom = r.bottom;
		}

		// Sprawdza czy prostokat r nachodzi na ten jesli tak zwraca true
		bool IsRectCollided(const FRect& r) const { return (left < r.right) && (right > r.left) && (top < r.bottom) && (bottom > r.top); }
		// Sprawdza czy punkt jest w odrebie prostokata jesli tak zwraca true
		bool IsPointInside(const FVector2& p) const {  return (p.x >= left) && (p.x <= right) && (p.y >= top) && (p.y <= bottom); }

		// Tworzy nowy obiekt i wykonuje na nim translacje
		FRect NewByMove(const FVector2& v) const { FRect r(*this); r.Move(v.x, v.y); return r; }
	};

	struct IntegerRect
	{
		int  left;
		int  top;
		int  right;
		int  bottom;

		IntegerRect() {}
		IntegerRect(int l, int t, int r, int b): left(l), top(t), right(r), bottom(b) {}
		IntegerRect(const IntegerRect& rect) : left(rect.left), top(rect.top), right(rect.right), bottom(rect.bottom) {}

		void Set(int l, int t, int r, int b) { left = l; top = t; right = r; bottom = b; }

		int GetWidth() const { return (right-left); }
		int GetHeight() const { return (bottom-top); }
		int Field() const { return GetWidth()*GetHeight(); }

		bool IsValid() const { return ((left<right) && (top<bottom)); }

		bool operator ==(const IntegerRect& r) const { return (left==r.left)&&(top==r.top)&&(right==r.right)&&(bottom==r.bottom); }
		bool operator !=(const IntegerRect& r) const { return (left!=r.left)||(top!=r.top)||(right!=r.right)||(bottom!=r.bottom); }

	};

	inline FRect IntersectRect(const FRect &rect1, const FRect &rect2)
	{
	   FRect rect( std::max(rect1.left,   rect2.left),
				   std::max(rect1.top,    rect2.top),
				   std::min(rect1.right,  rect2.right),
				   std::min(rect1.bottom, rect2.bottom) );

	   if (!rect.IsValid()) rect.Zero();
	   return rect;
	}

	inline FRect UnionRect(const FRect &rect1, const FRect &rect2)
	{
	   return FRect( std::min(rect1.left, rect2.left),
					 std::min(rect1.top,  rect2.top),
					 std::max(rect1.right, rect2.right),
					 std::max(rect1.bottom, rect2.bottom) );
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////// Takie tam //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	inline float VectLength(const FVector2& v)
	{
		return sqrt(v.x*v.x + v.y*v.y);
	}

	inline FVector2 VectNormalize(const FVector2& v)
	{
		float l = 1.0f / VectLength(v);
		return FVector2(v.x*l, v.y*l);
	}

	inline float VectDistance(const FVector2& u, const FVector2& v)
	{
		float dx = u.x - v.x;
		float dy = u.y - v.y;
		return sqrt( dx*dx + dy*dy );
	}

	inline float VectDot(const FVector2& u, const FVector2& v)
	{
		return (u.x * v.x) + (u.y * v.y);
	}

	inline bool IsZero(float v, const float eps = 0.00001f)
	{
		return fabs(v) < eps;
	}

	inline bool IsNotZero(float v, const float eps = 0.00001f)
	{
		return fabs(v) > eps;
	}

	inline bool FloatIs(float expected_result, float result, const float eps = 0.00001f)
	{
		return fabs(result - expected_result) < eps;
	}

	inline bool Equal(float a, float b, float eps = 0.00001f)
	{
		return FloatIs(a, b, eps);
	}

	inline bool Equal(const FVector2& a, const FVector2& b, float eps = 0.00001f)
	{
		return ( FloatIs(a.x, b.x, eps) &&
				 FloatIs(a.y, b.y, eps) );
	}

	inline bool Equal(const FVector3& a, const FVector3& b, float eps = 0.00001f)
	{
		return ( FloatIs(a.x, b.x, eps) &&
				 FloatIs(a.y, b.y, eps) &&
				 FloatIs(a.z, b.z, eps) );
	}

	inline bool Equal(const FRect& a, const FRect& b, float eps = 0.00001f)
	{
		return ( FloatIs(a.left, b.left, eps) &&
				 FloatIs(a.top, b.top, eps) &&
				 FloatIs(a.right, b.right, eps) &&
				 FloatIs(a.bottom, b.bottom, eps) );
	}

	// Funkcja na podstawe artykulu
	// http://www.mochima.com/articles/cuj_geometry_article/cuj_geometry_article.html
	// by Carlos Moreno
	inline int Triangle_Turn(const FVector2& p1, const FVector2& p2, const FVector2& p3)
	{
		float s_a =  ( p1.x * (p2.y - p3.y) +
					   p2.x * (p3.y - p1.y) +
					   p3.x * (p1.y - p2.y) );

		return s_a > 0.0f ? 1 : (s_a < 0.0f ? -1 : 0);
	}

	inline bool TriangleContainsPoint(const FVector2& v1, const FVector2& v2, const FVector2& v3, const FVector2& p)
	{
		return Triangle_Turn (v1,v2,p) == Triangle_Turn (v2,v3,p) &&
			   Triangle_Turn (v2,v3,p) == Triangle_Turn (v3,v1,p);
	}

	inline float CenterPoint(float size, float a, float l)
	{
		return (a + l * 0.5f) - size * 0.5f;
	}

	inline int WhatSide(const FVector2& A, const FVector2& B, const FVector2& P)
	{
		if (!Equal(A.y, B.y))
		{
			float d = P.x - P.y * (A.x-B.x) / (A.y-B.y) + B.x * A.y - A.x * B.y;
			if (d > 0) return 1;
			else
			{
				if (d < 0) return -1;
				else return 0;
			}
		}
		else
		{
			if (A.y < P.y) return 1;
			else if (A.y > P.y) return -1;
			else return 0;
		}
	}

	inline bool LineIntersects(const FVector2& a1, const FVector2& a2, const FVector2& b1, const FVector2& b2)
	{
		FVector2 b = a2 - a1;
		FVector2 d = b2 - b1;
		float bDotDPerp = b.x * d.y - b.y * d.x;
		if (IsZero(bDotDPerp)) return false;

		FVector2 c = b1 - a1;
		float t = (c.x * d.y - c.y * d.x) / bDotDPerp;
		if (t < 0.0f || t > 1.0f) return false;

		float u = (c.x * b.y - c.y * b.x) / bDotDPerp;
		if (u < 0.0f || u > 1.0f) return false;

		return true;
	}

	// Funckja zwraca przesuniecie dynamic_one
	inline bool RectCollision(const FRect& dynamic_one, const FRect& static_one, FVector2& overlap)
	{
		if (static_one.IsRectCollided(dynamic_one))
		{
			FVector2 d_center = dynamic_one.GetCenter();
			FVector2 s_center = static_one.GetCenter();

			FVector2 green( d_center.x - s_center.x,
							d_center.y - s_center.y );

			FVector2 d_half = dynamic_one.GetSize() * 0.5f;
			FVector2 s_half = static_one.GetSize() * 0.5f;

			if (green.x < 0.0f) overlap.x = -(d_half.x + s_half.x) - green.x;
			else                overlap.x =  (d_half.x + s_half.x) - green.x;

			if (green.y < 0.0f) overlap.y = -(d_half.y + s_half.y) - green.y;
			else                overlap.y =  (d_half.y + s_half.y) - green.y;

			return true;
		}

		return false;
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// Kolory /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	struct FColor
	{
		float R, G, B, A;

		FColor() {}
		FColor(float r, float g, float b, float a = 1.0f) : R(r), G(g), B(b), A(a) {}
		FColor(float gray, float a) : R(gray), G(gray), B(gray), A(a) {}

		void Set(float r, float g, float b, float a = 1.0f) { R=r; G=g; B=b; A=a; }
		void Zero() {  R = G = B = A = 0.0f; }
	};


	inline FColor MakeColorf(const u8 r, const u8 g, const u8 b, const u8 a = 0xFF)
	{
		return FColor(float(r)/255.0f, float(g)/255.0f, float(b)/255.0f, float(a)/255.0f);
	}

	inline FColor MakeColorf(const u32 color)
	{
		return FColor(float((color&0xFF000000)>>24)/255.0f,
					  float((color&0x00FF0000)>>16)/255.0f,
					  float((color&0x0000FF00)>> 8)/255.0f,
					  float((color&0x000000FF)    )/255.0f);
	}

	inline FColor LerpColor(const FColor& a, const FColor& b, float x)
	{
		return FColor( Lerp(a.R, b.R, x),
					   Lerp(a.G, b.G, x),
					   Lerp(a.B, b.B, x),
					   Lerp(a.A, b.A, x) );
	}

	inline bool Equal(const FColor& a, const FColor& b, float eps = 0.00001f)
	{
		return ( FloatIs(a.R, b.R, eps) &&
				 FloatIs(a.G, b.G, eps) &&
				 FloatIs(a.B, b.B, eps) &&
				 FloatIs(a.A, b.A, eps) );
	}

	typedef FColor colorf;
	typedef FVector2 vect2f;
	typedef FVector3 vect3f;
	typedef FRect rectf;
	typedef FCircle circlef;

	typedef IntegerRect recti;

	const rectf N_RECT(0.0f, 0.0f, 1.0f, 1.0f);			// Kwadrat 1x1
	const rectf N_COORD(0.0f, 0.0f, 1.0f, 1.0f);		// Typowe koordynaty tekstury
	const colorf N_COLOR(1.0f, 1.0f, 1.0f, 1.0f);		// Pelny kolor

	inline float Round(float value) { return floor(value + 0.5f); }
	inline double Round(double value) { return floor(value + 0.5); }

	inline vect2f CalcAspectRatio(const vect2f& originalsize, const vect2f& field, vect2f& offset)
	{
		offset.Set(0.0f, 0.0f);

		float width = (float)field.x;
		float height = (float)field.y;
		float aspectratio = width / height;

		float picwidth = (float)originalsize.x;
		float picheight = (float)originalsize.y;
		float picaspectratio = picwidth / picheight;

		if (picaspectratio > aspectratio)
		{
			float newheight = Round(width/picwidth*picheight);
			offset.Set( 0, (field.y - newheight) / 2 );
			return vect2f(field.x, newheight);
		}
		else if (picaspectratio < aspectratio)
		{
			float newwidth =  Round(height/picheight*picwidth);
			offset.Set( (field.x - newwidth) / 2, 0 );
			return vect2f(newwidth, field.y);
		}

		return field;
	}

} // End oo
