﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkowe
#pragma once
#include "oCommon.hpp"

namespace oo
{

	// Wylicza wszystkie pliki w danym folderku
	std::vector<tstring> sysListFiles(const tstring& dir, bool files = true, bool dirs = true);

	// Tworzy pojedynczy folder (foldery)
	bool sysMakeDirectory(const tstring& strdir);
	// Usuwa folder (foldery)
	bool sysDeleteDirectory(const tstring& dir);

	// Przemieszcza albo zmienia nazwe pliku
	bool sysMoveFile(const tstring& existing_filename, const tstring& new_filename);

	// Sprawdza czy plik istnieje
	bool sysFileExists(const tstring& path);

	// Sprawdza czy folder istnieje
	bool sysDirectoryExists(const tstring& path);

	// Usuwa plik
	bool sysRemoveFile(const tstring& filename);

	// Otwieranie
	void sysOpen(const tstring& str);

	// /sample/path/module.exe -> module.exe
	tstring sysFileName(const tstring& path);
	// /sample/path/module.exe -> module
	tstring sysStemName(const tstring& path);
	// /sample/path/module.exe -> /sample/path
	tstring sysDirName(const tstring& path);

	// Zwraca sciezke do danych projektu na danej maszynie (jesli nie ma to generuje)
	tstring GetAppDataDirectory(const tstring& appname);

	// /sample/path/module.exe
	tstring GetExeFullPath();
	// /sample/path
	tstring GetExeDirectory();
	// module[.exe]
	tstring GetExeName(bool with_ext);

	// Pobiera rozszerzenie pliku
	tstring GetFileExtension(const tstring& filename, bool with_dot = true);
	
	// Sprawdza czy plik jest danego rozszerzenia (mozna podac kilka np. "bmp,jpg,tga,png")
	bool CheckFileExtension(const tstring& filename, const tstring& exts);

} // End oo
