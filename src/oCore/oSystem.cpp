﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oSystem.hpp"

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace oo
{

	std::vector<tstring> sysListFiles(const tstring& path, bool files, bool dirs)
	{
		std::vector<tstring> strtab;
		if (fs::exists(path) && fs::is_directory(path))
		{
			fs::recursive_directory_iterator end_iter;
			for (fs::recursive_directory_iterator dir_iter(path); dir_iter != end_iter; ++dir_iter)
			{
				if (files && fs::is_regular_file(dir_iter->status()) )
				{
					strtab.push_back(dir_iter->path().string());
				}

				if (dirs && fs::is_directory(dir_iter->status()))
				{
					strtab.push_back(dir_iter->path().string());
				}
			}
		}
		return strtab;
	}

	bool sysFileExists(const tstring& path)
	{
		boost::system::error_code ec;
		return fs::exists(path, ec);
	}

	bool sysDirectoryExists(const tstring& path)
	{
		boost::system::error_code ec;
		return fs::is_directory(path, ec);
	}

	bool sysMakeDirectory(const tstring& strdir)
	{
		if (!sysDirectoryExists(strdir))
		{
			boost::system::error_code ec;
			return fs::create_directories(strdir, ec);
		}
		else return true;
	}

	bool sysDeleteDirectory(const tstring& dir)
	{
		if (sysDirectoryExists(dir))
		{
			boost::system::error_code ec;
			fs::remove_all(dir, ec);
			return !ec;
		}
		else return false;
	}

	bool sysMoveFile(const tstring& existing_filename, const tstring& new_filename)
	{
		boost::system::error_code ec;
		fs::rename(existing_filename, new_filename, ec);
		return !ec;
	}

	bool sysRemoveFile(const tstring& filename)
	{
		boost::system::error_code ec;
		return fs::remove(filename, ec);
	}

	void sysOpen(const tstring& str)
	{
#if (defined O2_SYS_WINDOWS) && (!defined OMIKRON2_DEDICATED_SERVER)
		ShellExecuteW(nullptr, L"open", StringToWString(str).c_str(), nullptr, nullptr, SW_SHOWNORMAL);
#endif
	}

	tstring sysBaseName(const tstring& path)
	{
		return fs::path(path).filename().string();
	}

	tstring sysDirName(const tstring& path)
	{
		return fs::path(path).parent_path().string();
	}

	tstring sysStemName(const tstring& path)
	{
		return fs::path(path).stem().string();
	}

	tstring GetExeFullPath()
	{
#if (defined O2_SYS_WINDOWS)
		wchar_t tmpstr[MAX_PATH+1];
		GetModuleFileNameW(nullptr, tmpstr, MAX_PATH);
		return WStringToString(tmpstr);
#elif (defined O2_SYS_LINUX)
		fs::path self("/proc/self/exe");
		boost::system::error_code ec;
		if (fs::exists(self)) 
		{
			fs::path exe = fs::read_symlink(self, ec);
			if (!ec) return exe.string();
			else "/module.exe";
		}
		else return "/module.exe";
#else 
	#error "Please implement"
#endif
	}

	tstring GetExeDirectory()
	{
		return sysDirName(GetExeFullPath());
	}

	tstring GetExeName(bool with_ext)
	{
		if (with_ext) return sysBaseName(GetExeFullPath());
		else return sysStemName(GetExeFullPath());
	}

	tstring GetAppDataDirectory(const tstring& appname)
	{
#if (defined O2_SYS_WINDOWS)
		return GetExeDirectory();
/*
		char v[512]; v[0] = 0;
		GetEnvironmentVariable("APPDATA", v, 512);

		tstring dir = (v + ("\\" + appname));
		CreateDirectory(dir.c_str(), nullptr);
		return dir;
*/
#elif (defined O2_SYS_LINUX)
		const char* home_cstr = getenv("HOME");
		if (home_cstr == nullptr) return GetExeDirectory();
		tstring str = tstring(home_cstr) + "/.config/" + appname;
		sysMakeDirectory(str);
		return str;
#else
	#error "Please implement"
#endif
	}

	tstring GetFileExtension(const tstring& filename, bool with_dot)
	{
		msize pos = filename.find_last_of('.');
		if ( (pos == tstring::npos) || ((pos+1) == filename.size()) ) return tstring();

		return filename.substr(pos + (with_dot?0:1));
	}

	bool CheckFileExtension(const tstring& filename, const tstring& exts)
	{
		std::vector<tstring> strs = Split(exts, ',');
		tstring myext = GetFileExtension(filename, false);

		for (const auto& str : strs)
		{
			if (myext == str) return true;
		}

		return false;
	}

} // End oo
