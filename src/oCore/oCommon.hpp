﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkowe
#pragma once

typedef std::string tstring;
typedef size_t msize;

typedef std::uint8_t  u8;
typedef std::uint16_t u16;
typedef std::uint32_t u32;
typedef std::uint64_t u64;

namespace p = std::placeholders;

namespace oo
{

	///////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////// Wyjatek ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Ten wyjatek wykorzystujemy do wysokopoziomowej obslugi bledow
	class omikron_exception
	{
		private:
			tstring Message;

		public:
			omikron_exception(const tstring& s) : Message(s) {}
			omikron_exception() : Message() {}

			const char* what() const { return Message.c_str(); }
			tstring str() const { return Message; }
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////// Unicode //////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	inline std::u32string Decode(const std::string& str)
	{
		std::u32string result;
		utf8::unchecked::utf8to32(str.begin(), str.end(), std::back_inserter(result));
		return result;
	}

	inline std::string Encode(const std::u32string& str)
	{
		std::string result;
		utf8::unchecked::utf32to8(str.begin(), str.end(), std::back_inserter(result));
		return result;
	}

	inline std::u32string DecodeSafe(const std::string& str)
	{
		std::u32string result;
		try { utf8::utf8to32(str.begin(), str.end(), std::back_inserter(result)); }
		catch (const utf8::exception&) { throw omikron_exception("DecodeSafe (utf8 -> utf32) error"); }
		return result;
	}

	inline std::string EncodeSafe(const std::u32string& str)
	{
		std::string result;
		try { utf8::utf32to8(str.begin(), str.end(), std::back_inserter(result)); }
		catch (const utf8::exception&) { throw omikron_exception("DecodeSafe (utf32 -> utf8) error"); }
		return result;
	}

#ifdef O2_SYS_WINDOWS
	inline std::wstring StringToWString(const std::string& str)
	{
		std::wstring result;
		utf8::unchecked::utf8to16(str.begin(), str.end(), std::back_inserter(result));
		return result;
	}

	inline std::string WStringToString(const std::wstring& str)
	{
		std::string result;
		utf8::unchecked::utf16to8(str.begin(), str.end(), std::back_inserter(result));
		return result;
	}
#endif

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// Rozne consty /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Skrocona forma
	const  u8 MAX_U8  = std::numeric_limits<u8> ::max();
	const u16 MAX_U16 = std::numeric_limits<u16>::max();
	const u32 MAX_U32 = std::numeric_limits<u32>::max();

	const float FLOAT_NAN = std::numeric_limits<float>::quiet_NaN();

	const tstring LINE_TERM(O2_LINE_TERMINATOR);
	const char PATH_SEP(O2_PATH_SEPARATOR);
	const tstring PATH_SEP_STR(1, PATH_SEP);
	const tstring N_STRING("");

	///////////////////////////////////////////////////////////////////////////////////////////////////

	template <typename T>
	inline bool TryParse(const tstring& str, T& val)
	{
		std::basic_istringstream<char> ss(str);
		ss.unsetf(std::ios::skipws);

		if ( (ss >> val) &&
			 (ss.get()==std::char_traits<char>::eof()) ) return true;
		else return false;
	}

	template <typename T>
	inline T Parse(const tstring& str)
	{
		T v;
		if (TryParse(str, v)) return v;
		else throw omikron_exception("Parse error : " + str);
	}

	template <typename T>
	inline tstring ToString(T value) { return std::to_string(value); }
	template <> inline tstring ToString(tstring value) { return value; }
	template <> inline tstring ToString(const char* value) { return value; }
	template <> inline tstring ToString(char* value) { return value; }

	// Interpoluje a i b o p przy czym, p(0.0f) == a && p(1.0f) == b (wersja bardziej dokladna)
	template<typename T>
	inline T Lerp(T a, T b, float p)
	{
		return a * (1.0f - p) + b * p;
	}

	// Interpoluje a i b o p przy czym, p(0.0f) == a && p(1.0f) == b
	template<typename T>
	inline T LerpFast(T a, T b, float p)
	{
		return a + (b - a) * p;
	}

	// Przycina wartosc wg. podanej granicy
	template <typename T>
	inline T ClipValue(T value, T min, T max)
	{
		assert(min <= max);

		if (value > max) return max;
		else if (value < min) return min;
			 else return value;
	}

	// Przycina wartosc wg. podanej granicy
	template <typename T>
	inline void ClipValueRef(T& value, T min, T max)
	{
		assert(min <= max);

		if (value > max) value = max;
		else if (value < min) value = min;
	}

	// Sprawdza czy wartosc wychodzi poza granice
	template <typename T>
	inline bool InRange(T value, T min, T max)
	{
		assert(min <= max);

		if (value > max || value < min) return false;
		else return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////// Podstawowa obsluga lancuchow znakow /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Sprawdza czy to znak
	inline bool IsCharAscii(char c) { return c > 32; }

	// Sprawdza czy znak jest jednym ze specialnych znakow bialych
	inline bool IsWhspaAscii(char c) { return ((c==' ') || (c=='\t') || (c=='\n') ||  (c=='\r')); }

	// Sprawdza czy znak jest litera z podstawowej tablicy ASCII
	inline bool IsAlphaAscii(char c) {
										  return ( c>='a' && c<='z' ) ||
												 ( c>='A' && c<='Z' ) ;
									}

	// Sprawdza czy znak jest litera albo cyfra z podstawowej tablicy ASCII
	inline bool IsAlnumAscii(char c)	{
										  return ( c>='a' && c<='z' ) ||
												 ( c>='A' && c<='Z' ) ||
												 ( c>='0' && c<='9' );
									}

	// Zmienia cyfre dziesietna na szesnastkowa (w przypadku bledu zwraca znak #)
	inline char IntToHex(u8 i)
	{
		if (i<=9)	return char('0' + i);
		else if (i<=16) return char('A' + (i-10));
			 else return '#';
	}

	// Zmienia cyfre szestanstkowa na dziesietna (w przypadku bledu zwraca 0xFF)
	inline u8 HexToInt(char c)
	{
		if (c>='0' && c<='9') return u8(c - '0');
		else if (c>='a' && c<='f') return u8(c - 'a' + 10);
			 else if (c>='A' && c<='F') return u8(c - 'A' + 10);
				  else return 0xFF;
	}

	inline bool ParseHexStr(const char* hexstr, u32& out)
	{
		assert(hexstr);

		char c;
		u32 val;
		while ((c = *hexstr++) != 0)
		{
			val = HexToInt(c); if (val==0xFF) return false;
			out = out * 16 + val;
		}

		return true;
	}

	// Ucina biale znaki z lewej i prawej
	inline tstring Trim(const tstring& s)
	{
		if (s.empty()) return tstring();

		msize b = 0;
		while (!IsCharAscii(s[b])) {
			if (++b == s.size()) return tstring();
		}

		msize e = s.size()-b;
		while (!IsCharAscii(s[b+e-1])) e--;

		return s.substr(b, e);
	}

	// Pomocnicze funkcje do Split
	namespace detail
	{
		inline msize ReturnLenForSplit(char) { return 1; }
		inline msize ReturnLenForSplit(const tstring& str) { return str.size(); }
	}

	// Rodziela lancuch znakow separatoram w postaci innego lancucha znakow
	template <typename T>
	inline std::vector<tstring> Split(const tstring& str, T c)
	{
		std::vector<tstring> out;
		msize begin =0;
		msize end =0;

		while ((end = str.find(c, begin)) != tstring::npos )
		{
			out.push_back( str.substr(begin, end - begin) );
			begin = end + detail::ReturnLenForSplit(c);
		}
		if (begin < str.size()) out.push_back( str.substr(begin, str.size() - begin) );

		return out;
	}

	// Rozdzielamy lancuch znakow na podstawie bialych znakow
	inline std::vector<tstring> SplitWhitespace(const tstring& str)
	{
		std::vector<tstring> out;
		bool newpart = false;
		for (auto c : str)
		{
			if (IsCharAscii(c) == false)
			{
				newpart = false;
				continue;
			}
			else
			{
				if (newpart == false)
				{
					out.push_back(N_STRING);
					newpart = true;
				}
				out.back().append(1, c);
			}
		}

		return out;
	}

	// Usuwa biale znaki
	inline tstring CleanWhitespace(const tstring& str)
	{
		tstring result;

		for (auto c : str)
		{
			if (IsCharAscii(c)) result += c;
		}

		return result;
	}

	// Porownianie za pomoca znakow wieloznacznych (?, *)
	inline int CompareByWildcard(const tstring& wildstr, const tstring& str)
	{
		const char* wild = wildstr.c_str();
		const char* strstr = str.c_str();

		// Written by Jack Handy - jakkhandy@hotmail.com
		const char *cp = nullptr;
		const char *mp = nullptr;

		while ((*strstr) && (*wild != '*'))
		{
			if ((*wild != *strstr) && (*wild != '?'))
			{
				return 0;
			}

			wild++;
			strstr++;
		}

		while (*strstr)
		{
			if (*wild == '*')
			{
				if (!*++wild)
				{
					return 1;
				}

				mp = wild;
				cp = strstr+1;
			}
			else if ((*wild == *strstr) || (*wild == '?'))
			{
				wild++;
				strstr++;
			}
			else
			{
				wild = mp;
				strstr = cp++;
			}
		}

		while (*wild == '*')
		{
			wild++;
		}

		return !*wild;
	}


	// Sprowadza wszystkie biale znaki do pojedynczej spacji
	inline tstring NormalizeWhitespace(const tstring& str)
	{
		tstring result;
		bool space = false;

		for (auto c : str)
		{
			if (IsCharAscii(c))
			{
				result += c;
				space = false;
			}
			else if (space == false) { result += ' '; space = true; }
		}
		return result;
	}

	/////////////////////////////////////////////////////

	// Przeksztalcenia wielkosci liter (tylko dla ASCII)
	inline char appToLowerCopy(char c) { return InRange(c, 'A', 'Z') ? c + 32 : c; }
	inline char appToUpperCopy(char c) { return InRange(c, 'a', 'z') ? c - 32 : c; }
	inline void appToLower(tstring& str) { for (auto& c : str) c = appToLowerCopy(c); }
	inline void appToUpper(tstring& str) { for (auto& c : str) c = appToUpperCopy(c); }
	inline tstring appToLowerCopy(const tstring& str) { tstring result; for (auto c : str) result += appToLowerCopy(c); return result; }
	inline tstring appToUpperCopy(const tstring& str) { tstring result; for (auto c : str) result += appToUpperCopy(c); return result; }

	/////////////////////////////////////////////////////

	// Funkcja haszujaca djb2 bez casesensitive (wersja dla const char*)
	inline u32 HashString(const char *str)
	{
		u32 hash = 5381; char c;
		while ((c = *str++) != 0) hash = ((hash << 5) + hash) + appToLowerCopy(c);
		return hash;
	}

	// Funkcja haszujaca djb2 bez casesensitive (wersja dla tstring)
	inline u32 HashString(const tstring& str)
	{
		u32 hash = 5381;
		for (auto c : str)
		{
			hash = ((hash << 5) + hash) + appToLowerCopy(c);
		}
		return hash;
	}

	// Funkcja haszujaca djb2 (wersja dla const char*)
	inline u32 HashString_cs(const char *str)
	{
		u32 hash = 5381; char c;
		while ((c = *str++) != 0) hash = ((hash << 5) + hash) + c;
		return hash;
	}

	// Funkcja haszujaca djb2 (wersja dla tstring)
	inline u32 HashString_cs(const tstring& str)
	{
		u32 hash = 5381;
		for (auto c : str)
		{
			hash = ((hash << 5) + hash) + c;
		}
		return hash;
	}

	//////////////////////////////////////////////////////

	// Unifikuje dowolne separatory w sciezce na systemowe (uwaga C:/ nie jest prawidlowe)
	// Dlatego nie znalezma uzywania tego do sciezek bezwzglednych na platformie windows
	inline tstring SetTypeSeparator(const tstring& path, const char sep = PATH_SEP) {
		tstring result = path;
		msize pos = tstring::npos;
		while((pos = result.find_first_of("\\/", pos+1)) != tstring::npos) {
			result[pos] = sep;
		}
		return result;
	}

	template <typename T>
	inline std::vector<T> Group(std::initializer_list<T> args)
	{
		return std::vector<T>(args.begin(), args.end());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// Pomocnicze klasy, funkcje //////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	class CRandom
	{
		public:
			u64 Seed;	// Ziarno

		public:
			CRandom() {}
			CRandom(u32 s) : Seed( static_cast<u64>(s) ) {}

		public:
			// Losowa wartosc calkowita bez znaku
			u32 Uint()
			{
				Seed = 2862933555777941757ll * Seed + 3037000493ll;
				return static_cast<u32>(Seed >> 32);
			}

			// Losowa liczba z przedzialu [0.0f - 1.0f)
			float Float()
			{
				// Kod ze strony http://rgba.scenesp.org/articles/sfrand/sfrand.htm
				u32 v = ( Uint() & 0x007FFFFF ) | 0x3F800000;
				return ( (*reinterpret_cast<float*>(&v)) - 1.0f );
			}

			// Losowa wartosc calkowita ze znakiem
			int Int()
			{
				return static_cast<int>(Uint());
			}

		public:
			// Losuje wartosc calkowita ze znakiem z przedzialu [min-max]
			int Int(int min, int max) { return ( min + Uint() % ( max - min + 1 ) ); }

			// Losuje wartosc calkowita bez znaku z przedzialu [min-max]
			u32 Uint(u32 min, u32 max) { return (min + Uint() % ( max - min + 1 ) ); }

			// Losuje wartosc zmiennoprzecinkowa z przedzialu [min-max)
			float Float(float min, float max) { return Float() * (max - min) + min; }

	};

	namespace detail
	{
		// Nie uzywac tej klasy!!!
		class FormatString
		{
			private: // Obiekty CFormatString nie sa do kopiowania
				FormatString( const FormatString& );
				FormatString& operator=( const FormatString& );

			private:
				tstring Input;
				tstring Output;
				tstring::iterator Cur;

			public:
				FormatString(tstring s) : Input(std::move(s)), Output(), Cur(Input.begin()) {}

				// Po tej operacji FormatString jest nie do uzytku
				tstring&& move_output()
				{
					Output.append(Cur, Input.end());
					return std::move(Output);
				}

				template <typename T>
				FormatString& operator %(T value)
				{
					while (Cur != Input.end())
					{
						if (*Cur == '%' && *(++Cur) != '%')
						{
							if (*Cur != '\0') Cur++;
							Output += ToString<T>(value);
							return *this;
						}
						Output += *Cur++;
					}

					return *this;
				}
		};

		// Nie uzywac tej klasy!!!
		class ArgFormatString
		{
			private: // Obiekty CFormatString nie sa do kopiowania
				ArgFormatString( const ArgFormatString& );
				ArgFormatString& operator=( const ArgFormatString& );

			private:
				tstring Output;

			public:
				ArgFormatString(tstring beg) : Output(std::move(beg)) {}

				// Po tej operacji FormatString jest nie do uzytku
				tstring&& move_output()
				{
					return std::move(Output);
				}

				template <typename T>
				ArgFormatString& operator %(T value)
				{
					Output += ' ';
					Output += ToString<T>(value);
					return *this;
				}
		};

		template <typename FORMAT_TYPE>
		inline FORMAT_TYPE& format(FORMAT_TYPE& frm) { return frm; }

		template <typename FORMAT_TYPE, typename T, typename... ARGS>
		inline FORMAT_TYPE& format(FORMAT_TYPE& frm, T arg, ARGS... args) { return format(frm % arg, args...); }
	}

	template <typename T, typename... ARGS>
	inline tstring format(tstring str, T arg, ARGS... args) { return detail::format(detail::FormatString(std::move(str)) % arg, args...).move_output(); }

	template<typename R>
	inline bool is_ready(const std::future<R>& f)
	{
		return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
	}

	template<typename CONTAINER, typename PRED>
	inline CONTAINER& remove_erase_if(CONTAINER& on, PRED pred)
	{
		on.erase( std::remove_if(std::begin(on), std::end(on), pred),
				  std::end(on) );
		return on;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////// oCore /////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	class Core;
		class CEngineLogger;		// Loggerek
		class CEngineScheduler;		// Harmonogram
		class CEngineLocal;			// Lokalizacja
		class CEngineConfig;		// Ustawienia
		class CEngineInterface;		// Sterowanie podsystemami
		class CEngineVarTable;		// Tablica zmiennych

	// Rdzen silnika
	class Core
	{
		private:
			static CEngineLogger*		EngineLogger;
			static CEngineScheduler*	EngineScheduler;
			static CEngineLocal*		EngineLocal;
			static CEngineConfig*		EngineConfig;
			static CEngineInterface*	EngineInterface;
			static CEngineVarTable*		EngineVarTable;

		public:
			static void SetUpCore();
			static void ShutDownCore();

			static CEngineLogger*		GetEngineLogger()		{ assert(EngineLogger);			return EngineLogger; }
			static CEngineScheduler*	GetEngineScheduler()	{ assert(EngineScheduler);		return EngineScheduler; }
			static CEngineLocal*		GetEngineLocal()		{ assert(EngineLocal);			return EngineLocal; }
			static CEngineConfig*		GetEngineConfig()		{ assert(EngineConfig);			return EngineConfig; }
			static CEngineInterface*	GetEngineInterface()	{ assert(EngineInterface);		return EngineInterface; }
			static CEngineVarTable*		GetEngineVarTable()		{ assert(EngineVarTable);		return EngineVarTable; }
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////// Wyjscie ////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Wyjscie
	class IOutputDevice
	{
		public:
			virtual ~IOutputDevice() {}

			virtual void Write(const tstring&) =0;
			virtual void Writeln(const tstring&) =0;

	};

	// Czarna dziura
	class CNullOutput : public IOutputDevice
	{
		public:
			void Write(const tstring&) override {}
			void Writeln(const tstring&) override {}

	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// Loggerek //////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	class CEngineLogger
	{
		private: // Obiekty CEngineLogger nie sa do kopiowania
			CEngineLogger( const CEngineLogger& );
			CEngineLogger& operator=( const CEngineLogger& );

		private:
			std::unique_ptr<IOutputDevice> PrimaryOutput;	// Podstawowe wyjscie
			IOutputDevice* ExternalOutput;					// Zenetrzne wyjscie do uzytku wlasnego (np. konsola)
			std::mutex LoggerMutex;							// Mutex

		public:
			CEngineLogger() : ExternalOutput(nullptr) {}

			// Ustawia podstawowe wjyscie
			void SetPrimaryOutput(IOutputDevice* dev)
			{
				std::lock_guard<std::mutex> lock(LoggerMutex);
				PrimaryOutput.reset(dev);
			}

			// Ustawia zewnetrzne wyjscie
			void SetExternalOutput(IOutputDevice* dev)
			{
				std::lock_guard<std::mutex> lock(LoggerMutex);
				ExternalOutput = dev;
			}

			void Writeln(const tstring& message)
			{
				std::lock_guard<std::mutex> lock(LoggerMutex);
				if (PrimaryOutput) PrimaryOutput->Writeln(message);
				if (ExternalOutput) ExternalOutput->Writeln(message);
			}

			void Write(const tstring& message)
			{
				std::lock_guard<std::mutex> lock(LoggerMutex);
				if (PrimaryOutput) PrimaryOutput->Write(message);
				if (ExternalOutput) ExternalOutput->Write(message);
			}
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////// Crash ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	inline void ThrowCriticalError(const tstring& msg)
	{
		Core::GetEngineLogger()->Writeln("[!!!] App terminate: " + msg);
		assert(0);
		std::terminate();
	}

	extern CNullOutput GNullOutput;

	extern tstring GLogFilename;
	extern tstring GConfigFilename;

} // End oo
