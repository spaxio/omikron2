﻿/*
 | oCore.hpp
 |
 | Copyright © 2006-2012 Karol Pałka
 |
 | oCore jest szablonem pomagajacym przy tworzeniu
 | gier. Jest takze gowna czescia silnika Omikron2
 |
 | Kontakt:
 |     spaxio at gmail.com
 |
 |
 */

#pragma once
#include "oCommon.hpp"
#include "oSystem.hpp"
#include "oBinData.hpp"
#include "oTxtData.hpp"
#include "oEngineStuff.hpp"
#include "oMath.hpp"
