﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkowe
#pragma once
#include "oTxtData.hpp"

namespace oo
{

	namespace detail
	{
		inline bool ConfigCanGet(const CSxVal& n, float)	        { return n.CanGetAsFloat(); }
		inline bool ConfigCanGet(const CSxVal& n, int)		        { return n.CanGetAsInt(); }
		inline bool ConfigCanGet(const CSxVal& n, const tstring&)	{ return n.CanGetAsString(); }

		inline float	ConfigGet(const CSxVal& n, float)	        { return n.GetAsFloat(); }
		inline int		ConfigGet(const CSxVal& n, int)		        { return n.GetAsInt(); }
		inline tstring	ConfigGet(const CSxVal& n, const tstring&)	{ return n.GetAsString(); }
	}

	// Obsluga plikow ustawien
	class CEngineConfig
	{
		private:
			CSxNode MainConfigNode;

		public:
			// Wczytuje plik ustawien i tworzy baze danych
			bool LoadConfigFile(const tstring& filename);
			// Zapisuje plik ustawien
			void SaveConfigFile(const tstring& filename);

		public:
			// num - Numer wartosci, jesli num bedzie zbyt wysoki funkcja zwroci wartosc def
			// def - Zostanie zwrocona jesli klucz nie zostanie odnaleziony, albo jego wartosc bedzie nieprawidlowa (np. klucz zamiast liczby)

			template <typename T>
			void SetValue(const char* path, const T& value, msize index =0)
			{
				CSxNode* n = CSxHigWriter::FindElement(path, &MainConfigNode);
				if (index < n->Values.size()) n->Values[index] = value;
				else
				{
					msize num = index - n->Values.size();
					while (num--) n->Values.push_back(0);
					n->Values.push_back(value);
				}
			}

			template <typename T>
			T GetValue(const char* path, const T& def, msize index =0)
			{
				CSxNode* n = CSxHigReader::FindElement(path, &MainConfigNode);

				if ( !n || (n->Values.size() <= index) || (detail::ConfigCanGet(n->Values[index], def) == false) )
				{
					SetValue(path, def, index);
					return def;
				}

				return detail::ConfigGet( n->Values[index], def );
			}

			template <typename T, typename F>
			T CheckAndGetValue(const char* path, F checker, const T& def, msize index =0)
			{
				CSxNode* n = CSxHigReader::FindElement(path, &MainConfigNode);

				if ( !n || (n->Values.size() <= index) || (detail::ConfigCanGet(n->Values[index], def) == false) )
				{
					SetValue(path, def, index);
					return def;
				}

				T val = detail::ConfigGet( n->Values[index], def );
				T fixed_val = checker(val);
				SetValue(path, fixed_val, index);

				return fixed_val;
			}

	};

	// Lokalizuje ;p
	class CEngineLocal
	{
		private:
			typedef std::pair<u32, tstring> DicPair_t;
			typedef std::vector<DicPair_t>  Dictionary_t;
			Dictionary_t Dictionary;

		public:
			// Wybieramy plik jezykowy
			bool LoadDictionary(const tstring& filename);

			// Lokalizuje tekst
			tstring LocalStr(const tstring& unistr);
	};

	inline tstring LocalStr(const tstring& unistr) { return Core::GetEngineLocal()->LocalStr(unistr.c_str()); }

	template <typename STR, typename T, typename... ARGS>
	inline tstring LocalStr(STR str, T arg, ARGS... args)
	{
		return format(LocalStr(str), arg, args...);
	}

	// Zabawka do rejestrowania i edytowania wlasciwosci
	// Uwaga! To nie sa typowe Propertiesy, raczej powinno byc traktowane jako:
	// "Tablica zmiennych ktore mozna edytowac z dowolnego miejsca"

	enum ESetResult
	{
		SR_OK,				// Wszystko ok
		SR_NOTFOUND,		// Nie znaleziono zmiennej
		SR_BADTYPE,			// Zly typ zmiennej
		SR_BADVALUE			// Wartosc ma zly format
	};

	class CEngineVarTable
	{
		private:
			typedef bool (*StringChecker_t)(const tstring&);

			enum EPropType
			{
				PT_INT,
				PT_FLOAT,
				PT_STRING
			};

			union UVarPtr
			{
				int* IntVar;
				float* FloatVar;
				tstring* StringVar;
			};

			union UMinMax
			{
				struct { int MinI, MaxI; };
				struct { float MinF, MaxF; };
				StringChecker_t StringChecker;
			};

			struct SVar
			{
				const char* Name;		// Nazwa
				UVarPtr		 Var;		// Wskaznik do zmiennej
				EPropType	 Type;		// Jej typ
				UMinMax		 MinMax;	// Minimum i maksimum
			};

			typedef std::map<u32, SVar> VarTable_t;

		private:
			bool CheckName(const char* name);
			ESetResult CheckForWrite(VarTable_t::iterator& it);

			VarTable_t VarTable;

		public:

			// false (i assert(0)) jest zwracane w przypadku
			// nazwa zawiera niedozwolone znaki (tylko a-z 0-9 _ (nie zaczyna sie od cyfry))
			// var jest rowny nullptr
			// min jest wieksze od max
			// gdy taki element juz istnieje

			void RegisterInt(const char* name, int* var, int min = -65536, int max = 65536);
			void RegisterFloat(const char* name, float* var, float min = 0.0f, float max = 1.0f);
			void RegisterString(const char* name, tstring* var, StringChecker_t stringchecker = nullptr);

			// Zmienia wynik setera na lancuch znakow
			static const char* ResultToString(ESetResult result);

			// Jesli przy wywolaniu SetIntValue zmienna o wartosci name okaze sie typu PT_FLOAT, int value zostanie zrzutowane na float
			// Jesli przy wywolaniu SetFloatValue zmienna o wartosci name okaze sie typu PT_INT, float value zostanie zrzutowane na int (zgodnie z zasadami C++)

			ESetResult SetIntValue(const tstring& name, int value);
			ESetResult SetFloatValue(const tstring& name, float value);
			ESetResult SetStringValue(const tstring& name, const tstring& value);

			// Wartosci z CVarTable mozemy pobierac tylko jako STRING
			// W przypadku braku wartosci zostanie zwrocona wartosc "NULL"
			tstring GetValue(const tstring& name);

			// Wylicza wszystkie zmienne wraz z wartosciami i wypisuje w formacie "nazwa = wartosc\n"
			void WriteStat(IOutputDevice& out);

	};


	/*
	 *	Tip: Linia komend
	 *
	 *	Na linie komend skladaja sie dwa elementy nazwa i argumenty (lol ale mi sie rymlo ;p)
	 *  Nazwa - moze skladac sie jedynie ze znakow [a-z] [A-Z] [0-9] i [_]
	 *  Argumenty - sa w formacie sx;2 (odsylam do jego naglowka ;p) w postaci arg1 arg2 arg3 itp.
	 *
	 */

	typedef std::vector<SxVal_t> Args_t;

	inline tstring GetS(const SxVal_t& val) { return val.GetAsString(); }
	inline float GetF(const SxVal_t& val) { return val.GetAsFloat(); }
	inline int GetI(const SxVal_t& val) { return val.GetAsInt(); }

	class CEngineInterface
	{
		public:
			//typedef std::function<void(const Args_t&, IOutputDevice&)> Func_t;
			typedef void (*Func_t)(const Args_t&, IOutputDevice&);
			typedef void (*Caller_t)(const tstring&);

		private:
			class CCommand
			{
				private:
					tstring Name;				// Nazwa komendy
					const char* ArgTypes;		// Typy argumentow
					const char* ArgNames;		// Nazwy argumentow
					Func_t Func;				// Funkcja
					Caller_t Caller;			// Alternatywna metoda wywolania

					bool CheckNames();
					void LogCall(const Args_t& args);

				public:
					CCommand() :
						ArgTypes(nullptr), ArgNames(nullptr), Func(nullptr), Caller(nullptr) { }

					CCommand(const tstring& name, const char* arg, const char* help, Func_t func, Caller_t caller) :
						Name(name), ArgTypes(arg), ArgNames(help), Func(func), Caller(caller) { assert(CheckNames()); }

				public:
					bool ParseOnly(const tstring& args, IOutputDevice& out, Args_t& args_out);
					void ExecOnly(const Args_t& args, IOutputDevice& out);
					void ParseAndExec(const tstring& args, IOutputDevice& out);

					tstring GetFuncPrototype(bool color = false);

					// Zwraca nazwe komendy
					tstring GetName() { return Name; }

					// Zwracany jest lancuch argumentow np. "fffiiivvvss"
					tstring GetArgTypes() { return ArgTypes?ArgTypes:N_STRING; }
					// Zwracany jest lanuch nazw argumentow oddzielonych przecinkiem np. "arg1,arg2,arg3"
					tstring GetArgNames() { return ArgNames?ArgNames:N_STRING; }

					// Zwraca pelne typy np. {int, float, string, key, int, any}
					std::vector<tstring> GetFullTypes();
					// Zwraca pelne nazwy argumentow, jesli funkcja nie ma nazw to zwracany jest pusty wektor
					std::vector<tstring> GetFullNames();

					bool CallerExist() { return Caller != nullptr; }
					void ExecCaller(const tstring& commandline) { if (Caller) Caller(commandline); }
			};

		private:
			typedef std::map<u32, CCommand> Commands_t;

		private:
			Commands_t Commands;		// Mapa funkcji
			IOutputDevice* Out;			// Wskaznik na urzadzenie wyjscia
			IOutputDevice* QuietOut;	// Wskaznik na ciche urzadzenie wyjscia (zwykle GnullptrOutput)

		private:
			// Odziela nazwe funkcji od jego argumentow
			void SeparateArguments(const tstring& commandline, tstring& name, tstring& args);

		public:
			CEngineInterface();

		public:
			// Dodaje nowa funkcje
			void AddCommand(const char* name, Func_t func, const char* argformat = nullptr, const char* help = nullptr, Caller_t caller = nullptr);
			// Wykonuje dowolna funkcje (z wlasnym callerem albo bez)
			void Exec(const tstring& commandline, bool quiet = false);
			// Wykonuje funkcje (wymagane jest aby funkcja miala swojego callera inaczej zwracamy false)
			bool RawExec(u32 hash, const Args_t& args);

			// Wyciaga hash funckji i jej argumenty (sprawdza tez czy argumenty sie zgadzaja)
			bool ExtractHashAndArgs(const tstring& commandline, u32& out_hash, Args_t& out_args);

			// Zwraca format argumentow dla danej komendy
			tstring GetArgTypes(const tstring& command);
            // Zwraca nazwy argumentow dla danej komendy
            std::vector<tstring> GetArgNames(const tstring& command);

		public:
			// Ustawia wyjscie
			void SetOutputDevice(IOutputDevice* dev);

			// Wylicza podobne komendy (zakladamy ze firstchars jest po Trim)
			std::vector<tstring> GetCommandList(const tstring& firstchars);

			// Pomoc do komendy (wyjasnia argumenty) (zakladamy ze command jest po Trim)
			tstring GetCommandHelp(const tstring& command, bool color = false);
	};

	// Prosze zaimplementowac ta funkcje
	void GameFunctionCaller(const tstring& commandline);

	namespace detail
	{
		struct FuncRegister
		{
			const FuncRegister& operator<<(CEngineInterface::Func_t func);
		};

		FuncRegister Prototype(const char* name, const char* args, const char* help, int flag);
	}

	void UseFuncListToInitEngineInterface();

	const char * const NOARGS = nullptr;
	const char * const NOHELP = nullptr;

	#define DEFINE_FUNC_SYS(fname, args, help) auto Func_a_##fname = ::oo::detail::Prototype( #fname, args, help, 0) << [](const Args_t& arg, IOutputDevice& out) -> void
	#define DEFINE_FUNC_GAME(fname, args, help) auto Func_b_##fname = ::oo::detail::Prototype( #fname, args, help, 1) << [](const Args_t& arg, IOutputDevice& out) -> void

	/*
	 *		Harmonogram
	 *
	 *		Pozwala nam rozlozyc wykonywanie jakies czynnosci w czasie
	 */


	class CEngineScheduler
	{
		private:
			typedef std::function<void()> TaskFunc_t;

			struct STask
			{
				float Time;
				int Category;
				TaskFunc_t Func;
				float Interval;
			};

		private:
			typedef std::vector<STask> Tasks_t;
			Tasks_t Tasks;
			Tasks_t NewTasks;
			float SchedulerTime;

			void MergeTasks();

			void AddTaskPrivate(float time, int category, float interval, const TaskFunc_t& func);

		public:
			CEngineScheduler() : SchedulerTime(0.0f) { NewTasks.reserve(64); Tasks.reserve(64); }
			~CEngineScheduler() { ClearTasks(); }

		public:
			double GetSchedulerTime() { return SchedulerTime; };
			void Update(float dt);
			void ClearTasks();
			void ClearTasks(int category);

		public:
			// Wykonaj funkcje
			void AddTask(float time, int category, const TaskFunc_t& func) { AddTaskPrivate(time, category, -1.0f, func); }
			void AddTask(float time, const TaskFunc_t& func) { AddTaskPrivate(time, 0, -1.0f, func); }

			void AddRepetitiveTask(float time, int category, float interval, const TaskFunc_t& func)
			{
				assert(interval >= 0.0f);
				AddTaskPrivate(time, category, interval, func);
			}
	};

	/*
	 *		Pomocnicza klasa do managerkow danych
	 */

	template <class RESOURCE>
	class CDataManager
	{
		private:
			typedef std::shared_ptr<RESOURCE> Resource_t;
			typedef std::map<u32, Resource_t> DataBase_t;
			DataBase_t DataBase;

			u32 NameToKey(const tstring& name) 
			{
				return HashString(SetTypeSeparator(name));
			}

		public:
			// Tylko sprawdza
			bool Exist(const tstring& name)
			{
				return DataBase.find(NameToKey(name)) != DataBase.end();
			}

			// Sprawdza czy zasob o danej nazwie istnieje w bazie, jesli tak to zwraca go
			Resource_t GetExist(const tstring& name)
			{
				auto it = DataBase.find(NameToKey(name));
				if (it != DataBase.end()) return it->second;
				else return Resource_t(static_cast<RESOURCE*>(0));
			}

			// Bezposrednio dodaje zasob (nie uzywac bezposrednio!)
			Resource_t InsertResource(const tstring& name, const Resource_t& resource)
			{
				// Przed wywolaniem InsertResource musisz wywolac GetExist!
				assert(!Exist(name));

				DataBase.insert( std::make_pair(NameToKey(name), resource) );
				return resource;
			}

			void Erase(const std::function<bool(const Resource_t&)>& only_if)
			{
				for (auto it = DataBase.begin(); it != DataBase.end(); )
				{
					if (only_if(it->second)) 
					{
						DataBase.erase(it++);
					}
					else ++it;
				}
			}

			// Usuwa wszystkie zasoby
			void Clear() { DataBase.clear(); }

	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////// Licznik //////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	void timSetup();

	// Czas z mikrosekundach
    long long timNow();
	// Pobiera FPS
	int timGetFps();
	// Czas w sekundach
	double timGetTime();
	// Na poczatek klatki
	void timBeginFrame();
	// Na koniec klatki
	void timEndFrame();
	// Pobiera delte (czas klatki w sekundach)
	double timGetDeltaTime();
	// Pobiera czas klatki w mikrosekundach
	long long timGetFrameTime();
	// Pauza
	void timPause();
	// Wznowienie
	void timResume();

	inline float GetDeltaTime() { return static_cast<float>(timGetDeltaTime()); }

} // End oo
