﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oCore.hpp"

namespace oo
{

	CEngineLogger*		Core::EngineLogger		= nullptr;
	CEngineScheduler*	Core::EngineScheduler	= nullptr;
	CEngineConfig*		Core::EngineConfig		= nullptr;
	CEngineLocal*		Core::EngineLocal		= nullptr;
	CEngineInterface*	Core::EngineInterface	= nullptr;
	CEngineVarTable*	Core::EngineVarTable	= nullptr;

	tstring GLogFilename = GetAppDataDirectory("omikron2") + PATH_SEP_STR + GetExeName(false) + ".log";
	tstring GConfigFilename = GetAppDataDirectory("omikron2") + PATH_SEP_STR + GetExeName(false) + ".sx2";

	void Core::SetUpCore()
	{
		EngineLogger = new CEngineLogger();
		EngineLogger->SetPrimaryOutput( new CFileLogger( GLogFilename ) );
		LogMsg( "[@@@] Omikron2::Core" );

		timSetup();
		vfsRefreshList();

		EngineConfig = new CEngineConfig();
		EngineConfig->LoadConfigFile( GConfigFilename );

		EngineScheduler = new CEngineScheduler();

		EngineLocal = new CEngineLocal();
		EngineInterface = new CEngineInterface();
		EngineVarTable = new CEngineVarTable();
	}

	void Core::ShutDownCore()
	{
		delete EngineVarTable; EngineVarTable = nullptr;
		delete EngineInterface; EngineInterface = nullptr;
		delete EngineLocal; EngineLocal = nullptr;
		delete EngineScheduler; EngineScheduler = nullptr;

		EngineConfig->SaveConfigFile( GConfigFilename );
		delete EngineConfig; EngineConfig = nullptr;

		delete EngineLogger; EngineLogger = nullptr;
	}

} // End oo
