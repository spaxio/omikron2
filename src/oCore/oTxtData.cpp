﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oTxtData.hpp"

namespace oo
{
	const u8  BOM_UTF8_1	= 0xEF;			// UTF-8
	const u8  BOM_UTF8_2	= 0xBB;			// UTF-8
	const u8  BOM_UTF8_3	= 0xBF;			// UTF-8

	// Sprawdza czy znak jest czescia liczby
	inline bool SX_IsNumDigit(char c) {
											return ( c=='-' ) || ( c=='+' ) ||
												   ( c=='.' ) ||
												   ( c>='0' && c<='9' );
									   }

	// Sprawdza czy znak jest czescia klucza
	inline bool SX_IsKeyChar(char c) {
										  return ( c>='a' && c<='z' ) ||
												 ( c>='A' && c<='Z' ) ||
												 ( c>='0' && c<='9' ) ||
												   c=='_' || c=='-';
									  }

	///////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// Zapis/odczyt tekstu //////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	bool SaveTextToFile(const tstring& filename, const tstring& str)
	{
		CFile file(filename, CFile::OM_WRITE);

		if (!file.IsOK()) return false;

		file.Write(&BOM_UTF8_1, 1);
		file.Write(&BOM_UTF8_2, 1);
		file.Write(&BOM_UTF8_3, 1);

		file.Write(str.data(), str.size());

		return true;
	}

	bool LoadTextFromFile(const tstring& filename, tstring& str)
	{
		RefFile stream = vfsGetFile(filename);

		str.clear();
		if (!stream->IsOK()) return false;

		// Pobieramy rozmiar pliku
		msize size = stream->GetSize();

		if (size == 0)
		{
			str.clear();
			return true;
		}

		// Tworzymy bufor
		std::vector<char> text(size);
		stream->Read(text.data(), size);

		if ( utf8::is_valid(text.begin(), text.end()) == false )
		{
			LogMsg("[ERR] [TXT] Nie mozna wczytac pliku tekstowego (plik nie jest w formacie UTF-8)");
			str.clear();
			return false;
		}

		std::vector<char>::iterator it = text.begin();
		if (utf8::starts_with_bom(text.begin(), text.end())) 
		{
			std::advance(it, 3);
		}

		str.assign(it, text.end());

		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////// Logger //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	CFileLogger::CFileLogger(const tstring& filename) : File(filename, CFile::OM_WRITE)
	{
		File.Write(&BOM_UTF8_1, 1);
		File.Write(&BOM_UTF8_2, 1);
		File.Write(&BOM_UTF8_3, 1);

		Write( format("\r\nRozpoczynam logowanie do pliku - %s %s\r\n\r\n", GetDataString(), GetTimeString()) );
	}

	CFileLogger::~CFileLogger()
	{
		Write( format("\r\nLogowanie do pliku zakonczone - %s %s\r\n\r\n", GetDataString(), GetTimeString()) );
	}

	void CFileLogger::Write(const tstring& message)
	{
		if (!message.empty())
		{
			File.Write(message.data(), message.size());
			File.Flush();
		}
	}

	void CFileLogger::Writeln(const tstring& message)
	{
		Write(message + LINE_TERM);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////// Parser sx;2 /////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	void CSxWriter::WsLeft(u32 n)
	{
		if (n & L_EOL) { Code += LINE_TERM; SetTab(Depth); }
		if (n & L_SPACE) Code += ' ';
		if (n & L_TAB) Code += '\t';
	}

	void CSxWriter::WsRight(u32 n)
	{
		if (n & R_EOL) { Code += LINE_TERM; SetTab(Depth); }
		if (n & R_SPACE) Code += ' ';
		if (n & R_TAB) Code += '\t';
	}

	// Sprawdza czy danego stringa mozna zapisac jako klucz
	bool CSxWriter::CanWriteAsKey(const tstring& str)
	{
		if (str.empty()) return false;
		if (SX_IsNumDigit(str[0])) return false;

		for (auto c : str)
		{
			if (!SX_IsKeyChar(c)) return false;
		}

		return true;
	}

	tstring CSxWriter::CorectString(const tstring& str)
	{
		if (CanWriteAsKey(str)) return str;

		tstring result;
		result += S_QUOTE;

		for (auto c : str)
		{
			switch (c)
			{
				case S_QUOTE: result += "\\\""; break;
				case S_SPECIALSIG: result += "\\\\"; break;
				case '\n': result += "\\n"; break;
				case '\r': result += "\\r"; break;

				default:
					result += c;
			}
		}

		result += S_QUOTE;
		return result;
	}

	void CSxReader::GetPos(msize& ln, msize& col)
	{
		col = ln = 1;
		for (msize i = 0; i < Index; i++)
		{
			if (Code->at(i) == '\n') { col = 1; ln++; }
			else col++;
		}
	}

	tstring CSxReader::GetPosAsString()
	{
		msize ln;
		msize col;
		GetPos(ln, col);
		return format("[%i,%i]", ln, col);
	}

	bool CSxReader::Parse_ch(msize& index, char& c)
	{
		if (index == Code->size()) return false;
		c = Code->at(index);
		index++;
		return true;
	}

	bool CSxReader::Parse_pch(msize& index, char c)
	{
		msize ix = index;
		char a = 0;

		if (Parse_ch(ix, a))
		{
			if (a == c)
			{
				index = ix;
				return true;
			}
			else return false;
		}
		else return false;
	}

	bool CSxReader::Parse_w(msize& index)
	{
		msize ix = index;
		char a = 0;

		if (Parse_ch(ix, a))
		{
			if (!IsCharAscii(a))
			{
				index = ix;
				return true;
			}
			else if (a == S_LINECOMMENT)
				 {
					msize n = 0;

					// Jesli tak to zwracamy index konca lini
					n = Code->find('\n', ix);

					// Jesli nie ma to znaczy ze mamy koniec pliku
					if (n == tstring::npos) index = Code->size();
					else index = n + 1;

					return true;
				 }
				 else return false;
		}
		else return false;
	}

	void CSxReader::Parse_W(msize& index)
	{
		while (Parse_w(index)) { }
	}

	bool CSxReader::Parse_value(msize& index, tstring& outval)
	{
		Parse_W(index);
		outval.clear();
		char c = 0;

		while (Parse_ch(index, c))
		{
			if (SX_IsNumDigit(c)) outval += c;
			else { index--; break; }
		}

		// Jesli dlugosc jest zerowa to znaczy ze parsowanie liczby nie powiodlo sie
		return (outval.size() != 0);
	}

	bool CSxReader::Parse_char_code(msize& index, u32& out, u32 size)
	{
		char c = 0;
		u32 val = 0;
		u32 p = 0;
		out = 0;

		while (p < size)
		{
			if (!Parse_ch(index, c)) break;
			val = HexToInt(c); if (val==0xFF) break;
			out = out * 16 + val;
			p++;
		}

		return true;
	}

	bool CSxReader::ReadString(tstring& str)
	{
		Parse_W(Index);

		str.clear();
		char c = 0;

		msize tmpindex = Index;

		// Zaczyna sie od cudzyslowia a wiec jest to zwykly string
		if (Parse_pch(tmpindex, S_QUOTE))
		{
			while (Parse_ch(tmpindex, c))
			{
				if (c == S_SPECIALSIG)
				{
					if (!Parse_ch(tmpindex, c)) return false;
					switch (c)
					{
						case S_QUOTE: str += S_QUOTE; break;
						case S_SPECIALSIG: str += S_SPECIALSIG; break;
						case 'n': str += '\n'; break;
						case 'r': str += '\r'; break;

						default: str += S_SPECIALSIG; str += c; break;
					}
				}
				else
				{
					if (c == S_QUOTE) { Index = tmpindex; return true; }
					else str += c;
				}
			}
		}
		else // Zaczyna sie od czegos innego niz cudzyslow, byc moze to klucz
		{
			// Sprawdzamy czy klucz rozpoczyna sie poprawinie (np. czy pierwszy znak to nie liczba etc.)
			if (!Parse_ch(tmpindex, c)) return false;
			if ( IsAlphaAscii(c) || c=='_' ) str += c;
			else return false;

			// Czytamy az do czasu trafienia na znak inny niz znak klucza
			while (Parse_ch(tmpindex, c))
			{
				if ( SX_IsKeyChar(c) ) str += c;
				else { tmpindex--; break; }
			}

			// Jesli dlugosc jest zerowa to znaczy ze klucz jest nieprawidlowy
			if (str.empty()) return false;
			else { Index = tmpindex; return true; }
		}

		return false;
	}

	bool CSxReader::ReadFloat(float &v)
	{
		tstring strvalue;
		msize tmpindex = Index;

		if ( (Parse_value(Index, strvalue)) && TryParse(strvalue, v) ) return true;
		else { Index = tmpindex; return false; }
	}

	bool CSxReader::ReadInteger(int &v)
	{
		tstring strvalue;
		msize tmpindex = Index;

		if ( (Parse_value(Index, strvalue)) && TryParse(strvalue, v) ) return true;
		else { Index = tmpindex; return false; }
	}

	bool CSxReader::ReadOperator()
	{
		Parse_W(Index);
		return Parse_pch(Index, S_OPERATOR);
	}

	bool CSxReader::ReadOpenSection()
	{
		Parse_W(Index);
		return Parse_pch(Index, S_OPENSECTION);
	}

	bool CSxReader::ReadCloseSection()
	{
		Parse_W(Index);
		return Parse_pch(Index, S_CLOSESECTION);
	}

	// =================================================

	void CSxVal::Copy(const CSxVal& other)
	{
		if (other.SxValType == VT_STRING) // Czy nowa zmienna to string?
		{
			Allocate(strlen(other.StringVal) + 1);
			strcpy(StringVal, other.StringVal);
		}
		else // int, float
		{
			IntVal = other.IntVal;
		}
		SxValType = other.SxValType;
	}

	void CSxVal::Serialize(ISerializer& archive)
	{
		if (archive.IsReader())
		{
			int type;
			archive.Serialize(type);

			switch (type)
			{
				case VT_INT: archive.Serialize(IntVal); break;
				case VT_FLOAT: archive.Serialize(FloatVal); break;
				default:
				{
					tstring str;
					archive & str;
					CheckAndSetString(str);
				}
			}

			SxValType = type;
		}
		else // Writer
		{
			archive.Serialize(SxValType);
			switch (SxValType)
			{
				case VT_INT: archive.Serialize(IntVal); break;
				case VT_FLOAT: archive.Serialize(FloatVal); break;
				default:
				{
					tstring str(StringVal);
					archive & str;
				}
			}
		}
	}

	// =================================================

	// Funkcja (pomocnicza) inline poprawiajaca czytelnosc Funktora FSxIsMyNode
	inline CSxNode::Elements_t::iterator SxFindElementInNode(CSxNode* node, u32 hash)
	{
		return std::find_if(node->Elements.begin(),
							node->Elements.end(),
							[hash](const CSxNode& n) {
								return n.KeyHash == hash;
							}
		);
	}

	// Poprawia dira
	bool ValidDir(const tstring& dir)
	{
		for (auto c : dir)
		{
			if ( (!SX_IsKeyChar(c)) && (c!='.') ) return false;
		}

		return true;
	}

	CSxNode* CSxHigWriter::FindElement(const tstring& dir, CSxNode* n)
	{
		assert(n);					// SxNode nie moze byc rowny nullptr
		assert(ValidDir(dir));		// Sciezka musi byc poprawna (A-Z; a-z; _; .;)

		std::vector<tstring> elements = Split(dir, '.');
		if (elements.empty()) return n;

		CSxNode* node = n;

		for (auto& key : elements)
		{
			u32 hash = HashString(key);

			auto it = SxFindElementInNode(node, hash);
			if (it != node->Elements.end())
			{
				node = &(*it);
			}
			else
			{
				CSxNode e;
				e.Key = (key);
				e.KeyHash = hash;

				// Dodajemy nowo utworzony element do listy
				node->Elements.push_back(e);
				// Ustawiamy go jako aktywny
				node = &(node->Elements.back());
			}
		}

		return node;
	}

	// Zapisuje wartosci do writerka
	void WriteValues(CSxWriter& writer, const CSxNode::Values_t& values)
	{
		for (auto& val : values)
		{
			switch (val.GetType())
			{
				case VT_FLOAT: writer.WriteFloat(val.GetAsFloat(), CSxWriter::L_SPACE); break;
				case VT_INT: writer.WriteInteger(val.GetAsInt(), CSxWriter::L_SPACE); break;
				case VT_STRING: writer.WriteString(val.GetAsString(), CSxWriter::L_SPACE); break;
			}
		}
	}

	// Zapisuje elementy do writerka
	void WriteNode(CSxWriter& writer, const CSxNode::Elements_t& node)
	{
		// Dla kazdego elementu (podkreslam roznice pomiedzy elementem (lista na wezly) a wartoscia)
		for (auto& sub_node : node)
		{
			if (sub_node.Elements.empty())	// Jesli dany wezel nie ma 'dzieci'
			{
				if (!sub_node.Values.empty())	// Jesli dany wezel ma wartosci
				{
					// To je zapisz
					writer.WriteString(sub_node.Key);
					writer.WriteOperator(CSxWriter::NONE);
					WriteValues(writer, sub_node.Values);
					writer.WriteFree("", CSxWriter::R_EOL);
				}
				else // Jesli dany wezel nie ma wartosci
				{
					// To zapisz sam klucz
					writer.WriteString(sub_node.Key);
					writer.WriteOperator(CSxWriter::R_EOL);
				}
			}
			else // Jesli dany wezel ma 'dzieci'
			{
				if (!sub_node.Values.empty()) // Jesli dany wezel ma wartosci
				{
					// To je zapisz, ale bez separatora bo dalej musimy zapisac jeszcze 'dzieci'
					writer.WriteString(sub_node.Key, CSxWriter::L_EOL);
					writer.WriteOperator(CSxWriter::NONE);
					WriteValues(writer, sub_node.Values);
				} // Jesli dany wezel nie ma wartosci to zapisz tylko klucz i przygotuj sie do zapisania 'dzieci'
				else writer.WriteString(sub_node.Key, CSxWriter::L_EOL);

				// Zapisz 'dzieci' po przez rekurencje
				writer.WriteOpenSection( CSxWriter::R_EOL | CSxWriter::L_SPACE);
				WriteNode(writer, sub_node.Elements);
				writer.WriteCloseSection(CSxWriter::L_EOL | CSxWriter::R_EOL);
			}
		}
	}

	bool CSxHigWriter::SaveNodeToFile(CSxNode* n, const tstring& filename)
	{
		assert(n);
		CSxWriter writer;
		WriteNode(writer, n->Elements);
		return writer.SaveToFile(filename);
	}

	void CSxHigWriter::SaveNodeToString(CSxNode* n, tstring& code)
	{
		assert(n);
		CSxWriter writer;
		WriteNode(writer, n->Elements);
		code = writer.GetCode();
	}

	//////////////////////////////////////////////////////////

	CSxNode* CSxHigReader::FindElement(const tstring& dir, CSxNode* n)
	{
		assert(n);					// SxNode nie moze byc rowny nullptr
		assert(ValidDir(dir));		// Dir musi byc poprawny (A-Z; a-z; 0-9; _; .;)

		std::vector<tstring> elements = Split(dir, '.');
		if (elements.empty()) return n;

		CSxNode* node = n;

		for (auto& element : elements)
		{
			auto it = SxFindElementInNode(node, HashString(element));
			if (it != node->Elements.end()) node = &(*it);
			else return nullptr;
		}

		return node;
	}

	// Wczytuje wartosci
	void ReadValues(CSxReader& reader, CSxNode::Values_t& values)
	{
		tstring value1;
		float value2 = 0.0f;
		int value3 = 0;
		bool good_value = true;

		while (good_value)
		{
			CSxReader backup_point(reader);

			if (reader.ReadInteger(value3)) { values.push_back(value3); continue; }
			if (reader.ReadFloat(value2)) { values.push_back(value2); continue; }

			// # Przyklad
			// klucz1: wartosc1 wartosc2 wartosc3
			// klucz2: # <- jesli po kluczu trafimy na ten operator
			// # to zaczy ze powinnismy wrocic sie do punktu przed klucz2
			if (reader.ReadString(value1))
			{
				if (reader.ReadOperator() || reader.ReadOpenSection())
				{
					reader = backup_point;
				}
				else
				{
					values.push_back(value1);
					continue;
				}
			}

			// Nie ma poprawnych wartosci? Znaczy ze koniec
			good_value = false;
		}
	}

	// Wczytuje baze danych
	void ReadNode(CSxReader& reader, CSxNode::Elements_t& node)
	{
		tstring key;

		// Zaczynamy czytanie od klucza
		while (reader.ReadString(key))
		{
			CSxNode e;
			e.Key = key;
			e.KeyHash = HashString(key);
			node.push_back(e);
			auto node_it = std::next(node.rbegin()).base();

			bool key_without_operator = false;

			// Jak wystapi operator to czytamy wartosci
			if (reader.ReadOperator()) ReadValues(reader, node_it->Values);
			else key_without_operator = true;

			CSxReader backup_point(reader);
			bool key_and_operator_is_present = reader.ReadString(key) && reader.ReadOperator();
			reader = backup_point;

			if (!key_and_operator_is_present)
			{
				if (reader.ReadOpenSection())
				{
					ReadNode(reader, node_it->Elements);
					reader.MustReadCloseSection();
				}
				else if (key_without_operator)
				{
					throw omikron_exception( format("Niespodziewany token po kluczu '%s' (pusty klucz powinien miec postac KLUCZ: albo KLUCZ[])", e.Key) );
				}
			}
		}
	}

	void CSxHigReader::LoadNodeFromFile(CSxNode* n, const tstring& filename)
	{
		assert(n);
		tstring code;
		if (!LoadTextFromFile(filename, code)) throw omikron_exception("Nie mozna otworzyc pliku " + filename);
		LoadNodeFromString(n, code);
	}

	void CSxHigReader::LoadNodeFromString(CSxNode* n, const tstring& code)
	{
		assert(n);
		CSxReader reader(&code);

		// Zerujemy baze danych
		n->Elements.clear();

		try { ReadNode(reader, n->Elements); }
		catch (omikron_exception& e)
		{
			throw omikron_exception( format("Blad parsera - %s %s", e.what(), reader.GetPosAsString() ) );
		}
	}

	void CSxHigReader::ValidIndex()
	{
		if (ActiveNode->Values.empty() || ActiveNode->Values.size() < Index)
		{
			throw omikron_exception( format("klucz %s nie ma %i wartosci", ActiveNode->Key, Index) );
		}
	}

	CSxHigReader& CSxHigReader::At(const tstring& dir)
	{
		ActiveNode = FindElement(dir, MainNode);
		if (ActiveNode == nullptr) { ActiveNode = MainNode; throw omikron_exception("Nie mozna znalezc elementu " + dir); }

		Index = 0;
		return *this;
	}

	bool CSxHigReader::Exist(const char* dir, bool set)
	{
		CSxNode* node = FindElement(dir, MainNode);
		if (node == nullptr) return false;
		else
		{
			if (set)
			{
				ActiveNode = node;
				Index = 0;
			}

			return true;
		}
	}

	CSxHigReader& CSxHigReader::operator[](const char* dir)
	{
		return At(dir);
	}

	CSxHigReader& CSxHigReader::operator>>(int& c)
	{
		ValidIndex();

		// Sprawdzamy czy wartosc da sie zinterpretowac jako int (ew. jako float zrzutowany do int'a)
		if ( ActiveNode->Values[Index].CanGetAsInt() )
		{
			c = ActiveNode->Values[Index].GetAsInt();
			Index++; if (Index >= ActiveNode->Values.size()) Index = 0;
			return *this;
		}
		// Jesli jest to lancuch znakow to staramy sie go zinterpretowac jako bool (0, 1, yes, no, true, false)
		else if ( ActiveNode->Values[Index].CanGetAsString() )
		{
			c = StringToBoolThrow(ActiveNode->Values[Index].ToString(), format("%s[%i] - Nie mozna zinterpretowac tej wartosci <%s> jako wartosc logiczna", ActiveNode->Key, Index, ActiveNode->Values[Index].ToString()));
		}
		else throw omikron_exception( format("%s[index:%i] - nieprawidlowa wartosc integer (nie mozna zrzutowac wartosci <%s> na typ int)", ActiveNode->Key, Index, ActiveNode->Values[Index].ToString()) );

		Index++; if (Index >= ActiveNode->Values.size()) Index = 0;
		return *this;
	}

	CSxHigReader& CSxHigReader::operator>>(float& c)
	{
		ValidIndex();

		if ( ActiveNode->Values[Index].CanGetAsFloat() )
		{
			c = ActiveNode->Values[Index].GetAsFloat();
		}
		else throw omikron_exception( format("%s[%i] - nieprawidlowa wartosc float (nie mozna zrzutowac wartosci <%s> na typ float)", ActiveNode->Key, Index, ActiveNode->Values[Index].ToString()) );

		Index++; if (Index >= ActiveNode->Values.size()) Index = 0;
		return *this;
	}

	CSxHigReader& CSxHigReader::operator>>(tstring& c)
	{
		ValidIndex();

		switch ( ActiveNode->Values[Index].GetType() )
		{
			case VT_STRING: c =         (ActiveNode->Values[Index].GetAsString()); break;
			case VT_FLOAT:  c = ToString(ActiveNode->Values[Index].GetAsFloat()); break;
			case VT_INT:    c = ToString(ActiveNode->Values[Index].GetAsInt()); break;
		}

		Index++; if (Index >= ActiveNode->Values.size()) Index = 0;
		return *this;
	}

	CSxHigReader& CSxHigReader::operator>>(SxVal_t& c)
	{
		ValidIndex();

		c = ActiveNode->Values[Index];

		Index++; if (Index >= ActiveNode->Values.size()) Index = 0;
		return *this;
	}

	CSxHigReader& CSxHigReader::operator>>(bool& c)
	{
		ValidIndex();

		switch ( ActiveNode->Values[Index].GetType() )
		{
			case VT_STRING:
				c = StringToBoolThrow(ActiveNode->Values[Index].ToString(), format("%s[%i] - Nie mozna zinterpretowac tej wartosci <%s> jako wartosc logiczna", ActiveNode->Key, Index, ActiveNode->Values[Index].ToString()));
			break;

			case VT_FLOAT:
				throw omikron_exception( format("%s[%i] - Nie mozna zinterpretowac tej wartosci <%s> jako wartosc logiczna", ActiveNode->Key, Index, ActiveNode->Values[Index].ToString() ) );
			break;

			case VT_INT:
				c = ActiveNode->Values[Index].GetAsInt() != 0;
			break;
		}

		Index++; if (Index >= ActiveNode->Values.size()) Index = 0;
		return *this;
	}

} // End oo
