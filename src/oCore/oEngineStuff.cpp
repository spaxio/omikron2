﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oEngineStuff.hpp"

namespace oo
{

	CNullOutput		GNullOutput;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////// Engine Config //////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool CEngineConfig::LoadConfigFile(const tstring& filename)
	{
		try
		{
			CSxHigReader::LoadNodeFromFile(&MainConfigNode, filename);
		}
		catch (omikron_exception& e)
		{
			LogMsg("[ERR] [CFG] Nie mozna bylo odczytac pliku konfiguracyjnego %s - %s", filename, e.what());
			MainConfigNode = CSxNode();
			return false;
		}

		return true;
	}

	void CEngineConfig::SaveConfigFile(const tstring& filename)
	{
		CSxHigWriter::SaveNodeToFile(&MainConfigNode, filename);
	}

	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////

	bool CEngineLocal::LoadDictionary(const tstring& filename)
	{
		std::vector< boost::tuple<tstring, tstring> > db;
		if (!MapScript(filename, db))
		{
			LogMsg("[ERR] [DIC] Nie mozna wczytac slownika : " + filename);
			return false;
		}

		for (msize i = 0; i < db.size(); i++)
		{
			DicPair_t toadd(HashString_cs(db[i].get<0>()), db[i].get<1>());
#ifdef _DEBUG
			for (auto it = Dictionary.begin(); it != Dictionary.end(); ++it)
			{
				if (it->first == toadd.first) assert(0);
			}
#endif
			Dictionary.push_back(toadd);
		}

		std::sort(Dictionary.begin(), Dictionary.end(),
			[](const DicPair_t& a, const DicPair_t& b) {
				return a.first < b.first;
			}
		);

		return true;
	}

	tstring CEngineLocal::LocalStr(const tstring& unistr)
	{
		DicPair_t tofind(HashString_cs(unistr), N_STRING);
		auto it = std::lower_bound(Dictionary.begin(), Dictionary.end(), tofind);

		if (it != Dictionary.end() && it->first == tofind.first)
		{
			return it->second;
		}
		else
		{
			return unistr;
		}
	}

	/////////////////////////////////////////////////
	/////////////////////////////////////////////////

	bool CEngineVarTable::CheckName(const char* name)
	{
		assert(name);
		return CSxWriter::CanWriteAsKey(name);
	}

	ESetResult CEngineVarTable::CheckForWrite(VarTable_t::iterator& it)
	{
		if (it == VarTable.end()) return SR_NOTFOUND;

		return SR_OK;
	}

	void CEngineVarTable::RegisterInt(const char* name, int* var, int min, int max)
	{
		u32 hash = HashString_cs(name);
		if (name && var && (min <= max) && CheckName(name) && (VarTable.find(hash) == VarTable.end()))
		{
			SVar v;
			v.Name			= name;
			v.Var.IntVar	= var;
			v.Type			= PT_INT;
			v.MinMax.MinI	= min;
			v.MinMax.MaxI	= max;

			VarTable[hash] = v;
		}
		else assert(0);		// Zle zarejestrowales zmienna
	}

	void CEngineVarTable::RegisterFloat(const char* name, float* var, float min, float max)
	{
		u32 hash = HashString_cs(name);
		if (name && var && (min <= max) && CheckName(name) && (VarTable.find(hash) == VarTable.end()))
		{
			SVar v;
			v.Name			= name;
			v.Var.FloatVar	= var;
			v.Type			= PT_FLOAT;
			v.MinMax.MinF	= min;
			v.MinMax.MaxF	= max;

			VarTable[hash] = v;
		}
		else assert(0);	// Zle zarejestrowales zmienna
	}

	void CEngineVarTable::RegisterString(const char* name, tstring* var, StringChecker_t stringchecker)
	{
		u32 hash = HashString_cs(name);
		if (name && var && CheckName(name) && (VarTable.find(hash) == VarTable.end()))
		{
			SVar v;
			v.Name					= name;
			v.Var.StringVar			= var;
			v.Type					= PT_STRING;
			v.MinMax.StringChecker	= stringchecker;

			VarTable[hash] = v;
		}
		else assert(0);	// Zle zarejestrowales zmienna
	}

	ESetResult CEngineVarTable::SetIntValue(const tstring& name, int value)
	{
		auto it = VarTable.find(HashString_cs(name));

		ESetResult sr = CheckForWrite(it);
		if ( sr == SR_OK )
		{
			SVar& v = it->second;
			if (v.Type == PT_INT) (*(v.Var.IntVar)) = ClipValue(value, v.MinMax.MinI, v.MinMax.MaxI);
			else if (v.Type == PT_FLOAT) (*(v.Var.FloatVar)) = ClipValue(static_cast<float>(value), v.MinMax.MinF, v.MinMax.MaxF);
			else return SR_BADTYPE;
		}

		return sr;
	}

	ESetResult CEngineVarTable::SetFloatValue(const tstring& name, float value)
	{
		auto it = VarTable.find(HashString_cs(name));

		ESetResult sr = CheckForWrite(it);
		if ( sr == SR_OK )
		{
			SVar& v = it->second;
			if (v.Type == PT_FLOAT) (*(v.Var.FloatVar)) = ClipValue(value, v.MinMax.MinF, v.MinMax.MaxF);
			else if (v.Type == PT_INT) (*(v.Var.IntVar)) = ClipValue(static_cast<int>(value), v.MinMax.MinI, v.MinMax.MaxI);
			else return SR_BADTYPE;
		}

		return sr;
	}

	ESetResult CEngineVarTable::SetStringValue(const tstring& name, const tstring& value)
	{
		auto it = VarTable.find(HashString_cs(name));

		ESetResult sr = CheckForWrite(it);
		if ( sr == SR_OK )
		{
			SVar& v = it->second;
			if (v.Type != PT_STRING) return SR_BADTYPE;
			if (v.MinMax.StringChecker != nullptr)
			{
				if (v.MinMax.StringChecker(value))
				{
					(*(v.Var.StringVar)) = value;
				}
				else return SR_BADVALUE;
			}
			else
			{
				(*(v.Var.StringVar)) = value;
			}
		}

		return sr;
	}

	tstring CEngineVarTable::GetValue(const tstring& name)
	{
		auto it = VarTable.find(HashString_cs(name));
		if (it != VarTable.end())
		{
			SVar& v = it->second;

			switch (v.Type)
			{
				case PT_INT: return ToString(*v.Var.IntVar); break;
				case PT_FLOAT: return ToString(*v.Var.FloatVar); break;
				case PT_STRING: return *v.Var.StringVar; break;
				default: { assert(0); return "NULL"; }
			}
		}
		else return "NULL";
	}

	void CEngineVarTable::WriteStat(IOutputDevice& out)
	{
		for (auto& it : VarTable)
		{
			SVar& var = it.second;
			out.Write( var.Name );
			out.Write( " = " );

			switch (var.Type)
			{
				case PT_INT: out.Writeln(ToString(*var.Var.IntVar)); break;
				case PT_FLOAT: out.Writeln(ToString(*var.Var.FloatVar)); break;
				case PT_STRING: out.Writeln(*var.Var.StringVar); break;
			}
		}
	}

	const char* CEngineVarTable::ResultToString(ESetResult result)
	{
		switch (result)
		{
			case SR_OK:	return "";
			case SR_NOTFOUND: return "Zmienna o takiej nazwie nie zostala odnaleziona w tabeli";
			case SR_BADTYPE: return "Typ zmiennej jest inny niz wartosc podana w argumencie";
			case SR_BADVALUE: return "Zly format wartosci";
			default: return "Nieznany blad";
		}
	}

	/////////////////////////////////////////////////
	/////////////////////////////////////////////////

	void CEngineScheduler::MergeTasks()
	{
		// Jak sa nowe zadania to dodajemy
		if (!NewTasks.empty())
		{
			Tasks.insert(Tasks.end(), NewTasks.begin(), NewTasks.end());
			NewTasks.clear();

			std::sort(Tasks.begin(), Tasks.end(),
				[](const STask& m1, const STask& m2) {
					return m1.Time > m2.Time;
				}
			);
		}
	}

	void CEngineScheduler::Update(float dt)
	{
		SchedulerTime += dt;

		MergeTasks();

		while ( !Tasks.empty() )
		{
			STask& t = Tasks.back();
			if ( t.Time > SchedulerTime ) { return; }
			else
			{
				assert(t.Func);
				t.Func();

				if (t.Interval >= 0.0f)
				{
					t.Time = SchedulerTime + t.Interval;
					NewTasks.push_back(t);
				}

				Tasks.pop_back(); // Wazna uwaga, to musi byc przed Func(), inaczej moga pojawic sie problemy gdy to Func() dodaje nam nowe Zadania
			}
		}
	}

	void CEngineScheduler::ClearTasks()
	{
		MergeTasks();
		Tasks.clear();
	}

	void CEngineScheduler::ClearTasks(int category)
	{
		MergeTasks();

		Tasks.erase(
			std::remove_if( Tasks.begin(), Tasks.end(),
				[category](const STask& task) {
					return task.Category == category;
				}
			),
			Tasks.end()
		);
	}

	void CEngineScheduler::AddTaskPrivate(float time, int category, float interval, const TaskFunc_t& func)
	{
		assert(func);

		STask task;
		task.Time = SchedulerTime + time;
		task.Func = func;
		task.Category = category;
		task.Interval = interval;

		NewTasks.push_back(task);
	}

	/////////////////////////////////////////////////
	/////////////////////////////////////////////////

	CEngineInterface::CEngineInterface()
	{
		Out = &GNullOutput;
		QuietOut = &GNullOutput;
	}

	bool CEngineInterface::CCommand::ParseOnly(const tstring& args, IOutputDevice& out, Args_t& args_out)
	{
		CSxReader reader(&args);
		args_out.clear();

		tstring tmp_s;
		float tmp_f;
		int tmp_i;

		if (ArgTypes != nullptr)
		{
			try
			{
				msize len = strlen(ArgTypes);
				for (msize i = 0; i < len; i++)
				{
					switch (ArgTypes[i])
					{
						case 'f':
							reader.MustReadFloat(tmp_f);
							args_out.push_back(tmp_f);
							break;

						case 's':
							reader.MustReadString(tmp_s);
							args_out.push_back(tmp_s);
							break;

						case 'i':
							if (reader.ReadInteger(tmp_i)) args_out.push_back(tmp_i);
							else if (reader.ReadString(tmp_s))
							{
								bool x = StringToBoolThrow(tmp_s, "Nie mozna zinterpretowac wartosci jako int (konwersja bool->int)");
								args_out.push_back(x?1:0);
							}
							else throw omikron_exception("Zla wartosc pod typem int");

							break;

						case 'k':
							reader.MustReadString(tmp_s);
							if (CSxWriter::CanWriteAsKey(tmp_s))
							{
								args_out.push_back(tmp_s);
							}
							else throw omikron_exception("Zla wartosc pod typem key");
							break;

						case 'v':
							if (reader.ReadInteger(tmp_i)) args_out.push_back(tmp_i);
							else if (reader.ReadFloat(tmp_f)) args_out.push_back(tmp_f);
							else if (reader.ReadString(tmp_s)) args_out.push_back(tmp_s);
							else throw omikron_exception("Zla wartosc pod typem value");

							break;

						default:
							assert(0);
							return false;
					}
				}
			}
			catch (omikron_exception& e)
			{
				out.Writeln("Nieprawidlowe argumenty : " + e.str());
				out.Writeln("Poprawna postac funkcji: " + GetFuncPrototype());

				args_out.clear();

				return false;
			}

			// Dla pewnosci
			assert(args_out.size() == strlen(ArgTypes));
		}

		return true;
	}

	void CEngineInterface::CCommand::ParseAndExec(const tstring& args, IOutputDevice& out)
	{
		CSxReader reader(&args);
		Args_t arg_vector;

		if (!ParseOnly(args, out, arg_vector)) return;

		ExecOnly(arg_vector, out);
	}

	void CEngineInterface::CCommand::LogCall(const Args_t& args)
	{
		std::vector<tstring> typetab = GetFullTypes();
		std::vector<tstring> nametab;

		if (ArgNames)
		{
			nametab = GetFullNames();
		}

		tstring log = "!> ";
		log += GetName();
		log += '[';
		for (msize i = 0; i < args.size(); i++)
		{
			if (ArgNames) log += nametab[i];
			else log += typetab[i];

			log += ':';

			if (args[i].GetType() == VT_STRING)
			{
				log += '"';
				log += args[i].GetAsString();
				log += '"';
			}
			else
			{
				log += args[i].ToString();
			}

			if ((i+1) < args.size())
			{
				log += " ";
			}
		}
		log += ']';

		LogMsg(log);
	}

	void CEngineInterface::CCommand::ExecOnly(const Args_t& args, IOutputDevice& out)
	{
		// Logujemy wywolanie
		LogCall(args);

		// Wykonujemy funkcje
		if (Func)
		{
			try
			{
				Func(args, out);
			}
			catch (oo::omikron_exception& e)
			{
				out.Writeln("Error: " + e.str());
			}
		}
		else out.Writeln( "Nie mozna wykonoac komendy, funkcja nie jest prawidlowa (nullptr?)" );
	}

	std::vector<tstring> CEngineInterface::CCommand::GetFullTypes()
	{
		if (ArgTypes == nullptr) return std::vector<tstring>();

		std::vector<tstring> result;
		for (char c : boost::as_literal(ArgTypes))
		{
			switch (c)
			{
				case 'f': result.push_back("float"); break;
				case 's': result.push_back("string"); break;
				case 'i': result.push_back("int"); break;
				case 'k': result.push_back("key"); break;
				case 'v': result.push_back("any"); break;
			}
		}

		return result;
	}

	std::vector<tstring> CEngineInterface::CCommand::GetFullNames()
	{
		return Split(ArgNames?ArgNames:N_STRING, ',');
	}

	bool CEngineInterface::CCommand::CheckNames()
	{
		// Brak argumentow
		if (ArgTypes == nullptr)
		{
			// Brak nazw
			if (ArgNames == nullptr) return true;
			else return false;
		}
		else // Sa argumenty
		{
			// Nie ma ich nazw
			if (ArgNames == nullptr) return true;
			else // Nazwy tez sa
			{
				// Sprawdzamy czy taka sama ilosc
				if (strlen(ArgTypes) == GetFullNames().size()) return true;
				else return false;
			}
		}
	}

	tstring CEngineInterface::CCommand::GetFuncPrototype(bool color)
	{
		if (ArgTypes == nullptr) return "[void]";

		std::vector<tstring> typetab = GetFullTypes();
		std::vector<tstring> nametab;

		if (ArgNames)
		{
			nametab = GetFullNames();
		}

		tstring result;

		if (color) result += "^0";
		result += '[';

		for (msize i = 0; i < typetab.size(); i++)
		{
			if (color) result += "^4";
			result += typetab[i];

			if (ArgNames)
			{
				if (color) result += "^6";
				result += ' ';
				result += nametab[i];
			}

			if (i != (typetab.size()-1))
			{
				if (color) result += "^0";
				result += ", ";
			}
		}

		if (color) result += "^0";
		result += ']';

		return result;
	}

	void CEngineInterface::SeparateArguments(const tstring& commandline, tstring& name, tstring& args)
	{
		name.clear();
		args.clear();

		tstring cmd = Trim(commandline);

		msize i = 0;
		while (i < cmd.size())
		{
			if ( (IsAlnumAscii(cmd[i])) || (cmd[i]=='_') ) name += cmd[i];
			else break;
			i++;
		}

		appToLower(name);
		args = cmd.substr(i);
	}

	void CEngineInterface::AddCommand(const char* name, Func_t func, const char* argformat, const char* help, Caller_t caller)
	{
		Commands[HashString(name)] = CCommand(name, argformat, help, func, caller);
	}

	void CEngineInterface::Exec(const tstring& commandline, bool quiet)
	{
		if (commandline.empty()) return;

		tstring name;
		tstring args;

		SeparateArguments(commandline, name, args);
		if (name.empty()) return;

		IOutputDevice& out = (quiet ? (*QuietOut) : (*Out));

		auto it = Commands.find( HashString(name) );
		if (it != Commands.end())
		{
			if (it->second.CallerExist())
			{
				Args_t tmp;
				if (it->second.ParseOnly(args, out, tmp))
				{
					it->second.ExecCaller(commandline);
				}
			}
			else
			{
				it->second.ParseAndExec(args, out);
			}
		}
		else out.Writeln( "Unknown command: " + name );
	}

	bool CEngineInterface::RawExec(u32 hash, const Args_t& args)
	{
		auto it = Commands.find(hash);
		if (it == Commands.end()) return false;

		if (it->second.CallerExist())
		{
			it->second.ExecOnly(args, *QuietOut);
			return true;
		}

		return false;
	}

	bool CEngineInterface::ExtractHashAndArgs(const tstring& commandline, u32& out_hash, Args_t& out_args)
	{
		if (commandline.empty()) return false;

		tstring name;
		tstring args;

		SeparateArguments(commandline, name, args);
		if (name.empty()) return false;

		out_hash = HashString(name);

		auto it = Commands.find(out_hash);
		if (it == Commands.end()) return false;

		return it->second.ParseOnly(args, *QuietOut, out_args);
	}

	tstring CEngineInterface::GetArgTypes(const tstring& command)
	{
		auto it = Commands.find(HashString(command));
		if (it == Commands.end()) return N_STRING;
		return it->second.GetArgTypes();
	}

	std::vector<tstring> CEngineInterface::GetArgNames(const tstring& command)
	{
		auto it = Commands.find(HashString(command));
		if (it == Commands.end()) return std::vector<tstring>();
		return it->second.GetFullNames();
	}

	void CEngineInterface::SetOutputDevice(IOutputDevice* dev)
	{
		assert(dev);
		Out = dev;

#ifdef OMIKRON2_DEDICATED_SERVER
		QuietOut = dev;
#endif
	}

	std::vector<tstring> CEngineInterface::GetCommandList(const tstring& firstchars)
	{
		std::vector<tstring> commandlist;
		tstring name;

		for (auto it = Commands.begin(); it != Commands.end(); ++it)
		{
			name = it->second.GetName();
			if ( name.find(firstchars, 0) == 0 )
			{
				commandlist.push_back(name);
			}
		}

		return commandlist;
	}

	tstring CEngineInterface::GetCommandHelp(const tstring& command, bool color)
	{
		Commands_t::iterator it = Commands.find(HashString(command));
		if (it == Commands.end()) return "^0There is no such command";

		tstring result;
		if (color) result += "^5";
		result += command;
		result += ' ';
		result += it->second.GetFuncPrototype( color );

		return result;
	}

	namespace detail
	{
		struct FuncPrototype
		{
			CEngineInterface::Func_t Func;
			const char* Args;
			const char* Help;
			int Flag;
		};

		std::map<const char*, FuncPrototype>& FuncTableSingleton()
		{
			static std::map<const char*, FuncPrototype> func_table;
			return func_table;
		}

		const char* NameTmp;
		const char* ArgsTmp;
		const char* HelpTmp;
		int FlagTmp;
		bool LockTmp;

		FuncRegister Prototype(const char* name, const char* args, const char* help, int flag)
		{
			NameTmp = name;
			ArgsTmp = args;
			HelpTmp = help;
			FlagTmp = flag;
			LockTmp = false;

			return FuncRegister();
		}

		const FuncRegister& FuncRegister::operator<<(CEngineInterface::Func_t func)
		{
			assert(LockTmp == false);

			FuncPrototype prototype = { func, ArgsTmp, HelpTmp, FlagTmp };
			FuncTableSingleton()[NameTmp] = prototype;

			LockTmp = true;

			return *this;
		}
	}

	void UseFuncListToInitEngineInterface()
	{
		auto table = detail::FuncTableSingleton();
		auto ei = Core::GetEngineInterface();

		for (auto& row : table)
		{
			if (row.second.Flag == 0)
			{
				ei->AddCommand(row.first, row.second.Func, row.second.Args, row.second.Help);
			}
			else
			{
				ei->AddCommand(row.first, row.second.Func, row.second.Args, row.second.Help, GameFunctionCaller);
			}
		}

		decltype(table) empty_one;
		std::swap(table, empty_one);
	}

	/////////////////////////////////////////////////
	/////////////////////////////////////////////////

	typedef std::chrono::steady_clock SysClock;

	long long	Time;	    // Czas jaki uplynal od odpalenia aplikacji
	long long	StartTime;  // Czas jaki uplynal od startu systemu

	long long NewTime;		// Nowy czas
	double Delta;			// Czas jaki uplynal od ostatniej klatki (sekundy:floatpoint)
	long long FrameTime;	// Czas jaki uplynal od ostatniej klatki (mikrosekundy:fixedpoint)
	long long OldTime;		// Stary czas

	long long	PrevTime;	// Czas do nastepnego obliczenia fps(1s)
	int			Fps;		// Liczba klatek na sekunde
	int			FpsTmp;		// Pomocznicza
	long long	PauseTime;	// Czas pauzy

	const long long Micro = 1000000LL;
	const double MicroDouble = 1.0 / static_cast<double>(Micro);

	long long timNow()
	{
		auto time = SysClock::now().time_since_epoch();
		return std::chrono::duration_cast<std::chrono::microseconds>(time).count();
	}

	void timSetup()
	{
		Time = 0LL;
		NewTime = 0LL;
		Delta = 1.0f;
		FrameTime = 0LL;
		OldTime = 0LL;
		PrevTime = 0LL;
		Fps = 0;
		FpsTmp = 0;
		PauseTime = 0LL;

		StartTime = timNow();
	}

	int timGetFps()
	{
		return Fps;
	}

	double timGetDeltaTime()
	{
		return Delta;
	}

	long long timGetFrameTime()
	{
		return FrameTime;
	}

	double timGetTime()
	{
		auto time = timNow();
		return static_cast<double>(time) * MicroDouble;
	}

	void timBeginFrame()
	{
		auto time = timNow();
		Time = time - StartTime;
		NewTime = Time;
	}

	void timEndFrame()
	{
		FrameTime = NewTime - OldTime;
		Delta = static_cast<double>(FrameTime) * MicroDouble;

		OldTime = NewTime;

		// Liczy fps
		FpsTmp++;
		if((Time - PrevTime) > Micro)
		{
			PrevTime = Time;
			Fps = FpsTmp;
			FpsTmp = 0;
		}
	}

	void timPause()
	{
		PauseTime = timNow();
	}

	void timResume()
	{
		if (PauseTime != 0LL)
		{
			auto time = timNow();
			StartTime += time - PauseTime;
			Delta = 0.0;
			PauseTime = 0LL;
		}
	}

} // End oo
