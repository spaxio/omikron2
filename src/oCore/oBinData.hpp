﻿/*
 * Copyright © 2013 Karol Pałka
 */

#pragma once
#include "oSystem.hpp"
#include "oMath.hpp"

namespace oo
{

	/////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// Strumienie /////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	// Interfejs czysto abstrakcyjny strumienia
	class IStream
	{
		private: // Obiekty dziedziczone po IStream nie sa do kopiowania
			IStream( const IStream& );
			IStream& operator=( const IStream& );

		public:
			enum EStreamPos { SP_END, SP_BEG, SP_CUR };

		public:
			IStream() {};
			virtual ~IStream() {};

			// Pobiera rozmiar strumienia
			virtual msize GetSize() =0;
			// Pobiera pozycje kursora w strumieniu
			virtual msize GetPos() =0;
			// Ustawia pozycje kursora w strumieniu
			virtual void SetPos(msize p, EStreamPos s) =0;

			// Sprawdza czy strumien nadaje sie do uzytku
			virtual bool IsOK() =0;
			// Sprawdza czy kursor jest na koncu strumienia
			virtual bool EndOfStreame() =0;

			// Sprawdza czy strumien jest do odczytu
			virtual bool CanRead() =0;
			// Sprawdza czy strumien jest do zapisu
			virtual bool CanWrite() =0;

			// Odczytuje dane ze strumienia
			virtual msize Read(void* data, msize size) =0;
			// Zapisuje dane do strumienia
			virtual msize Write(const void* data, msize size) =0;

			// Odczytuje dane ze strumienia, gdy nie odczyta wszystkich bajtow rzuca wyjatkiem
			void ReadSafe(void* data, msize size)
			{
				if (Read(data, size) != size) throw omikron_exception("IStream::Read niespodziewany koniec");
			}
			// Zapisuje dane do strumienia, gdy nie zapisze wszystkich bajtow rzuca wyjatkiem
			void WriteSafe(const void* data, msize size)
			{
				if (Write(data, size) != size) throw omikron_exception("IStream::Write za malo miejsca");
			}
	};

	// Strumien plikowy do odczytu
	class CFile : public IStream
	{
		public:
			enum EOpenMode { OM_READ, OM_WRITE };

		private:
			FILE* File;
			EOpenMode OpenMode;

		public:
			CFile();
			CFile(const tstring& filename, EOpenMode om);
			~CFile();

			CFile(CFile&& other) :
				File(other.File),
				OpenMode(other.OpenMode)
			{
				other.File = nullptr;
			}

			CFile& operator=(CFile&& other)
			{
				if (this != &other)
				{
					Close();
					File = other.File;
					OpenMode = other.OpenMode;
					other.File = nullptr;
				}
				return *this;
			}

			// Otwiera plik
			bool Open(const tstring& filename, EOpenMode om);
			// Zamyka plik
			void Close();

			// Wmusza zapisanie danych
			void Flush();

			// Zwraca rozmiar pliku
			msize GetSize();
			// Zwraca pozycje kursora w pliku
			msize GetPos();
			// Ustawia pozycje kursora w pliku
			void SetPos(msize p, EStreamPos s);

			// Sprawdza czy plik nadaje sie do zapisu/odczytu
			bool IsOK();
			// Sprawdza czy kursor jest na koncu pliku
			bool EndOfStreame();

			// Sprawdza czy plik jest do odczytu
			bool CanRead();
			// Sprawdza czy plik jest do zapisu
			bool CanWrite();

			// Odczytuje dane z pliku
			msize Read(void* data, msize size);
			// Zapisuje dane do pliku
			msize Write(const void* data, msize size);
	};

	// Strumien pamieciowy do odczytu
	class CBuffer : public IStream
	{
		private:
			std::vector<u8> Data;	// Dane
			msize Pos;				// Pozycja kursora

		public:
			explicit CBuffer(std::vector<u8>&& data) : Data(std::move(data)), Pos(0) {}
			explicit CBuffer(const u8* data, msize size) : Data(data, data + size), Pos(0) {}
			explicit CBuffer(msize size) : Data(size), Pos(0) {}

			CBuffer(CBuffer&& other) : Data(std::move(other.Data)), Pos(other.Pos)
			{
			}

			CBuffer& operator=(CBuffer&& other)
			{
				if (this != &other)
				{
					Data = std::move(other.Data);
					Pos = other.Pos;
				}

				return *this;
			}

			// Zwraca wskaznik na dane
			u8* GetPtr();

			// Zwraca rozmiar bufora
			msize GetSize();
			// Zwraca pozycje kursora w buforze
			msize GetPos();
			// Ustawia pozycje kursora w buforze
			void SetPos(msize p, EStreamPos s);

			// Sprawdza czy bufor nadaje sie do zapisu/odczytu
			bool IsOK();
			// Sprawdza czy kursor jest na koncu bufora
			bool EndOfStreame();

			// Sprawdza czy strumien danych jest do odczytu
			bool CanRead() ;
			// Sprawdza czy strumien danych jest do zapisu
			bool CanWrite();

			// Odczytuje dane z bufora
			msize Read(void* data, msize size);
			// Zapisuje dane do bufora
			msize Write(const void* data, msize size);
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////// Serializacja /////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Jesli chcesz aby ISerializer mogl zapisywac/odczytywac twoj obiekt, dodaj do jego klasy
	// void Serialize(ISerializer& archive)
	// Nastepnie wykorzystaj metode SerializeObject

	class ISerializer
	{
		private:
			virtual void SerializeRawData(u8* ptr, msize size) = 0;

		public:
			// Sa tylko dwa rodzaje serializerow
			virtual bool IsWriter() = 0;
			virtual bool IsReader() = 0;

			// Dla typow PODowych
			template <typename T>
			void Serialize(T& val) { SerializeRawData((u8*)&val, sizeof(T)); }

			// Obiekty
			template <typename T>
			void SerializeObject(T& obj) { obj.Serialize(*this); }

			// Ladniejsza skladnia
			void operator&(int& v) { Serialize(v); }
			void operator&(float& v) { Serialize(v); }
			void operator&(bool& v) { Serialize(v); }

			void operator&(msize& v) { Serialize(v); }

			virtual void operator&(tstring& str) = 0;

			// Typy skladane
			void operator&(vect2f& t)
			{
				operator&(t.x);
				operator&(t.y);
			}

			void operator&(vect3f& t)
			{
				operator&(t.x);
				operator&(t.y);
				operator&(t.z);
			}

			void operator&(rectf& t)
			{
				operator&(t.left);
				operator&(t.top);
				operator&(t.right);
				operator&(t.bottom);
			}

			void operator&(colorf& t)
			{
				operator&(t.R);
				operator&(t.G);
				operator&(t.B);
				operator&(t.A);
			}
	};

	class CSerializerWrite : public ISerializer
	{
		private:
			IStream* Stream;

			void SerializeRawData(u8* ptr, msize size) override { Stream->WriteSafe(ptr, size); }

		public:
			CSerializerWrite(IStream* stream) : Stream(stream)
			{
				if (stream->CanWrite() == false) throw omikron_exception("CSerializerWrite() dostal strumien do ktorego nie mozna zapisywac");
			}

			bool IsWriter() override { return true; }
			bool IsReader() override { return false; }

		public:
			void operator&(tstring& str) override
			{
				msize size = str.size();
				ISerializer::Serialize(size);
				Stream->WriteSafe(str.data(), size);
			}

	};

	class CSerializerRead : public ISerializer
	{
		private:
			IStream* Stream;

			void SerializeRawData(u8* ptr, msize size) override
			{
				Stream->ReadSafe(ptr, size);
			}

		public:
			CSerializerRead(IStream* stream) : Stream(stream)
			{
				if (stream->CanRead() == false) throw omikron_exception("CSerializerRead() dostal strumien z ktorego nie mozna czytac");
			}

			bool IsWriter() override { return false; }
			bool IsReader() override { return true; }

		public:
			void operator&(tstring& str) override
			{
				msize size;
				ISerializer::Serialize(size);
				if (size == 0) { str.clear(); return; }

				std::vector<char> buff(size);
				Stream->ReadSafe(buff.data(), size);
				if (utf8::is_valid(buff.begin(), buff.end()))
				{
					throw omikron_exception("CSerializerRead::operator&(tstring) nieprawidlowa sekwencja UTF-8");
				}

				str.assign(buff.begin(), buff.end());
			}

	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// Rozne fajne struktury i kontenery ///////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	/*
	 *		Memory Pool
	 *
	 *		Obiekty tej klasy powinny byc globalne
	 *
	 *		Zakladamy ze mamy obiekty dzieci Rakieta, Granat i rodzica Pocisk
	 *		Wiemy ze max(sizeof(Rakieta), sizeof(Granat)) == 32
	 *		W tej chwili mozemy uzyc do tego CMemoryPool
	 *
	 *		Jak uzywac
	 *		CMemoryPool PociskMemoryPool(32, 1000); // Gdzies dostepna globalnie
	 *
	 *		I dwie metody
	 *
	 *		template<typename T>
	 *		inline T* NewPocisk(argumenty konstruktora)
	 *		{
	 *			return new (PociskMemoryPool.Alloc()) T(argumenty konstuktora);
	 *		}
	 *
	 *		Jesli Rakieta i Granat maja rozne konstuktory tworzymy dwie metody
	 *
	 *		inline void DeleteBullet(Pocisk* bullet)
	 *		{
	 *			bullet->~Pocisk();
	 *			BulletsMemoryPool.Free(bullet);
	 *		}
	 */

	class CMemoryPool
	{
		private: // Obiekty CMemoryPool nie sa do kopiowania
			CMemoryPool( const CMemoryPool& );
			CMemoryPool& operator=( const CMemoryPool& );

		private:
			void* FreeChunk;
			void* Pool;

			const char* Name;

			const msize ChunkSize;
			const msize ChunkCount;
			const msize PoolSize;

			msize Allocated;

		public:
			msize GetChunkSize() const { return ChunkSize; }
			msize GetChunkCount() const { return ChunkCount; }
			msize GetPoolSize() const { return PoolSize; }

			msize GetAllocated() const { return Allocated; }
			const char* GetName() const { return Name; }

			// Statystyki w postaci stringa
			tstring GetTextStats() const;

		public:
			CMemoryPool(const char* name, msize chunksize, msize chunkcount);
			~CMemoryPool();

		public:
			void* Alloc()
			{
				#ifdef _DEBUG
				if (FreeChunk == nullptr) { assert(!"Out of memory"); return nullptr; }

				Allocated++;
				#endif

				void* result = FreeChunk;
				FreeChunk = (void*)*((msize*)FreeChunk);
				return result;
			}

			void Free(void* ptr)
			{
				#ifdef _DEBUG
				msize rptr = (msize)ptr;
				msize rpool = (msize)Pool;
				if (rptr < rpool || rptr > (rpool + PoolSize)) assert(!"Pointer out of pool range");
				if (((rptr - rpool) % ChunkSize) != 0) assert(!"Bad pointer");

				Allocated--;
				if (Allocated == 0xFFFFFFFF) assert(0);
				#endif

				*((msize*)ptr) = (msize)FreeChunk;
				FreeChunk = ptr;
			}
	};

	void MemoryPoolWriteStat(IOutputDevice& out);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////// Kompresja ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Kompresuje dane
	bool CompressData(const u8* data, msize datasize, u8* buffer, msize& buffersize);
	// Dekompresuje
	bool DecompressData(const u8* data, msize datasize, u8* buffer, msize& buffersize);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////// Sumy kontrolne CRC32 /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Generuje sume CRC z lancucha znakow
	u32 GetCRC(const tstring& str);
	// Generuje sume CRC ze strumienia
	u32 GetCRC(IStream& stream);
	// Generuje sume CRC z danych
	u32 GetCRC(u8* data, msize size);
	// Łaczy dwie sumy CRC
	u32 CombinationTwoCRC(u32 crc1, u32 crc2);
	// Generuje CRC
	void GenCRC(u8 *data, msize size, u32 &crc);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////// Wirtualny system plikow /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// RefFile
	typedef std::shared_ptr<IStream> RefFile;

	// Pakuje wybrany folder do pliku mixfile
	bool Vfs_Pack(const tstring& folder, const tstring& mixfile, const tstring& exceptions, const tstring& compress);
	// Rozpakowywuje pliki do folderu
	bool Vfs_Unpack(const tstring& mixfile, const tstring& folder);
	// Wylicza pliki w folderze
	bool Vfs_ListFiles(const tstring& mixfile, std::vector<tstring>& filesout);

	// Odswierza liste plikow w VFS (funkcja powinna byc wykonana tylko raz)
	void vfsRefreshList();
	// Szuka plikow w vfs
	std::vector<tstring> vfsSearch(const tstring& wildcard);

	class CFileMounter
	{
		private:
			CFileMounter( const CFileMounter& );
			CFileMounter& operator=( const CFileMounter& );

		private:
			tstring Filename;

		public:
			CFileMounter(const RefFile& stream);
			~CFileMounter();

			tstring GetFilename() { return Filename; }
	};

	// Zwraca referencje do wybranego pliku
	RefFile vfsGetFile(const tstring& filename);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// Data i czas systemowy /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////

	// Pobiera aktualny czas systemowy
	tstring GetTimeString();
	// Pobiera aktualna date
	tstring GetDataString();

	////////////////////////////////// LOGOWANIE

	inline void LogMsg(tstring message)
	{
		Core::GetEngineLogger()->Writeln(
			format("[%s] %s", 
				GetTimeString(), 
				std::move(message)
			)
		);
	}

	template <typename T, typename... ARGS>
	inline void LogMsg(tstring str, T arg, ARGS... args)
	{
		Core::GetEngineLogger()->Writeln(
			format("[%s] %s", 
				GetTimeString(), 
				format(std::move(str), arg, args...)
			)
		);
	}

} // End oo
