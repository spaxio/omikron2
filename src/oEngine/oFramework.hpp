﻿/*
 * Copyright © 2013 Karol Pałka
 */

#pragma once

#include "../oCore/oCore.hpp"

#include "oAudio.hpp"
#include "oInput.hpp"
#include "oNetwork.hpp"

// Krok symulacji
const long long PHISICSTEP_FIXED = 10000;	// 10 milisekund 1/100 sekundy
const float PHISICSTEP_FLOAT = 1.0f / static_cast<float>(PHISICSTEP_FIXED / 100LL); // Fizyka 100 Hz

inline float GetPhysicStep() { return PHISICSTEP_FLOAT; }

extern PFNGLBINDBUFFERARBPROC glBindBuffer;
extern PFNGLDELETEBUFFERSARBPROC glDeleteBuffers;
extern PFNGLGENBUFFERSARBPROC glGenBuffers;
extern PFNGLBUFFERDATAARBPROC glBufferData;
extern PFNGLBUFFERSUBDATAARBPROC glBufferSubData;
extern PFNGLMAPBUFFERARBPROC glMapBuffer;
extern PFNGLUNMAPBUFFERARBPROC glUnmapBuffer;

// Te funkcje warto zaimplementowac :)
void OnRender();			// Renderowanie
void OnUpdate();			// Odswiezanie
void OnInput();				// Wejscie
void OnPhysic();			// Cos jak update ale tutaj mamy zapewniony staly krok czasowy
void OnResize();			// Okno zmienia rozmiar
void OnLoad();				// Wczytywanie
void OnUnload();			// Wymazywanie
void OnNetwork();			// Nowe dane czekaja na odebranie

void OnEvent(const sf::Event& e);

namespace oo
{

	void CopyToClipboard(const tstring& text);
	tstring GetFromClipboard();

	// Pokazuje kursor
	void ShowSysCursor();
	// Ukrywa kursor
	void HideSysCursor();

	enum ESysCursor
	{
		SC_ARROW = 0,
		SC_IBEAM,
		SC_SIZENWSE,
		SC_SIZENESW,
		SC_SIZEWE,
		SC_SIZENS,
		SC_SIZEALL
	};

	// Zmienia wyglad kursora
	void ChangeSysCursor(ESysCursor sc);
	// Blokuje ostatnio ustawiony kursor
	void LockSysCursor(void* who = nullptr);
	// Zdejmuje blokade na kursor
	void UnlockSysCursor(void* who = nullptr);
	// Zdejmuje blokade na kursor
	void ForceUnlockSysCursor();

	u32 GetRandomSeed();

	enum EAspectRatio
	{
		AR_UNKNOWN=0,	// ???
		AR_16_9,		// 1,7777777777777777777777777777778f
		AR_16_10,		// 1,6f
		AR_3_2,			// 1,5f
		AR_4_3,			// 1,3333333333333333333333333333333f
		AR_5_4,			// 1,25f

		AR_MAX
	};

	// Czy karta wspiera tekstury o dowolnych rozmiarach?
	bool TextureNonPower2Support();

	const char* AspectRatioToString(EAspectRatio ar);

	const float BASE_SCREEN_X = 800.0f;
	const float BASE_SCREEN_Y = 600.0f;

	// Tryb graficzny
	class CDisplayMode
	{
		public:
			int Width;
			int Height;
			int Bits;

		public:
			CDisplayMode() : Width(0), Height(0), Bits(0) {}
			CDisplayMode(int w, int h, int b) : Width(w), Height(h), Bits(b) {}

			void Change(int w, int h, int b) { Width = w; Height = h; Bits = b; }

			EAspectRatio GetAspectRatio() const;

			bool operator==(const CDisplayMode &t) const { return (Width == t.Width) && (Height == t.Height) && (Bits == t.Bits); }
			bool operator!=(const CDisplayMode &t) const { return (Width != t.Width) || (Height != t.Height) || (Bits != t.Bits); }

			tstring ResolutionToString() { return format("%i x %i", Width, Height); }
	};

	// Wylicza wszystkie tryby graficzne
	// bits - wylicza z okreslonej bitowosci (-1 oznacza wszystkie)
	// aspectratio - wyliczy tylko te o podanym wspolczynniku proporcji
	std::vector<CDisplayMode> EnumDisplayModes(int bits = -1, EAspectRatio aspectratio = AR_MAX);
	// Sprawdza poprawnosc trybu graficznego
	bool CheckDisplayMode(const CDisplayMode& mode);

	CDisplayMode GetSysDisplayMode();

	bool GetVSync();
	void SetVSync(bool on);

	// Startuje/konczy aplikacje i glowna petle gry
	void StartApp();
	void CloseApp();

	enum EFullScreenMode
	{
		FSM_NONE,	    // Tryb okienkowy
		FSM_STANDARD,	// Normalny tryb pelnoekranowy
		FSM_NOBORDER	// Tryb okienkowy udajacy tryb ekranowy (pozwala na szybsze przelaczanie sie do pulpitu za cene wydajnosci)
	};

	// Ustawia wybrany tryb graficzny
	void SetDisplayMode(const CDisplayMode& mode, EFullScreenMode full);
	void GetDisplayMode(CDisplayMode& mode, EFullScreenMode& full);

	// To uzywamy przy FixedScreen
	float GetScreenX();
	float GetScreenY();

	// A to przy FloatScreen
	float GetFloatScreenX();
	float GetFloatScreenY();

	// Rozmiar obrazu
	vect2f GetScreenXY();
	vect2f GetFloatScreenXY();

	vect2f GetClientCursorPos();

	// Obraz bedzie wygladal podobnie na roznych rozdzielczosciach (bierze pod uwage aspect ratio)
	void SetFloatScreen(const vect2f& offset = vect2f(0.0f, 0.0f));
	void SetFloatScreenFlip(const vect2f& offset = vect2f(0.0f, 0.0f));

	// Jednostka idealny pixel monitora
	void SetFixedScreen();

	// =================================== Wejscie ====================================

	bool KeyState(u32 key, bool once = true);
	bool MouseState(u32 button, bool once = true);
	bool JoyState(u32 button, bool once = true);

	vect2f GetAxis(int joystick = 0);

	// ================================= Engine Tools =================================

	class Tools;
		class CFontManager;
		class CGuiManager;
		class CVectorManager;
		class CGraphManager;

	class Tools
	{
		private:
			static CFontManager*			FontManager;
			static CGuiManager*				GuiManager;
			static CVectorManager*			VectorManager;
			static CGraphManager*			GraphManager;

		public:
			static void SetUpTools();		// Uwaga! Jesli chcesz korzystac z tych obiektow musisz wywolac SetUpTools w OnLoad
			static void ShutDownTools();	// A to w OnUnload

			static CFontManager* GetFontManager() { return FontManager; }
			static CGuiManager* GetGuiManager() { return GuiManager; }
			static CVectorManager* GetVectorManager() { return VectorManager; }
			static CGraphManager* GetGraphManager() { return GraphManager; }

	};

} // End oo
