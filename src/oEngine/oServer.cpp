﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oServer.hpp"
#include <iostream>

namespace oo
{
	class CConsoleOutput : public IOutputDevice
	{
		public:
			void Write(const tstring& message) { std::cout << message; }
			void Writeln(const tstring& message) { std::cout << message << std::endl; }
	};
	CConsoleOutput ConsoleOutput;

	u32 GServerRandomSeed = 0;
	tstring GServerName = "noname";
	u16 GServerPort = 4996;

	bool CheckServerName(const tstring& name) { return CSxWriter::CanWriteAsKey(name); }

	void PrintHelpScreen()
	{
		LogMsg("Usage:");
		LogMsg("\t--name, -n \tServer name");
		LogMsg("\t--port, -p \tPort number");
		LogMsg("Example:");
		LogMsg("\tserver --name server_id_01 --port 4966");
		LogMsg("\tserver -n server_id_01 -p 4966");
	}

	void UseArguments(int argc, char** argv)
	{
		if (argc <= 1) throw omikron_exception("No arguments?");

		// Parse stage
		std::vector<tstring> args;
		for (int i = 0; i < argc; i++) { args.push_back(argv[i]); }

		for (msize i = 1; i < args.size(); i++)
		{
			const tstring& str = args[i];

			if (str == "-p" || str == "--port")
			{
				auto port = args.at(++i);
				GServerPort = Parse<u16>(port);
			}
			else if (str == "-n" || str == "--name")
			{
				auto name = args.at(++i);
				if (CheckServerName(name)) GServerName = name;
				else throw omikron_exception("Incorrect server name: " + str);
			}
			else
			{
				throw omikron_exception("Unknown argument: " + str);
			}
		}
	}

	void EnterMessageLoopDedicatedServers()
	{
		LogMsg("[ENG] Server is running");

		boost::system::error_code error;

		for (;;)
		{
			timBeginFrame();
			OnTick_DS();
			UpdateNetworkSystem();
			std::this_thread::sleep_for( std::chrono::milliseconds(10) );
			timEndFrame();
		}
	}

	int StartDedicatedServer(int argc, char** argv)
	{
		Core::GetEngineLogger()->SetExternalOutput(&ConsoleOutput);
		Core::GetEngineInterface()->SetOutputDevice(&ConsoleOutput);

		try { UseArguments(argc, argv); }
		catch (const omikron_exception& e) { LogMsg(e.str()); PrintHelpScreen(); return EXIT_FAILURE; }
		catch (const std::out_of_range& e) { LogMsg(e.what()); PrintHelpScreen(); return EXIT_FAILURE; }

		GServerRandomSeed = static_cast<u32>( time(nullptr) );

		if (StartAcceptingSockets(GServerPort))
		{
			LogMsg("[NET] Tworze serwer na porcie " + ToString(GServerPort));
		}
		else
		{
			LogMsg("[ERR] [NET] Nie udalo sie stworzyc serwera gry");
			return EXIT_FAILURE;
		}

		// Main Loop
		OnLoad_DS();
			EnterMessageLoopDedicatedServers();
		OnUnload_DS();
		// End Main Loop

		// Juz nie nasluchujemy
		StopAcceptingSockets();
		DisconnectAllClients();

		LogMsg("[NET] Bye");

		return EXIT_SUCCESS;
	}

} // End oo
