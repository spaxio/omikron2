﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkow
#pragma once
#include "oGraph.hpp"

namespace oo
{
	class CGuiManager;

	// Interfejs kontrolki
	class IControl
	{
		protected:
			vect2f Location;		// Pozycja bezwzgledna
			vect2f LocationN;		// Pozycja wzgledna
			vect2f Size;			// Rozmiar
			tstring Label;			// Etykietka

			bool Visible;			// Widzalnosc
			bool Enable;			// Dziala toto?
			bool ReadyToDelete;		// Czy kontrolke mozna usunac

			bool TabStop;			// Czy kontrolke mozna wybrac tabem
			msize TabIndex;			// Indeks taba

			RefFont Font;			// Czcionka

			int	Id;					// Identyfikator, na potrzeby uzytkownika

			bool MultiWindow;		// Okresla czy na warstwie beda okna/kontrolki pokrywajace sie wzajemnie

		// NIE DOTYKAC !!!
		public:
			typedef std::list<IControl*> ChildControls_t;	// Typ wektora kontrolek
			IControl* Parent;								// Rodzic kontrolki
			ChildControls_t ChildControls;					// Kontrolki w kontenerku

			bool BackgroundWindow;	// Okresla czy okno/kontrolka ma pozostawac na 'dnie' wektora

		public:
			// !! UWAGA !!
			// Przekazujac wskaznik poprzez metode AddControl, przekazujemy klasie IContainer
			// kontrole nad usunieciem danych, znajdujacych sie pod tym wskaznikiem. Wtedy nie
			// wolno ich usuwac samodzielnie!
			// Aha ta metoda dodaje kontrolke :PP
			void AddControl(IControl* control);

		public:
			// Oznacza kontrolke jako 'gotowa do usuniecia'
			void Remove();

			// Ustawia kontroke na szczycie wektora
			void BringToTop();

		public:
			// Domyslny konstruktor
			IControl() : Location(0.0f, 0.0f), Size(0.0f, 0.0f), Visible(true), Enable(true), ReadyToDelete(false),
						 TabStop(false), TabIndex(0), Id(0), MultiWindow(false), Parent(nullptr), BackgroundWindow(false) { }

			// Wirtualny destruktor kontrolki
			virtual ~IControl();

			// Tekst dla kontrolki
			void SetLabel(const tstring& label) { Label = label; }
			tstring GetLabel() { return Label; }

			// Pozycja wygledna
			void SetLocationN(const vect2f& locationn) { LocationN = locationn; }
			vect2f GetLocationN() { return LocationN; }

			// Pozycja
			// Tutaj drobna uwaga, gdy uzyjesz tej funkcji przed AddControl(ta_kontrolka)
			// SetLocation bedzie ustawialo pozycje wzgledna
			// Po AddControl(ta_kontrolka) bedzie ustawiac pozycje bezwzgledna
			void SetLocation(const vect2f& location) { Location = location; }
			vect2f GetLocation() { return Location; }

			// Rozmiar
			void SetSize(const vect2f& size) { Size = size; }
			vect2f GetSize() { return Size; }

			// Czy kontrolke mozna wybrac tabem
			void SetTabStop(bool tabstop) { TabStop = tabstop; }
			bool GetTabStop() { return TabStop; }

			// Numerek w kolejce do wybrania tabem
			void SetTabIndex(msize tabindex) { TabIndex = tabindex; }
			msize GetTabIndex() { return TabIndex; }

			// Region kontrolki
			rectf GetRegion() { return rectf(Location, Size.x, Size.y); }
			void SetRegion(const rectf& region) { Location = region.LeftTop(); Size = region.GetSize(); }

			void UpdateRectangles();

			// Ustawia czcionke
			void SetFont(const RefFont& font) { Font = font; }

			// Ustawia widzalnosc kontrolki
			virtual void SetVisible(bool visible) { Visible = visible; }
			bool GetVisible() { return Visible; }

			// Aktywuje kontrolke
			void SetEnable(bool enable) { Enable = enable; }
			bool GetEnable() { return Enable; }

			// Ustawia identyfikator
			void SetId(int id) { Id = id; }
			int GetId() { return Id; }

			// Czy kursor jest nad kontrolka
			bool GetHover();
			// Czy kontrolka ma skupienie
			bool GetFocus();
			// Sprawdza czy kontrolka ma wylacznosc na myszke
			bool GetMouseCapture();

		protected:
			// Sprawdzamy jaki element zostal klikniety (i ew. jakos na niego reagujemy)
			IControl* HitTest(const vect2f& pos, u32 flags);

			// Sprawdzamy na jaki element najechala myszka.
			IControl* CheckHover(const vect2f& pos);

			// Sygnal myszki od systemu operacyjnego
			virtual void SignalMouse(const vect2f& pos, u32 flags) =0;
			// Wprowadzanie tekstu
			virtual void SignalChar(char32_t c) {}
			// Klawisze systemowe
			virtual void SignalKeys(int key, u32 flags) {}

		public:
			// Wyswietla cialo
			virtual void OnRenderBody(CVectorLayer& vl);
			// Wyswietla tekst
			virtual void OnRenderText(CFontManager& fm) = 0;

			// Odswiezanie
			virtual void OnUpdate();

			virtual void OnCreate() {}

		public:
			typedef std::function<void(IControl*)> Event_t;
			typedef std::function<void(IControl*, int)> ChangeEvent_t;

		public:
			Event_t OnEnter;			// Kursor wjechal na obszar kontrolki
			Event_t OnLeave;			// Kursor wyjechal poza obszar kontrolki
			Event_t OnFocusGot;			// Kontrolka zdobyla fokus
			Event_t OnFocusLost;		// Kontrolka stracila fokus

		public:
			friend class CGuiManager;

	};

	class CPlane : public IControl
	{
		public:
			CPlane() { MultiWindow = true; }
			~CPlane() {}

		public:
			void SignalMouse(const vect2f& pos, u32 flags) {}
			void SignalChar(char32_t c) { IControl::SignalChar(c); }
			void SignalKeys(int key, u32 flags) {}

			void OnRenderBody(CVectorLayer& vl) { IControl::OnRenderBody(vl); }
			void OnRenderText(CFontManager& fm) { }

			void OnUpdate() { IControl::OnUpdate(); }

	};

	class CButton : public IControl
	{
		public:
			Event_t OnClick;

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c) {}
			void SignalKeys(int key, u32 flags) {}

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);
		};

	class CCheckBox : public IControl
	{
		protected:
			rectf RectText;
			rectf RectCheck;

			SPoints4 Crossp1;		//
			SPoints4 Crossp2;		//

			bool MouseButtonDown;	// Przycisk myszki jest wcisnietny
			bool Check;				// Zaznaczony
			float CheckRectSize;	// Rozmiar tego kwadracika

		public:
			CCheckBox();
			~CCheckBox();

		public:
			ChangeEvent_t OnCheckedChanged;

		public:
			void SetCheck(bool check) { Check = check; }
			bool GetCheck() { return Check; }

			void SetCheckRectSize(float checkrectsize) { CheckRectSize = checkrectsize; }
			float GetCheckRectSize() { return CheckRectSize; }

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c) {}
			void SignalKeys(int key, u32 flags) {}

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);

			void OnUpdate();

	};

	class CTrackBar : public IControl
	{
		protected:
			rectf RectInside;		// Wewnetrzny prostokat
			rectf RectMini;			// Suwak

			vect2f MouseOffset;		// Przesuniecie kursora
			float Point;			// Offset suwaka

			int Bsize;				//

			int Min;				// Minimalna wartosc
			int Max;				// Maksymalna wartosc
			int Value;				// Aktualna wartosc

		public:
			// Ustawia minimalna i miaksymalna wartosc
			void SetMinMax(int min, int max) { if (min<max) { Min = min; Max = max; } }
			// Ustawia aktualna wartosc
			void SetValue(int value);
			// Zwraca aktualna wartosc
			int GetValue() { return Value; }

			CTrackBar();

		public:
			ChangeEvent_t OnValueChange;

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c) {}
			void SignalKeys(int key, u32 flags);

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm) { }

			void OnUpdate();

	};

	class CLabel : public IControl
	{
		private:
			bool Fill;				// Czy etykieta ma tlo
			colorf BodyColor;		// Kolor tla
			colorf TextColor;		// Domyslny kolor etykiety
			u32 Style;				// Styl
			bool MultiColor;		// Czy etykieta moze miec wiele barw

		public:
			enum { H_LEFT		= FM_H_LEFT };		// Tekst do lewej
			enum { H_CENTER		= FM_H_CENTER };	// Tekst do srodka
			enum { H_RIGHT		= FM_H_RIGHT };		// Tekst do prawej
			enum { V_TOP		= FM_V_TOP };		// Tekst od gory do dolu
			enum { V_CENTER		= FM_V_CENTER };	// Tekst na srodek
			enum { V_BOTTOM		= FM_V_BOTTOM };	// Tekst od dolu do gory

		public:
			void SetFill(bool fill) { Fill = fill; }
			void SetBodyColor(const colorf& bodycolor) { BodyColor = bodycolor; }
			void SetTextColor(const colorf& textcolor) { TextColor = textcolor; }
			void SetStyle(u32 style) { Style = style; }
			void SetMultiColor(bool multicolor) { MultiColor = multicolor; }

		public:
			CLabel();
			~CLabel();

		public:
			void SignalMouse(const vect2f& pos, u32 flags) {}
			void SignalChar(char32_t c) {}
			void SignalKeys(int key, u32 flags) {}

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);

			void OnUpdate() {}

	};

	class CMemo : public IControl
	{
		private:
			colorf BodyColor;		// Kolor tla
			colorf BorderColor;		// Kolor ramki
			colorf TextColor;		// Kolor tekstu
			colorf ScrollColor;		// Kolor scrollbara

			rectf SubRect;			// Rect na ktorym wyswietlamy tekst
			float SizeX;			// Pomocnicze zmienna do SubRect
			float SizeY;			// Pomocnicze zmienna do SubRect

			float Offset;			// Offset scrolla
			float Hf;				// Wysokosc linii

			boost::circular_buffer<tstring> Lines;

		public:
			void SetBodyColor(const colorf& bodycolor) { BodyColor = bodycolor; }

		public:
			CMemo(msize max_lines);

		public:
			void SetRegion(const rectf& region);
			void SetSize(const vect2f& size);
			void SetFont(const RefFont& font);

		public:
			void Write(const tstring& message)
			{
				if (Lines.empty()) Lines.push_back(message);
				else Lines.back() += message;
			}

			void Writeln(const tstring& message)
			{
				Lines.push_back(message);
			}

		private:
			void UpdateSubRect();
			float CalcScrollSize();

			float OffsetToPos(float offset, float size)
			{
				assert(InRange(offset, 0.0f, 1.0f));

				if (Lines.empty()) return 0.0f;
				else return (SizeY - size) * offset;
			}

			float PosToOffset(float pos)
			{
				float c = pos - Location.y;
				return c / SizeY;
			}

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c) {}
			void SignalKeys(int key, u32 flags);

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);

			void OnUpdate();
	};

	const colorf LB_Border			= MakeColorf(0x002545ff);
	const colorf LB_Background		= MakeColorf(0xb2bfcdff);
	const colorf LB_Font			= MakeColorf(0x000000FF);
	const colorf LB_SelectColor		= MakeColorf(0x19d3ff44);
	const colorf LB_SelectBorder	= MakeColorf(0x1966ffff);

	class CListBox : public IControl
	{
		protected:
			std::vector<tstring> Items;	// Pozycje na liscie

			bool FitToItems;			//
			float ItemHeight;			// Wysokosc jednej pozycji listy

			int DisplaySelectedItem;	//
			int SelectedItem;			//
			int LastSelectedItem;		//

			bool TrackMouse;			//

			rectf PrivateRect;
			rectf ControlRect;
			rectf SelectedRect;

			bool Clip;					// Przycinanie do regionu kontrolki
			float ScrollOffset;			// Offset przesuniecia (od 0 do 1)

			rectf MoveRectByScrollOffset(const rectf& r);

		protected:
			void TrackItem();

		public:
			void SetItemHeight(float itemheight) { ItemHeight = itemheight; }
			void SetTrackMouse(bool trackmouse) { TrackMouse = trackmouse; }
			void SetFitToItems(bool fittoitems) { FitToItems = fittoitems; }

			float GetScrollOffset() { return ScrollOffset; }
			void SetScrollOffset(float offset) { ScrollOffset = offset; }

			void SetClip(bool clip) { Clip = clip; }

		public:
			void SetItems(const std::vector<tstring>& items);
			void AddItem(const tstring& item) { Items.push_back(item); }

			void Clear() { SelectedItem = -1; Items.clear(); ScrollOffset = 0.0f; }
			msize GetItemsCount() { return Items.size(); }
			msize GetSelectedIndex() { return SelectedItem; }
			tstring GetSelectedItem() { return (SelectedItem >= 0) ? Items[SelectedItem] : Label; }

			void SelectItemByIndex(int index);

		public:
			ChangeEvent_t OnItemSelectedChanged;
			Event_t OnClick;

		public:
			CListBox();

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c) {}
			void SignalKeys(int key, u32 flags);

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);

			void OnUpdate();

	};

	class CTextBox : public IControl
	{
		public:
			enum ETextBoxStyle { TBS_Normal, TBS_ReadOnly, TBS_NoSelect };

		private:
			std::u32string Text;	// Tekst

		protected:
			float CursorBegin;	// W tym miejscu zaczyna sie zaznaczenie (moze byc wieksze od CursorEnd!)
			float CursorEnd;	// W tym miejscu sie konczy zaznaczenie

			int CharBegin;		// Zaznaczenie zaczyna sie od tego zaku (moze byc wieksze od CharEnd!)
			int CharEnd;		// A konczy na tym

			float TextWidth;	// Szerokosc tekstu

			bool UpDown;		// Pomocnicza zmienna
			bool DrawBody;		// Czy rysowac tlo kontrolki

			msize MaxLength;	// Maksymalna liczba znakow (domyslnie 512)

			float Cursor;		// Widocznosc kursora
			float Offset;		// Przesuniecie calego tekstu

			ETextBoxStyle TextBoxStyle; // Zachowanie

		private:
			void CursorChange();
			void UpdateCursor(const vect2f& pos, bool start);
			void CheckTextWidth();

			void GetSelectToPosLen(int& pos, int& len);

			void ChangeSelect(int begin, int end);
			void CopySelect(tstring& out_string);

			void EraseSelect();
			void InsertText(const tstring& str);

		public:
			// Czy rysowac tlo kontrolki
			void SetDrawBody(bool drawbody) { DrawBody = drawbody; }
			// Maxymalna liczba znakow
			void SetMaxLength(msize maxlength) { MaxLength = maxlength; Text = Text.substr(0, maxlength); CheckTextWidth(); }
			// Ustawia tekst
			void SetText(const tstring& text) { Text = Decode(text).substr(0, MaxLength); CheckTextWidth(); ChangeSelect(Text.size(), Text.size()); }
			// Pobiera tekst
			tstring GetText() { return Encode(Text); }
			// Usuwa tekst
			void ClearText() { Text.clear(); CheckTextWidth(); ChangeSelect(0, 0);  }

			void SetFont(const RefFont& font) { IControl::SetFont(font); ChangeSelect(Text.size(), Text.size()); }
			// Ustawia styl [normalny, tylko do odczytu, bez zaznaczania] dla textboxa
			void SetStyle(ETextBoxStyle textboxstyle) { TextBoxStyle = textboxstyle; }
			// Pobiera styl textboxa
			ETextBoxStyle GetTextBoxStyle() { return TextBoxStyle; }

			CTextBox();
			~CTextBox();

		public:
			ChangeEvent_t OnTextChange;
			Event_t OnClick;

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c);
			void SignalKeys(int key, u32 flags);

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);

			void OnUpdate();

	};

	class CComboBox : public IControl
	{
		public:
			enum EComboBoxStyle
			{
				CBSx_NORMAL = 0,
				CBSx_DROPDOWNLIST = 1,
				CBSx_SHOWONLY = 2
			};

		protected:
			std::vector<tstring> Items;	// Wszystkie pozycje
			int SelectedItem;			// Zaznaczona pozycja

			EComboBoxStyle Style;		// Styl comboboxa

			rectf RectInside;			// Wewnetrzny prostokat
			bool MouseButtonDown;		// Przycisk myszki jest wcisnietny

			bool ShowList;				//
			float ItemHeight;			// Wysokosc pojedynczego itemu w listboxie

			bool BlockFocus;

			rectf ControlRect;
			rectf ButtonRect;
			rectf TextRect;
			rectf ListRect;

			vect2f Triangle[3];			// Trojkacik ;p

			CTextBox* TextBox;
			CListBox* ListBox;

			int LastIndex;
			bool SignalOnSameValue;		// Czy ma wysylac sygnal OnItemSelectedChanged w przypadku gdy wartosc sie nie zmienila (domyslnie true)

		protected:
			void ListBoxFocusLost(IControl* control);
			void ListBoxItemSelectedChanged(IControl* control, int val);

		public:
			void SetItemHeight(float itemheight) { ItemHeight = itemheight; }
			float GetItemHeight() { return ItemHeight; }

		protected:
			void RefreshRects();
			virtual void ShowListBox();
			void CheckContent(IControl* control);

		public:
			void SetRegion(const rectf& region);
			void SetSize(const vect2f& size);
			void SetFont(const RefFont& font);

			void SetItems(const std::vector<tstring>& items);
			void AddItem(const tstring& item) { Items.push_back(item); }

			void Clear()
			{
				SelectedItem = -1;
				Items.clear();
				if (TextBox) TextBox->ClearText();
			}

			int GetItemsCount() { return Items.size(); }
			int GetSelectedIndex() { return SelectedItem; }
			tstring GetSelectedItem() { return (SelectedItem >= 0) ? Items[SelectedItem] : tstring(); }
			tstring GetItemByIndex(int index);

			int Find(const tstring& item, bool cs = true);
			void TrySelect(const tstring& item, bool cs = true);
			void SelectItemByIndex(int index);

			void SetStyle(EComboBoxStyle style);

			tstring GetTextContent();
			void SetTextContent(const tstring& text);
			void FocusTexContent();

			void SetSignalOnSameValue(bool s) { SignalOnSameValue = s; }
			bool GetSignalOnSameValue() { return SignalOnSameValue; }

		public:
			ChangeEvent_t OnItemSelectedChanged;
			Event_t OnDropDown;
			Event_t OnCheckTextContent;

		public:
			CComboBox();
			~CComboBox();

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c) {}
			void SignalKeys(int key, u32 flags) {}

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);

			void OnUpdate();
			void OnCreate();
	};

	class CWindow : public IControl
	{
		protected:
			rectf RectLabel;	// Belka tytulowa
			rectf RectCenter;	// Okno
			rectf RectBody;		// Cialo

			bool IsMoving;		// Czy okno jest przemieszczane
			vect2f MouseOffset; // Pomocnicza zmienna

			vect2f MinimumSize;	// Minimalny rozmiar okna
			bool Resize;		// Czy mozna zmieniac rozmiar
			bool NoMove;		// Czy okno ma byc nieprzesowalne

			vect2f BoundMouseOffset;	// Pomocnicza zmienna

			float LabelHeight;		// Wysokosc etykietki okna

		private:
			enum RectBound
			{
				RB_TOP = 0,		//	|
				RB_BOTTOM,		//	|
				RB_LEFT,		//	-
				RB_RIGHT,		//	-
				RB_LT1,			//	\.
				RB_LT2,			//	\.
				RB_RT1,			//	/
				RB_RT2,			//	/
				RB_LB1,			//	/
				RB_LB2,			//	/
				RB_RB1,			//	\.
				RB_RB2,			//	\.
				RB_RBM			//	\.
			};

			bool MouseButtonDownX;	// Przycisk myszki jest wcisnietny

			int HoverBound;
			enum { RECTBOUND_COUNT = RB_RBM + 1 };
			enum { BOUND_NONE = -1 };
			rectf RectBounds[RECTBOUND_COUNT];

			rectf CrossRect;

		private:
			void UpdateRects();

			// W sytuacji gdy kursor wychodzi poza okno
			void ResetMove();

		protected:
			bool IsBoundMove() { return IsMoving && HoverBound != BOUND_NONE; }

		public:
			CWindow();
			~CWindow();

		public:
			Event_t OnX;
			Event_t OnClose;

		public:
			void SetMinimumSize(const vect2f& minimumsize) { MinimumSize = minimumsize; }
			vect2f GetMinimumSize() { return MinimumSize; }

			void SetResize(bool resize) { Resize = resize; }
			bool GetResize() { return Resize; }

			void SetNoMove(bool nomove) { NoMove = nomove; }
			bool GetNoMove() { return NoMove; }

			void SetLabelHeight(float labelheight) { LabelHeight = labelheight; }

			// Jesli chcemy cos narysowac na oknie to uzywamy tej metody
			virtual void OverDrawWindow(CVectorLayer& vl) {}

		public:
			void SignalMouse(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c);
			void SignalKeys(int key, u32 flags);

			void OnRenderBody(CVectorLayer& vl);
			void OnRenderText(CFontManager& fm);
			void OnUpdate();

	};

	// Flagi do GUI managerka przy odbieraniu sygnalow systemowych
	enum { GM_UP = 1 };
	enum { GM_DOWN = 2 };

	enum { GM_CTRL = 4 };
	enum { GM_ALT =	 8 };
	enum { GM_SHIFT = 16 };

	enum { GM_LEFT = 32	};
	enum { GM_MIDDLE = 64 };
	enum { GM_RIGHT = 128 };

	enum { GM_DBCLICK = 256 };

	inline u32 GetFlagsForSfKey(const sf::Event::KeyEvent& ke)
	{
		u32 flags = 0;
		if (ke.shift) flags |= GM_SHIFT;
		if (ke.alt) flags |= GM_ALT;
		if (ke.control) flags |= GM_CTRL;
		return flags;
	}

	class CGuiManager
	{
		// NIE DOTYKAC !!!
		public:
			CPlane		MainPlane;		// Drzewo kontrolek
			IControl*	FocusControl;	// Aktywna kontrolka
			IControl*	MouseCapture;	// Kontrolka ktora przejmuje wszystkie komunikaty od myszki
			IControl*   HoverControl;	// Kontrolka nad ktora znajduje sie myszka

			IControl*   FocusRequest;	// Kontrolka ktora rzada Focusa w czasie wykonywania HitTest/SignalMouse

		public:
			CGuiManager();
			~CGuiManager();

			// Pozwala okreslic czy uzytkownik pracuje na jakims oknie i zapobiegac ew. strzelaniu w grze przy klikaniu na okienkach ;p
			bool CanGameInput()
			{
				if (FocusControl == nullptr || FocusControl == &MainPlane) return true;
				return FocusControl->BackgroundWindow;
			}

			// Ustawia domyslny Focus i MouseCapture
			void ResetFocus();

			// Wymusza obsluzenie focus requesta
			void CheckFocusRequest();

			void Update();
			void Render();

			// Dodaje nowe okno do systemu GUI (dziala tylko dla okien!)
			void AddControl(IControl* control) { MainPlane.AddControl(control); }

			void CheckHover(const vect2f& pos);

			void HitTest(const vect2f& pos, u32 flags);
			void SignalChar(char32_t c);
			void SignalKeys(int key, u32 flags);
	};


} // End oo
