﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkowe
#pragma once
#include "../oCore/oCore.hpp"

namespace oo
{

	// Niskopoziomowa klasa obrazka
	class CImage
	{
		private: // Obiekty CImage nie sa do kopiowania
			CImage( const CImage& );
			CImage& operator=( const CImage& );

		public:
			u32 Width;    // Szerokosc
			u32 Height;   // Wysokosc
			u32 Size;     // Rozmiar
			u32 Bpp;      // Bity na pixel
			u8* Data;    // Wskaznik na dane bitmapy

			void Reset(u32 w, u32 h, u32 b);
			void Delete() { free(Data); Data = nullptr; }

			CImage() : Data(nullptr) {}
			CImage(u32 w, u32 h, u32 b) : Data(nullptr) { Reset(w,h,b); }
			~CImage() { free(Data); }

		public:
			CImage( CImage&& other ) :
				Width(other.Width), Height(other.Height), Size(other.Size), Bpp(other.Bpp), Data(other.Data)
			{
				other.Data = nullptr;
			}

			CImage& operator=( CImage&& other )
			{
				if (this != &other)
				{
					free(Data);

					Width = other.Width;
					Height = other.Height;
					Size = other.Size;
					Bpp = other.Bpp;
					Data = other.Data;

					other.Data = nullptr;
				}

				return *this;
			}
	};

	// Metody do odczytywania i zapisywania tga (a po co wiecej?)
	bool LoadTgaFile(const tstring& filename, CImage& image);
	bool SaveTgaFile(const tstring& filename, CImage& image);

	// Wczytuje plik png
	bool LoadPngFile(const tstring& filename, CImage& image);

	// Wczytuje plik jpg
	bool LoadJpegFile(const tstring& filename, CImage& image);

	// Wczytuje na podstawie rozszerzenia
	bool LoadImageFile(const tstring& filename, CImage& image, const tstring& mime_type = N_STRING);

	// Wczytuje obrazek przeznaczony pod teksture
	bool LoadImageTexture(const tstring& filename, CImage& image, float& u, float& v, int& owidth, int& oheight);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////    ATLAS    ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	struct AtlasTextureElement
	{
		tstring Name;
		recti AtlasRect;
		int OriginalWidth;
		int OriginalHeight;
		recti Part;
	};

    // Generuje atlas w locie (folder - jest to sciezka w VFS)
    void GenerateAtlas(const tstring& folder, CImage& out, std::vector<AtlasTextureElement>& coords_out, int size = 1024, int diff = 4);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////    HSV    ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	vect3f RGB2HSV(const colorf& rgb );
	colorf HSV2RGB(const vect3f& hsv );

} // End oo
