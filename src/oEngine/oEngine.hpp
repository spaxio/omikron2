﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkow
#pragma once
#include "oFramework.hpp"
#include "oServer.hpp"
#include "oGraph.hpp"
#include "oGui.hpp"
#include "oFont.hpp"
#include "oDownload.hpp"

namespace oo
{
	const int GAME_BUILD = 1900;
	tstring GetEngineVersion();
} // End oo
