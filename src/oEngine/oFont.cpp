﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oFont.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H

namespace oo
{
	const SScope DefaultCharset = Ascii_Scope;
	ScopeVector_t ScopeVector(1, DefaultCharset);

	void SetActiveCharset(const ScopeVector_t& charset)
	{
		ScopeVector = charset;
	}

	// Kopiuje mniejszy obrazek (bitmap) do wiekszego obrazka (image)
	void CopyBitmapToImage(CImage& bitmap, u32 x, u32 y, CImage& image)
	{
		u32 x_max = x + bitmap.Width;
		u32 y_max = y + bitmap.Height;

		for (u32 j = y, q = 0; j < y_max; j++, q++ ) 
		{
			for (u32 i = x, p = 0; i < x_max; i++, p++ ) 
			{
				if ( i >= image.Width || j >= image.Height ) continue;
				image.Data[j*image.Width + i] |= bitmap.Data[q * bitmap.Width + p];
			}
		}
	}

	struct SGlyph
	{
		u32 Index;			// Index
		CImage Image;		// Wskaznik na obrazek
		SMetric Metric;		// Miary
		int Height;			// Wysokosc

		SGlyph(int index = 0) : Index(index), Height(0)
		{
			memset(&Metric, 0, sizeof(Metric));
		}

		SGlyph(SGlyph&& other) :
			Index(other.Index),
			Image(std::move(other.Image)),
			Metric(other.Metric),
			Height(other.Height)
		{
		}

		SGlyph& operator=( SGlyph&& other )
		{
			if (this != &other)
			{
				Index = other.Index;
				Image = std::move(other.Image);
				Metric = other.Metric;
				Height = other.Height;
			}

			return *this;
		}

#ifdef O2_GCC
        SGlyph( const SGlyph& ) = delete;
        SGlyph& operator=( const SGlyph& ) = delete;
#else
        private:
            SGlyph( const SGlyph& );
            SGlyph& operator=( const SGlyph& );
#endif
	};

	bool MakeFont(const tstring& fontfilename, int size, CImage& image, MetricVector_t& metrics, ScopeVector_t& scopes, int& lineheight)
	{
		std::vector<SGlyph> glyph_v; // Wektor wyrenderowanych znakow

		FT_Library library;		// FreeType2 stuff
		FT_Face face;			// FreeType2 stuff

		FT_Error error;	// Zmienna na blad FT2

		// Odpalamy FreeType
		error = FT_Init_FreeType(&library);
		if (error)
		{
			LogMsg( "[ERR] [FNT] Nie mozna zainicjalizowac biblioteki FreeType2" );
			return false;
		}

		// Otwieramy plik
		RefFile file = vfsGetFile(fontfilename);
		if (!file->IsOK())
		{
			LogMsg( "[ERR] [FNT] Nie mozna otworzyc pliku : " + fontfilename );
			FT_Done_FreeType(library);
			return false;
		}

		// Wczytujemy dane do bufora
		std::vector<u8> Buffer(file->GetSize());
		file->Read(&Buffer[0], Buffer.size());

		// Wczytujemy fonta
		error = FT_New_Memory_Face(library, &Buffer[0], Buffer.size(), 0, &face);
		if (error)
		{
			LogMsg( "[ERR] [FNT] Nie mozna wczytac czcionki : " + fontfilename );
			FT_Done_FreeType(library);
			return false;
		}

		// Ustawiamy rozmiary
		FT_Set_Char_Size(face, 0, size*64, 0, 0);
		FT_Set_Pixel_Sizes(face, 0, size);

		u32 gindex =0;			// Index glifa
		u32 lastoffset =0;		// Index glifa w poprzedniej interacji
		u32 numpixels =0;		// Liczba pixeli

		// Czyscimy wektor zakresow
		scopes.clear();

		for (msize charset = 0; charset < ScopeVector.size(); charset++)
		{
			for (u32 c = ScopeVector[charset].Begin; c < (ScopeVector[charset].End+1); c++, gindex++)
			{
				FT_UInt glyph_index = FT_Get_Char_Index(face, (FT_ULong)c);
				if (glyph_index == 0) { glyph_v.push_back(SGlyph(gindex)); continue; }

				error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);
				if (error) { glyph_v.push_back(SGlyph(gindex)); continue; }

				FT_GlyphSlot glyph = face->glyph;

				error = FT_Render_Glyph(glyph, FT_RENDER_MODE_NORMAL);
				if (error) { glyph_v.push_back(SGlyph(gindex)); continue; }

				CImage work_image(glyph->metrics.width/64, glyph->metrics.height/64, 8);
				if (work_image.Size == 0)
				{
					work_image.Reset(1, 1, 8);
				}
				else
				{
					memcpy(work_image.Data, glyph->bitmap.buffer, work_image.Size);
				}

				// Rozmiar glifu + odstep jednopixelowy z prawej i z dolu
				numpixels += work_image.Size + work_image.Width + work_image.Height;

				// Wypelniamy w polowie strukture glifa
				SGlyph g;
				g.Index = gindex;
				g.Height = work_image.Height;
				g.Image = std::move(work_image);	// Od tego momentu work_image jest nieprawidlowy!

				// Przepisujemy wartosci i przeliczamy na bardziej uzyteczne
				g.Metric.HoriAdvance = glyph->metrics.horiAdvance/64;
				g.Metric.HoriBearingX = glyph->metrics.horiBearingX/64;
				g.Metric.HoriBearingY =  (glyph->metrics.height - glyph->metrics.horiBearingY)/64;

				// I dodajemy do listy
				glyph_v.push_back(std::move(g));
			}

			// Tworzymy i dodajemy zakres do generowanej czcionki
			SScope scope = { ScopeVector[charset].Begin, ScopeVector[charset].End, lastoffset };
			scopes.push_back( scope );
			lastoffset = gindex;
		}

		if (gindex==0)
		{
			LogMsg( "[ERR] [FNT] Nie udalo sie wyrenderowac znakow" );
			FT_Done_FreeType(library);
			return false;
		}

		// Sortujemu od najwyzszego do najnizszego glifa
		std::sort(glyph_v.begin(), glyph_v.end(), [](const SGlyph& l, const SGlyph& r) {
				return (l.Height) > (r.Height);
			}
		);

		// Wysokosc wiersza
//		lineheight = glyph_v[0].Metric.HoriBearingY + glyph_v[0].Image->Height;
		lineheight =  face->height * face->size->metrics.y_ppem / face->units_per_EM;

		// Dodajemy do liczby pixeli 20%
		numpixels += int((float)numpixels*0.2f);

		// Okreslamy rozmiar duzej bitmapy
		u32 isx = 64;
		u32 isy = 64;
		u32	mulxy = isx*isy;
		bool t = true;

		while (mulxy<numpixels)
		{
			if (t) isx*=2;
			else isy*=2;

			t = !t;
			mulxy = isx*isy;
		}

		// Tworzymy obrazek na znaki
		image.Reset(isx, isy, 8);

		u32 sizex =0;	// Szerokosc gilfa
		u32 sizey =0;	// Wysokosc gilfa
		u32 x =0;		// Pozycja x
		u32 y =0;		// Pozycja y
		u32 bigly=0;	// Najwieksza wysokosc glifu w aktualnym wierszu

		for (msize i=0; i<glyph_v.size(); i++)
		{
			if (glyph_v[i].Height == 0) continue;

			sizex = glyph_v[i].Image.Width;
			sizey = glyph_v[i].Image.Height;
			bigly = std::max<u32>(bigly, sizey);

			// Warunek przejscia do nastepnego wiersza
			if ((x+sizex+1) > image.Width)
			{
				x = 0;
				y += bigly+1;
				bigly = sizey;
			}

			// Rysujemy glif na glownym obrazku
			CopyBitmapToImage(glyph_v[i].Image, x, y, image);

			// Dopelniamy stkture gilfa
			glyph_v[i].Metric.LeftCoord = x;
			glyph_v[i].Metric.TopCoord = y;
			glyph_v[i].Metric.RightCoord = x+sizex;
			glyph_v[i].Metric.BottomCoord = y+sizey;

			if((y + bigly+1) > image.Height) break;
			x += sizex+1;
		}

		// Sortujemu od najwyzszego do najnizszego glifa
		std::sort(glyph_v.begin(), glyph_v.end(), [](const SGlyph& l, const SGlyph& r) {
				return (l.Index) < (r.Index);
			}
		);

		// Czyscimy wektory
		metrics.clear();

		// Wypelniamy wektor miar
		for (const auto& glyph : glyph_v) 
		{
			metrics.push_back(glyph.Metric);
		}

		FT_Done_FreeType(library);
		return true;
	}

} // End oo
