﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oImage.hpp"

#include <png.h>
#include <jpeglib.h>

namespace oo
{

	void CImage::Reset(u32 w, u32 h, u32 b)
	{
		free(Data);

		Width = w;
		Height = h;
		Bpp = b;

		Size = w*h*(b/8);
		Data = (u8*)calloc(1, Size);
	}

	#pragma pack(1)
	struct TGAHEADER
	{
		u8  IdentSize;
		u8  ColourMapType;
		u8  ImageType;

		unsigned short ColourmapStart;
		unsigned short ColourmapLength;
		u8  ColourmapBits;

		unsigned short Xstart;
		unsigned short Ystart;
		unsigned short Width;
		unsigned short Height;
		u8  Bits;
		u8  Descriptor;
	};
	#pragma pack()

	void BGRtoRGB(CImage& image)
	{
		if (image.Data==nullptr) return;
		if ((image.Bpp != 32) && (image.Bpp != 24)) return;

		// Startowy wskaznik na dane bitmapy
		u8* cur    = image.Data;

		// Obliczamy liczbe pixeli
		u32 pixels = image.Width*image.Height;

		// Rozmiar pixela w bajtach
		u32 pixelsize = image.Bpp/8;

		u8   temp;			// Tymczasowy bajtek :P

		while (pixels--)
		{
			temp=*cur;
			*cur=*(cur+2);
			*(cur+2)=temp;

			cur += pixelsize;
		}
	}

	void FlipImage(CImage& image)
	{
		if (image.Data==nullptr) return;

		// Dlugosc lini
		u32 LineLen= image.Width*(image.Bpp/8);

		// Wskaznik na pierwsza i druga linie
		u8* Line1= image.Data;
		u8* Line2= &image.Data[LineLen * (image.Height - 1)];

		u8 Temp;        // Tymczasowy bajtek ;P

		for( ; Line1 < Line2; Line2-=(LineLen*2))
		{
			for( u32 Index=0; Index != LineLen; Line1++, Line2++, Index++)
			{
			 Temp=*Line1;
			 *Line1=*Line2;
			 *Line2=Temp;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////////////////////////// JPEG //////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	bool LoadJpegFile(const tstring& filename, CImage& image)
	{
		struct jpeg_decompress_struct cinfo;
		struct jpeg_error_mgr jerr;

		cinfo.err = jpeg_std_error( &jerr );
		jpeg_create_decompress( &cinfo );

		RefFile file = vfsGetFile(filename);
		if(!file->IsOK())
		{
			jpeg_destroy_decompress( &cinfo );
			LogMsg( "[ERR] [IMG] Nie mozna otworzyc pliku JPEG : " + filename );
			return false;
		}

		msize s = file->GetSize();
		CBuffer buff(s);
		if (file->Read(buff.GetPtr(), s) != s)
		{
			jpeg_destroy_decompress( &cinfo );
			LogMsg( "[ERR] [IMG] Niespodziewany koniec pliku : " + filename );
			return false;
		}

		u8 magic[] = { 0xFF, 0xD8 };

		if (memcmp(buff.GetPtr(), magic, 2) != 0)
		{
			LogMsg("[ERR] [IMG] Plik jpg jest uszkodzony : " + filename);
			return false;
		}

		jpeg_mem_src( &cinfo, (unsigned char*)buff.GetPtr(), buff.GetSize() );
		jpeg_read_header( &cinfo, TRUE );
		jpeg_start_decompress( &cinfo );

		if ( (cinfo.output_components != 3) &&
			 (cinfo.output_components != 1) )
		{
			jpeg_finish_decompress( &cinfo );
			jpeg_destroy_decompress( &cinfo );

			LogMsg( "[ERR] [IMG] Wspieramy tylko RGB i grayscale : " + filename );
			return false;
		}

		image.Reset(cinfo.output_width, cinfo.output_height, cinfo.output_components * 8);
		u8* ptr = image.Data;

		while( cinfo.output_scanline < cinfo.output_height )
		{
			jpeg_read_scanlines( &cinfo, &ptr, 1 );
			ptr += image.Width * cinfo.output_components;
		}

		jpeg_finish_decompress( &cinfo );
		jpeg_destroy_decompress( &cinfo );

		//BGRtoRGB(image);

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// PNG //////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	static void ReadCallback(png_structp png_ptr, png_bytep data, png_size_t length)
	{
		CFile* fp = (CFile*)png_get_io_ptr(png_ptr);
		fp->Read(data, length);
	}

	bool LoadPngFile(const tstring& filename, CImage& image)
	{
		RefFile file = vfsGetFile(filename);
		if(!file->IsOK())
		{
			LogMsg( "[ERR] [IMG] Nie mozna otworzyc pliku PNG : " + filename );
			return false;
		}

		// create libpng handles
		png_structp png_ptr;
		png_infop info_ptr;

		png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr );
		assert(png_ptr);

		info_ptr = png_create_info_struct( png_ptr );
		assert(info_ptr);

		#pragma warning(disable : 4611)
		if (setjmp(png_jmpbuf(png_ptr)))
		{
			png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
			LogMsg( "[ERR] [IMG] Blad wewnetrzny biblioteki libpng : " + filename );
			return false;
		}
		#pragma warning(default : 4611)

		png_set_read_fn(png_ptr, file.get(), &ReadCallback);

		// read header
		png_uint_32 width, height;
		int bit_depth, color_type, interlace_type;

		png_read_info( png_ptr, info_ptr );

		png_get_IHDR( png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, nullptr, nullptr );

		// make some adjustments to support more types of png file
		if (bit_depth == 16)
		{
			png_set_strip_16(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_palette_to_rgb(png_ptr);
		}

		if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		{
			png_set_tRNS_to_alpha(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		{
			png_set_gray_to_rgb(png_ptr);
		}

		if (bit_depth < 8)
		{
			png_set_packing(png_ptr);
		}

		int bpp = 4;
		if (color_type == PNG_COLOR_TYPE_RGB) bpp = 3;

		image.Reset(width, height, (u16)(bpp * 8));

		int number_passes = png_set_interlace_handling(png_ptr);

		for (int pass = 0; pass < number_passes; pass++)
		{
			for (msize y = 0; y < height; y++ )
			{
				u8* p = image.Data + y * width * bpp;
				png_read_rows(png_ptr, (png_bytepp)&p, nullptr, 1);
			}
		}
		png_read_end(png_ptr, info_ptr);
		png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// TGA //////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	bool LoadTgaFile(const tstring& filename, CImage& image)
	{
		RefFile file = vfsGetFile(filename);
		if(!file->IsOK())
		{
			LogMsg( "[ERR] [IMG] Nie mozna otworzyc pliku TGA : " + filename );
			return false;
		}

		TGAHEADER infoheader;
		memset(&infoheader, 0, sizeof(TGAHEADER));
		file->Read(&infoheader,sizeof(TGAHEADER));

		u32 width  = infoheader.Width - infoheader.Xstart;
		u32 height = infoheader.Height - infoheader.Ystart;

		if( (width<1) || (height<1) )
		{
			LogMsg( "[ERR] [IMG] Naglowek TGA wydaje sie byc uszkodzony : " + filename );
			return false;
		}

		// Wspieramy tylko bitmapy grayscale/24/32/24RLE/32RLE
		if(!( ((infoheader.ImageType == 3) || (infoheader.ImageType == 2 )                                ) &&
			  ((infoheader.Bits      == 8) || (infoheader.Bits      == 24) || (infoheader.Bits      == 32)) ))
		{
			LogMsg( "[ERR] [IMG] Funkcja wspiera tylko bitmapy grayscale/24/32/24RLE/32RLE : " + filename );
			return false;
		}

		msize offset = infoheader.IdentSize+sizeof(TGAHEADER);
		file->SetPos(offset, IStream::SP_BEG);

		// Szykujemy obrazek
		image.Reset(width, height, infoheader.Bits);

		if (file->Read(image.Data, image.Size) != image.Size)
		{
			image.Delete();
			LogMsg( "[ERR] [IMG] Niespodziewany koniec pliku : " + filename );
			return false;
		}

		// Konwertujemy z BGR do RGB (jesli obrazek JEST RGB :P)
		if (infoheader.ImageType==2) BGRtoRGB(image);

		// Sprawdzamy czy bitmapa nie jest przypadkiem do gory nogami
		if(!(infoheader.Descriptor & 0x20)) FlipImage(image);

		return true;
	}

	bool SaveTgaFile(const tstring& filename, CImage& image)
	{
		if (image.Data == nullptr) return false;

		CFile file(filename.c_str(), CFile::OM_WRITE);

		if (!file.IsOK())
		{
			LogMsg( "[ERR] [IMG] Nie mozna utworzyc pliku : " + filename );
			return false;
		}

		u8 imagetype;
		u8 descriptor;
		if ( (image.Bpp==32) || (image.Bpp==24) ) { imagetype = 2; descriptor = ((image.Bpp==32) ? 0x28 : 0x20); }
		else if (image.Bpp==8) { imagetype = 3; descriptor = 0x20; }
			 else
			 {
				LogMsg( "[ERR] [IMG] Funkcja do zapisu plikow TGA wspiera tylko bitmapy 8/24/32 : " + filename );
				return false;
			 }

		TGAHEADER header;
		memset(&header, 0, sizeof(header));
		header.ImageType = imagetype;
		header.Width = (unsigned short)image.Width;
		header.Height = (unsigned short)image.Height;
		header.Bits = (u8)image.Bpp;
		header.Descriptor = descriptor;

		file.Write(&header, sizeof(header));

		if (imagetype==2) BGRtoRGB(image);
		file.Write(image.Data, image.Size);
		if (imagetype==2) BGRtoRGB(image);

		return true;
	}

	bool LoadImageFile(const tstring& filename, CImage& image, const tstring& mime_type)
	{
		// Na podstawie rozszerzenia
		if (mime_type.empty())
		{
			tstring ext = GetFileExtension(filename);
			if (ext == ".png") return LoadPngFile(filename, image);
			else if ((ext == ".jpg") ||
					 (ext == ".jpeg")) return LoadJpegFile(filename, image);
			else if (ext == ".tga") return LoadTgaFile(filename, image);
			else return false;
		}
		else // Na podstawie mimetype
		{
			if (mime_type == "image/png") return LoadPngFile(filename, image);
			else if ((mime_type == "image/jpg") ||
					 (mime_type == "image/jpeg")) return LoadJpegFile(filename, image);
			else if ((mime_type == "image/x-tga") ||
					 (mime_type == "image/x-targa")) return LoadTgaFile(filename, image);
			else return false;
		}
	}

	bool LoadImageTexture(const tstring& filename, CImage& image, float& u, float& v, int& owidth, int& oheight)
	{
		CImage in;
		if (!LoadImageFile(filename, in))
		{
			LogMsg( "[ERR] [TEX] Nie mozna wczytac tekstury z pliku " + filename );
			return false;
		}

		u32 isx = 1;
		u32 isy = 1;

		while (in.Width>isx) { isx *= 2; }
		while (in.Height>isy) { isy *= 2; }

		image.Reset(isx, isy, in.Bpp);

		if ((in.Width == isx) && (in.Height == isy))
		{
			memcpy(image.Data, in.Data, in.Size);
			u = 1.0f;
			v = 1.0f;
			owidth = in.Width;
			oheight = in.Height;

			return true;
		}

		int ps = in.Bpp / 8;

		u8* d_ptr = image.Data;
		u8* s_ptr = in.Data;
		size_t line = in.Width * ps;

		assert(isx >= in.Width);
		assert(isy >= in.Height);

		size_t st_line = (isx - (int)in.Width) * ps;

		for (size_t y = 0; y < (size_t)in.Height; y++)
		{
			memcpy(d_ptr, s_ptr, line);

			d_ptr += line;

			// Kopiowanie ostatniej kolumny
			if (image.Width > in.Width)
			{
				for (int i = 0; i < ps; i++)
				{
					d_ptr[i] = s_ptr[i];
				}
			}

			d_ptr += st_line;
			s_ptr += line;
		}

		owidth = in.Width;
		oheight = in.Height;
		u = (float)in.Width / (float)isx;
		v = (float)in.Height / (float)isy;

		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////    HSV    ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	vect3f RGB2HSV(const colorf& rgb)
	{
		float max = std::max(rgb.R, std::max(rgb.G, rgb.B));
		float min = std::min(rgb.R, std::min(rgb.G, rgb.B));
		float delta = max - min;

		vect3f out;

		out.z = max;
		if (IsNotZero(max)) out.y = delta / max;
		else out.y = 0.0;

		if (IsZero(out.y)) out.x = 0.0f;
		else
		{
			if (FloatIs(max, rgb.R)) out.x = (rgb.G - rgb.B) / delta;
			else if (FloatIs(max, rgb.G)) out.x = 2.0f + (rgb.B - rgb.R) / delta;
			else if (FloatIs(max, rgb.B)) out.x = 4.0f + (rgb.R - rgb.G) / delta;
			out.x *= 60.0;
			if (out.x < 0.0f) out.x += 360.0;
		}

		return out;
	}

	colorf HSV2RGB(const vect3f& hsv)
	{
		float hue;
		if (hsv.x > 359.5f) hue = 0.0f;
		else hue = hsv.x / 60.0f;
		int i = static_cast<int>(floor(hue));

		float f = hue - static_cast<float>(i);
		float p = hsv.z*(1.0f-hsv.y);
		float q = hsv.z*(1.0f-(hsv.y*f));
		float t = hsv.z*(1.0f-(hsv.y*(1.0f-f)));

		switch (i)
		{
			case 0: return colorf(hsv.z, t, p);
			case 1: return colorf(q, hsv.z, p);
			case 2: return colorf(p, hsv.z, t);
			case 3: return colorf(p, q, hsv.z);
			case 4: return colorf(t, p, hsv.z);
			default: return colorf(hsv.z, p, q);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////    ATLAS    ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	struct Node
	{
		typedef std::vector<std::unique_ptr<Node>> NodeGC_t;

		Node* child_first;
		Node* child_second;
		CImage* image;
		recti rc;
		int diff;
		NodeGC_t* node_gc;

		Node(int diff_, NodeGC_t* gc) : child_first(nullptr), child_second(nullptr), image(nullptr), rc(0,0,0,0), diff(diff_), node_gc(gc) {}

		Node* Insert(CImage* img)
		{
			// Sprawdzamy czy ma dzieci
			if (child_first)
			{
				// Czy uda sie umiescic w pierwszym?
				Node* nnode = child_first->Insert(img);
				if (nnode) return nnode;

				// Czy uda sie umiescic w drugim?
				return child_second->Insert(img);
			}
			else
			{
				// Olewamy gdy obrazek tutaj juz jest
				if (image) return 0;

				int imgw = (int)img->Width + diff;
				int imgh = (int)img->Height + diff;

				// (if we're too small, return)
				if (rc.GetWidth() < imgw || rc.GetHeight() < imgh)
				{
					return 0;
				}

				// Gotowe mozemy tutaj umiescic obrazek
				if (rc.GetWidth() == imgw && rc.GetHeight() == imgh)
				{
					return this;
				}

				// (otherwise, gotta split this node and create some kids)
				child_first = new Node(diff, node_gc);
				child_second = new Node(diff, node_gc);

				// Aby na koncu wszystko zwolnic
				node_gc->push_back( std::unique_ptr<Node>(child_first) );
				node_gc->push_back( std::unique_ptr<Node>(child_second) );

				// (decide which way to split)
				int dw = rc.GetWidth() - imgw;
				int dh = rc.GetHeight() - imgh;

				if (dw > dh)
				{
					child_first->rc.Set(rc.left, rc.top,
										rc.left + imgw, rc.bottom);

					child_second->rc.Set(rc.left + imgw, rc.top,
										 rc.right, rc.bottom);
				}
				else
				{
					child_first->rc.Set(rc.left, rc.top,
										rc.right, rc.top + imgh);

					child_second->rc.Set(rc.left, rc.top + imgh,
										 rc.right, rc.bottom);
				}

				// Idzemy do pierwszego stworzonego dziecka
				return child_first->Insert(img);
			}
		}
	};

	void CopyImageToImage32(CImage& small, u32 x, u32 y, CImage& big)
	{
		assert(small.Bpp == 32);
		assert(big.Bpp == 32);

		const u32 x_max = x + small.Width;
		const u32 y_max = y + small.Height;

		u32* big_ptr = (u32*)big.Data;
		const u32* small_ptr = (u32*)small.Data;

		msize p1;
		msize p2;

		for (u32 j = y, q = 0; j < y_max; j++, q++ )
		{
			if (j >= big.Height) break;

			for (u32 i = x, p = 0; i < x_max; i++, p++ )
			{
				if (i >= big.Width) break;

				p1 = j * big.Width + i;
				p2 = q * small.Width + p;

				big_ptr[p1] = small_ptr[p2];
			}
		}
	}

	void CopyImageToImage32(CImage& big, recti& rect, CImage& out)
	{
		assert(rect.top >= 0);
		assert(rect.left >= 0);
		assert(rect.right >= 0);
		assert(rect.bottom >= 0);

		assert(big.Bpp == 32);
		out.Reset(rect.GetWidth(), rect.GetHeight(), 32);

		u32* big_ptr = (u32*)big.Data;
		u32* out_ptr = (u32*)out.Data;

		msize p1;
		msize p2;

		for (u32 j = rect.top, q = 0; j < static_cast<u32>(rect.bottom); j++, q++ )
		{
			if (j >= big.Height) break;

			for (u32 i = rect.left, p = 0; i < static_cast<u32>(rect.right); i++, p++ )
			{
				if (i >= big.Width) break;

				p1 = j * big.Width + i;
				p2 = q * out.Width + p;

				out_ptr[p2] = big_ptr[p1];
			}
		}
	}

	recti ScanRectInImage32(CImage& small)
	{
		assert(small.Bpp == 32);

		recti result(small.Width, small.Height, 0, 0);

		const u32* small_ptr = (u32*)small.Data;
		msize p2;

		bool fill_line;

		for (u32 y = 0; y < small.Height; y++)
		{
			fill_line = false;

			for (u32 x = 0; x < small.Width; x++)
			{
				p2 = y * small.Width + x;

				if ( (small_ptr[p2] & 0xFF000000) != 0 )
				{
					fill_line = true;
					result.left = std::min<int>(result.left, x);
					result.right = std::max<int>(result.right, x+1);
				}
			}

			if (fill_line)
			{
				result.top = std::min<int>(result.top, y);
				result.bottom = std::max<int>(result.bottom, y+1);
			}
		}

		if (result.IsValid()) return result;
		else return recti(0, 0, 1, 1);
	}

	struct AtlasPreElement
	{
		tstring Name;
		std::shared_ptr<CImage> Image;
		int OriginalWidth;
		int OriginalHeight;
		recti Part;
	};

	void GenerateAtlas(const tstring& folder, CImage& out, std::vector<AtlasTextureElement>& coords_out, int size, int diff)
	{
		if (folder.empty()) return;

		std::vector<tstring> tab = vfsSearch(folder + "/*");

		std::vector<AtlasPreElement> images;

		for (auto imgname : tab)
		{
			CImage img;
			if (LoadImageFile(imgname, img))
			{
				if (img.Bpp == 32 && img.Width != 0 && img.Height != 0)
				{
					recti outr = ScanRectInImage32(img);
					auto toimage = std::make_shared<CImage>();
					CopyImageToImage32(img, outr, *toimage);

					AtlasPreElement pre_element;
					pre_element.Name = imgname;
					pre_element.Image = toimage;
					pre_element.OriginalWidth = img.Width;
					pre_element.OriginalHeight = img.Height;
					pre_element.Part = outr;

					images.push_back(pre_element);
				}
			}
		}

		std::sort(images.begin(), images.end(),
			[](const AtlasPreElement& a, const AtlasPreElement& b)
			{
				return (a.Image->Width*a.Image->Height) > (b.Image->Width*b.Image->Height);
			}
		);

		Node::NodeGC_t gc;
		Node root(diff, &gc);
		root.rc.Set(0, 0, size, size);

		out.Reset(size, size, 32);

		for (msize i = 0; i < out.Size; i += 4)
		{
			out.Data[i+0] = 0xFF;
			out.Data[i+1] = 0xFF;
			out.Data[i+2] = 0xFF;
			out.Data[i+3] = 0x00;
		}

		for (msize i = 0; i < images.size(); i++)
		{
			Node* n = root.Insert(images[i].Image.get());
			if (n)
			{
				CopyImageToImage32(*(images[i].Image), n->rc.left, n->rc.top, out);
				n->image = images[i].Image.get();

				recti r = n->rc;
				r.right -= diff;
				r.bottom -= diff;

				AtlasTextureElement texture_element;
				texture_element.Name = images[i].Name;
				texture_element.AtlasRect = r;
				texture_element.OriginalWidth = images[i].OriginalWidth;
				texture_element.OriginalHeight = images[i].OriginalHeight;
				texture_element.Part = images[i].Part;

				coords_out.push_back(texture_element);
			}
		}
	}

} // End oo
