﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oGraph.hpp"

namespace oo
{

	bool GTextureEnable = false;
	bool GBlendEnable = false;
	EBlendMode GBlendMode = BM_NORMAL;

	bool GUseVertex = false;
	bool GUseTexCoord = false;
	bool GUseColor = false;

	vect2f ScreenC(1, 1);
	vect2f ScreenV(800, 600);
	vect2f ScreenOffsetBase(0, 0);
	vect2f ScreenOffset(0, 0);
	float FontScaleFactor = 1.0f;

	// ---------------------------------------- TEXTURE 2D ------------------------------------------------ //

	void CTexture::Zero()
	{
		TexID = 0;
		Coords.Set(0, 0, 1, 1);
		Weak = false;
		Width = 0;
		Height = 0;
		OriginalWidth = 0;
		OriginalHeight = 0;
		Part.Set(0, 0, 1, 1);
		W.Set(0.0f, 0.0f);
		Off[0].Set(0.0f, 0.0f);
		Off[1].Set(0.0f, 0.0f);
		Off[2].Set(0.0f, 0.0f);
		Off[3].Set(0.0f, 0.0f);
	}

	bool CTexture::Make(const CImage& image, u32 flags)
	{
		Unload();

		if (image.Data == nullptr)
		{
			LogMsg( "[ERR] [TEX] Nie mozna stworzyc tekstury, brak zrodlowych danych" );
			return false;
		}

		GLint iformat;
		GLint format;
		if (!GetFormat(image.Bpp, iformat, format))
		{
			LogMsg( "[ERR] [TEX] Loader tekstur wspiera tylko bitmapy 8g/24/32bitowe" );
			Unload();
			return false;
		}

		glGenTextures(1, &TexID);
		glBindTexture(GL_TEXTURE_2D, TexID);

		GLint mipmap = ((flags&TF_MIPMAPS) != 0) ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
		GLint rep_s = ((flags&TF_REPEAT_S) != 0) ? GL_REPEAT : GL_CLAMP_TO_EDGE;
		GLint rep_t = ((flags&TF_REPEAT_T) != 0) ? GL_REPEAT : GL_CLAMP_TO_EDGE;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, rep_s);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, rep_t);

		if (mipmap == GL_LINEAR_MIPMAP_LINEAR)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		}
		glTexImage2D(GL_TEXTURE_2D, 0, iformat, image.Width, image.Height, 0, format, GL_UNSIGNED_BYTE, image.Data);

		Width = image.Width;
		Height = image.Height;
		OriginalWidth = image.Width;
		OriginalHeight = image.Height;
		Part.Set(0, 0, image.Width, image.Height);
		W.Set(1.0f, 1.0f);

		return true;
	}

	bool CTexture::GetFormat(int bpp, GLint& iformat, GLint& format)
	{
		switch (bpp)
		{
			case 32:
				iformat = GL_RGBA8;
				format = GL_RGBA;
			break;

			case 24:
				iformat = GL_RGB8;
				format = GL_RGB;
			break;

			case  8:
				iformat = GL_ALPHA8;
				format = GL_ALPHA;
			break;

			default:
				iformat = 0;
				format = 0;
				return false;
		}

		return true;
	}

	void CTexture::Unload()
	{
		if (!Weak) { glDeleteTextures(1, &TexID); }
		Zero();
	}

	CDataManager<CTexture> GTextureDataBase;
	CDataManager<CTexture> GAtlasDataBase;

	void ClearAtlasDataBase()
	{
		GAtlasDataBase.Erase(
			[](const RefTexture& atlas) -> bool
			{
				GTextureDataBase.Erase( 
					[&atlas](const RefTexture& tex) 
					{
						return (tex->Weak == true) && (tex->TexID == atlas->TexID); 
					});

				return true;
		});
	}

	void ClearTextureDataBase() { GTextureDataBase.Clear(); }
	void CheckForUnusedTexture() 
	{ 
		GTextureDataBase.Erase( 
			[](const RefTexture& tex) 
			{
				return (tex.use_count() == 1) && (tex->Weak == false); 
			}
		);
	}

	void LoadAtlas(const tstring& name, int size, int diff)
	{
		CImage out;
		std::vector<AtlasTextureElement> script;
		GenerateAtlas(name, out, script, size, diff);

		auto atlastexture = std::make_shared<CTexture>();
		atlastexture->Make(out);

		float bx = (float)atlastexture->Width;

		for (msize i = 0; i < script.size(); i++)
		{
			recti r = script[i].AtlasRect;

			auto part = std::make_shared<CTexture>();
			part->TexID = atlastexture->TexID;

			part->Coords.Set( static_cast<float>(r.left) / bx,
							  static_cast<float>(r.top) / bx,
							  static_cast<float>(r.right) / bx,
							  static_cast<float>(r.bottom) / bx );

			part->Width				= r.GetWidth();
			part->Height			= r.GetHeight();
			part->OriginalWidth		= script[i].OriginalWidth;
			part->OriginalHeight	= script[i].OriginalHeight;
			part->Part				= script[i].Part;
			part->Weak = true;

			part->W.Set( (float)part->Width /  (float)part->OriginalWidth,
						 (float)part->Height / (float)part->OriginalHeight );

			part->Off[0].Set( (float)part->Part.left / (float)part->OriginalWidth,
							  (float)(part->OriginalHeight - part->Part.bottom) / (float)part->OriginalHeight );

			part->Off[1].Set( (float)part->Part.left / (float)part->OriginalWidth,
							  (float)part->Part.top / (float)part->OriginalHeight );

			part->Off[2].Set( (float)(part->OriginalWidth - part->Part.right) / (float)part->OriginalWidth,
							  (float)(part->OriginalHeight - part->Part.bottom) / (float)part->OriginalHeight );

			part->Off[3].Set( (float)(part->OriginalWidth - part->Part.right) / (float)part->OriginalWidth,
							  (float)part->Part.top / (float)part->OriginalHeight );

			GTextureDataBase.InsertResource(script[i].Name, part);
		}

		GAtlasDataBase.InsertResource(name, atlastexture);
	}

	RefTexture LoadTexture(const tstring& filename, u32 flags)
	{
#ifdef OMIKRON2_DEDICATED_SERVER
		return RefTexture();
#else
		RefTexture out = GTextureDataBase.GetExist(filename);
		if (out) return out;

		CImage img;
		bool error = false;
		float u = 1.0f;
		float v = 1.0f;
		int ow = 1;
		int oh = 1;

		// Wczytujemy plik
		if (!LoadImageTexture(filename, img, u, v, ow, oh)) {
			LogMsg( "[ERR] [TEX] Nie mozna stworzyc tekstury" );
			error = true;
		}

		GLint iformat = 0;
		GLint format = 0;
		if ( (error==false) && (!CTexture::GetFormat(img.Bpp, iformat, format)) )
		{
			LogMsg( "[ERR] [TEX] Loader tekstur wspiera tylko bitmapy 8g/24/32bitowe" );
			error = true;
		}

		if (error)
		{
			LogMsg( "[ERR] [TEX] Wczytuje teksture awaryjna" );
			if (!LoadImageTexture("error.png", img, u, v, ow, oh)) assert(0);
			if (!CTexture::GetFormat(img.Bpp, iformat, format)) assert(0);
		}

		GLuint gid = 0;

		glGenTextures(1, &gid);
		glBindTexture(GL_TEXTURE_2D, gid);

		GLint mipmap = ((flags&TF_MIPMAPS) != 0) ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
		GLint rep_s = ((flags&TF_REPEAT_S) != 0) ? GL_REPEAT : GL_CLAMP_TO_EDGE;
		GLint rep_t = ((flags&TF_REPEAT_T) != 0) ? GL_REPEAT : GL_CLAMP_TO_EDGE;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // GL_NEAREST
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, rep_s);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, rep_t);

		if (mipmap == GL_LINEAR_MIPMAP_LINEAR)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		}
		glTexImage2D(GL_TEXTURE_2D, 0, iformat, img.Width, img.Height, 0, format, GL_UNSIGNED_BYTE, img.Data);

		auto tex = std::make_shared<CTexture>();
		tex->TexID = gid;
		tex->Coords.Set(0.0f, 0.0f, u, v);
		tex->Width = img.Width;
		tex->Height = img.Height;
		tex->OriginalWidth = ow;
		tex->OriginalHeight = oh;
		tex->Part.Set(0, 0, ow, oh);
		tex->W.Set(1.0f, 1.0f);

		out = GTextureDataBase.InsertResource(filename, tex);
		return out;
#endif
	}

	// ---------------------------------------- END TEXTURE 2D ------------------------------------------------ //

	// ---------------------------------------- FONT MANAGER -------------------------------------------------- //

	CFont::CFont(const tstring& filename, int size)
	{
		CImage image;
		MetricVector_t metric_v;

		int lineheight =0;

		if (!MakeFont(filename, size, image, metric_v, Scope_v, lineheight))
		{
			LogMsg( "[ERR] [FNT] Nie mozna wyrenderowac czcionki z pliku : " + filename );
			IsFontOk = false;
			return;
		}

		LineHeight = (float)lineheight;

		GlyphMetric_v.resize(metric_v.size());

		for (msize i=0; i < metric_v.size(); i++)
		{
			GlyphMetric_v[i].Width = float(metric_v[i].RightCoord - metric_v[i].LeftCoord);
			GlyphMetric_v[i].Height = float(metric_v[i].BottomCoord - metric_v[i].TopCoord);

			GlyphMetric_v[i].LeftCoord	 = float(metric_v[i].LeftCoord)   /  float(image.Width);
			GlyphMetric_v[i].TopCoord    = float(metric_v[i].TopCoord)    /  float(image.Height);
			GlyphMetric_v[i].RightCoord  = float(metric_v[i].RightCoord)  /  float(image.Width);
			GlyphMetric_v[i].BottomCoord = float(metric_v[i].BottomCoord) /  float(image.Height);

			GlyphMetric_v[i].HoriAdvance = float(metric_v[i].HoriAdvance);
			GlyphMetric_v[i].HoriBearingX = float(metric_v[i].HoriBearingX);
			GlyphMetric_v[i].HoriBearingY = float(metric_v[i].HoriBearingY);
		}

		//SpaceWidth = GlyphMetric_v[GetGlyphIndex(20)].HoriAdvance;
		//GlyphMetric_v[GetGlyphIndex(' ')].Width = SpaceWidth;
		SpaceWidth = GlyphMetric_v[GetGlyphIndex(0x20)].HoriAdvance;
//		GlyphMetric_v[GetGlyphIndex(' ')].Width = SpaceWidth;
//		GlyphMetric_v[GetGlyphIndex(' ')].HoriBearingX = 0;

		if (Texture.Make(image)) IsFontOk = true;
		else IsFontOk = false;
	}

	CFont::~CFont()
	{
	}

	CDataManager<CFont> GFontDataBase;

	void ClearFontDataBase() { GFontDataBase.Clear(); }

	RefFont LoadFont( const tstring& filename, int size )
	{
#ifdef OMIKRON2_DEDICATED_SERVER
		return RefFont();
#else
		tstring name = format("%s:%i", filename, size);
		RefFont font = GFontDataBase.GetExist(name);
		if (font) return font;

		font = std::make_shared<CFont>(filename, size);
		if (font->IsOk())
		{
			return GFontDataBase.InsertResource(name, font);
		}
		else
		{
			ThrowCriticalError("Nie mozna wygenerowac czcionki");
			return RefFont();
		}
#endif
	}

	// ========================================================

	CFontManager::CFontManager()
	{
		const msize size = MaxVertexCount * sizeof(SCharVertex);
		const msize isize = MaxIndexCount * sizeof(u16);

		// Generujemy buforek
		glGenBuffers(1, &VBOVertices);
		glBindBuffer(GL_ARRAY_BUFFER, VBOVertices);
		glBufferData(GL_ARRAY_BUFFER, size, nullptr, GL_DYNAMIC_DRAW);

		std::vector<u16> indices(MaxIndexCount);
		const u16 indices_size = static_cast<u16>(indices.size());
		for (u16 ii = 0, vi = 0; ii < indices_size; ii += 6, vi += 4)
		{
			indices[ii + 0] = vi + 0;
			indices[ii + 1] = vi + 1;
			indices[ii + 2] = vi + 2;
			indices[ii + 3] = vi + 2;
			indices[ii + 4] = vi + 3;
			indices[ii + 5] = vi + 0;
		}

		glGenBuffers(1, &VBOIndices);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIndices);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, isize, &indices[0], GL_STATIC_DRAW);

		// Ustawiamy nasz buforek
		Vertices_tmp.resize( MaxVertexCount );

		// Rezerwujemy
		Words.reserve(256);
		Lines.reserve(64);

		// Zerujemy
		CharIndex = 0;

		ShadowVect.Set(0.0f, 0.0f);

		// Domyslne wartosci dla kolorow
		ColorChar = '^';
		CurrentColor.Set(1.0f, 0.0f, 0.0f, 1.0f);
		ColorTable.resize(16, CurrentColor);

		// Dodajemy podstawowe (domyslne) kolory
		ColorTable[0] = MakeColorf(0xFF, 0x00, 0x00);	// Czerwony
		ColorTable[1] = MakeColorf(0x00, 0xFF, 0x00);	// Zielony
		ColorTable[2] = MakeColorf(0xFF, 0xFF, 0x00);	// Zolty
		ColorTable[3] = MakeColorf(0x00, 0x00, 0xFF);	// Niebieski
		ColorTable[4] = MakeColorf(0xFF, 0xD7, 0x00);	// Rozowy

		ColorTable[5] = MakeColorf(0xAF, 0xAF, 0xAF);	// Szary
		ColorTable[6] = MakeColorf(0x00, 0x00, 0x00);	// Czarny
		ColorTable[7] = MakeColorf(0xFF, 0xFF, 0xFF);	// Bialy

		ColorTable[0x8] = MakeColorf(0xFF, 0x00, 0x00);	// Czerwony
		ColorTable[0x9] = MakeColorf(0x80, 0x00, 0xFF);	// Fioletowy
		ColorTable[0xA] = MakeColorf(0xFF, 0x00, 0xFF);	// Rozowy
		ColorTable[0xB] = MakeColorf(0xFF, 0xFF, 0x00);	// Zolty
		ColorTable[0xC] = MakeColorf(0xFF, 0x80, 0x00);	// Pomaranczowy
		ColorTable[0xD] = MakeColorf(0x00, 0x40, 0xFF);	// Niebieski
		ColorTable[0xE] = MakeColorf(0x00, 0xFF, 0x00);	// Zielony
		ColorTable[0xF] = MakeColorf(0x9D, 0x50, 0x0A);	// Brazowy

	}

	CFontManager::~CFontManager()
	{
		ClearBuffer();
		glDeleteBuffers(1, &VBOIndices);
		glDeleteBuffers(1, &VBOVertices);

		// Co by sie usunela przy wywolaniu ClearAll
		ActiveFont.reset();

		// Bez font managera fonty nie beda nam juz potrzebne.
		ClearFontDataBase();
	}

	// Algorytm na Word wrap : znaleziony na wiki ;p
	//SpaceLeft := LineWidth
	//for each Word in Text
	//    if Width(Word) > SpaceLeft
	//        insert line break before Word in Text
	//        SpaceLeft := LineWidth - Width(Word)
	//    else
	//        SpaceLeft := SpaceLeft - Width(Word) + SpaceWidth

	std::vector<CFontManager::STextElement> CFontManager::SplitWords(const RefFont& font, const std::u32string& str, float boxwidth)
	{
		std::vector<STextElement> words;
		bool space = false;
		STextElement word = { 0.0f, 0, 0 };

		for (auto chr : str)
		{
			// Gdy napotykamy nowy wiersz dodajemy specjalny 'wyraz'
			if (chr == '\n')
			{
				if (word.Begin != word.End) words.push_back(word);
				word.End += 1;
				word.Begin = word.End;
				word.Width = 0.0f;
				space = true;

				STextElement tmp = { -1.0f, word.Begin, 0 };
				words.push_back(tmp);
				continue;
			}

			// Gdy pojedyncze slowo nie miesci sie w wierszu
			if (boxwidth > 0.0f)
			{
				if ((word.Width + font->GetGlyphWidth(chr)) > boxwidth)
				{
					if (word.Begin != word.End) words.push_back(word);
					word.End += 1;
					word.Begin = word.End;
					word.Width = 0.0f;
					space = true;
					continue;
				}
			}

			if (chr != ' ')
			{
				word.Width += font->GetGlyphWidth(chr);
				word.End++; space = false;
			}
			else if (space == false)
			{
				words.push_back(word);
				word.End += 1;
				word.Begin = word.End;
				word.Width = 0.0f;
				space = true;
			} else { word.End++; word.Begin++; }
		}

		if (!str.empty()) words.push_back(word);

		return words;
	}

	void CFontManager::SplitLinesWordWrap(const RefFont& font, const std::u32string& str,const rectf& rect, std::vector<STextElement>& lines)
	{
		lines.clear();
		Words = SplitWords(font, str, rect.GetWidth());
		STextElement tl = { 0.0f, 0, 0 };

		const float space_width = font->GetSpaceWidth();
		float space_left = rect.GetWidth() + space_width;
		
		msize last_end = 0;

		for (const auto& word : Words)
		{
			if ( ((word.Width + space_width) > space_left) || (word.Width < 0.0f) )
			{
				tl.Width = rect.GetWidth() - space_left;
				tl.Begin = last_end;
				tl.End = word.Begin - ((word.Width<0.0f)?1:0);

				last_end = word.Begin;
				lines.push_back(tl);

				if (word.Width < 0.0f) space_left = rect.GetWidth() + space_width;
				else space_left = rect.GetWidth() - word.Width;
			}
			else { space_left -= word.Width + space_width; }
		}

		if (last_end<str.size())
		{
			tl.Width = rect.GetWidth() - space_left;
			tl.Begin = last_end;
			tl.End = (*Words.rbegin()).End;

			lines.push_back(tl);
		}
	}

	void CFontManager::SplitLinesCharWrap(const RefFont& font, const std::u32string& str, const rectf& rect, std::vector<STextElement>& lines)
	{
		lines.clear();
		STextElement tl = { 0.0f, 0, 0 };

		float char_width = 0.0f;
		float space_left = rect.GetWidth();
		bool newline = false;
		msize last_end =0;

		for (msize c = 0; c < str.size(); c++)
		{
			char_width = font->GetGlyphWidth(str[c]);
			newline = (str[c] == '\n');

			if (char_width > space_left || newline)
			{
				tl.Width = rect.GetWidth() - space_left;
				tl.Begin = last_end;
				tl.End = c;
				last_end = c + (newline?1:0);
				lines.push_back(tl);

				space_left = rect.GetWidth() - char_width + (newline?char_width:0);
			}
			else space_left -= char_width;
		}

		if (last_end<str.size())
		{
			tl.Width = rect.GetWidth() - space_left;
			tl.Begin = last_end;
			tl.End = str.size();

			lines.push_back(tl);
		}
	}

	void CFontManager::SplitLinesNoWrap(const RefFont& font, const std::u32string& str, const rectf& rect, std::vector<STextElement>& lines)
	{
		lines.clear();
		STextElement tl = { 0.0f, 0, 0 };

		float char_width = 0.0f;
		float space_left = rect.GetWidth();
		msize last_end =0;

		for (msize c = 0; c < str.size(); c++)
		{
			char_width = font->GetGlyphWidth(str[c]);

			if (str[c] == '\n')
			{
				tl.Width = rect.GetWidth() - space_left;
				tl.Begin = last_end;
				tl.End = c;
				last_end = c+1;
				lines.push_back(tl);

				space_left = rect.GetWidth() - char_width + char_width;
			}
			else space_left -= char_width;
		}

		if (last_end<str.size())
		{
			tl.Width = rect.GetWidth() - space_left;
			tl.Begin = last_end;
			tl.End = str.size();

			lines.push_back(tl);
		}
	}

	void CFontManager::SetClip(const RefFont& font, const std::u32string& str, const rectf& rect, std::vector<STextElement>& lines, u32 style)
	{
		assert(!((H_CLIP&style)&&((CHAR_WRAP|WORD_WRAP)&style))); // Nieprawidlowa kombinacja flag: H_CLIP nie pasuje do CHAR_WRAP albo do WORD_WRAP
		assert(!((H_CLIP&style)&&(style&H_CENTER))); // Nieprawidlowa kombinacja flag: H_CLIP i H_CENTER nie moga byc razem

		msize gbegin = 0;
		msize gend = 0;
		msize mvalue = std::min( msize(rect.GetHeight() / font->GetLineHeight()), lines.size());

		if ( (V_CLIP&style) && (mvalue<lines.size()) )
		{
			if (style&V_CENTER) gbegin = gend = (lines.size() - mvalue)/2;
			else if (style&V_BOTTOM) gbegin = lines.size() - mvalue;
				 else gend = lines.size() - mvalue;

			for (msize i = gbegin, b = 0; i < lines.size()-gend; i++, b++)
			{
				lines[b] = lines[i];
			}
			lines.resize(lines.size()-gend-gbegin);
		}

		if (H_CLIP&style)
		{
			for (auto& line : lines)
			{
				// Olewamy puste wiersze
				if (line.Begin == line.End) continue;

				float char_width = 0.0f;
				float space_side = rect.GetWidth();

				line.Width = 0.0f;
				int wend = line.End;
				int wbegin = line.Begin;

				// if (style&H_CENTER) nieaktywne
				// if (style&H_CENTER) ostatni else
				if (style&H_RIGHT)
				{
					while (wbegin < wend--)
					{
						char_width = font->GetGlyphWidth(str[wend]);
						if (char_width > space_side) { line.Begin = wend+1; break; }
						else { space_side -= char_width; }
						line.Width += char_width;
					}
				}
				else
				{
					while (wend > wbegin)
					{
						char_width = font->GetGlyphWidth(str[wbegin]);
						line.Width += char_width;
						if (char_width > space_side) { line.End = wbegin?wbegin-1:0; break; }	// Unikamy dzialania (0 - 1)
						else { space_side -= char_width; }
						wbegin++;
					}
				}
			}
		}
	}

	void CFontManager::DrawBufferElement(const SBufferElement& be)
	{
#ifndef OMIKRON2_DEDICATED_SERVER
		EnableTexture();
		EnableBlend();

		// Bindujemy teksturke...
		be.FontPtr->BindTexture();

		// Okreslamy dla VBO gdzie co sie znajduje
		glVertexPointer(2, GL_FLOAT, sizeof(SCharVertex), 0);
		glColorPointer(4, GL_FLOAT, sizeof(SCharVertex), (char*)0 + sizeof(vect2f));
		glTexCoordPointer(2, GL_FLOAT, sizeof(SCharVertex), (char*)0 + sizeof(vect2f)+sizeof(colorf));

		// Rysujemy odpowiedni zakres
	//	glEnableClientState( GL_VERTEX_ARRAY );
	//	glEnableClientState( GL_COLOR_ARRAY );
	//	glEnableClientState( GL_TEXTURE_COORD_ARRAY );
		glDrawElements(GL_TRIANGLES, be.Count, GL_UNSIGNED_SHORT, (const GLvoid *)(be.Index * sizeof(u16) ) );
	//	glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	//	glDisableClientState( GL_COLOR_ARRAY );
	//	glDisableClientState( GL_VERTEX_ARRAY );
#endif
	}

	bool CFontManager::TryDraw(u32 paramshash)
	{
		for (const auto& text : TextList)
		{
			if (text.ParamsHash == paramshash)
			{
				DrawBufferElement(text);
				return true;
			}
		}
		return false;
	}

	int CFontManager::CountLines(const tstring& text, float width, u32 style)
	{
		assert((H_LEFT|V_BOTTOM)&style);	// Przy zliczaniu linii dawaj tylko H_LEFT i V_BOTTOM

		rectf rect(0.0f, 0.0f, width, 1000.0f);

		// Nic? to nie rysujemy...
		if (text.empty() || !rect.IsValid())
		{
			return 0;
		}

		std::u32string t = Decode(text);
		if (WORD_WRAP&style) SplitLinesWordWrap(ActiveFont, t, rect, Lines);
		else if (CHAR_WRAP&style) SplitLinesCharWrap(ActiveFont, t, rect, Lines);
			 else SplitLinesNoWrap(ActiveFont, t, rect, Lines);

		return Lines.size();
	}

	void CFontManager::SplitLines(const tstring& text, float width, std::vector<tstring>& out, u32 style)
	{
		assert((H_LEFT|V_BOTTOM)&style);	// Przy zliczaniu linii dawaj tylko H_LEFT i V_BOTTOM

		rectf rect(0.0f, 0.0f, width, 1000.0f);

		out.clear();

		// Nic? to nie rysujemy...
		if (text.empty() || !rect.IsValid())
		{
			out.push_back("");
			return;
		}

		std::u32string t = Decode(text);
		if (WORD_WRAP&style) SplitLinesWordWrap(ActiveFont, t, rect, Lines);
		else if (CHAR_WRAP&style) SplitLinesCharWrap(ActiveFont, t, rect, Lines);
			 else SplitLinesNoWrap(ActiveFont, t, rect, Lines);

		for (const auto& line : Lines)
		{
			out.push_back(Encode( t.substr(line.Begin, line.End - line.Begin) ));
		}
	}

	class IndexBufferObjectBinder
	{
		private: // Obiekty CImage nie sa do kopiowania
			IndexBufferObjectBinder( const IndexBufferObjectBinder& );
			IndexBufferObjectBinder& operator=( const IndexBufferObjectBinder& );

		private:
			GLuint Id;

		public:
			IndexBufferObjectBinder(GLuint id) : Id(id) { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id); }
			~IndexBufferObjectBinder() { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); }

	};

	void CFontManager::DrawStaticText(const tstring& txt, const rectf& rect, const colorf& color1, const colorf& color2, u32 style)
	{
		if ( txt.empty() || (!rect.IsValid()) ) return;

		// Generujemy skrot
		u32 paramshash = HashString_cs(txt) +
						 GetCRC((u8*)&rect, sizeof(rect)) +
						 GetCRC((u8*)&color1, sizeof(color1)) +
						 GetCRC((u8*)&color2, sizeof(color2)) +
						 style;

		//  Bindujemy buforek
		glBindBuffer(GL_ARRAY_BUFFER, VBOVertices);
		IndexBufferObjectBinder ibo_binder(VBOIndices);

		// I sprawdzamy czy takie parametry juz byly podawane - jesli tak to rysujemy z bufora
		if (TryDraw(paramshash)) return;

		std::u32string text = Decode(txt);
		assert(MaxCharCount >= text.size());

		// Jesli uzywamy cieniowania, musimy miec dwa razy wiekszy buffor
		bool shadow = (style&SHADOW) != 0;
		msize strlen_ = (shadow?(text.size()*2):(text.size()));

		// Gdy nie ma juz miejsca
		if ( (MaxCharCount-CharIndex) < strlen_)
		{
			TextList.clear();
			CharIndex = 0;
			if (MaxCharCount < strlen_) return;
		}

		if (WORD_WRAP&style) SplitLinesWordWrap(ActiveFont, text, rect, Lines);
		else if (CHAR_WRAP&style) SplitLinesCharWrap(ActiveFont, text, rect, Lines);
			 else SplitLinesNoWrap(ActiveFont, text, rect, Lines);

		if ((H_CLIP|V_CLIP)&style) SetClip(ActiveFont, text, rect, Lines, style);

		rectf coords;			// Pomocnicza
		colorf color;			// Pomocnicza
		msize index = 0;		// Pomocnicza	(w VERTEXACH)
		msize offset_in_vbo = CharIndex * 4 * sizeof(SCharVertex); // Offset danych w VBO (w BAJTACH), od tego miejsca beda sie zaczynac dane jakie zapiszemy

		// (xy)  (x+y)
		//  0------3
		//  |      |
		//  |      |
		//  |      |
		//  1------2
		// (xy+)  (x+y+)

		SBufferElement be;

		be.ParamsHash = paramshash;
		be.FontPtr = ActiveFont;
		be.Index = CharIndex * 6;
		be.Count = 0;

		// Wysokosc wiersza
		float height = ActiveFont->GetLineHeight();

		// Maly procent wysokosci
		const float point = height * 0.2f;

		// Aktualna pozycja x, y
		float pos_x;
		float pos_y;

		CFont::GlyphMetric_t metric = *(ActiveFont->GetGlyphMetircPtr(text[0]));

		bool flip = (style&FLIPUV) != 0;
		bool hinting = (style&NOHINTING) == 0;
		bool nogradient = (style&GRADIENT) == 0;

		// Dwie iteracje 0 to cien 1 to kolor
		for (int g = (shadow?0:1); g < 2; g++)
		{

		/* if (style&V_TOP) ostatni else */
		if (style&V_CENTER) pos_y = (rect.top + ((rect.GetHeight() - ((float)Lines.size() * height - point + height)) / 2.0f) + height + point);
		else if (style&V_BOTTOM) pos_y = (rect.bottom - (float)Lines.size() * height - point + height);
			 else pos_y = (rect.top + height - point);

		if (g == 0) pos_y += ShadowVect.y;
		if (hinting) pos_y = floor(pos_y);

		for (const auto& line : Lines)
		{
			/* if (style&H_LEFT) ostatni else */
			if (style&H_CENTER) pos_x = (rect.left + (rect.GetWidth() - line.Width) / 2.0f);
			else if (style&H_RIGHT) pos_x = (rect.left + (rect.GetWidth() - line.Width));
				 else pos_x = (rect.left);

			if (g == 0) pos_x += ShadowVect.x;
			if (hinting) pos_x = floor(pos_x);

			for (msize i = line.Begin; i < line.End; i++, CharIndex++)
			{
				metric = *(ActiveFont->GetGlyphMetircPtr(text[i]));
				if (flip) std::swap( metric.BottomCoord, metric.TopCoord );
				pos_x += metric.HoriBearingX;

				// p1
				Vertices_tmp[index+0].pos.Set(pos_x, (pos_y - metric.Height) + metric.HoriBearingY);
				Vertices_tmp[index+0].uv.Set(metric.LeftCoord, metric.TopCoord);

				// p2
				Vertices_tmp[index+1].pos.Set(pos_x, pos_y + metric.HoriBearingY);
				Vertices_tmp[index+1].uv.Set(metric.LeftCoord, metric.BottomCoord);

				// p3
				Vertices_tmp[index+2].pos.Set(pos_x + metric.Width, pos_y + metric.HoriBearingY);
				Vertices_tmp[index+2].uv.Set(metric.RightCoord, metric.BottomCoord);

				// p4
				Vertices_tmp[index+3].pos.Set(pos_x + metric.Width, (pos_y - metric.Height) + metric.HoriBearingY);
				Vertices_tmp[index+3].uv.Set(metric.RightCoord, metric.TopCoord);

				if (g == 0)	// Pierwsza iteracja g to cien
				{
					Vertices_tmp[index+0].color.Set(0.0f, 0.0f, 0.0f, color1.A);
					Vertices_tmp[index+1].color.Set(0.0f, 0.0f, 0.0f, color1.A);
					Vertices_tmp[index+2].color.Set(0.0f, 0.0f, 0.0f, color1.A);
					Vertices_tmp[index+3].color.Set(0.0f, 0.0f, 0.0f, color1.A);
				}
				else // Druga to kolor
				{
					// Kolory (gradient standardowy - od lewej do prawej)
					if (nogradient)
					{
						Vertices_tmp[index+0].color = color2;
						Vertices_tmp[index+1].color = color1;
						Vertices_tmp[index+2].color = color1;
						Vertices_tmp[index+3].color = color2;
					}
					else // Gradient z gory na dol
					{
						color = LerpColor(color1, color2, (float)i/(float)text.size());
						Vertices_tmp[index+0].color = color;
						Vertices_tmp[index+1].color = color;
						Vertices_tmp[index+2].color = color;
						Vertices_tmp[index+3].color = color;
					}
				}

				index += 4;
				be.Count += 6;
				pos_x += metric.HoriAdvance - metric.HoriBearingX;
			}
			pos_y += height;
		}

		}	// End for  (int g = (shadow?0:1); g < 2; g++)

		glBufferSubData(GL_ARRAY_BUFFER, offset_in_vbo, index*sizeof(SCharVertex), &Vertices_tmp[0]);

		TextList.push_back(be);
		DrawBufferElement(be);
	}

	void CFontManager::DrawColorText(const tstring& txt, const rectf& rect, u32 style, float alpha)
	{
		if ( txt.empty() || (!rect.IsValid()) ) return;

		// Generujemy skrot
		u32 paramshash = HashString_cs(txt) +
						 GetCRC((u8*)&rect, sizeof(rect)) +
						 (style ^ (*reinterpret_cast<u32*>(&alpha)));

		//  Bindujemy buforek
		glBindBuffer(GL_ARRAY_BUFFER, VBOVertices);
		IndexBufferObjectBinder ibo_binder(VBOIndices);

		// I sprawdzamy czy takie parametry juz byly podawane - jesli tak to rysujemy z bufora
		if (TryDraw(paramshash)) return;

		std::u32string text = Decode(txt);
		assert(MaxCharCount >= text.size());

		msize colorindex =0;
		std::vector< std::pair<msize, msize> > colorindex_v; // Wektor par, index<>kolor

		// Tutaj bedzie nowy string bez znacznikow kolorow
		std::u32string result;

		// Przerabiamy string'a (wywalamy znaczniki kolorow)
		for (msize i =0, id =0; i < text.size(); i++) {
			if ((text[i]==ColorChar) && ((i+1)<text.size())) {
				u8 val = HexToInt(text[i+1]);
				// Dodajemy do wektora par, nowa pare <indexu w tekscie, indexu w tablicy kolorow>
				if (val != 0xFF)
				{
					// Sprawdzamy czy dochodzi do przypadku np. ^2^3^4napis (i ew. korygujemy do ^4napis)
					if ((colorindex_v.empty() == false) && (colorindex_v.back().first == id))
					{
						colorindex_v.back().second = val;
					}
					else
					{
						colorindex_v.push_back( std::make_pair(id, val) );
					}

					i++;
				}
				else { result += text[i+1]; id++; i++; }
			}
			else { result += text[i]; id++; }
		}

		// Jesli uzywamy cieniowania, musimy miec dwa razy wiekszy buffor
		bool shadow = (style&SHADOW) != 0;
		msize strlen_ = (shadow?(result.size()*2):(result.size()));

		// Gdy nie ma juz miejsca
		if ( (MaxCharCount-CharIndex) < strlen_) {
			TextList.clear();
			CharIndex = 0;
			if (MaxCharCount < strlen_) return;
		}

		if (WORD_WRAP&style) SplitLinesWordWrap(ActiveFont, result, rect, Lines);
		else if (CHAR_WRAP&style) SplitLinesCharWrap(ActiveFont, result, rect, Lines);
		else SplitLinesNoWrap(ActiveFont, result, rect, Lines);

		if ((H_CLIP|V_CLIP)&style) SetClip(ActiveFont, result, rect, Lines, style);

		rectf coords;			// Pomocnicza
		colorf color;			// Pomocnicza
		msize index =0;			// Pomocnicza	(w VERTEXACH)
		msize offset_in_vbo = CharIndex * 4 * sizeof(SCharVertex); // Offset danych w VBO (w BAJTACH), od tego miejsca beda sie zaczynac dane jakie zapiszemy

		// (xy)  (x+y)
		//  0------3
		//  |      |
		//  |      |
		//  |      |
		//  1------2
		// (xy+)  (x+y+)

		SBufferElement be;

		be.ParamsHash = paramshash;
		be.Count = 0;
		be.Index = CharIndex * 6;
		be.FontPtr = ActiveFont;

		// Stala wysokosc znaku
		const float height = ActiveFont->GetLineHeight();

		// Maly procent wysokosci
		const float point = height*0.2f;

		// Aktualna pozycja x, y
		float pos_x;
		float pos_y;

		// Domyslnym kolorem jest zawsze pierwszy w tabeli kolorow
		CurrentColor = ColorTable[0];

		CFont::GlyphMetric_t metric = *(ActiveFont->GetGlyphMetircPtr(text[0]));

		bool flip = (style&FLIPUV) != 0;
		bool hinting = (style&NOHINTING) == 0;

		// Dwie iteracje 0 to cien 1 to kolor
		for (int g = (shadow?0:1); g < 2; g++)
		{

		/* if (style&V_TOP) ostatni else */
		if (style&V_CENTER) pos_y = (rect.top + ((rect.GetHeight() - ((float)Lines.size() * height - point + height)) / 2.0f) + height + point);
		else if (style&V_BOTTOM) pos_y = (rect.bottom - (float)Lines.size() * height - point + height);
			 else pos_y = (rect.top + height - point);

		if (g == 0) pos_y += ShadowVect.y;
		if (hinting) pos_y = floor(pos_y);

		for (const auto& line : Lines)
		{
			// if (style&H_LEFT) ostatni else
			if (style&H_CENTER) pos_x = (rect.left + (rect.GetWidth()-line.Width)/2.0f);
			else if (style&H_RIGHT) pos_x = (rect.left + (rect.GetWidth()-line.Width));
				 else pos_x = (rect.left);

			if (g == 0) pos_x += ShadowVect.x;
			if (hinting) pos_x = floor(pos_x);

			for (msize i = line.Begin; i < line.End; i++, CharIndex++)
			{
				if (g == 0)
				{
					CurrentColor.Set(0.0f, 0.0f, 0.0f, alpha);
				}
				else
				{
					// Ustalamy kolor
					if (colorindex < colorindex_v.size()) {
						if (colorindex_v[colorindex].first <= i) {
							CurrentColor = ColorTable[colorindex_v[colorindex].second];
							CurrentColor.A = alpha;
							colorindex++;
						}
					}
				}

				metric = *(ActiveFont->GetGlyphMetircPtr(result[i]));
				if (flip) std::swap( metric.BottomCoord, metric.TopCoord );
				pos_x += metric.HoriBearingX;

				// p1
				Vertices_tmp[index+0].pos.Set(pos_x, (pos_y - metric.Height) + metric.HoriBearingY);
				Vertices_tmp[index+0].uv.Set(metric.LeftCoord, metric.TopCoord);
				Vertices_tmp[index+0].color = CurrentColor;

				// p2
				Vertices_tmp[index+1].pos.Set(pos_x, pos_y + metric.HoriBearingY);
				Vertices_tmp[index+1].uv.Set(metric.LeftCoord, metric.BottomCoord);
				Vertices_tmp[index+1].color = CurrentColor;

				// p3
				Vertices_tmp[index+2].pos.Set(pos_x+metric.Width, pos_y + metric.HoriBearingY);
				Vertices_tmp[index+2].uv.Set(metric.RightCoord, metric.BottomCoord);
				Vertices_tmp[index+2].color = CurrentColor;

				// p4
				Vertices_tmp[index+3].pos.Set(pos_x+metric.Width, (pos_y - metric.Height) + metric.HoriBearingY);
				Vertices_tmp[index+3].uv.Set(metric.RightCoord, metric.TopCoord);
				Vertices_tmp[index+3].color = CurrentColor;

				index += 4;
				be.Count += 6;
				pos_x += metric.HoriAdvance - metric.HoriBearingX;
			}
			pos_y += height;
		}

		}	// End for  (int g = (shadow?0:1); g < 2; g++)

		glBufferSubData(GL_ARRAY_BUFFER, offset_in_vbo, index*sizeof(SCharVertex), &Vertices_tmp[0]);

		TextList.push_back(be);
		DrawBufferElement(be);
	}

	// ---------------------------------------- END FONT MANAGER ---------------------------------------------- //

	// ---------------------------------------- POLYGON MANAGER ----------------------------------------------- //

	//////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// CVectorManager ///////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////

	CMemoryPool GVLVertexMemoryPool("VL vertex set", CVectorLayer::ChunkSize, 4);
	CMemoryPool GVLIndexMemoryPool("VL index set", CVectorLayer::IndexChunkSize, 4);

	CVectorLayer::CVectorLayer() : Count(0), IndexCount(0)
	{
		VertexBuffer = (SVectVertex*)GVLVertexMemoryPool.Alloc();
		IndexBuffer = (u16*)GVLIndexMemoryPool.Alloc();
	}

	CVectorLayer::~CVectorLayer()
	{
		GVLIndexMemoryPool.Free(IndexBuffer);
		GVLVertexMemoryPool.Free(VertexBuffer);
	}

	void CVectorLayer::AutoFlush(float p)
	{
		if (Count > msize( float(MaxVertexCount) * p ))
		{
			Tools::GetVectorManager()->FlushLayer(*this);
		}
	}

	// Drawing nearly perfect 2D line segments in OpenGL
	// You can use this code however you want.
	// I just hope you to cite my name and the page of this technique:
	// http://artgrammer.blogspot.com/2011/05/drawing-nearly-perfect-2d-line-segments.html
	// http://www.codeproject.com/KB/openGL/gllinedraw.aspx
	//
	// Enjoy. Chris Tsang.
	void CVectorLayer::DrawLine(vect2f a, vect2f b, float w, colorf c)
	{
		if (NoSpaceForVertex(20) || NoSpaceForIndex(40)) { assert(0); return; }

		float t; float R; float f = w - static_cast<int>(w);

		// Determine parameters t, R
		if ( w>=0.0f && w<1.0f) { t=0.05f; R=0.48f+0.32f*f; c.A*=f; }
		else if ( w>=1.0f && w<2.0f) { t=0.05f+f*0.33f; R=0.768f+0.312f*f; }
		else if ( w>=2.0f && w<3.0f){ t=0.38f+f*0.58f; R=1.08f; }
		else if ( w>=3.0f && w<4.0f){ t=0.96f+f*0.48f; R=1.08f; }
		else if ( w>=4.0f && w<5.0f){ t=1.44f+f*0.46f; R=1.08f; }
		else if ( w>=5.0f && w<6.0f) { t=1.9f+f*0.6f; R=1.08f; }
		else
		{
			float ff=w-6.0f;
			t=2.5f+ff*0.50f; R=1.08f;
		}

		//determine angle of the line to horizontal
		float tx=0.0f,ty=0.0f; //core thinkness of a line
		float Rx=0.0f,Ry=0.0f; //fading edge of a line
		float cx=0.0f,cy=0.0f; //cap of a line
		float ALW=0.01f;
		float dx = b.x - a.x;
		float dy = b.y - a.y;

		if (abs(dx) < ALW)
		{
			//vertical
			tx = t; ty = 0.0f;
			Rx = R; Ry = 0.0f;
			if (w > 0.0f && w <= 1.0f) { tx = 0.5f; Rx = 0.0f; }
		}
		else if (abs(dy) < ALW)
		{
			//horizontal
			tx = 0.0f; ty = t;
			Rx = 0.0f; Ry = R;
			if (w > 0.0f && w <= 1.0f) { ty = 0.5f; Ry = 0.0f; }
		}
		else
		{
			if (w < 3.0f)
			{ //approximate to make things even faster
				float m=dy/dx;
				//and calculate tx,ty,Rx,Ry
				if (m>-0.4142f && m<=0.4142f) {
					// -22.5< angle <= 22.5, approximate to 0 (degree)
					tx=t*0.1f; ty=t;
					Rx=R*0.6f; Ry=R;
				}
				else if (m>0.4142f && m<=2.4142f) {
					// 22.5< angle <= 67.5, approximate to 45 (degree)
					tx=t*-0.7071f; ty=t*0.7071f;
					Rx=R*-0.7071f; Ry=R*0.7071f;
				}
				else if (m>2.4142f || m<=-2.4142f) {
					// 67.5 < angle <=112.5, approximate to 90 (degree)
					tx=t; ty=t*0.1f;
					Rx=R; Ry=R*0.6f;
				}
				else if (m>-2.4142f && m<-0.4142f) {
					// 112.5 < angle < 157.5, approximate to 135 (degree)
					tx=t*0.7071f; ty=t*0.7071f;
					Rx=R*0.7071f; Ry=R*0.7071f;
				}
			}
			else
			{ //calculate to exact
				dx = a.y - b.y;
				dy = b.x - a.x;
				float L = sqrt(dx*dx+dy*dy);
				dx /= L;
				dy /= L;
				cx = -dy; cy = dx;
				tx = t * dx; ty = t * dy;
				Rx = R * dx; Ry = R * dy;
			}
		}

		a.x += cx * 0.5f; a.y += cy * 0.5f;
		b.x -= cx * 0.5f; b.y -= cy * 0.5f;

		u16 i = (u16)Count;

		AddVertex( vect2f(a.x-tx-Rx-cx, a.y-ty-Ry-cy), colorf(c.R, c.G, c.B, 0.0f) ); //fading edge1
		AddVertex( vect2f(b.x-tx-Rx+cx, b.y-ty-Ry+cy), colorf(c.R, c.G, c.B, 0.0f) );
		AddVertex( vect2f(a.x-tx-cx,    a.y-ty-cy),    colorf(c.R, c.G, c.B, c.A) ); //core
		AddVertex( vect2f(b.x-tx+cx,    b.y-ty+cy),    colorf(c.R, c.G, c.B, c.A) );
		AddVertex( vect2f(a.x+tx-cx,    a.y+ty-cy),    colorf(c.R, c.G, c.B, c.A) );
		AddVertex( vect2f(b.x+tx+cx,    b.y+ty+cy),    colorf(c.R, c.G, c.B, c.A) );
		AddVertex( vect2f(a.x+tx+Rx-cx, a.y+ty+Ry-cy), colorf(c.R, c.G, c.B, 0.0f) ); //fading edge2
		AddVertex( vect2f(b.x+tx+Rx+cx, b.y+ty+Ry+cy), colorf(c.R, c.G, c.B, 0.0f) );

		AddIndex(i+0);
		AddIndex(i+1);
		AddIndex(i+2);

		AddIndex(i+1);
		AddIndex(i+2);
		AddIndex(i+3);

		AddIndex(i+2);
		AddIndex(i+3);
		AddIndex(i+4);

		AddIndex(i+3);
		AddIndex(i+4);
		AddIndex(i+5);

		AddIndex(i+4);
		AddIndex(i+5);
		AddIndex(i+6);

		AddIndex(i+5);
		AddIndex(i+6);
		AddIndex(i+7);

		// Cap
		if ( w > 3.0f)
		{
			i = (u16)Count;

			AddVertex( vect2f(a.x-tx-Rx-cx, a.y-ty-Ry-cy), colorf(c.R, c.G, c.B, 0.0f) ); //cap1
			AddVertex( vect2f(a.x-tx-Rx, a.y-ty-Ry),       colorf(c.R, c.G, c.B, 0.0f) );
			AddVertex( vect2f(a.x-tx-cx, a.y-ty-cy),       colorf(c.R, c.G, c.B, c.A) );
			AddVertex( vect2f(a.x+tx+Rx, a.y+ty+Ry),       colorf(c.R, c.G, c.B, 0.0f) );
			AddVertex( vect2f(a.x+tx-cx, a.y+ty-cy),       colorf(c.R, c.G, c.B, c.A) );
			AddVertex( vect2f(a.x+tx+Rx-cx, a.y+ty+Ry-cy), colorf(c.R, c.G, c.B, 0.0f) );

			AddIndex(i+0);
			AddIndex(i+1);
			AddIndex(i+2);

			AddIndex(i+1);
			AddIndex(i+2);
			AddIndex(i+3);

			AddIndex(i+2);
			AddIndex(i+3);
			AddIndex(i+4);

			AddIndex(i+3);
			AddIndex(i+4);
			AddIndex(i+5);

			i = (u16)Count;

			AddVertex( vect2f(b.x-tx-Rx+cx, b.y-ty-Ry+cy), colorf(c.R,c.G,c.B, 0) ); //cap2
			AddVertex( vect2f(b.x-tx-Rx, b.y-ty-Ry),       colorf(c.R,c.G,c.B, 0) );
			AddVertex( vect2f(b.x-tx+cx, b.y-ty+cy),       colorf(c.R,c.G,c.B, c.A) );
			AddVertex( vect2f(b.x+tx+Rx, b.y+ty+Ry),       colorf(c.R,c.G,c.B, 0) );
			AddVertex( vect2f(b.x+tx+cx, b.y+ty+cy),       colorf(c.R,c.G,c.B, c.A) );
			AddVertex( vect2f(b.x+tx+Rx+cx, b.y+ty+Ry+cy), colorf(c.R,c.G,c.B, 0) );

			AddIndex(i+0);
			AddIndex(i+1);
			AddIndex(i+2);

			AddIndex(i+1);
			AddIndex(i+2);
			AddIndex(i+3);

			AddIndex(i+2);
			AddIndex(i+3);
			AddIndex(i+4);

			AddIndex(i+3);
			AddIndex(i+4);
			AddIndex(i+5);
		}

	}

	void CVectorLayer::DrawFillRectangle(const rectf& rect, const colorf& colorLT, const colorf& colorRT,  const colorf& colorRB,  const colorf& colorLB)
	{
		if (NoSpaceForVertex(4) || NoSpaceForIndex(6)) { assert(0); return; }

		u16 i = (u16)Count;

		AddVertex( rect.LeftTop(),		colorLT );
		AddVertex( rect.RightTop(),		colorRT );
		AddVertex( rect.RightBottom(),	colorRB );
		AddVertex( rect.LeftBottom(),	colorLB );

		AddIndex(i+0);
		AddIndex(i+1);
		AddIndex(i+2);
		AddIndex(i+2);
		AddIndex(i+3);
		AddIndex(i+0);
	}

	void CVectorLayer::DrawFillRectangle(const SPoints4& points, const colorf& colorA, const colorf& colorB, const colorf& colorC, const colorf& colorD)
	{
		if (NoSpaceForVertex(4) || NoSpaceForIndex(6)) { assert(0); return; }

		u16 i = (u16)Count;

		AddVertex( points.p1, colorA );
		AddVertex( points.p2, colorB );
		AddVertex( points.p3, colorC );
		AddVertex( points.p4, colorD );

		AddIndex(i+0);
		AddIndex(i+1);
		AddIndex(i+2);
		AddIndex(i+2);
		AddIndex(i+3);
		AddIndex(i+0);
	}

	vect2f DrawBuffer[8];
	const vect2f StarPattern[7] = { vect2f(1.0f - 0.131f, 1.0f - 0.075f), vect2f(1.0f - 0.400f, 1.0f - 0.680f), vect2f(1.0f - 0.500f, 1.0f - 0.926f), vect2f(1.0f - 0.620f, 1.0f - 0.680f), vect2f(1.0f - 0.869f, 1.0f - 0.075f), vect2f(1.0f - 0.068f, 1.0f - 0.680f), vect2f(1.0f - 0.932f, 1.0f - 0.680f) };
	const msize StarPatternIB[9] = { 0, 1, 6, 4, 3, 5, 1, 2, 3 };
	const vect2f CrossPattern[8] = { vect2f(0.076f, 0.253f), vect2f(0.747f, 0.924f), vect2f(0.924f, 0.747f), vect2f(0.253f, 0.076f), vect2f(0.924f, 0.253f), vect2f(0.253f, 0.924f), vect2f(0.076f, 0.747f), vect2f(0.747f, 0.076f) };
	const msize CrossPatternIB[12] = { 0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4 };

	void CVectorLayer::DrawStar(const rectf& r, const colorf& color)
	{
		if (NoSpaceForVertex(9) || NoSpaceForIndex(9)) { assert(0); return; }

		vect2f pos = r.LeftTop();
		vect2f scale(r.GetWidth(), r.GetHeight());
		int i = 7;
		// przeskalowanie
		while (i--) { DrawBuffer[i] = pos + StarPattern[i] * scale; }
		// Wyswietlenie
		i = 9;
		while (i--) { AddVertexAndIndex( DrawBuffer[StarPatternIB[i]], color ); }
	}

	void CVectorLayer::DrawCross(const rectf& r, const colorf& color)
	{
		if (NoSpaceForVertex(12) || NoSpaceForIndex(12)) { assert(0); return; }

		vect2f pos = r.LeftTop();
		vect2f scale(r.GetWidth(), r.GetHeight());
		int i = 8;
		// przeskalowanie
		while (i--) { DrawBuffer[i] = pos + CrossPattern[i] * scale; }
		// Wyswietlenie
		i = 12;
		while (i--) { AddVertexAndIndex( DrawBuffer[CrossPatternIB[i]], color ); }
	}


	void CVectorLayer::DrawFullRectangle(rectf r, float bsize, const colorf& bcolor, const colorf& fill)
	{
		if (NoSpaceForVertex(12) || NoSpaceForIndex(30)) { assert(0); return; }

		u16 i = (u16)Count;

		//
		// *p0                              *p1
		//
		//     *p4                     *p5
		//
		//     *p7                     *p6
		//
		// *p3                              *p2
		//

		AddVertex( r.LeftTop(),		bcolor );
		AddVertex( r.RightTop(),	bcolor );
		AddVertex( r.RightBottom(),	bcolor );
		AddVertex( r.LeftBottom(),	bcolor );

		r.Expand(-bsize);

		AddVertex( r.LeftTop(),		bcolor );
		AddVertex( r.RightTop(),	bcolor );
		AddVertex( r.RightBottom(),	bcolor );
		AddVertex( r.LeftBottom(),	bcolor );

		AddVertex( r.LeftTop(),		fill );
		AddVertex( r.RightTop(),	fill );
		AddVertex( r.RightBottom(),	fill );
		AddVertex( r.LeftBottom(),	fill );

		// top
		AddIndex(i+0);
		AddIndex(i+1);
		AddIndex(i+4);
		AddIndex(i+1);
		AddIndex(i+5);
		AddIndex(i+4);

		// right
		AddIndex(i+5);
		AddIndex(i+1);
		AddIndex(i+2);
		AddIndex(i+5);
		AddIndex(i+2);
		AddIndex(i+6);

		// bottom
		AddIndex(i+6);
		AddIndex(i+2);
		AddIndex(i+3);
		AddIndex(i+3);
		AddIndex(i+7);
		AddIndex(i+6);

		// left
		AddIndex(i+3);
		AddIndex(i+0);
		AddIndex(i+4);
		AddIndex(i+4);
		AddIndex(i+7);
		AddIndex(i+3);

		// center
		AddIndex(i+8);
		AddIndex(i+9);
		AddIndex(i+10);
		AddIndex(i+10);
		AddIndex(i+11);
		AddIndex(i+8);

	}

	void CVectorLayer::DrawFillRoundRectangle(const rectf& r, float cs, int steps, const colorf& color)
	{
		rectf nr(r);
		nr.Expand(-cs);

		DrawFillRectangle(nr, color);	// center

		DrawFillRectangle( rectf(r.left, nr.top, nr.left, nr.bottom), color );		// left
		DrawFillRectangle( rectf(nr.left, r.top, nr.right, nr.top), color );			// top
		DrawFillRectangle( rectf(nr.right, nr.top, r.right, nr.bottom), color );		// right
		DrawFillRectangle( rectf(nr.left, nr.bottom, nr.right, r.bottom), color );	// bottom

		const vect2f nrcorr[] = { nr.LeftTop(), nr.RightTop(), nr.RightBottom(), nr.LeftBottom() };
		const float xarr[] = { -1, 1, 1, -1 };
		const float yarr[] = { -1, -1, 1, 1 };



		for (int g = 0; g < 4; g++)
		{
			vect2f I1 = nrcorr[g];
			float delta = HALFPI / steps;
			vect2f last = I1 + vect2f(xarr[g] * cs, 0);

			int i = 1;
			float w = delta;
			for( ; i <= steps; w += delta, ++i )
			{
				const float x = xarr[g] * cos(w);
				const float y = yarr[g] * sin(w);
				vect2f H = I1 + vect2f(x, y) * cs;

				DrawFillTriangle(last, H, I1, color);

				last = H;
			}
		}
	}

	// Wzor pochodzi ze strony http://forum.gamedev.pl/index.php/topic,7136.0.html
	void CVectorLayer::DrawCircle(const circlef& circle, const colorf& color, float eps)
	{
		float theta = powf(12 * eps / (circle.r * circle.r), 1.0f/3.0f);
		int p = (int)(6.283f / theta) + 1;

		if (NoSpaceForVertex(p*3) || NoSpaceForIndex(p*3)) { assert(0); return; }

		float d = TWOPI / p;
		vect2f pos( circle.pos.x + (circle.r*sin(d*p)), circle.pos.y + (circle.r*cos(d*p)) );
		vect2f prevpos = pos;

		while (p--)
		{
			pos.Set( circle.pos.x + (circle.r*sin(d*p)), circle.pos.y + (circle.r*cos(d*p)) );

			AddVertexAndIndex(circle.pos, color);
			AddVertexAndIndex(prevpos, color);
			AddVertexAndIndex(pos, color);

			prevpos = pos;
		}
	}

	void CVectorLayer::DrawFillTriangle(const vect2f& a, const vect2f& b, const vect2f& c, const colorf& colora, const colorf& colorb, const colorf& colorc)
	{
		if (NoSpaceForVertex(3) || NoSpaceForIndex(3)) { assert(0); return; }

		AddVertexAndIndex(a, colora);
		AddVertexAndIndex(b, colorb);
		AddVertexAndIndex(c, colorc);
	}

	CVectorManager::CVectorManager()
	{
		// Generujemy buforeki
		glGenBuffers( 1, &VBOVertices );
		glBindBuffer( GL_ARRAY_BUFFER, VBOVertices );
		glBufferData( GL_ARRAY_BUFFER, CVectorLayer::ChunkSize, nullptr, GL_DYNAMIC_DRAW );

		glGenBuffers(1, &VBOIndices);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIndices);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, CVectorLayer::IndexChunkSize, nullptr, GL_DYNAMIC_DRAW);
	}

	CVectorManager::~CVectorManager()
	{
		glDeleteBuffers( 1, &VBOIndices );
		glDeleteBuffers( 1, &VBOVertices );
	}

	void CVectorManager::FlushLayer(CVectorLayer& vl)
	{
		if ( vl.Count == 0 ) return;

		// Okreslamy dla VBO gdzie co sie znajduje
		glBindBuffer( GL_ARRAY_BUFFER, VBOVertices );
		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, VBOIndices );

#ifndef OMIKRON2_DEDICATED_SERVER
		DisableTexture();

		glVertexPointer( 2, GL_FLOAT, sizeof(CVectorLayer::SVectVertex), (char*)0 );
		glColorPointer( 4, GL_FLOAT, sizeof(CVectorLayer::SVectVertex), (char*)0 + sizeof(vect2f) );

		glBufferSubData( GL_ARRAY_BUFFER, 0, vl.Count * sizeof(CVectorLayer::SVectVertex), vl.VertexBuffer );
		glBufferSubData( GL_ELEMENT_ARRAY_BUFFER, 0, vl.IndexCount * sizeof(u16), vl.IndexBuffer );

		// Rederujemy dane
		//glEnableClientState( GL_VERTEX_ARRAY );
		//glEnableClientState( GL_COLOR_ARRAY );
		DoNotUseTexCoord();
			glDrawElements(GL_TRIANGLES, vl.IndexCount, GL_UNSIGNED_SHORT, 0);
		RevertTexCoord();
		//glDisableClientState( GL_COLOR_ARRAY );
		//glDisableClientState( GL_VERTEX_ARRAY );
#endif

		vl.Count = 0;
		vl.IndexCount = 0;

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// CGraphManager ///////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////

	void CGraphManager::GenLayers(msize count)
	{
		assert(count);

		Layer_v.clear();
		Layer_v.resize(count);
		CurrentLayer = &Layer_v[0];

		for (msize i =0; i < count; i++)
		{
			Layer_v[i].reserve(8);
		}
	}

	void CGraphManager::SelectLayer(msize i)
	{
		assert(i < Layer_v.size());

		CurrentLayer = &Layer_v[i];

		CurrentRectHead = nullptr;
		ActiveTexture.reset();
	}

	void CGraphManager::SelectTexture(const RefTexture& texture)
	{
		assert(texture);

		if (ActiveTexture == texture) return;

		ActiveTexture = texture;
		ActiveCoords = texture->Coords;
		W = texture->W;

		for (auto it = CurrentLayer->begin(); it != CurrentLayer->end(); ++it)
		{
			if (it->Texture->TexID == texture->TexID)
			{
				CurrentRectHead = &(*it);
				return;
			}
		}

		SRectHead tmp;
		tmp.Texture = texture;
		tmp.PloyCount = 0;

		CurrentLayer->push_back( tmp );
		CurrentRectHead = &CurrentLayer->back();
		CurrentRectHead->VertexBuffer.resize(MaxVertexCount);
	}

	CGraphManager::CGraphManager()
	{
		// Generujemy buforki
		glGenBuffers(1, &VBOVertices);
		glBindBuffer(GL_ARRAY_BUFFER, VBOVertices);
		glBufferData(GL_ARRAY_BUFFER, MaxVertexCount*sizeof(SRastVertex), nullptr, GL_DYNAMIC_DRAW);

		std::vector<u16> indices(MaxIndexCount);
		const u16 indices_size = static_cast<u16>(indices.size());
		for (u16 ii = 0, vi = 0; ii < indices_size; ii += 6, vi += 4)
		{
			indices[ii + 0] = vi + 0;
			indices[ii + 1] = vi + 1;
			indices[ii + 2] = vi + 2;
			indices[ii + 3] = vi + 2;
			indices[ii + 4] = vi + 3;
			indices[ii + 5] = vi + 0;
		}

		glGenBuffers(1, &VBOIndices);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIndices);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, MaxIndexCount*sizeof(u16), &indices[0], GL_STATIC_DRAW);
	}

	CGraphManager::~CGraphManager()
	{
		glDeleteBuffers(1, &VBOIndices);
		glDeleteBuffers(1, &VBOVertices);
	}

	void CGraphManager::CalcPoints(const rectf& r, const vect2f& p, float angle, SPoints4& points)
	{
		float mcos = cos( angle );
		float msin = sin( angle );

		rectf rt( r.left - p.x, r.top - p.y, r.right - p.x, r.bottom - p.y );
		rectf rcos( rt.left * mcos, rt.top * mcos, rt.right * mcos, rt.bottom * mcos );
		rectf rsin( rt.left * msin, rt.top * msin, rt.right * msin, rt.bottom * msin );

		points.p1.Set( ( rsin.top + rcos.left ) + p.x,		( rcos.top - rsin.left ) + p.y );
		points.p2.Set( ( rsin.top + rcos.right ) + p.x,		( rcos.top - rsin.right ) + p.y );
		points.p3.Set( ( rsin.bottom + rcos.right ) + p.x,	( rcos.bottom - rsin.right ) + p.y );
		points.p4.Set( ( rsin.bottom + rcos.left ) + p.x,	( rcos.bottom - rsin.left ) + p.y );
	}

	void CGraphManager::DrawPoints(const SPoints4& points, const rectf& coord, const colorf& color)
	{
		assert( CurrentRectHead );
		assert( CurrentRectHead->PloyCount < MaxPloyCount );
		// Dodajemy do bufora vertexy
		msize tmpv = CurrentRectHead->PloyCount * 4;

		float atx = ActiveCoords.GetWidth();
		float aty = ActiveCoords.GetHeight();

		SRastVertex* buf = &(CurrentRectHead->VertexBuffer[0]);

		// p1
		buf[tmpv  ].pos = points.p1;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left + coord.left * atx, ActiveCoords.top + coord.bottom * aty);

		// p2
		buf[tmpv  ].pos = points.p2;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left + coord.right * atx, ActiveCoords.top + coord.bottom * aty);

		// p3
		buf[tmpv  ].pos = points.p3;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left + coord.right * atx, ActiveCoords.top + coord.top * aty);

		// p4
		buf[tmpv  ].pos = points.p4;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left + coord.left * atx, ActiveCoords.top + coord.top * aty);

		CurrentRectHead->PloyCount++;
	}

	void CGraphManager::DrawRaw(SRastVertex* table, msize num)
	{
		assert(CurrentRectHead);
		assert(table);
		assert((num % 4) == 0);

		SRastVertex* begin = CurrentRectHead->VertexBuffer.data() + (CurrentRectHead->PloyCount * 4);
		SRastVertex* end = begin + num;

		for ( ; begin != end; ++begin, ++table)
		{
			*begin = *table;
		}

		CurrentRectHead->PloyCount += num / 4;
	}

	void CGraphManager::DrawPoints(const SPoints4& points, const colorf& color)
	{
		assert( CurrentRectHead );
		assert( CurrentRectHead->PloyCount < MaxPloyCount );

		// Dodajemy do bufora vertexy
		msize tmpv = CurrentRectHead->PloyCount * 4;

		float atx = ActiveCoords.GetWidth();
		float aty = ActiveCoords.GetHeight();

		SRastVertex* buf = CurrentRectHead->VertexBuffer.data();

		// p1
		buf[tmpv  ].pos = points.p1;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left, ActiveCoords.top + aty);

		// p2
		buf[tmpv  ].pos = points.p2;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left + atx, ActiveCoords.top + aty);

		// p3
		buf[tmpv  ].pos = points.p3;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left + atx, ActiveCoords.top);

		// p4
		buf[tmpv  ].pos = points.p4;
		buf[tmpv  ].color = color;
		buf[tmpv++].uv.Set(ActiveCoords.left, ActiveCoords.top);

		CurrentRectHead->PloyCount++;
	}

	void CGraphManager::FrameCheckForUnusedTextures()
	{
		for (auto& layer : Layer_v)
		{
			layer.erase( std::remove_if(layer.begin(), layer.end(), 
				[](SRectHead& head)
				{
					return (++(head.Frame)) > 100;
				}
			), layer.end() );
		}
	}

	void CGraphManager::FlushLayer(msize i, EBlendMode bm)
	{
#ifndef OMIKRON2_DEDICATED_SERVER
		assert(i<Layer_v.size());

		EnableTexture();

		// Okreslamy dla VBO gdzie co sie znajduje
		glBindBuffer(GL_ARRAY_BUFFER, VBOVertices);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIndices);

		for (auto it = Layer_v[i].begin(); it != Layer_v[i].end(); ++it)
		{
			if (it->PloyCount == 0) continue;
			it->Frame = 0;
			it->Texture->Bind();

			glBufferSubData( GL_ARRAY_BUFFER, 0, it->PloyCount*4*sizeof(SRastVertex), &(it->VertexBuffer[0]) );

			glVertexPointer(2, GL_FLOAT, sizeof(SRastVertex), (char*)0);
			glColorPointer(4, GL_FLOAT, sizeof(SRastVertex), (char*)0 + sizeof(vect2f));
			glTexCoordPointer(2, GL_FLOAT, sizeof(SRastVertex), (char*)0 + sizeof(vect2f)+sizeof(colorf));

			// Rederujemy dane
			//glEnableClientState( GL_VERTEX_ARRAY );
			//glEnableClientState( GL_COLOR_ARRAY );
			//glEnableClientState( GL_TEXTURE_COORD_ARRAY );

				SetBlendMode( bm );
					glDrawElements(GL_TRIANGLES, it->PloyCount * 6, GL_UNSIGNED_SHORT, 0);
				SetBlendMode( BM_NORMAL );

			//glDisableClientState( GL_TEXTURE_COORD_ARRAY );
			//glDisableClientState( GL_COLOR_ARRAY );
			//glDisableClientState( GL_VERTEX_ARRAY );

			it->PloyCount = 0;
		}
#endif
	}

} // End oo
