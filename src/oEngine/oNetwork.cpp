﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oNetwork.hpp"

using boost::asio::ip::tcp;

namespace oo
{
	boost::asio::io_service GIOService;

	// Do wysylania danych
	CBuffer GNetworkBuffer(MAX_BUFFER_SIZE);

	std::shared_ptr<Acceptor_t> GAcceptor;		// Akceptuje nowe polaczenia

	std::vector<tstring> GBlackList;			// Czarna lista, maski adresow ktore nie beda dopuszczane do gry

	void RecvDataFromClient(const RefNet& scache);
	void RecvDataFromServer(const RefNet& scache);

	/////////////////////
	/////////////////////
	/////////////////////

	std::vector<RefNet> GConnections;
	RefNet GClientStream;

	void AddConnection(const RefNet& connection)
	{
		assert( std::find(GConnections.begin(), GConnections.end(), connection) == GConnections.end() );
		GConnections.push_back(connection);
	}

	void RemoveClient(const RefNet& connection)
	{
		auto it = std::find(GConnections.begin(), GConnections.end(), connection);
		if (it != GConnections.end())
		{
			GConnections.erase(it);
		}
	}

	/////////////////////
	/////////////////////
	/////////////////////

	double PingPongTime = 0.0;

	void Ping()
	{
		PingPongTime = timGetTime();
	}

	float Pong()
	{
		double NewTime = timGetTime();
		return static_cast<float>(NewTime - PingPongTime);
	}

	inline tstring ErrorCodeToString(const boost::system::error_code& ec)
	{
		return format("%s (%i)", ec.message(), ec.value());
	}

	void LogMsgNetwork(const tstring& message, const boost::system::error_code& ec)
	{
		LogMsg("[ERR] [NET] " + message);
		LogMsg("[ERR] [NET] " + ErrorCodeToString(ec));
	}

	void Shutdown(const RefNet& connection, tcp::socket::shutdown_type what)
	{
		boost::system::error_code ec;
		connection->Socket.shutdown(what, ec);
		if (ec) LogMsgNetwork("client->shutdown()", ec);
	}

	bool IsServer()
	{
		return GAcceptor != nullptr;
	}

	bool IsConnectionActive()
	{
		return GClientStream && GClientStream->Socket.is_open();
	}

	void SetupNewConnection(const RefNet& connection)
	{
		boost::system::error_code error;

		// Sa wolne miejsca
		if (GConnections.size() < MAX_CONNECTIONS)
		{
			std::string ipv4str = connection->Socket.remote_endpoint().address().to_string();
			LogMsg( "Poczaczono z: " + ipv4str );

			connection->Socket.set_option( tcp::no_delay(true), error);
			if (error) LogMsgNetwork("client->set_option(no_delay)", error);

			AddConnection(connection);
			RecvDataFromClient(connection);
		}
		else // Brak wolnych miejsc
		{
			Shutdown(connection, tcp::socket::shutdown_both);
		}
	}

	void AcceptNext()
	{
		assert(GAcceptor);

		auto connection_new = std::make_shared<CNet>(GIOService);
		GAcceptor->async_accept(connection_new->Socket,
			[connection_new](const boost::system::error_code& error)
			{
				if (GAcceptor && GAcceptor->is_open())
				{
					if (!error) SetupNewConnection(connection_new);
					else LogMsgNetwork("Nie mozna zaakceptowac nowego klienta", error);

					AcceptNext();
				}
			}
		);

	}

	inline std::vector<int> GenIdBufferTab(int n)
	{
		std::vector<int> r;
		for (int i = 0; i < n; i++) { r.push_back(i); }
		return r;
	}

	bool StartAcceptingSockets(unsigned short port)
	{
		try
		{
			GAcceptor = std::make_shared<Acceptor_t>(GIOService);
			tcp::endpoint endpoint(tcp::v4(), port);
			GAcceptor->open(endpoint.protocol());
			GAcceptor->bind(endpoint);
			GAcceptor->listen();
		}
		catch (const boost::system::system_error& error)
		{
			GAcceptor.reset();
			LogMsg("Nie mozna utworzyc akceptora");
			LogMsg(error.what());
			return false;
		}

		AcceptNext();

		return true;
	}

	void StopAcceptingSockets()
	{
		GAcceptor.reset();
	}

	void MoveDataInBuffer(CBuffer& workbuffer, msize workpos, msize pkgsize)
	{
		workbuffer.SetPos(0, IStream::SP_BEG);
		u8* ndata = (u8*)workbuffer.GetPtr() + pkgsize;
		msize s = workpos - pkgsize;
		msize n = workbuffer.Write(ndata, s);
		assert(n == s); // Upewniamy sie, ze wszystko zapisalismy
	}

	bool CheckHeader(SHeaderPacket* header)
	{
		// Zrobic lepsza metode weryfikowania pakietow!
		/*
		if (header == 0) return false;
		if (header->Type > SCall_LAST) return false;
		if (header->Size > 2048) return false;
		*/

		return true;
	}

	void RecvData(const RefNet& scache, ProcessPacket_t callback)
	{
		while ( scache->WorkBuffer.GetPos() != 0 )
		{
			u8* workbufferptr = (u8*)scache->WorkBuffer.GetPtr();
			msize workpos = scache->WorkBuffer.GetPos();
			SHeaderPacket* hp = (SHeaderPacket*)workbufferptr;

			if (workpos >= HP_SIZE)	// Pobralismy wystarczajaco danych aby zinterpretowac naglowek?
			{
				// Sprawdzamy poprawnosc naglowka
				if (!CheckHeader(hp))
				{
					// Naglowek ktory otrzymalismy nie jest prawidlowy
	#ifdef OMIKRON2_DEDICATED_SERVER
					DisconnectByContext(scache, DR_COMMUNICATION_ERROR);
	#else
					DisconnectFromServerAndResetGame();
	#endif
					return;
				}

				// Zapisujemy naglowek do scache
				scache->HeaderPacket = *hp;
				msize pkgsize = HP_SIZE + hp->Size;

				if (pkgsize != HP_SIZE) // Czy naglowek zawiera jakies dane?
				{
					if (workpos >= pkgsize) // Pobralismy dane do tego naglowka? (wiemy, ze sa jakies dane)
					{
						// Kopiujemy dane (pomijajac dane naglowka) do bufora roboczego
						assert(scache->Buffer.GetPos() == 0);
						int n = scache->Buffer.Write(workbufferptr + HP_SIZE, hp->Size);
						assert(n == hp->Size); // Upewniamy sie, ze wszystko zapisalismy

						// Przesowamy reszte danych (te ktore sa poza naglowkiem i jego danymi) na poczatek bufora
						MoveDataInBuffer(scache->WorkBuffer, workpos, pkgsize);

						// Pobralismy wszystkie dane do naglowka. Jestesmy gotowi do przetwarzania pakietu
					}
					else return; // Naglowek twierdzi, ze sa dane jednak my tych danych jeszcze nie mamy. Wychodzimy i czekamy na kolejne OnServer
				}
				else
				{
					// Przesowamy reszte danych (te ktore sa poza naglowkiem) na poczatek bufora
					MoveDataInBuffer(scache->WorkBuffer, workpos, HP_SIZE);
				}
			}
			else return; // Brak naglowka. Wychodzimy i czekamy na kolejne OnServer

			// Przetwarzamy pakiet zaladowany w scache
			callback(scache);
		}
	}

	void RecvDataFromClient(const RefNet& scache)
	{
		auto& buff = scache->WorkBuffer;

		void* data_begin = buff.GetPtr() + buff.GetPos();
		msize max_to_read = MAX_BUFFER_SIZE - buff.GetPos();

		scache->Socket.async_read_some(boost::asio::buffer(data_begin, max_to_read),
			[=](const boost::system::error_code& error, size_t bytes_transferred)
			{
				if (!error)
				{
					// Po odbieraniu danych do naszego bufora musimy ustawic poprawnie kursor
					// Bo ASIO samo tego nie zrobi...
					scache->WorkBuffer.SetPos(bytes_transferred, IStream::SP_CUR);

					// Informujemy klienta o tym, ze jest nowa porcja danych do przetworzenia
					RecvData(scache, ProcessPacketOnServer);

					// Powtarzamy cykl odbierania danych
					RecvDataFromClient(scache);
				}
				else
				{
					if (error == boost::asio::error::eof)
					{
						DisconnectByContext(scache, DR_BYE);
					}
					else
					{
						LogMsgNetwork("Wystapil blad przy odbieraniu danych od klienta", error);
						DisconnectByContext(scache, DR_COMMUNICATION_ERROR);
					}
				}
			}
		);
	}

	void RecvDataFromServer(const RefNet& scache)
	{
		auto& buff = scache->WorkBuffer;

		void* data_begin = buff.GetPtr() + buff.GetPos();
		msize max_to_read = MAX_BUFFER_SIZE - buff.GetPos();

		scache->Socket.async_read_some(boost::asio::buffer(data_begin, max_to_read),
			[=](const boost::system::error_code& error, size_t bytes_transferred)
			{
				if (!error)
				{
					// Po odbieraniu danych do naszego bufora musimy ustawic poprawnie kursor
					// Bo asio sam tego nie zrobi...
					scache->WorkBuffer.SetPos(bytes_transferred, IStream::SP_CUR);

					// Informujemy klienta o tym, ze jest nowa porcja danych do przetworzenia
					RecvData(scache, ProcessPacketOnNetwork);

					// Powtarzamy cykl odbierania danych
					RecvDataFromServer(scache);
				}
				else
				{
					if (error == boost::asio::error::eof)
					{
						LogMsg("Polaczenie z serwerem zerwane");
					}
					else
					{
						LogMsgNetwork("Blad przy odbieraniu danych", error);
					}

					DisconnectFromServerAndResetGame();
				}
			}
		);
	}

	void ConnectToServerAsync(const tstring& address, unsigned short port, OnAsyncConnectSuccess_t success, OnAsyncConnectFail_t fail)
	{
		assert(success);
		assert(fail);

		if (GClientStream)
		{
			LogMsg("[NET] [WAR] Aktualnie trwa laczenie z innym serwerem");
			return;
		}

		tcp::resolver resolver(GIOService);
		tcp::resolver::query query(address, ToString(port));
		auto it = resolver.resolve(query);

		auto new_connection = std::make_shared<CNet>(GIOService);
		boost::asio::async_connect(new_connection->Socket, it,
			[=](const boost::system::error_code& error, boost::asio::ip::tcp::resolver::iterator it)
			{
				if (!error)
				{
					boost::system::error_code ec;
					new_connection->Socket.set_option( tcp::no_delay(true), ec);
					if (!!ec) LogMsgNetwork("client->set_option(no_delay)", ec);

					GClientStream = new_connection;
					success();

					RecvDataFromServer(new_connection);
				}
				else
				{
					fail(ErrorCodeToString(error));
				}
			}
		);
	}

	void DisconnectClient(const RefNet& connection)
	{
		Shutdown(connection, tcp::socket::shutdown_send);
		RemoveClient(connection);
	}

	void DisconnectAllClients()
	{
		for (auto& scache : GConnections)
		{
			DisconnectClient(scache);
		}
	}

	void DisconnectFromServer()
	{
		if (GClientStream)
		{
			Shutdown(GClientStream, tcp::socket::shutdown_send);
			GClientStream.reset();
		}
	}

	void SendData(const RefNet& connection)
	{
		assert(connection->SendBuffer.empty());
		std::swap(connection->SendBuffer, connection->WorkSendBuffer);

		boost::asio::async_write(connection->Socket, boost::asio::buffer(connection->SendBuffer),
			[=](const boost::system::error_code& error, size_t bytes_transferred)
			{
				if (!!error)
				{
					LogMsgNetwork("SendData", error);
					LogMsg("Status wysylania do %i/%i bajtow", bytes_transferred, connection->SendBuffer.size());
				}

				connection->SendBuffer.clear();
			}
		);
	}

	void NetworkTrySendAwatingData()
	{
#ifdef OMIKRON2_DEDICATED_SERVER
		for (auto& scache : GConnections)
		{
			if (scache->WorkSendBuffer.empty()) return;
			if (scache->SendBuffer.empty())
			{
				SendData(scache);
			}
		}
#else
		if (GClientStream)
		{
			if (GClientStream->WorkSendBuffer.empty()) return;
			if (GClientStream->SendBuffer.empty())
			{
				SendData(GClientStream);
			}
		}
#endif
	}

	void UpdateNetworkSystem()
	{
		NetworkTrySendAwatingData();

		boost::system::error_code error;
		GIOService.poll(error);
		if (error) LogMsgNetwork("Pool error", error);
	}

	void SendData(const void* data, msize size)
	{
		if (size == 0) return;

		const u8* b = static_cast<const u8*>(data);
		const u8* e = b + size;
		GClientStream->WorkSendBuffer.insert(GClientStream->WorkSendBuffer.end(), b, e);
	}

	void ServerSend(const RefNet& scache, const void* data, msize size)
	{
		if (size == 0) return;

		const u8* b = static_cast<const u8*>(data);
		const u8* e = b + size;
		scache->WorkSendBuffer.insert(scache->WorkSendBuffer.end(), b, e);
	}

} // End oo
