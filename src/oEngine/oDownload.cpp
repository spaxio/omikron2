﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oDownload.hpp"

#include <boost/algorithm/string.hpp>

using boost::asio::ip::tcp;

namespace oo
{

	enum EDownloadError
	{
		DE_OK = 0,
		DE_BAD_ADDRESS,
		DE_CONNECT_ERROR,
		DE_SEND_ERROR,
		DE_RECV_ERROR,
		DE_HTTP_ERROR,
		DE_BAD_FILE_SIZE,
		DE_TERMINATE
	};

	const char* GDownloadErrorList[] =
	{
		"DE_OK",
		"DE_BAD_ADDRESS",
		"DE_CONNECT_ERROR",
		"DE_SEND_ERROR",
		"DE_RECV_ERROR",
		"DE_HTTP_ERROR",
		"DE_BAD_FILE_SIZE"
		"DE_TERMINATE"
	};

	class CDownload;
	typedef std::shared_ptr<CDownload> RefDownload;

	std::atomic<bool> TermianteAllTasks(false);

	void DoWork(std::string host, std::string file, int port, CDownload* listener);

	class CDownload
	{
		private: // Obiekty CDownload nie sa do kopiowania
			CDownload( const CDownload& );
			CDownload& operator=( const CDownload& );

		private:
			std::vector<u8> File;
			std::string MimeType;

			int ToDownload;
			EDownloadError Status;

			tcp::socket* socket;

			std::mutex DownloadMutex;
			std::thread DownloadThread;

		public:
			CDownload(const std::string& host, const std::string& file)
				: ToDownload(-1),
				  Status(DE_OK),
				  socket(nullptr),
				  DownloadThread(&DoWork, host, file, 80, this)
			{
			}

			~CDownload() 
			{ 
				TerminateActiveSocket();
				DownloadThread.join(); 
			}

			void SetActiveSocket(tcp::socket* s)
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				socket = s;
			}

			void TerminateActiveSocket()
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				if (socket) 
				{
					boost::system::error_code error;
					socket->close(error);
				}
			}

			const char* GetError()
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				return GDownloadErrorList[Status];
			}

			void SetStatus(EDownloadError status)
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				Status = status;
			}

			void SetMimeType(const std::string& mimetype)
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				MimeType = mimetype;
			}

			std::string GetMimeType()
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				return MimeType;
			}

			bool GetDownloadProgress(std::pair<int, int>& progress)
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);

				progress = std::pair<int, int>(File.size(), ToDownload);
				return (Status == DE_OK);
			}

			void SetDownloadSize(int size)
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				ToDownload = size;
			}

			template <typename IT>
			void AddData(const IT& beg_it, const IT& end_it)
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				File.insert(File.end(), beg_it, end_it);
			}

			std::vector<u8> MoveFile()
			{
				std::lock_guard<std::mutex> lock(DownloadMutex);
				std::vector<u8> result;
				std::swap(File, result);

				return result;
			}
	};

	std::string StripValueFromResponse(const std::string& response, const char* name)
	{
		size_t beg = response.find(name);
		size_t end = response.find("\r\n", beg);

		if (beg != std::string::npos && end != std::string::npos)
		{
			beg += strlen(name);
			return response.substr(beg, end - beg);
		}

		return std::string();
	}

	EDownloadError GetResponse(tcp::socket& socket, CDownload* listener)
	{
		assert(listener);

		std::string header;
		header.reserve(256);

		std::array<char, 2048> buffer;

		bool getheader = true;
		int todownload = -1;

		boost::system::error_code error;

		for (;;)
		{
			if (TermianteAllTasks) return DE_TERMINATE;

			size_t result = socket.read_some(boost::asio::buffer(buffer), error);
			if (error == boost::asio::error::eof)
			{
				break;
			}
			else if (error)
			{
				return DE_RECV_ERROR;
			}

			if (getheader)
			{
				// Pobieramy naglowek
				header.insert(header.end(), buffer.begin(), buffer.begin() + result);
				size_t off = header.find("\r\n\r\n");
				if (off != std::string::npos)
				{
					if (header.find("200 OK") == std::string::npos)
					{
						return DE_HTTP_ERROR;
					}

					std::string hcopy = boost::to_lower_copy(header);

					std::string content_length = StripValueFromResponse(hcopy, "content-length: ");
					if (content_length.empty() || TryParse(content_length, todownload) == false)
					{
						return DE_BAD_FILE_SIZE;
					}

					std::string content_type = StripValueFromResponse(hcopy, "content-type: ");
					if (content_type.empty() == false)
					{
						listener->SetMimeType(content_type);
					}

					listener->SetDownloadSize(todownload);
					listener->AddData(header.begin() + off + 4, header.end());

					// Znalezlismy koniec naglowka
					getheader = false;
				}
			}
			else
			{
				// Pobieramy dane wlasciwe
				listener->AddData(buffer.begin(), buffer.begin() + result);
			}
		}

		return DE_OK;
	}

	EDownloadError DoWinsock(const std::string& host, const std::string& file, const std::string& service, CDownload* listener)
	{
		boost::system::error_code error;

		tcp::resolver resolver(GIOService);
		tcp::resolver::query query(host, service);

		tcp::resolver::iterator endpoint_it = resolver.resolve(query, error);
		if (error) return DE_BAD_ADDRESS;

		tcp::socket socket(GIOService);

		listener->SetActiveSocket(&socket);
		boost::asio::connect(socket, endpoint_it, error);
		listener->SetActiveSocket(nullptr);

		if (error) return DE_CONNECT_ERROR;

		std::string request = "GET " + file + " HTTP/1.0\r\n"
							  "Host: " + host + "\r\n"
							  "Accept: */*\r\n"
							  "Connection: close\r\n\r\n";

		boost::asio::write(socket, boost::asio::buffer(request), error);
		if (!error)
		{
			return GetResponse(socket, listener);
		}
		else return DE_SEND_ERROR;
	}

	void DoWork(std::string host, std::string file, int port, CDownload* listener)
	{
		EDownloadError result = DoWinsock(host, file, ToString(port), listener);
		listener->SetStatus(result);
	}

	std::vector< std::pair<DownloadFileAsync, RefDownload> > DownloadTaskList;

	void DownloadFileAsync::Start()
	{
		DownloadTaskList.push_back(
			std::make_pair(
				std::move(*this),
				std::make_shared<CDownload>(Server, File)
			)
		);
	}

	void DownloadDrop() 
	{
		TermianteAllTasks = true; 
		DownloadTaskList.clear();
	}

	bool DoAndCheck(const std::pair<DownloadFileAsync, RefDownload>& item)
	{
		std::pair<int, int> progress;
		if (item.second->GetDownloadProgress(progress))
		{
			if (progress.first == progress.second)
			{
				// Gotowe!
				item.first.SuccessFunc(
					std::make_shared<CBuffer>(item.second->MoveFile()),
					item.second->GetMimeType()
				);

				return true;
			}
			else
			{
				if (item.first.ProgressFunc)
				{
					bool can_continue = item.first.ProgressFunc(progress.first, progress.second);

					if (!can_continue)
					{
						// Anuluj zadanie
					}
				}

				return false;
			}
		}
		else
		{
			if (item.first.FailFunc)
			{
				item.first.FailFunc(item.second->GetError(), "");
			}

			return true;
		}
	}

	void CheckDownloadTaskList()
	{
		// Przejdziemy po wszystkich elementach i wywalimy niepotrzebne
		remove_erase_if(DownloadTaskList, DoAndCheck);
	}

	EUpdateStatus DownloadState = US_IDLE;
	RefDownload ActiveFile;
	std::pair<int, int> ActiveFileProgress;

	int TmpDownloaded = 0;
	int FullSizeDownloaded = 0;
	int FullSizeToDownload = 0;

	int GetFullToDownload() { return FullSizeToDownload; }
	int GetFullDownloaded() { return FullSizeDownloaded; }
	EUpdateStatus GetUpdateStatus() { return DownloadState; }

	struct SFileToUpdate
	{
		tstring file;
		u32 hash;
		int size;
	};

	std::vector<SFileToUpdate> UpdateFileList;
	std::vector<SFileToUpdate>::iterator ListIt;

	inline void SetUpdateFileList( std::vector<SFileToUpdate>& list )
	{
		UpdateFileList = list;
		ListIt = UpdateFileList.begin();
	}

	const char* PARSE_ERROR_SCRIPT = "Zla zawartosc pliku index.sx2";

	bool SetupDownloadListByScript(const tstring& script)
	{
		// Etap parsowania
		try
		{
			CSxNode mainnode;
			CSxHigReader::LoadNodeFromString(&mainnode, script);

			CSxHigReader reader;
			reader.SetDataBase(&mainnode);

			reader["list"];
			if (reader.GetNumValues() == 1) // Pojedyncza wartosc okresla brak aktualizacji
			{
				UpdateFileList.clear();
				ListIt = UpdateFileList.end();
				FullSizeToDownload = 0;
				return true;
			}

			if (reader.GetNumValues() % 3 != 0) throw omikron_exception(PARSE_ERROR_SCRIPT);

			for (msize x = 0; x < reader.GetNumValues(); x += 3)
			{
				SFileToUpdate toupdate;
				tstring hexstr;
				reader >> toupdate.file >> hexstr >> toupdate.size;

				if (ParseHexStr(hexstr.c_str(), toupdate.hash) == false) throw omikron_exception(PARSE_ERROR_SCRIPT);

				UpdateFileList.push_back(toupdate);
			}
		}
		catch (omikron_exception& e)
		{
			LogMsg( "[ERR] [GAME] Nie udalo sie wczytac pliku index.sx2 odpowiedzialnego za aktualizacje gry - " + e.str() );
			return false;
		}

		tstring path = GetExeDirectory();

		std::vector<SFileToUpdate> newlist;

		tstring exefile = GetExeName(true);

		// Etap porowniania
		for (msize i = 0; i < UpdateFileList.size(); i++)
		{
			tstring fname = UpdateFileList[i].file;
			if (fname == exefile) continue;	// Glowny plik wykonywalny ma byc jako 'module' olewamy jak jest jako zwykly plik
			else if (fname == "module") fname = exefile;

			CFile file(path + "/" + fname, CFile::OM_READ);
			u32 hash = GetCRC(file);
			file.Close();

			if (hash != UpdateFileList[i].hash)
			{
				FullSizeToDownload += UpdateFileList[i].size;
				newlist.push_back(UpdateFileList[i]);
			}
		}

		SetUpdateFileList(newlist);

		return true;
	}

	void AbordDownload()
	{
		ActiveFile.reset();
		DownloadState = US_IDLE;
	}

	void AutoUpdateInterval()
	{
		switch (DownloadState)
		{
			case US_IDLE:
				LogMsg("[UPP] Sciagam indeks plikow w celu sprawdzenia czy sa nowe aktualizacje");
				ActiveFile = std::make_shared<CDownload>("crazypony.net", "/update/list.txt");
				DownloadState = US_DOWNLOAD_INDEX;
			break;

			case US_DOWNLOAD_INDEX:
				if (ActiveFile->GetDownloadProgress(ActiveFileProgress))
				{
					if (ActiveFileProgress.first == ActiveFileProgress.second)
					{
						std::vector<u8> buff(ActiveFile->MoveFile());
						tstring script(buff.begin(), buff.end());

						if (SetupDownloadListByScript(script))
						{
							LogMsg("[UPP] Pobrano i sparsowano plik indeksu, rozpoczynam proces sciagania aktualizacji");
							if (UpdateFileList.empty())
							{
								LogMsg("[UPP] Aplikacja jest aktualna");
								AbordDownload();
							}
							else
							{
								DownloadState = US_SELECT_FILE;
							}
						}
						else
						{
							LogMsg("[UPP] Pobrano plik indeksu, jednak nie udalo sie go poprawnie sparsowac");
							AbordDownload();
						}
					}
				}
				else
				{
					LogMsg("[ERR] [UPP] " + tstring(ActiveFile->GetError()) );
					LogMsg("[UPP] Wystapil problem przy sciaganiu indeksu nowych plikow");
					AbordDownload();
				}
			break;

			case US_SELECT_FILE:
				if (ListIt != UpdateFileList.end())
				{
					LogMsg("[UPP] sciagam plik : " + ListIt->file);

					// Sciagamy plik
					std::string webfile = "/update/" + ListIt->file;
					ActiveFile = std::make_shared<CDownload>("crazypony.net", webfile);
					DownloadState = US_DOWNLOAD_FILE;
				}
				else
				{
					LogMsg("[UPP] Pobrano wszystkie pliki, rozpoczynam proces instalacji");
					// Skonczyly sie pliki
					DownloadState = US_INSTALL;
				}
			break;

			case US_DOWNLOAD_FILE:
				if (ActiveFile->GetDownloadProgress(ActiveFileProgress))
				{
					FullSizeDownloaded = TmpDownloaded + ActiveFileProgress.first;

					if (ActiveFileProgress.first == ActiveFileProgress.second)
					{
						// Plik zostal pobrany
						tstring path = GetExeDirectory();
						tstring fname = ListIt->file + ".new";
						tstring full = GetExeDirectory() + "/" + fname;
						CFile newfile(full, CFile::OM_WRITE);
						if (newfile.IsOK())
						{
							std::vector<u8> file(ActiveFile->MoveFile());
							newfile.Write(file.data(), file.size());
							newfile.Close();
							++ListIt; // Wybieramy kolejny plik na liscie
							DownloadState = US_SELECT_FILE;
							TmpDownloaded += file.size();
						}
						else
						{
							LogMsg("[UPP] System nie pozwolil na zapisanie nowopobranego pliku. Przerywam atkualizacje : " + full);
							AbordDownload();
						}
					}
				}
				else
				{
					LogMsg("[ERR] [UPP] " + tstring(ActiveFile->GetError()) );
					LogMsg("[UPP] Wystapil problem w czasie sciagania pliku. Przerywam aktualizacje : " + ListIt->file);
					AbordDownload();
				}
			break;

			// Sciagnelismy wszystkie mozemy podmieniac nazwy
			case US_INSTALL:
			{
				tstring path = GetExeDirectory() + "/";
				for (auto it = UpdateFileList.begin(); it != UpdateFileList.end(); ++it)
				{
					LogMsg("[UPP] Instaluje : " + it->file);

					// Glowny plik wykonywalny
					tstring normal;
					tstring asold;
					tstring asnew;

					if (it->file == "module")
					{
						normal = path + GetExeName(true);			// ../tvst.exe
						asold = path + "module.old";				// ../module.old
						asnew = path + "module.new";				// ../module.new
					}
					else
					{
						normal = path + it->file;		// ../data.mix
						asold = normal + ".old";		// ../data.mix.old
						asnew = normal + ".new";		// ../data.mix.new
					}

					CFile f(normal, CFile::OM_READ);
					bool ex = f.IsOK();
					f.Close();

					bool r1 = true;
					bool r2 = true;

					if (ex)
					{
						LogMsg("Zamieniam %s na %s", normal, asold);
						r1 = sysMoveFile(normal, asold);
						LogMsg("Zamieniam %s na %s", asnew, normal);
						r2 = sysMoveFile(asnew, normal);
					}
					else
					{
						r2 = sysMoveFile(asnew, normal);
					}

					if (!r1 || !r2)
					{
						// TODO opracowac revert i metode lockowania plikow.
						// Potem musimy opracowac revert
						ThrowCriticalError("Blad krytyczny w czasie aktualizacji. Aplikacja wymaga reinstalacji!");
						break;
					}
				}

				LogMsg("[UPP] Usuwam stare pliki");

				for (auto it = UpdateFileList.begin(); it != UpdateFileList.end(); ++it)
				{
					// W tym momencie nie mozemy usunac module.old (aktywny exe) poniewaz porgram jest uruchomiony
					// Usuniemy go przy nastepnym uruchomieniu
					if (it->file == "module") continue;

					tstring p = path + it->file + ".old";
					LogMsg("[UPP] Usuwam : " + p);

					sysRemoveFile(p);
				}

				LogMsg("[UPP] Aktualizacja zakonczona");

				AbordDownload();
				DownloadState = US_RESTART;
			}
			break;

			default:
			break;
		}

		if (DownloadState != US_IDLE && DownloadState != US_RESTART)
		{
			Core::GetEngineScheduler()->AddTask(0.2f, AutoUpdateInterval);
		}
	}


}; // End oo

