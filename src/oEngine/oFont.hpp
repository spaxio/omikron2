﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkow
#pragma once
#include "../oCore/oCore.hpp"
#include "oImage.hpp"

namespace oo
{

	struct SMetric
	{
		int  HoriAdvance;	// Szerokosc znaku + odstepy miedzy lewa i prawa krawedzia
		int  HoriBearingX;  // Odstep od lewej krawedzi
		int  HoriBearingY;	// Odstep od gornej krawedzi

		int  LeftCoord;		// Lewa krawedz znaku wzgledem obrazu
		int  TopCoord;		// Gorna krawedz znaku wzgledem obrazu
		int  RightCoord;	// Prawa krawedz znaku wzgledem obrazu
		int  BottomCoord;	// Dolna krawedz znaku wzgledem obrazu

	};

	typedef std::vector<SMetric> MetricVector_t;

	struct SScope
	{
		u32 Begin;		// Poczatek zakresu (w tablicy Unicode)
		u32 End;		// Koniec zakresu (w tablicy Unicode) znak o indeksie End tez jest brany pod uwage
		u32 Offset;			// Przesuniecie zakresu w wektorze miar (MetricVector_t)
	};

	typedef std::vector<SScope> ScopeVector_t;

	// Okresla jakie zakresy znakow maja byc renderowane przy funkcji MakeFont (domyslnie ASCII)
	void SetActiveCharset(const ScopeVector_t& charset);

	// Renderuje czcionke
	// Funckja zwraca przez referencje:
	// image - 8bitowy obraz w skali szarosci zawierajacy glify
	// metrics - Wektor o rozmiarze takim samym jak liczba glifow na 'image', zawiera koordynaty (patrz SMetric)
	// scopes - Wektor zakresow znakow (patrz SScope)
	// spacewidth - szerokosc spacji
	// lineheight - wysokosc wiersza
	bool MakeFont(const tstring& fontfilename, int size, CImage& image, MetricVector_t& metrics, ScopeVector_t& scopes, int& lineheight);

	// Zakresy znakow w Unicode
	// ASCII       0000 007F
	// LATIN-1     0080 00FF
	// LATIN ext A 0100 017F
	// CYRILLIC    0400 04FF
	// KANA		   3000 30FF
	// KANJI	   4E00 9F62

	//char* JOYO = "chartable/joyo_kanji.sx2";	// Okolo 2 tysiace znakow (Joyo Kanji) z 20 tysiecznego zakresu CJK

	const SScope Ascii_Scope = {0x0020, 0x007E, 0x00};	// ASCII (poza pierwszymi 31 znakami specjalnymi i niewidzalnym znakiem 0x007F)
	const SScope Latin_Scope = {0x00A1, 0x017F, 0x00};	// LATIN-1 (poza pierwszymi 33 znakami) i LATIN ext A
	const SScope Cyrli_Scope = {0x0410, 0x044F, 0x00};	// Cyrillic - Basic Russian Alphabet
	const SScope Kana_Scope =  {0x3000, 0x30FF, 0x00};	// Kana

	const SScope Test_Scope =  {0x0259, 0x0259, 0x00};  // lolol
	const SScope Sym_Scope  =  {0x2019, 0x2019, 0x00};	// '

	//const SScope Kanji_Scope = {0x4E00, 0x9F62, JOYO};	// Zakres CJK poza 157 ostatnimi znakami. Dodatkowo okreslamy selektywnie jakie znaki maja byc pobierane z tego zakresu

} // End oo
