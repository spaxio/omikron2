﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkowe
#pragma once
#include "../oCore/oCore.hpp"

namespace oo
{

	// Zorganizowana kompozycja dzwiekow
	class CComposition
	{
		private: // Obiekty CImage nie sa do kopiowania
			CComposition( const CComposition& );
			CComposition& operator=( const CComposition& );

		private:
			typedef unsigned int soundid_t;
			typedef std::vector<soundid_t> SrcList_t;

		private:
			std::vector<soundid_t> Channel;	// Lista zrodel dzwieku
			std::vector<vect2f> Pos;		// Pozycja zrodla w przestrzeni
			std::vector<float> Gain;		// Wzmocnienie dzwieku

			enum { BAD_CHANNEL = -1 };

		public:
			CComposition(msize max);
			~CComposition();

			void SetPos(msize channel, const vect2f& p);
			vect2f GetPos(msize channel);

			void SetGain(msize channel, float g);
			float GetGain(msize channel);

			void Update();

			// Odtwarza dzwiek
			void Play(msize channel, const tstring& filename, bool loop = false, float gain = 1.0f, const vect2f& pos = vect2f(0.0f, 0.0f));

			// Zatrzymuje dzwiek
			void Stop(msize channel);

			// Ucisza wszystko
			void Hush();

	};

	///////////
	// Music

	bool PlayMusicFile(const tstring& path, const std::function<void()>& on_end);
	void ReplayLastMusicFile();
	void StopMusicFile();
	void PauseMusicFile();
	void ResumeMusicFile();

	void SetMusicVolume(float v);
	float GetMusicVolume();

	tstring GetMusicComment(const char* str);

	//////////
	// Sound

	void SetSoundVolume(float v);
	float GetSoundVolume();

	void SetListenerPos(const vect2f& v);
	vect2f GetListenerPos();

	void Scream(const tstring& filename);
	void Hush();

	/////////
	// Audio

	bool CreateAudioSystem();
	void UpdateAudioSystem();
	void DeleteAudioSystem();

} // End oo
