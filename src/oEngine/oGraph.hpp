﻿/*
 * Copyright © 2013 Karol Pałka
 */

// Pliki naglowkowe
#pragma once
#include "oFramework.hpp"
#include "oImage.hpp"
#include "oFont.hpp"

namespace oo
{

	//////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////// Teksturka //////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////

	const u32 TF_MIPMAPS = 1;
	const u32 TF_REPEAT_S = 2;
	const u32 TF_REPEAT_T = 4;
	const u32 TF_REPEAT = TF_REPEAT_S | TF_REPEAT_T;

	class CTexture
	{
		private: // Obiekty CTexture nie sa do kopiowania
			CTexture( const CTexture& );
			CTexture& operator=( const CTexture& );

		private:
			void Unload();

		// Prosze tych zmiennych nie modyfikowac!
		public:
			GLuint TexID;	// ID tekstury w OpenGL
			rectf Coords;	// Koordynaty na atlasie
			bool Weak;		// Gdy true znaczy, ze ten obiekt nie jest wlascicielem tekstury
			int Width;		// Szerokosc elementu w pixelach
			int Height;		// Wysokosc elementu w pixelach

			// Gdy tekstura jest czescia atlasu
			int OriginalWidth;	// Oryginalna szerokosc przed dodaniem tekstury do atlasu (wymagany do poprawnego wyswietlania)
			int OriginalHeight;	// Oryginalna wysokosc przed dodaniem tekstury do atlasu (wymagany do poprawnego wyswietlania)
			recti Part;			// Wycinek z oryginalnej tekstury
			vect2f W;			// Stosunek rozmiaru tekstury do orginalnego rozmiaru
			vect2f Off[4];		// Offset tekstury (dla normalnej i odbitej)

		public:
			void Zero();

			CTexture() { Zero(); }
			~CTexture() { Unload(); }

		public:
			bool Make(const CImage& image, u32 flags = TF_REPEAT);
			static bool GetFormat(int bpp, GLint& iformat, GLint& format);

			// Pobiera szerokosc obrazka wymagana do poprawnego wyswietlenia
			int GetWidth() { return OriginalWidth; }
			// Pobiera wysokosc obrazka wymagana do poprawnego wyswietlenia
			int GetHeight() { return OriginalHeight; }

			void Bind()
			{
				static GLuint LastBind = 0;
				if (LastBind != TexID)
				{
					glBindTexture(GL_TEXTURE_2D, TexID);
					LastBind = TexID;
				}
			}
	};


	typedef std::shared_ptr<CTexture> RefTexture;

	// Wczytuje teksture przez managera
	RefTexture LoadTexture(const tstring& filename, u32 flags = TF_MIPMAPS);

	// Usuwa wszystkie tekstury z managera
	void ClearTextureDataBase();

	// Uruchamia GC na managerku
	void CheckForUnusedTexture();

	//////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////// Font manager //////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////

	class CFont
	{
		public:
			typedef struct
			{
				float Width;		// Szerokosc znaku
				float Height;		// Wysokosc znaku

				float HoriAdvance;	// Szerokosc znaku + odstepy miedzy lewa i prawa krawedzia
				float HoriBearingX;	// Odstep od lewej grawedzi
				float HoriBearingY;	// Odstep od gornej grawedzi

				float LeftCoord;	// Lewa krawedz znaku wzgledem obrazu
				float TopCoord;		// Gorna krawedz znaku wzgledem obrazu
				float RightCoord;	// Prawa krawedz znaku wzgledem obrazu
				float BottomCoord;	// Dolna krawedz znaku wzgledem obrazu

			} GlyphMetric_t;
			typedef std::vector<GlyphMetric_t> GlyphMetricVector_t;

		private:
			GlyphMetricVector_t	GlyphMetric_v;	// Wektor roznych miar
			ScopeVector_t		Scope_v;		// Zakresy
			CTexture Texture;   	// Tekstura czcionki
			float LineHeight;		// Wyskosc wiersza
			float SpaceWidth;		// Szerokosc spacji
			bool IsFontOk;			// Czy font jest zaladowany

		private:
			// Szuka znaku
			msize GetGlyphIndex(char32_t c)
			{
				for (msize a=0; a<Scope_v.size(); a++)
				{
					if ( (Scope_v[a].Begin <= c) && (Scope_v[a].End >= c) )
					{
						return Scope_v[a].Offset + (c - Scope_v[a].Begin);
					}
				}

				return 0;
			}

		public:
			CFont(const tstring& filename, int size);
			~CFont();

		public:
			// Pobiera szerokosc znaku
			float GetGlyphWidth(char32_t c) { return GlyphMetric_v[GetGlyphIndex(c)].HoriAdvance; }
			// Zwraca wysokosc wiersza
			float GetLineHeight() { return LineHeight; }
			// Zwraca szerokosc spacji
			float GetSpaceWidth() { return SpaceWidth; }

			// Zwraca wskaznik na miary danego znaku (jesli znak jest poza zakresem to zwracane sa miary pierwszego znaku)
			GlyphMetric_t* GetGlyphMetircPtr(char32_t c) { return &GlyphMetric_v[GetGlyphIndex(c)]; }

			// Zwraca szerokosc ciagu znakow
			float GetStringWidth(const tstring& str)
			{
				float tmp = 0.0f;
				auto it = str.begin();
				while (it != str.end())
				{
					tmp += GetGlyphWidth(utf8::unchecked::next(it));
				}
				return tmp;
			}

			// Zwraca szerokosc ciagu znakow
			float GetStringWidth(const std::u32string& str, msize a, msize b)
			{
				assert(a <= b);
				if (b > str.size() || a > str.size()) return 0.0f;

				float tmp = 0.0f;
				for (msize i = a; i < b; i++)
				{
					tmp += GetGlyphWidth(str[i]);
				}
				return tmp;
			}

			// Binduje teksture czcionki
			void BindTexture() { Texture.Bind(); }

			bool IsOk() { return IsFontOk; }
	};

	typedef std::shared_ptr<CFont> RefFont;

	// Wczytuje fonta przez managera
	RefFont LoadFont( const tstring& filename, int size );
	// Usuwa wszystkie fonty z managera
	void ClearFontDataBase();

	enum { FM_CHAR_WRAP		= 0x0001 };		// Przycinanie co do znaku
	enum { FM_WORD_WRAP		= 0x0002 };		// Przycinanie co do slowa
	enum { FM_GRADIENT		= 0x0004 };		// Tworzy gradient ktory leci od lewej do prawej
	enum { FM_H_CLIP		= 0x0008 };		// Przycinanie poziome
	enum { FM_H_LEFT		= 0x0010 };		// Tekst do lewej
	enum { FM_H_CENTER		= 0x0020 };		// Tekst do srodka
	enum { FM_H_RIGHT		= 0x0040 };		// Tekst do prawej
	enum { FM_V_CLIP		= 0x0080 };		// Przycinanie w pionie
	enum { FM_V_TOP			= 0x0100 };		// Tekst od DOlU do gory
	enum { FM_V_CENTER		= 0x0200 };		// Tekst na srodek
	enum { FM_V_BOTTOM		= 0x0400 };		// Tekst od GoRY do dolu
	enum { FM_FLIPUV		= 0x0800 };		// Czcionka wyswietla sie do-gory-nogami (czasami sie przydaje)
	enum { FM_NOHINTING		= 0x1000 };		// Przydatne gdy napis jest w ruchu
	enum { FM_SHADOW		= 0x2000 };		// Rzuca cien zgodnie z wektorem ShadowVect w CFontManager

	// Metoda buforowania jest autorstwa Adama Sawickiego (http://regedit.gamedev.pl)
	// Ja tylko przyspieszylem wyszukiwanie zbuforowanego elementu

	class CFontManager
	{
		private:
			RefFont ActiveFont;

		private:
			// Struktura naszego vertex'a
			struct SCharVertex
			{
				vect2f pos;
				colorf color;
				vect2f uv;
			};

			// Okresla pozycje w VBO
			struct SBufferElement
			{
				u32 ParamsHash;		// Skrot parametrow
				RefFont FontPtr;	// Wskaznik do font'a
				msize Index;		// Poczatek (w indeksach)
				msize Count;		// Liczba indeksow
			};

		public:
			// Element tekstu (wyraz albo linia)
			struct STextElement
			{
				float Width;	// Szerokosc wyrazu/linii
				msize Begin;	// Indeks pierwszego znaku w stringu
				msize End;		// Indeks ostateniego znaku w stringu
			};

		private:
			typedef std::vector<SBufferElement> TextList_t;		// Jakis typ :P
			TextList_t TextList;								// Lista zbuforowanych tekstow

			enum { MaxCharCount		= 8192 };					// Maksymalna liczba znakow
			enum { MaxVertexCount	= MaxCharCount*4 };			// Maksymalna liczba verteksow
			enum { MaxIndexCount	= MaxCharCount*6 };			// Maksymalna liczba indeksow

			static_assert( MaxVertexCount <= 0xFFFF, "CFontManager::MaxVertexCount is too big" );
			static_assert( MaxIndexCount <= 0xFFFF, "CFontManager::MaxIndexCount is too big" );

			u32 CharIndex;				// Indeks znaku
			GLuint VBOVertices;			// Uchwyt na VertexBufferObject
			GLuint VBOIndices;			// Uchwyt na IndexBufferObject
			std::vector<SCharVertex> Vertices_tmp;	// Pomocniczy bufforek

			// Rysuje odpowiedni fragment bufora
			void DrawBufferElement(const SBufferElement& be);

			// Sprawdza czy dany tekst nie jest juz w buforze
			bool TryDraw(u32 paramshash);

			std::vector<STextElement> Words;	// Buforek na slowa
			std::vector<STextElement> Lines;	// Buforek na linie

		public:
			// Rozdziela tekst na slowa
			static std::vector<STextElement> SplitWords(const RefFont& font, const std::u32string& str, float boxwidth);

		private:
			// Rozdziela tekst na linie (co do wyrazu)
			void SplitLinesWordWrap(const RefFont& font, const std::u32string& str, const rectf& rect, std::vector<STextElement>& lines);
			// Rozdziela tekst na znaki (co do znaku)
			void SplitLinesCharWrap(const RefFont& font, const std::u32string& str, const rectf& rect, std::vector<STextElement>& lines);
			// Rozdziela tekst tylko na linie jawne (po wystapieniu \n)
			void SplitLinesNoWrap(const RefFont& font, const std::u32string& str, const rectf& rect, std::vector<STextElement>& lines);

			// Przycina linie/znaki zgodnie ze stylem
			void SetClip(const RefFont& font, const std::u32string& str, const rectf& rect, std::vector<STextElement>& lines, u32 style);

		private:
			std::vector<colorf> ColorTable;		// Max 16 elementowa tablica kolorow
			char ColorChar;					// Znak ktory zapowiada kolor
			colorf CurrentColor;				// Aktualny kolor

			vect2f ShadowVect;					// Cien

		public:
			// Ustawia tablice kolorow, Uwaga rozmiar colortable musi byc rowny 16!
			void SetColorTable(const std::vector<colorf>& colortable) { if (colortable.empty() || colortable.size()!=0x0F) return; ColorTable = colortable; }
			// Ustawia znak jaki zapowiada kolor
			void SetColorChar(char colorchar) { ColorChar = colorchar; }

		public:
			enum { CHAR_WRAP    = 1     };		// Przycinanie co do znaku
			enum { WORD_WRAP    = 2     };		// Przycinanie co do slowa
			enum { GRADIENT		= 4     };		// Tworzy gradient ktory leci od lewej do prawej
			enum { H_CLIP       = 8     };		// Przycinanie poziome
			enum { H_LEFT		= 16    };		// Tekst do lewej
			enum { H_CENTER		= 32    };		// Tekst do srodka
			enum { H_RIGHT		= 64    };		// Tekst do prawej
			enum { V_CLIP       = 128   };		// Przycinanie w pionie
			enum { V_TOP		= 256   };		// Tekst od DOlU do gory
			enum { V_CENTER		= 512   };		// Tekst na srodek
			enum { V_BOTTOM		= 1024  };		// Tekst od GoRY do dolu
			enum { FLIPUV		= 2048  };		// Czcionka wyswietla sie do-gory-nogami (czasami sie przydaje)
			enum { NOHINTING	= 4096  };		// Przydatne gdy napis jest w ruchu
			enum { SHADOW		= 8192  };		// Rzuca cien zgodnie z wektorem ShadowVect w CFontManager

			enum { DEFAULT_STYLE = WORD_WRAP | H_LEFT | V_BOTTOM };

		public:
			CFontManager();
			~CFontManager();

			// Ustawia cien
			void SetShadowVect(const vect2f& v) { ShadowVect = v; }

			// Oczyszcza bufor
			void ClearBuffer() { TextList.clear(); }

			// Wybiera aktywna czcionke
			void SelectFont(const RefFont& fontptr) { assert(fontptr); ActiveFont = fontptr; }

			// Zalecane do tekstu ktory bardzo rzadko sie zmienia (bo wtedy metoda go buforuje)
			void DrawStaticText(const tstring& txt, const rectf& rect, const colorf& color1, const colorf& color2, u32 style = DEFAULT_STYLE);

			// Pzwala na wyswietlenie wielokolorowego tekstu
			void DrawColorText(const tstring& txt, const rectf& rect, u32 style = DEFAULT_STYLE, float alpha = 1.0f);

			// Dzieli text na linie dla danej szerokosci
			void SplitLines(const tstring& text, float width, std::vector<tstring>& out, u32 style = DEFAULT_STYLE);
			// Zlicza liczbe linii dla danej szerokosci
			int CountLines(const tstring& text, float width, u32 style = DEFAULT_STYLE);
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////// Przydatne w paru miejscach ////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////

	// Pomocnicza struktura
	struct SPoints4
	{
		vect2f p1;
		vect2f p2;
		vect2f p3;
		vect2f p4;

		// Zeruje strukture
		void Zero()
		{
			p1.Set(0.0f, 0.0f);
			p2.Set(0.0f, 0.0f);
			p3.Set(0.0f, 0.0f);
			p4.Set(0.0f, 0.0f);
		}
	};

	enum EBlendMode
	{
		BM_NORMAL = 0,
		BM_ADDITION = 1
	};

	const std::pair<GLenum, GLenum> GBlendType[] = {
														std::make_pair(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA),
														std::make_pair(GL_SRC_ALPHA, GL_ONE)
												   };

	// Tych zmiennych nie wolno modyfikowac samemu!
	extern bool GTextureEnable;
	extern bool GBlendEnable;
	extern EBlendMode GBlendMode;

	extern bool GUseVertex;
	extern bool GUseTexCoord;
	extern bool GUseColor;

	inline void EnableTexture() { if (!GTextureEnable) { glEnable(GL_TEXTURE_2D); GTextureEnable = true; } }
	inline void DisableTexture() { if (GTextureEnable) { glDisable(GL_TEXTURE_2D); GTextureEnable = false; } }

	inline void EnableBlend() { if (!GBlendEnable) { glEnable(GL_BLEND); GBlendEnable = true; } }
	inline void DisableBlend() { if (GBlendEnable) { glDisable(GL_BLEND); GBlendEnable = false; } }

	inline void SetBlendMode(EBlendMode bm)	{ if (GBlendMode != bm) { glBlendFunc( GBlendType[bm].first, GBlendType[bm].second ); GBlendMode = bm; } }

	inline void DoNotUseVertex() { assert(GUseVertex); glDisableClientState(GL_VERTEX_ARRAY); GUseVertex = false; }
	inline void DoNotUseTexCoord() { assert(GUseTexCoord); glDisableClientState(GL_TEXTURE_COORD_ARRAY); GUseTexCoord = false; }
	inline void DoNotUseColor() { assert(GUseColor); glDisableClientState(GL_COLOR_ARRAY); GUseColor = false; }

	inline void RevertVertex() { assert(!GUseColor); glEnableClientState(GL_VERTEX_ARRAY); GUseVertex = true; }
	inline void RevertTexCoord() { assert(!GUseTexCoord); glEnableClientState(GL_TEXTURE_COORD_ARRAY); GUseTexCoord = true; }
	inline void RevertColor() { assert(!GUseColor); glEnableClientState(GL_COLOR_ARRAY); GUseColor = true; }

	inline void CheckDefaultGLClientState()
	{
		#ifdef _DEBUG
				assert(GUseVertex && GUseTexCoord && GUseColor);
		#endif
	}

	////////////////////////////////////////////////////
	// Transformacje z przestrzeni bazowej (zwykle 800x600) na aktualna rozdzielczosc ekranu
	// Dzieki czemu mozna uzyc jednego layoutu na wszystkie ekrany

	extern vect2f ScreenC;
	extern vect2f ScreenV;
	extern vect2f ScreenOffsetBase;
	extern vect2f ScreenOffset;
	extern float FontScaleFactor;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// CVectorManager ///////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////

	class CVectorLayer;
	class CVectorManager
	{
		private:
			GLuint VBOVertices;			// Uchwyt na VertexBufferObject
			GLuint VBOIndices;			// Uchwyt na IndexBufferObject

		public:
			CVectorManager();
			~CVectorManager();

			// Renderuje warstwe
			void FlushLayer(CVectorLayer& vl);
	};

	class CVectorLayer
	{
		friend class CVectorManager;

		private:
			struct SVectVertex
			{
				vect2f pos;
				colorf color;

				void Set(const vect2f& p, const colorf& c) { pos = p; color = c; }

			};

			enum { MaxVertexCount = 4086 };
			enum { MaxIndexCount = MaxVertexCount };

		private:
			SVectVertex* VertexBuffer;		// Tablica wierzcholkow
			u16* IndexBuffer;				// Tablica indeksow
			msize Count;					// Liczba wierzcholkow
			msize IndexCount;				// Liczba indeksow

			bool NoSpaceForVertex(msize with_additional) { return (Count+with_additional) >= MaxVertexCount; }
			bool NoSpaceForIndex(msize with_additional) { return (IndexCount+with_additional) >= MaxIndexCount; }

			void AddVertexAndIndex(const vect2f& p, const colorf& c) { IndexBuffer[IndexCount++] = static_cast<u16>(Count); VertexBuffer[Count++].Set(p, c); }
			void AddVertex(const vect2f& p, const colorf& c) { VertexBuffer[Count++].Set(p, c); }
			void AddIndex(u16 idx) { IndexBuffer[IndexCount++] = idx; }

		public:
			enum { ChunkSize = MaxVertexCount * sizeof(SVectVertex) };
			enum { IndexChunkSize = MaxIndexCount * sizeof(u16) };
			CVectorLayer();
			~CVectorLayer();

		public:
			void DrawCircle(const circlef& circle, const colorf& color, float eps = 4.0f);

			void DrawCross(const rectf& r, const colorf& color);
			void DrawStar(const rectf& r, const colorf& color);

			void DrawLine(vect2f a, vect2f b, float w, colorf c);

			void DrawFillRectangle(const rectf& rect, const colorf& colorLT, const colorf& colorRT,  const colorf& colorRB,  const colorf& colorLB);
			void DrawFillRectangle(const rectf& rect, const colorf& color) { DrawFillRectangle(rect, color, color, color, color); }
			void DrawFillRectangle(const SPoints4& points, const colorf& colorA, const colorf& colorB,  const colorf& colorC,  const colorf& colorD);
			void DrawFillRectangle(const SPoints4& points, const colorf& color) { DrawFillRectangle(points, color, color, color, color); }
			void DrawFillRoundRectangle(const rectf& r, float cs, int steps, const colorf& color);

			void DrawFullRectangle(rectf r, float bsize, const colorf& bcolor, const colorf& fill);

			void DrawFillTriangle(const vect2f& a, const vect2f& b, const vect2f& c, const colorf& colora, const colorf& colorb, const colorf& colorc);
			void DrawFillTriangle(const vect2f& a, const vect2f& b, const vect2f& c, const colorf& color) { DrawFillTriangle(a, b, c, color, color, color); }

			void AutoFlush(float p = 0.9f);
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// CGraphManager ///////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////

	void LoadAtlas(const tstring& name, int size = 1024, int diff = 4);

	// Usuwa wszystkie tekstury z managera
	void ClearAtlasDataBase();

	// Klasa do rysowania grafiki rastrowej
	class CGraphManager
	{
		public:
			struct SRastVertex
			{
				vect2f pos;
				colorf color;
				vect2f uv;

			};

		private:
			GLuint VBOVertices;			// Uchwyt na VertexBufferObject
			GLuint VBOIndices;			// Uchwyt na IndexBufferObject

			enum { MaxPloyCount		= 1024 };				// Maksymalna liczba prostokatow
			enum { MaxVertexCount	= MaxPloyCount*4 };		// Maksymalna liczba vertexow
			enum { MaxIndexCount	= MaxPloyCount*6 };		// Maksymalna liczba indeksow

			struct SRectHead
			{
				std::vector<SRastVertex> VertexBuffer;
				RefTexture Texture;
				msize PloyCount;
				int Frame;				// Liczba klatek gdzie tekstura nie byla uzywana
			};							// Kazda tekstura ma swoj bufor

			typedef std::vector<SRectHead> RectHeadVector_t;		// Tekstur moze byc dowolna liczba
			std::vector<RectHeadVector_t> Layer_v;					// Warstwa

			SRectHead* CurrentRectHead;		// Aktualny wezel do ktorego wrzucamy wierzcholki
			RectHeadVector_t* CurrentLayer;	// Aktualna warstwa

			RefTexture ActiveTexture;

			rectf ActiveCoords;		// Aktywne coordynaty na danej teksturze
			vect2f W;

		public:
			CGraphManager();
			~CGraphManager();

		public:
			// Powinno byc wywolywane co klatke (kontroluje i usuwa nieuzywane tekstury)
			void FrameCheckForUnusedTextures();

			// Generuje nowe warstwy, na ktore mozna renderowac (UWAGA count musi byc rozne od zera)
			void GenLayers(msize count);
			// Wybiera aktualna warstwe
			void SelectLayer(msize i);
			// Wybiera aktualna teksture
			void SelectTexture(const RefTexture& texture);

			// Pozwala na samo wyliczenie pozycji wierzcholkow (kat w radianach!)
			static void CalcPoints(const rectf& r, const vect2f& p, float angle, SPoints4& points);
			// Rysowanie bezposrednie
			void DrawPoints(const SPoints4& points, const rectf& coord, const colorf& color);
			// Rysowanie bezposrednie z coordynatami 0 0 1 1
			void DrawPoints(const SPoints4& points, const colorf& color);

			// Rysowanie niskopoziomowe
			void DrawRaw(SRastVertex* table, msize num);

			// DrawBitmap* NIE uzywamy do atlasowych tekstur

			void DrawBitmapFast(const rectf& r, const rectf& coord, const colorf& color = N_COLOR)
			{
				SPoints4 po = { r.LeftTop(), r.RightTop(), r.RightBottom(), r.LeftBottom() };
				DrawPoints(po, coord, color);
			}
			void DrawBitmapFast(const rectf& r, const colorf& color = N_COLOR)
			{
				SPoints4 po = { r.LeftTop(), r.RightTop(), r.RightBottom(), r.LeftBottom() };
				DrawPoints(po, color);
			}
			void DrawBitmap(const rectf& r, const colorf& color, const vect2f& p, float angle)
			{
				SPoints4 po;
				CalcPoints(r, p, angle, po);
				DrawPoints(po, color);
			}
			void DrawBitmap(const rectf& r, const rectf& coord, const colorf& color, const vect2f& p, float angle)
			{
				SPoints4 po;
				CalcPoints(r, p, angle, po);
				DrawPoints(po, coord, color);
			}
			void DrawBitmap(const rectf& r, const colorf& color = N_COLOR, float angle = 0.0f)
			{
				DrawBitmap(r, color, r.GetCenter(), angle);
			}

			// DrawElement dziala dla zwyklych i atlasowych tekstur
			void DrawElement(const rectf& r, const colorf& color, const vect2f& p, float angle)
			{
				rectf nr(r.LeftTop() + r.GetSize() * ActiveTexture->Off[0], r.GetWidth() * W.x, r.GetHeight() * W.y );
				SPoints4 po;
				CalcPoints(nr, p, angle, po);
				DrawPoints(po, color);
			}
			void DrawElementFlipX(const rectf& r, const colorf& color, const vect2f& p, float angle)
			{
				rectf nr(r.LeftTop() + r.GetSize() * ActiveTexture->Off[2], r.GetWidth() * W.x, r.GetHeight() * W.y );
				SPoints4 po;
				CalcPoints(nr, p, angle, po);

				std::swap(po.p1, po.p2);
				std::swap(po.p3, po.p4);

				DrawPoints(po, color);
			}
			void DrawElementFlipY(const rectf& r, const colorf& color, const vect2f& p, float angle)
			{
				rectf nr(r.LeftTop() + r.GetSize() * ActiveTexture->Off[1], r.GetWidth() * W.x, r.GetHeight() * W.y );
				SPoints4 po;
				CalcPoints(nr, p, angle, po);

				std::swap(po.p1, po.p4);
				std::swap(po.p2, po.p3);

				DrawPoints(po, color);
			}
			void DrawElementFlipXY(const rectf& r, const colorf& color, const vect2f& p, float angle)
			{
				rectf nr(r.LeftTop() + r.GetSize() * ActiveTexture->Off[3], r.GetWidth() * W.x, r.GetHeight() * W.y );
				SPoints4 po;
				CalcPoints(nr, p, angle, po);

				std::swap(po.p1, po.p3);
				std::swap(po.p2, po.p4);

				DrawPoints(po, color);
			}

			// Renderuje warstwe
			void FlushLayer(msize i, EBlendMode bm = BM_NORMAL);
	};

} // End oo
