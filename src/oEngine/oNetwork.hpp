﻿/*
 * Copyright © 2013 Karol Pałka
 */

#pragma once

#include "../oCore/oCore.hpp"

namespace oo
{
	extern boost::asio::io_service GIOService;

	typedef boost::asio::ip::tcp::socket Socket_t;
	typedef boost::asio::ip::tcp::acceptor Acceptor_t;

	const msize MAX_BUFFER_SIZE = 4 * 1024;

	struct SHeaderPacket
	{
		u16 Type;
		u16 Size;

		SHeaderPacket() : Type(0), Size(0) {}
		SHeaderPacket(u16 type, u16 size) : Type(type), Size(size) {}
		void Zero() { Type = 0; Size = 0; }
	};

	const msize HP_SIZE = sizeof(SHeaderPacket);

	extern int GPort;

	const msize MAX_CONNECTIONS = 17;

	class CNet
	{
		private:
			CNet( const CNet& );
			CNet& operator=( const CNet& );

		public:
			Socket_t Socket;				// Gniazdo

			int PlayerId;					// -1 oznacza gracza jeszcze nie zweryfikowanego, inna wartosc to indeks w tablicy graczy

			SHeaderPacket HeaderPacket;		// Naglowek aktywnie analizowanego pakietu
			CBuffer Buffer;					// Bufor z danymi (to co zawiera zalezy od naglowka)
			CBuffer WorkBuffer;				// Robocze dane

			std::vector<u8> SendBuffer;     // Dane aktualnie wysylane przez boost::asio::async_write
			std::vector<u8> WorkSendBuffer; // Dane gromadzone przez SendData

			CNet(boost::asio::io_service& io_service)
				:	Socket(io_service),
					PlayerId(-1),
					Buffer(MAX_BUFFER_SIZE),
					WorkBuffer(MAX_BUFFER_SIZE) {}

	};

	typedef std::shared_ptr<CNet> RefNet;

	// Wywolywac co klatke
	void UpdateNetworkSystem();

	enum EDisconnectReason
	{
		DR_BYE,						// Gracz opuszcza gre
		DR_KICK,					// Gracz jest wywalany z gry
		DR_PING_TIMEOUT,			// Gracz stracil polaczenie z serwerem
		DR_COMMUNICATION_ERROR,		// Blad komunikacji
		DR_SERVER_CLOSE				// Serwer konczy dzialanie
	};

	// Te dwie funkcje implementuje uzytownik
	void DisconnectByContext(const RefNet& client, EDisconnectReason dr);
	void DisconnectFromServerAndResetGame();

	extern CBuffer GNetworkBuffer;

	extern RefNet GClientStream;

	// Zaczynamy nasluchiwac
	bool StartAcceptingSockets(unsigned short port);
	// Konczymy sluchanie
	void StopAcceptingSockets();

	// Sprawdza czy serwer jest aktywny
	bool IsServer();

	// Sprawdza czy jestesmy podlaczeni do serwera
	bool IsConnectionActive();

	// Zaczyna odliczac czas
	void Ping();

	// Zwraca odliczony czas
	float Pong();

	typedef void (*OnAsyncConnectFail_t)(const tstring&);
	typedef void (*OnAsyncConnectSuccess_t)(void);

	// Laczy w sposob asnychroniczny
	void ConnectToServerAsync(const tstring& address, unsigned short port, OnAsyncConnectSuccess_t success, OnAsyncConnectFail_t fail);

	// Odlaczamy klienta
	void DisconnectClient(const RefNet& connection);

	// Odlaczamy sie do serwera
	void DisconnectFromServer();

	// Odcinamy wszystkich klientow od serwera (wywolywac tylko gdy wychodziny z serwera)
	void DisconnectAllClients();

	// Serwer wysyla dane do danego klienta
	void ServerSend(const RefNet& scache, const void* data, msize size);

	// Wysyla dane do serwera
	void SendData(const void* data, msize size);

	// Te funkcje implementuje uzytkownik
	typedef void(*ProcessPacket_t)(const RefNet&);
	// Klient odbiera dane od serwera
	void ProcessPacketOnNetwork(const RefNet& scache);
	// Serwer odbiera dane od klientow
	void ProcessPacketOnServer(const RefNet& scache);

} // End oo
