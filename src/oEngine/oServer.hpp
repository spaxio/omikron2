﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "../oCore/oCore.hpp"

#include "oAudio.hpp"
#include "oInput.hpp"
#include "oNetwork.hpp"

void OnLoad_DS();		// Wczytywanie
void OnUnload_DS();		// Wymazywanie
void OnTick_DS();		// Co Xms

namespace oo
{

	// Startuje serwer dedykowany
	int StartDedicatedServer(int argc, char** argv);

} // End oo
