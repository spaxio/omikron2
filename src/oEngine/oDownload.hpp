﻿/*
 * Copyright © 2013 Karol Pałka
 */

#pragma once
#include "oNetwork.hpp"

namespace oo
{
	//
	//	Tak sciagamy plik
	//
	//	DownloadFileAsync("server.pl", "/file.big")
	//	.OnProgress([](int downloaded, int to_download)
	//	{
	// 		return true; // true oznacza ze chcemy kontynulowac sciaganie
	//	})
	//	.OnSuccess([](RefFile file, std::string mime_type)
	//	{
	// 		// Gotowe
	//	})
	//	.OnFail([](std::string error, std::string detail)
	//	{
	//		// Blad
	//	}).Start();

	struct DownloadFileAsync
	{
		typedef std::function<bool(int, int)> ProgressFunc_t;
		typedef std::function<void(RefFile, std::string)> SuccessFunc_t;
		typedef std::function<void(std::string, std::string)> FailFunc_t;

		ProgressFunc_t ProgressFunc;
		SuccessFunc_t SuccessFunc;
		FailFunc_t FailFunc;

		std::string Server;
		std::string File;

		DownloadFileAsync& OnProgress(const ProgressFunc_t& func) { assert(!ProgressFunc); ProgressFunc = func; return *this; }
		DownloadFileAsync& OnSuccess(const SuccessFunc_t& func) { assert(!SuccessFunc); SuccessFunc = func; return *this; }
		DownloadFileAsync& OnFail(const FailFunc_t& func) { assert(!FailFunc); FailFunc = func; return *this; }
		void Start();

		// Konsturkot
		DownloadFileAsync(const std::string& server, const std::string& file) : Server(server), File(file) {}
	};

	// Sprawdza ktore pliki juz sie sciagnely
	void CheckDownloadTaskList();
	// Anuluje wszystkie procesy sciagania (wywolywac przed wyjsciem z aplikacji)
	void DownloadDrop();

	int GetFullToDownload();
	int GetFullDownloaded();

	enum EUpdateStatus
	{
		US_IDLE = 0,			// "Checking for updates..."
		US_DOWNLOAD_INDEX,		// "Checking for updates..."
		US_SELECT_FILE,			// "Downloading update..."
		US_DOWNLOAD_FILE,		// "Downloading update..."
		US_INSTALL,				// "Downloading update..."
		US_RESTART				// "Downloading update..."
	};

	EUpdateStatus GetUpdateStatus();

	void AutoUpdateInterval();

}; // End oo
