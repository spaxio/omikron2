﻿/*
 * Copyright © 2013 Karol Pałka
 */

#pragma once

enum Keys
{
	M_LEFT           = 0x00,
	M_MIDDLE         = 0x01,
	M_RIGHT          = 0x02,

	K_BACK           = 0x08,
	K_TAB            = 0x09,

	K_CLEAR          = 0x0C,
	K_RETURN         = 0x0D,

	K_SHIFT          = 0x10,
	K_CONTROL        = 0x11,
	K_MENU           = 0x12,
	K_PAUSE          = 0x13,
	K_CAPITAL        = 0x14,

	K_ESCAPE         = 0x1B,

	K_CONVERT        = 0x1C,
	K_NONCONVERT     = 0x1D,
	K_ACCEPT         = 0x1E,
	K_MODECHANGE     = 0x1F,

	K_SPACE          = 0x20,
	K_PRIOR          = 0x21,
	K_NEXT           = 0x22,
	K_END            = 0x23,
	K_HOME           = 0x24,
	K_LEFT           = 0x25,
	K_UP             = 0x26,
	K_RIGHT          = 0x27,
	K_DOWN           = 0x28,
	K_SELECT         = 0x29,
	K_PRINT          = 0x2A,
	K_EXECUTE        = 0x2B,
	K_SNAPSHOT       = 0x2C,
	K_INSERT         = 0x2D,
	K_DELETE         = 0x2E,
	K_HELP           = 0x2F,

	K_0              = '0',	// 0x30
	K_1              = '1',
	K_2              = '2',
	K_3              = '3',
	K_4              = '4',
	K_5              = '5',
	K_6              = '6',
	K_7              = '7',
	K_8              = '8',
	K_9              = '9',

	K_A              = 'A',	// 0x41
	K_B              = 'B',
	K_C              = 'C',
	K_D              = 'D',
	K_E              = 'E',
	K_F              = 'F',
	K_G              = 'G',
	K_H              = 'H',
	K_I              = 'I',
	K_J              = 'J',
	K_K              = 'K',
	K_L              = 'L',
	K_M              = 'M',
	K_N              = 'N',
	K_O              = 'O',
	K_P              = 'P',
	K_Q              = 'Q',
	K_R              = 'R',
	K_S              = 'S',
	K_T              = 'T',
	K_U              = 'U',
	K_V              = 'V',
	K_W              = 'W',
	K_X              = 'X',
	K_Y              = 'Y',
	K_Z              = 'Z',

	K_LWIN           = 0x5B,
	K_RWIN           = 0x5C,
	K_APPS           = 0x5D,

	K_SLEEP          = 0x5F,

	K_NUMPAD0        = 0x60,
	K_NUMPAD1        = 0x61,
	K_NUMPAD2        = 0x62,
	K_NUMPAD3        = 0x63,
	K_NUMPAD4        = 0x64,
	K_NUMPAD5        = 0x65,
	K_NUMPAD6        = 0x66,
	K_NUMPAD7        = 0x67,
	K_NUMPAD8        = 0x68,
	K_NUMPAD9        = 0x69,
	K_MULTIPLY       = 0x6A,
	K_ADD            = 0x6B,
	K_SEPARATOR      = 0x6C,
	K_SUBTRACT       = 0x6D,
	K_DECIMAL        = 0x6E,
	K_DIVIDE         = 0x6F,
	K_F1             = 0x70,
	K_F2             = 0x71,
	K_F3             = 0x72,
	K_F4             = 0x73,
	K_F5             = 0x74,
	K_F6             = 0x75,
	K_F7             = 0x76,
	K_F8             = 0x77,
	K_F9             = 0x78,
	K_F10            = 0x79,
	K_F11            = 0x7A,
	K_F12            = 0x7B,
	K_F13            = 0x7C,
	K_F14            = 0x7D,
	K_F15            = 0x7E,
	K_F16            = 0x7F,
	K_F17            = 0x80,
	K_F18            = 0x81,
	K_F19            = 0x82,
	K_F20            = 0x83,
	K_F21            = 0x84,
	K_F22            = 0x85,
	K_F23            = 0x86,
	K_F24            = 0x87,

	K_NUMLOCK        = 0x90,
	K_SCROLL         = 0x91,

	K_OEM_NEC_EQUAL  = 0x92,   // '=' key on numpad

	K_OEM_FJ_JISHO   = 0x92,   // 'Dictionary' key
	K_OEM_FJ_MASSHOU = 0x93,   // 'Unregister word' key
	K_OEM_FJ_TOUROKU = 0x94,   // 'Register word' key
	K_OEM_FJ_LOYA    = 0x95,   // 'Left OYAYUBI' key
	K_OEM_FJ_ROYA    = 0x96,   // 'Right OYAYUBI' key

	K_LSHIFT         = 0xA0,
	K_RSHIFT         = 0xA1,
	K_LCONTROL       = 0xA2,
	K_RCONTROL       = 0xA3,
	K_LMENU          = 0xA4,
	K_RMENU          = 0xA5,

	K_TILDE          = 0xC0,

	J_P1_LEFT        = 0xE1,
	J_P1_RIGHT       = 0xE2,
	J_P1_UP          = 0xE3,
	J_P1_DOWN        = 0xE4,
	J_P1_BUTTON1     = 0xE5,
	J_P1_BUTTON2     = 0xE6,
	J_P1_BUTTON3     = 0xE7,
	J_P1_BUTTON4     = 0xE8,
	J_P1_BUTTON5     = 0xE9,
	J_P1_BUTTON6     = 0xEA,
	J_P1_BUTTON7     = 0xEB,
	J_P1_BUTTON8     = 0xEC,
	J_P1_BUTTON9     = 0xED,
	J_P1_BUTTON10    = 0xEE,

	J_P2_LEFT        = 0xEF,
	J_P2_RIGHT       = 0xF0,
	J_P2_UP          = 0xF1,
	J_P2_DOWN        = 0xF2,
	J_P2_BUTTON1     = 0xF3,
	J_P2_BUTTON2     = 0xF4,
	J_P2_BUTTON3     = 0xF5,
	J_P2_BUTTON4     = 0xF6,
	J_P2_BUTTON5     = 0xF7,
	J_P2_BUTTON6     = 0xF8,
	J_P2_BUTTON7     = 0xF9,
	J_P2_BUTTON8     = 0xFA,
	J_P2_BUTTON9     = 0xFB,
	J_P2_BUTTON10    = 0xFC

};

struct KeyAndStr
{
    const char* str;
    u8 type;
    u8 code;
};

static KeyAndStr N0 = { "???", 0x00, 0x00 };

// Nazwy klawiszy
static KeyAndStr Key2Str[0xFF] =
{
	{ "LMB", 1, sf::Mouse::Left },
	{ "MMB", 1, sf::Mouse::Middle },
	{ "RMB", 1, sf::Mouse::Right },
	N0,
	N0,
	N0,
	N0,
	N0,
	{ "BACKSPACE", 2, sf::Keyboard::BackSpace },
	{ "TAB", 2, sf::Keyboard::Tab },
	N0,
	N0,
	N0,
	{ "ENTER", 2, sf::Keyboard::Return },
	N0,
	N0,
	{ "SHIFT", 2, sf::Keyboard::LShift },
	{ "CONTROL", 2, sf::Keyboard::LControl },
	{ "ALT", 2, sf::Keyboard::LAlt },
	{ "PAUSE", 2, sf::Keyboard::Pause },
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	{ "ESC", 2, sf::Keyboard::Escape },
	N0,
	N0,
	N0,
	N0,
	{ "SPACE", 2, sf::Keyboard::Space }, // 32 0x20
	{ "PAGE UP", 2, sf::Keyboard::PageUp },
	{ "PAGE DOWN", 2, sf::Keyboard::PageDown },
	{ "END", 2, sf::Keyboard::End },
	{ "HOME", 2, sf::Keyboard::Home },
	{ "LEFT", 2, sf::Keyboard::Left },
	{ "UP", 2, sf::Keyboard::Up },
	{ "RIGHT", 2, sf::Keyboard::Right },
	{ "DOWN", 2, sf::Keyboard::Down },
	N0,
	N0,
	N0,
	N0,
	{ "INSERT", 2, sf::Keyboard::Insert },
	{ "DELETE", 2, sf::Keyboard::Delete },
	N0,
	{ "0", 2, sf::Keyboard::Num0 },
	{ "1", 2, sf::Keyboard::Num1 },
	{ "2", 2, sf::Keyboard::Num2 },
	{ "3", 2, sf::Keyboard::Num3 },
	{ "4", 2, sf::Keyboard::Num4 },
	{ "5", 2, sf::Keyboard::Num5 },
	{ "6", 2, sf::Keyboard::Num6 },
	{ "7", 2, sf::Keyboard::Num7 },
	{ "8", 2, sf::Keyboard::Num8 },
	{ "9", 2, sf::Keyboard::Num9 },
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	{ "A", 2, sf::Keyboard::A },
	{ "B", 2, sf::Keyboard::B },
	{ "C", 2, sf::Keyboard::C },
	{ "D", 2, sf::Keyboard::D },
	{ "E", 2, sf::Keyboard::E },
	{ "F", 2, sf::Keyboard::F },
	{ "G", 2, sf::Keyboard::G },
	{ "H", 2, sf::Keyboard::H },
	{ "I", 2, sf::Keyboard::I },
	{ "J", 2, sf::Keyboard::J },
	{ "K", 2, sf::Keyboard::K },
	{ "", 2, sf::Keyboard::L },
	{ "M", 2, sf::Keyboard::M },
	{ "N", 2, sf::Keyboard::N },
	{ "O", 2, sf::Keyboard::O },
	{ "P", 2, sf::Keyboard::P },
	{ "Q", 2, sf::Keyboard::Q },
	{ "R", 2, sf::Keyboard::R },
	{ "S", 2, sf::Keyboard::S },
	{ "T", 2, sf::Keyboard::T },
	{ "U", 2, sf::Keyboard::U },
	{ "V", 2, sf::Keyboard::V },
	{ "W", 2, sf::Keyboard::W },
	{ "X", 2, sf::Keyboard::X },
	{ "Y", 2, sf::Keyboard::Y },
	{ "Z", 2, sf::Keyboard::Z },
	{ "ANNOY KEY", 2, sf::Keyboard::LSystem },
	{ "ANNOY KEY", 2, sf::Keyboard::RSystem },
	N0,
	N0,
	N0,
	{ "NUM 0", 2, sf::Keyboard::Numpad0 },
	{ "NUM 1", 2, sf::Keyboard::Numpad1 },
	{ "NUM 2", 2, sf::Keyboard::Numpad2 },
	{ "NUM 3", 2, sf::Keyboard::Numpad3 },
	{ "NUM 4", 2, sf::Keyboard::Numpad4 },
	{ "NUM 5", 2, sf::Keyboard::Numpad5 },
	{ "NUM 6", 2, sf::Keyboard::Numpad6 },
	{ "NUM 7", 2, sf::Keyboard::Numpad7 },
	{ "NUM 8", 2, sf::Keyboard::Numpad8 },
	{ "NUM 9", 2, sf::Keyboard::Numpad9 },
	{ "*", 2, sf::Keyboard::Multiply },
	{ "+", 2, sf::Keyboard::Add },
	{ "ENTER", 2, sf::Keyboard::Return },
	{ "-", 2, sf::Keyboard::Subtract },
	{ ".", 2, sf::Keyboard::Period },
	{ "/", 2, sf::Keyboard::Divide },
	{ "F1", 2, sf::Keyboard::F1 },
	{ "F2", 2, sf::Keyboard::F2 },
	{ "F3", 2, sf::Keyboard::F3 },
	{ "F4", 2, sf::Keyboard::F4 },
	{ "F5", 2, sf::Keyboard::F5 },
	{ "F6", 2, sf::Keyboard::F6 },
	{ "F7", 2, sf::Keyboard::F7 },
	{ "F8", 2, sf::Keyboard::F8 },
	{ "F9", 2, sf::Keyboard::F9 },
	{ "F10", 2, sf::Keyboard::F10 },
	{ "F11", 2, sf::Keyboard::F11 },
	{ "F12", 2, sf::Keyboard::F12 },
	{ "F13", 2, sf::Keyboard::F13 },
	{ "F14", 2, sf::Keyboard::F14 },
	{ "F15", 2, sf::Keyboard::F15 },
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,
	N0,

	N0,		// 145
	{ "=", 2, sf::Keyboard::Equal },   // '=' key on numpad	// 146
	N0,
	N0,   // 'Unregister word' key
	N0,   // 'Register word' key

	N0,		// 150
	N0,
	N0,
	N0,
	N0,

	N0,		// 155
	N0,
	N0,
	N0,
	N0,

	{ "L-SHIFT", 2, sf::Keyboard::LShift },		// 160
	{ "R-SHIFT", 2, sf::Keyboard::RShift },
	{ "L-CTRL", 2, sf::Keyboard::LControl },
	{ "R-CTRL", 2, sf::Keyboard::RControl },
	N0,

	N0,		// 165
	N0,
	N0,
	N0,
	N0,

	N0,		// 170
	N0,
	N0,
	N0,
	N0,

	N0,		// 175
	N0,
	N0,
	N0,
	N0,

	N0,		// 180
	N0,
	N0,
	N0,
	N0,

	N0,		// 185
	{ "; :", 2, sf::Keyboard::SemiColon },
	{ "= +", 2, sf::Keyboard::Equal },
	{ ", <", 2, sf::Keyboard::Comma },
	{ "- _", 2, sf::Keyboard::Dash },

	{ ". >", 2, sf::Keyboard::Period },		// 190
	{ "/ ?", 2, sf::Keyboard::Slash },
	{ "` ~", 2, sf::Keyboard::Tilde },
	N0,
	N0,

	N0,		// 195
	N0,
	N0,
	N0,
	N0,

	N0,		// 200
	N0,
	N0,
	N0,
	N0,

	N0,		// 205
	N0,
	N0,
	N0,
	N0,

	N0,		// 210
	N0,
	N0,
	N0,
	N0,

	N0,		// 215
	N0,
	N0,
	N0,
	{ "[ {", 2, sf::Keyboard::LBracket },

	{ "\\ |", 2, sf::Keyboard::BackSlash },		// 220
	{ "] }", 2, sf::Keyboard::RBracket },
	{ "\" '", 2, sf::Keyboard::Quote },
	N0,
	N0,

	{ "P1 LEFT",  3, 0 },		// 225
	{ "P1 RIGHT", 3, 1 },
	{ "P1 UP",    3, 2 },
	{ "P1 DOWN",  3, 3 },
	{ "P1 B1",    3, 0 },

	{ "P1 B2",    3, 1 },		// 230
	{ "P1 B3",    3, 2 },
	{ "P1 B4",    3, 3 },
	{ "P1 B5",    3, 4 },
	{ "P1 B6",    3, 5 },

	{ "P1 B7",    3, 6 },		// 235
	{ "P1 B8",    3, 7 },
	{ "P1 B9",    3, 8 },
	{ "P1 B10",   3, 9 },
	{ "P2 LEFT",  3, 0 },

	{ "P2 RIGHT", 3, 1 },		// 240
	{ "P2 UP",    3, 2 },
	{ "P2 DOWN",  3, 3 },
	{ "P2 B1",    3, 0 },
	{ "P2 B2",    3, 1 },

	{ "P2 B3",    3, 2 },		// 245
	{ "P2 B4",    3, 3 },
	{ "P2 B5",    3, 4 },
	{ "P2 B6",    3, 5 },
	{ "P2 B7",    3, 6 },

	{ "P2 B8",    3, 7 },		// 250
	{ "P2 B9",    3, 8 },
	{ "P2 B10",   3, 9 },
	N0,
	N0

};
