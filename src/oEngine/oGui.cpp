﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oGui.hpp"

namespace oo
{

	// =================================== CONTROL ============================== //

	IControl::~IControl()
	{
		CGuiManager* gm = Tools::GetGuiManager();
		for (auto it = ChildControls.begin(); it != ChildControls.end(); ++it )
		{
			if ((*it) == gm->MouseCapture) gm->MouseCapture = nullptr;
			if ((*it) == gm->HoverControl) gm->HoverControl = nullptr;

			if ((*it) == gm->FocusControl)
			{
				gm->ResetFocus();
			}

			delete (*it);
		}
	}

	void IControl::AddControl(IControl* control)
	{
		assert( control );
		control->LocationN = control->GetLocation();
		control->Parent = this;
		control->Location = Location + control->GetLocation();
		if ( control->BackgroundWindow )
		{
			auto first_non_background = std::find_if(ChildControls.begin(), ChildControls.end(),
				[](IControl* c)
				{
					return c->BackgroundWindow != true;
				}
			);

			ChildControls.insert(first_non_background, control);
		}
		else ChildControls.push_back( control );

		control->OnCreate();
	}

	void IControl::Remove()
	{
		ReadyToDelete = true;
		Visible = false;
	}

	void IControl::BringToTop()
	{
		if ( Parent && !BackgroundWindow )
		{
			if ( !Parent->ChildControls.empty() )
			{
				// Szukamy trafionej kontrolki
				auto itc = std::find( Parent->ChildControls.begin(), Parent->ChildControls.end(), this );
				if ( itc != Parent->ChildControls.end() )
				{
					Parent->ChildControls.erase( itc );
					Parent->ChildControls.push_back( this );
				}
			}
		}
	}

	IControl* IControl::HitTest( const vect2f& pos, u32 flags )
	{
		auto it = ChildControls.rbegin();
		for ( ;it != ChildControls.rend(); ++it )
		{
			if ( (*it)->GetVisible() && (*it)->GetRegion().IsPointInside( pos ) )
			{
				BringToTop();
				SignalMouse( pos, flags );
				return (*it)->HitTest( pos, flags );
			}
		}

		BringToTop();
		SignalMouse( pos, flags );
		return this;
	}

	bool IControl::GetHover()
	{
		return Tools::GetGuiManager()->HoverControl == this;
	}

	bool IControl::GetFocus()
	{
		 return Tools::GetGuiManager()->FocusControl == this;
	}

	bool IControl::GetMouseCapture()
	{
		return Tools::GetGuiManager()->MouseCapture == this;
	}

	IControl* IControl::CheckHover(const vect2f& pos)
	{
		auto it = ChildControls.rbegin();
		for ( ;it != ChildControls.rend(); ++it )
		{
			if ( (*it)->GetVisible() && (*it)->GetRegion().IsPointInside( pos ) )
			{
				return (*it)->CheckHover( pos );
			}
		}

		return this;
	}

	void IControl::OnRenderBody(CVectorLayer& vl)
	{
		CFontManager& fm = *(Tools::GetFontManager());
		ChildControls_t::iterator it;

		if (MultiWindow)
		{
			for (it = ChildControls.begin(); it != ChildControls.end(); ++it)
			{
				if ((*it)->GetVisible())
				{
					(*it)->OnRenderBody(vl);
					Tools::GetVectorManager()->FlushLayer(vl);

					if ((*it)->Font != nullptr) fm.SelectFont((*it)->Font);
					(*it)->OnRenderText(fm);
				}
			}
		}
		else
		{
			for (it = ChildControls.begin(); it != ChildControls.end(); ++it)
			{
				if ((*it)->GetVisible())
				{
					(*it)->OnRenderBody(vl);
				}
			}

			Tools::GetVectorManager()->FlushLayer(vl);

			for (it = ChildControls.begin(); it != ChildControls.end(); ++it)
			{
				if ((*it)->GetVisible())
				{
					if ((*it)->Font != nullptr) fm.SelectFont((*it)->Font);
					(*it)->OnRenderText(fm);
				}
			}
		}
	}

	void IControl::UpdateRectangles()
	{
		for (auto it = ChildControls.begin(); it != ChildControls.end(); ++it )
		{
			(*it)->Location = Location + (*it)->LocationN;
		}
	}

	void IControl::OnUpdate()
	{
		if (!ChildControls.empty())
		{
			bool wanterase = false;	// Pomocnicza zmienna

			CGuiManager* gm = Tools::GetGuiManager();
			for (auto it = ChildControls.begin(); it != ChildControls.end(); ++it )
			{
				// Usuwamy to co mozna usunac
				if ((*it)->ReadyToDelete)
				{
					if ((*it) == gm->FocusControl)
					{
						gm->ResetFocus();
					}

					delete (*it);
					(*it) = nullptr;

					wanterase = true;
					continue;
				}

				if ( (*it)->GetVisible() ) (*it)->OnUpdate();
			}

			// Jak byly jakies nullptre to je usuwamy z kontenera
			if ( wanterase ) ChildControls.erase( std::remove( ChildControls.begin(), ChildControls.end(), (IControl*)nullptr ), ChildControls.end() );
		}
	}

	// ======================================== BUTTON =========================== //

	const colorf B_Border	= MakeColorf(0x000000ff);
	const colorf B_Body		= MakeColorf(0xFFFFFFff);
	const colorf B_BBody	= MakeColorf(0xAAAAAAff);
	const colorf B_Text		= MakeColorf(0x000000ff);

	void CButton::OnRenderBody( CVectorLayer& vl )
	{
		vl.DrawFullRectangle( GetRegion(), 4.0f * FontScaleFactor, B_Border, GetMouseCapture() ? B_BBody : B_Body );
	}

	void CButton::OnRenderText( CFontManager& fm )
	{
		fm.SelectFont( Font );
		fm.DrawStaticText( Label, GetRegion(), B_Text, B_Text, FM_WORD_WRAP | FM_V_CLIP | FM_H_CENTER | FM_V_CENTER );
	}

	void CButton::SignalMouse( const vect2f& pos, u32 flags )
	{
		if ( flags&GM_DBCLICK)
		{
			if (GetMouseCapture())
			{
				flags |= GM_UP;
			}
			else
			{
				flags |= GM_DOWN;
			}
		}

		if ( flags&GM_LEFT )
		{
			if  ( ( flags&GM_DOWN ) && GetRegion().IsPointInside( pos ) )
			{
				Tools::GetGuiManager()->MouseCapture = this;
			}
			else if ( ( flags&GM_UP ) && GetMouseCapture() )
			{
				Tools::GetGuiManager()->MouseCapture = nullptr;

				if ( GetRegion().IsPointInside( pos ) )
				{
					if ( OnClick )
					{
						OnClick( this );
					}
				}
			}
		}
	}

	// ======================================== CHECKBOX =========================== //

	const colorf CB_Border			= MakeColorf(0x073E73FF);
	const colorf CB_Background		= MakeColorf(0xFFFFFFFF);
	const colorf CB_HoverBackground	= MakeColorf(0xEFEFEFFF);
	const colorf CB_Cross			= MakeColorf(0xFF200CFF);
	const colorf CB_Text			= MakeColorf(0xFFFFFFFF);

	CCheckBox::CCheckBox() : MouseButtonDown(false), Check(false)
	{
		CheckRectSize = Size.y;
	}

	CCheckBox::~CCheckBox()
	{
	}

	void CCheckBox::OnRenderBody( CVectorLayer& vl )
	{
		vl.DrawFullRectangle( RectCheck, 1.0f, CB_Border, GetHover()?CB_HoverBackground:CB_Background );

		if (Check)
		{
			vl.DrawFillRectangle( Crossp1, CB_Cross );
			vl.DrawFillRectangle( Crossp2, CB_Cross );
		}



	}

	void CCheckBox::OnRenderText( CFontManager& fm )
	{
		fm.DrawStaticText( Label, RectText, CB_Text, CB_Text, FM_WORD_WRAP | FM_H_LEFT | FM_V_CENTER );

	}

	void CCheckBox::SignalMouse( const vect2f& pos, u32 flags )
	{
		if ( flags&GM_LEFT )
		{
			if  ( ( flags&GM_DOWN ) && GetRegion().IsPointInside( pos ) )
			{
				Tools::GetGuiManager()->MouseCapture = this;
				MouseButtonDown = true;
			}
			else if ( ( flags&GM_UP ) && ( MouseButtonDown == true ) )
			{
				Tools::GetGuiManager()->MouseCapture = nullptr;
				MouseButtonDown = false;
				if ( GetRegion().IsPointInside( pos ) )
				{
					Check = !Check;
					if ( OnCheckedChanged ) OnCheckedChanged( this, Check?1:0 );
				}
			}
		}
	}

	void CCheckBox::OnUpdate()
	{
		float halfheight = CheckRectSize * 0.5f;

		RectCheck.Set( vect2f( Location.x + 1.0f, Location.y + (Size.y * 0.5f - halfheight) ), CheckRectSize, CheckRectSize );
		RectText.Set( Location.x + (CheckRectSize * 1.2f) + 1.0f, Location.y + 1.0f, Location.x + Size.x - 1.0f, Location.y + Size.y - 1.0f );

		vect2f c( RectCheck.left + halfheight, RectCheck.top + halfheight );
		rectf tmp( 0, 0, halfheight * 1.5f, 3.0f );
		tmp.SetCenter( c );

		CGraphManager::CalcPoints( tmp, c, HALFPI/2.0f, Crossp1 );
		CGraphManager::CalcPoints( tmp, c, -HALFPI/2.0f, Crossp2 );

	}

	// ======================================== TRACKBAR =========================== //

	const colorf SB_Line = MakeColorf(0x25384fff);
	const colorf SB_HLine = MakeColorf(0x2c425dff);
	const colorf SB_Button = MakeColorf(0x5d7086ff);

	CTrackBar::CTrackBar()
	{
		Min = 0;
		Max = 100;
		Value = 0;
		Point = 0;
	}

	void CTrackBar::SetValue(int value)
	{
		Bsize = static_cast<int>(Size.x) - 16;

		Value = ClipValue<int>(value, Min, Max);
		Point = static_cast<float>( (Bsize*(Value-Min))/(Max-Min) );
		if (OnValueChange) OnValueChange(this, (int)Value);
	}

	void CTrackBar::OnRenderBody(CVectorLayer& vl)
	{
		const colorf black(0.0f, 0.0f, 0.0f, 1.0f);
		vl.DrawFillRectangle(RectInside, black);
		vl.DrawFullRectangle(RectMini, FontScaleFactor * 3.0f, black, N_COLOR);
	}

	void CTrackBar::OnUpdate()
	{
		Bsize = static_cast<int>(Size.x) - 16;

		if (GetMouseCapture())
		{
			vect2f pos = GetClientCursorPos();
			Point = pos.x - Location.x - 8.0f;
			Point = ClipValue<float>( Point, 0.0f, static_cast<float>(Bsize) );
			Value = Min+(int)((Point/Bsize)*(Max-Min));
			if (OnValueChange) OnValueChange(this, (int)Value);
		}

		RectInside = GetRegion();
		RectInside.Expand(-1.0f);
		RectInside.ExpandV(- RectInside.GetHeight() * 0.4f);

		float w = FontScaleFactor * 20.0f;

		RectMini.Set(vect2f(Location.x+8.0f+Point-(w*0.5f), Location.y), w, Size.y);
		RectMini.ExpandV(-3.0f);

		IControl::OnUpdate();
	}

	void CTrackBar::SignalMouse(const vect2f& pos, u32 flags)
	{
		if (flags&GM_LEFT)
		{
			if  ( (flags&GM_DOWN) && GetRegion().IsPointInside(pos) )
			{
				Tools::GetGuiManager()->MouseCapture = this;
			}
			else if (flags&GM_UP && GetMouseCapture())
			{
				Point = floor(((Bsize*(Value-Min))/(Max-Min)) + 0.5f);
				Tools::GetGuiManager()->MouseCapture = nullptr;
			}
		}
	}

	void CTrackBar::SignalKeys(int key, u32 flags)
	{
		if (flags&GM_DOWN)
		{
			if (key == sf::Keyboard::Left) SetValue( Value-1 );
			else if (key == sf::Keyboard::Right) SetValue( Value+1 );
		}
	}

	// ======================================== LABEL ============================= //

	CLabel::CLabel()
	{
		Fill = false;
		Style = H_LEFT | V_TOP;
		BodyColor.Set(1.0f, 1.0f, 1.0f, 1.0f);
		TextColor.Set(0.0f, 0.0f, 0.0f, 1.0f);
		MultiColor = false;
	}

	CLabel::~CLabel()
	{
	}

	void CLabel::OnRenderBody(CVectorLayer& vl)
	{
		if (Fill) vl.DrawFillRectangle(GetRegion(), BodyColor);
	}

	void CLabel::OnRenderText(CFontManager& fm)
	{
		rectf rect = GetRegion();

		fm.SelectFont(Font);
		if (MultiColor) fm.DrawColorText(Label, rect, FM_WORD_WRAP /*| FM_V_CLIP*/ | Style );
		else fm.DrawStaticText(Label, rect, TextColor, TextColor, FM_WORD_WRAP /*| FM_V_CLIP*/ | Style );
	}

	// ======================================== MEMO ============================= //

	const colorf ME_Border	= MakeColorf(0x002545FF);
	const colorf ME_Body	= MakeColorf(0x445A71FF);
	const colorf ME_Font	= MakeColorf(0xD78100ff);

	CMemo::CMemo(msize max_lines) : Lines(max_lines)
	{
		Offset = 0.0f;

		Hf = 0.0f;

		BodyColor = ME_Body;
		BorderColor = ME_Border;
		TextColor = ME_Font;
		ScrollColor.Set(1.0f, 0.5f, 0.0f);

		SizeX = 0.0f;
		SizeY = 0.0f;

		SubRect.Zero();
	}

	void CMemo::UpdateSubRect()
	{
		SubRect.Set(Location, Size.x, Size.y);
		SubRect.left += 3.0f;
		SubRect.right -= 6.0f;
		SubRect.ExpandV(-1.0f);

		SizeX = SubRect.GetWidth();
		SizeY = SubRect.GetHeight();
	}

	void CMemo::SetRegion(const rectf& region)
	{
		IControl::SetRegion(region);
		UpdateSubRect();
	}

	void CMemo::SetSize(const vect2f& size)
	{
		Size.Set( std::max(size.x, 50.0f), std::max(size.y, 50.0f) );
		UpdateSubRect();
	}

	void CMemo::SetFont(const RefFont& font)
	{
		Hf = font->GetLineHeight();
		Font = font;
	}

	float CMemo::CalcScrollSize()
	{
		float lc = static_cast<float>(Lines.size());
		float ls = (lc * Hf);

		float ps;
		if (ls > SizeY && IsNotZero(ls))
		{
			ps = SizeY / ls;
		}
		else ps = 2.0f;

		return SizeY * ps;
	}

	void CMemo::SignalMouse(const vect2f& pos, u32 flags)
	{
	}

	void CMemo::SignalKeys(int key, u32 flags)
	{
		float line_count = static_cast<float>(Lines.size());
		float g = (1.0f / line_count) * Hf;

		if (flags&GM_DOWN)
		{
			if (key == sf::Keyboard::Up)
			{
				Offset = ClipValue(Offset + g, 0.0f, 1.0f);
			}

			if (key == sf::Keyboard::Down)
			{
				Offset = ClipValue(Offset - g, 0.0f, 1.0f);
			}
		}
	}

	void CMemo::OnRenderBody(CVectorLayer& vl)
	{
		rectf r = GetRegion();
		vl.DrawFullRectangle(r, 1.0f, BorderColor, BodyColor);

		float size = CalcScrollSize();
		float goff = OffsetToPos(1.0f - Offset, size) + 1;

		if (size < SizeY)
		{
			vl.DrawFillRectangle( rectf( r.RightTop() + vect2f(-5.0f, goff+1.0f), 3.0f, size-2.0f ), ScrollColor );
			vl.DrawFillRectangle( rectf( r.RightTop() + vect2f(-4.0f, goff), 1.0f, size ), ScrollColor );
		}
	}

	void CMemo::OnRenderText(CFontManager& fm)
	{
		glEnable (GL_SCISSOR_TEST);
		glScissor(static_cast<GLint>(SubRect.left), 
				  static_cast<GLint>(GetScreenY() - SubRect.bottom), 
				  static_cast<GLint>(SubRect.GetWidth()), 
				  static_cast<GLint>(SubRect.GetHeight()) );

			fm.SelectFont(Font);

			float count_x_hf = static_cast<float>(Lines.size()) * Hf;
			float b = SubRect.bottom - count_x_hf;
			rectf r(SubRect.left, b, SubRect.right + 25.0f, b+Hf);
			r.Move(0, (count_x_hf*Offset) - (SizeY*Offset));

			for (auto& line : Lines)
			{
				if ((SubRect.top < r.bottom) && (SubRect.bottom > r.top))
				{
					fm.DrawStaticText( line, r, TextColor, TextColor, FM_H_CLIP | FM_H_LEFT | FM_V_TOP );
				}
				r.Move(0.0f, Font->GetLineHeight());
			}
		glDisable(GL_SCISSOR_TEST);
	}

	void CMemo::OnUpdate()
	{
		UpdateSubRect();
	}

	// ======================================== LISTBOX ============================ //

	CListBox::CListBox() : FitToItems(true), ItemHeight(16.0f),
						   DisplaySelectedItem(-1), SelectedItem(-1), LastSelectedItem(-1),
						   TrackMouse(true), Clip(false), ScrollOffset(0.0f)
	{
	}

	rectf CListBox::MoveRectByScrollOffset(const rectf& r)
	{
		float content_size = ItemHeight * Items.size();

		if (Size.y < content_size)
		{
			return r.NewByMove( vect2f(0.0f, -ScrollOffset * (content_size - Size.y) ) );
		}

		return r;
	}

	void CListBox::SignalMouse(const vect2f& pos, u32 flags)
	{
		if ( flags&GM_LEFT )
		{
			if  ( ( flags&GM_DOWN ) && GetRegion().IsPointInside( pos ) )
			{
				Tools::GetGuiManager()->MouseCapture = this;
				TrackItem();
			}
			else if ( ( flags&GM_UP ) && GetMouseCapture() )
			{
				Tools::GetGuiManager()->MouseCapture = nullptr;
				if ( GetRegion().IsPointInside( pos ) )
				{
					SelectedItem = DisplaySelectedItem;
					if ( OnItemSelectedChanged ) OnItemSelectedChanged(this, SelectedItem);
					TrackItem();

					if ( OnClick ) OnClick( this );
				}
			}
		}
	}

	void CListBox::SetItems(const std::vector<tstring>& items)
	{
		Clear();
		Items = items;
		SelectItemByIndex(0);
	}

	void CListBox::SelectItemByIndex(int index)
	{
		if (Items.empty()) SelectedItem = DisplaySelectedItem = -1;
		else SelectedItem = DisplaySelectedItem = ClipValue<int>(index, 0, Items.size() - 1);
	}

	void CListBox::SignalKeys(int key, u32 flags)
	{
		if (flags&GM_DOWN && !GetMouseCapture())
		{
			if (DisplaySelectedItem == -1) { DisplaySelectedItem = 0; return; }

			if (key == sf::Keyboard::Up) SelectItemByIndex(DisplaySelectedItem - 1);
			else if (key == sf::Keyboard::Down) SelectItemByIndex(DisplaySelectedItem + 1);
		}
	}

	void CListBox::OnRenderBody(CVectorLayer& vl)
	{
		vl.DrawFillRectangle(ControlRect, LB_Border);
		vl.DrawFillRectangle(PrivateRect, LB_Background);

		if (DisplaySelectedItem >= 0)
		{
			vl.DrawFullRectangle(MoveRectByScrollOffset(SelectedRect), 1, LB_SelectBorder, LB_SelectColor);
		}
	}

	void CListBox::OnRenderText(CFontManager& fm)
	{
		for (msize i = 0; i < Items.size(); i++)
		{
			rectf rect(vect2f(PrivateRect.left, PrivateRect.top + i * ItemHeight), PrivateRect.GetWidth(), ItemHeight);
			rect.ExpandH(-4.0f);

			fm.DrawStaticText( Items[i], MoveRectByScrollOffset(rect), LB_Font, LB_Font, FM_H_LEFT | FM_V_CENTER );
		}
	}

	void CListBox::TrackItem()
	{
		vect2f p = GetClientCursorPos();

		if (PrivateRect.IsPointInside(p))
		{
			float e = p.y - MoveRectByScrollOffset(ControlRect).top;
			e /= ItemHeight;
			SelectItemByIndex((int)floor(e));
		}
	}

	void CListBox::OnUpdate()
	{
		if (FitToItems)
		{
			ControlRect.Set( Location, Size.x, (Items.size() * ItemHeight) + 2.0f );
			Size.y = (Items.size() * ItemHeight) + 2.0f;
		}
		else ControlRect = GetRegion();

		PrivateRect = ControlRect;
		PrivateRect.Expand(-1.0f);

		SelectedRect.Set( vect2f(PrivateRect.left, PrivateRect.top + DisplaySelectedItem * ItemHeight), PrivateRect.GetWidth(), ItemHeight  );
		SelectedRect.Expand(-1.0f);

		if ((TrackMouse || GetMouseCapture()) && GetFocus())
		{
			TrackItem();
		}
	}

	// ======================================== COMBOBOX =========================== //

	const colorf CBX_Background		= MakeColorf(0xb2bfcdff);
	const colorf CBX_Border			= MakeColorf(0x002545ff);
	const colorf CBX_Triangle		= MakeColorf(0x1966ffff);

	CComboBox::CComboBox()
	{
		ItemHeight = 25.0f;

		Style = CBSx_DROPDOWNLIST;

		MultiWindow = false;

		MouseButtonDown = false;

		ShowList = false;

		SelectedItem = -1;

		TextBox = nullptr;
		ListBox = nullptr;

		BlockFocus = false;

		SignalOnSameValue = true;
		LastIndex = -1;
	}

	CComboBox::~CComboBox()
	{
	}

	void CComboBox::SetRegion(const rectf& region)
	{
		IControl::SetRegion(region);
		RefreshRects();
		if (TextBox) TextBox->SetRegion(TextRect);
	}

	void CComboBox::SetSize(const vect2f& size)
	{
		IControl::SetSize(size);
		RefreshRects();
		if (TextBox) TextBox->SetSize(TextRect.GetSize());
	}

	void CComboBox::SetFont(const RefFont& font)
	{
		IControl::SetFont(font);
		if (TextBox) TextBox->SetFont(this->Font);
	}

	void CComboBox::OnCreate()
	{
		RefreshRects();

		TextBox = new CTextBox;
		TextBox->SetSize(TextRect.GetSize());
		TextBox->SetFont(this->Font);
		TextBox->SetDrawBody(false);
		TextBox->SetText(GetSelectedItem());
		TextBox->OnFocusLost = std::bind(&CComboBox::CheckContent, this, p::_1);
		AddControl(TextBox);
		TextBox->SetLocation(TextRect.LeftTop());
		TextBox->SetEnable(false);

		SetStyle(Style);

		// Tworzone tylko przy pokazywaniu (niszczone przy znikaniu)
		ListBox = nullptr;
	}

	void CComboBox::ListBoxFocusLost(IControl* control)
	{
		ListBox->Remove();
		ListBox = nullptr;
	}

	void CComboBox::ListBoxItemSelectedChanged(IControl* control, int val)
	{
		bool inform = SignalOnSameValue || LastIndex != val;

		SelectedItem = val;
		if (TextBox && inform) TextBox->SetText(Items[val]);

		// HACK
		Tools::GetGuiManager()->FocusRequest = nullptr;
		ListBox->OnFocusLost = nullptr;

		ListBoxFocusLost(this);

		if (inform)
		{
			// Informujemy zainteresowanych
			if (OnItemSelectedChanged) OnItemSelectedChanged(this, SelectedItem);
		}
	}

	void CComboBox::SetStyle(EComboBoxStyle style)
	{
		Style = style;

		if (TextBox)
		{
			TextBox->SetStyle( (style==CBSx_NORMAL) ? CTextBox::TBS_Normal : CTextBox::TBS_NoSelect );
		}
	}

	void CComboBox::ShowListBox()
	{
		if (ListBox == nullptr)
		{
			rectf thisrect = GetRegion();

			ListBox = new CListBox;
			ListBox->SetFont(this->Font);
			ListBox->SetVisible(true);
			ListBox->SetSize( vect2f(thisrect.GetWidth(), 0.0f) );
			ListBox->SetFitToItems(true);
			ListBox->SetTrackMouse(true);
			ListBox->SetItemHeight(ItemHeight);
			ListBox->OnFocusLost = std::bind(&CComboBox::ListBoxFocusLost, this, p::_1);
			ListBox->OnItemSelectedChanged = std::bind(&CComboBox::ListBoxItemSelectedChanged, this, p::_1, p::_2);
			ListBox->SetItems(Items);
			ListBox->SelectItemByIndex(SelectedItem);

			Tools::GetGuiManager()->AddControl( ListBox );
			ListBox->SetLocation( thisrect.LeftBottom() + vect2f(0.0f, 1.0f) );

			this->BringToTop();

			Tools::GetGuiManager()->FocusRequest = ListBox;
		}
		else assert(0);	// Normalnie tutaj nie dochodzimy
	}

	void CComboBox::CheckContent(IControl* control)
	{
		if (OnCheckTextContent)
		{
			OnCheckTextContent(this);
		}
	}

	tstring CComboBox::GetTextContent()
	{
		assert(Style == CBSx_NORMAL);

		if (Style == CBSx_NORMAL)
		{
			return TextBox->GetText();
		}

		return N_STRING;
	}

	void CComboBox::SetTextContent(const tstring& text)
	{
		assert(Style == CBSx_NORMAL);

		if (Style == CBSx_NORMAL)
		{
			SelectItemByIndex(0);
			TextBox->SetText(text);
		}
	}

	void CComboBox::FocusTexContent()
	{
		assert(Style == CBSx_NORMAL);

		if (Style == CBSx_NORMAL)
		{
			Tools::GetGuiManager()->FocusRequest = TextBox;
			Tools::GetGuiManager()->CheckFocusRequest();
		}
	}

	void CComboBox::SetItems(const std::vector<tstring>& items)
	{
		Items = items;
		SelectItemByIndex(0);
	}

	tstring CComboBox::GetItemByIndex(int index)
	{
		if (InRange<int>(index, 0, Items.size()))
		{
			return Items[index];
		}
		else
		{
			return N_STRING;
		}
	}

	int CComboBox::Find(const tstring& item, bool cs)
	{
		assert(cs == true); // Brak implementacji dla szukania bez rozrozniania wielkosci liter TODO FIXME BUG

		for (msize i=0; i<Items.size(); i++)
		{
			if (Items[i] == item) return (int)i;
		}

		return -1;
	}

	void CComboBox::TrySelect(const tstring& item, bool cs)
	{
		int i = Find(item, cs);
		if (i != -1) SelectItemByIndex(i);
	}

	void CComboBox::SelectItemByIndex(int index)
	{
		if (Items.empty())
		{
			SelectedItem = -1;
			if (TextBox) TextBox->ClearText();
			return;
		}
		else
		{
			SelectedItem = ClipValue<int>(index, 0, Items.size() - 1);
		}

		if (TextBox) TextBox->SetText(Items[SelectedItem]);
	}

	void CComboBox::SignalMouse(const vect2f& pos, u32 flags)
	{
		bool hit = false;
		if (Style == CBSx_NORMAL) hit = ButtonRect.IsPointInside(pos);
		else hit = ControlRect.IsPointInside(pos);

		if ( (ListBox == nullptr) && (flags&GM_LEFT) && hit )
		{
			if (flags&GM_DOWN && (Style != CBSx_SHOWONLY))
			{
				if (OnDropDown) OnDropDown(this);
				ShowListBox();
				LastIndex = SelectedItem;
				BlockFocus = true;
			}
		}

		if ((flags&GM_UP) && BlockFocus)
		{
			if (ListBox)
			{
				ListBox->BringToTop();
				Tools::GetGuiManager()->FocusRequest = ListBox;
			}
			BlockFocus = false;
		}
	}

	void CComboBox::OnRenderBody(CVectorLayer& vl)
	{
		vl.DrawFullRectangle( GetRegion(), 1, CBX_Border, CBX_Background );

		if (Style != CBSx_SHOWONLY)
		{
			vl.DrawFillTriangle( Triangle[0], Triangle[1], Triangle[2], CBX_Triangle );
		}

		IControl::OnRenderBody(vl);
	}

	void CComboBox::OnRenderText(CFontManager& fm)
	{
	}

	void CComboBox::RefreshRects()
	{
		//Size.y = ItemHeight + 2.0f;
		ControlRect = GetRegion();

		if (Style != CBSx_SHOWONLY)
		{
		//	ControlRect.Expand(-1.0f);
			float s = ControlRect.GetWidth() * 0.35f;
			ButtonRect.Set(ControlRect.right - s, ControlRect.top, ControlRect.right, ControlRect.bottom);
			TextRect.Set(ControlRect.left + 1.0f, ControlRect.top, ControlRect.right - s - 1.0f, ControlRect.bottom);
		//	TextBox->UpdateRectangles();

			vect2f buttsize = ButtonRect.GetSize();
			Triangle[0].Set(ButtonRect.left + buttsize.x * 0.2f, ButtonRect.top + buttsize.y * 0.3f );
			Triangle[1].Set(ButtonRect.left + buttsize.x * 0.8f, ButtonRect.top + buttsize.y * 0.3f );
			Triangle[2].Set(ButtonRect.left + buttsize.x * 0.5f, ButtonRect.top + buttsize.y * 0.7f );
		}
		else
		{
			ButtonRect.Zero();
			TextRect.Set(ControlRect.left + 2.0f, ControlRect.top, ControlRect.right - 2.0f, ControlRect.bottom);
		}
	}

	void CComboBox::OnUpdate()
	{
		RefreshRects();
		if (TextBox) TextBox->SetLocation(TextRect.LeftTop());

	//	UpdateRectangles();
		IControl::OnUpdate();
	}

	// ======================================== TEXTBOX =========================== //

	const colorf TB_Background		= MakeColorf(0xb2bfcdff);
	const colorf TB_HoverBackground = MakeColorf(0xc1cdd9ff);
	const colorf TB_Text			= MakeColorf(0x000000FF);
	const colorf TB_Border			= MakeColorf(0x002545ff);
	const colorf TB_WhiteText		= MakeColorf(0x76899eFF);
	const colorf TB_SelectColor		= MakeColorf(0x19d3ff44);
	const colorf TB_SelectBorder	= MakeColorf(0x1966ffff);

	const float	TB_TextOffset = 2.0f;

	CTextBox::CTextBox()
	{
		CursorBegin = 0.0f;
		CursorEnd = 0.0f;

		CharBegin = 0;
		CharEnd = 0;

		TextWidth = 0.0f;

		UpDown = false;
		DrawBody = true;

		MaxLength = 128;

		Cursor = 1.0f;
		Offset = 0.0f;

		TextBoxStyle = TBS_Normal;
	}

	CTextBox::~CTextBox()
	{
	}

	void CTextBox::GetSelectToPosLen(int& pos, int& len)
	{
		if (CharBegin == CharEnd)
		{
			pos = CharEnd;
			len = 0;
			return;
		}

		if (CharBegin > CharEnd) // ___E____B___
		{
			pos = CharEnd;
			len = CharBegin - CharEnd;
		}
		else					 // ___B____E___
		{
			pos = CharBegin;
			len = CharEnd - CharBegin;
		}
	}

	void CTextBox::EraseSelect()
	{
		int pos;
		int len;

		// Pobieramy pozycje i dlugosc zaznaczenia w znakach
		GetSelectToPosLen(pos, len);
		if (len <= 0) return;

		// Usuwamy zaznaczone znaki
		Text.erase(pos, len);

		// Odswiezamy zaznaczenie
		ChangeSelect(pos, pos);
	}

	void CTextBox::InsertText(const tstring& str)
	{
		// Usuwamy zaznaczony tekst, jesli jest
		EraseSelect();

		// Wstawiamy tekst w pozycje kursora
		std::u32string str32 = Decode(str);
		Text.insert(CharEnd, str32);

		// Odswiezamy zaznaczenie
		ChangeSelect(CharEnd + str32.size(), CharEnd + str32.size());
	}

	void CTextBox::CopySelect(tstring& out_string)
	{
		int pos;
		int len;

		// Pobieramy pozycje i dlugosc zaznaczenia w znakach
		GetSelectToPosLen(pos, len);
		if (len <= 0) out_string.clear();

		out_string = Encode(Text.substr(pos, len));
	}

	void CTextBox::CheckTextWidth()
	{
		TextWidth = Font->GetStringWidth(Text, 0, Text.size());
	}

	void CTextBox::ChangeSelect(int begin, int end)
	{
		ClipValueRef<int>(begin, 0, Text.size());
		ClipValueRef<int>(end, 0, Text.size());

		if (begin == end)
		{
			float w = Font->GetStringWidth(Text, 0, end);
			CursorBegin = w;
			CursorEnd = w;

			CharBegin = end;
			CharEnd = end;

			CursorChange();
			return;
		}
		else
		{
			if (begin != CharBegin)
			{
				CursorBegin = Font->GetStringWidth(Text, 0, begin);
				CharBegin = begin;
			}

			if (end != CharEnd)
			{
				CursorEnd = Font->GetStringWidth(Text, 0, end);
				CharEnd = end;
			}
		}

		CursorChange();
	}

	void CTextBox::UpdateCursor(const vect2f& pos, bool start)
	{
		if (Text.empty())
		{
			CursorBegin = 0.0f;
			CursorEnd = 0.0f;
			TextWidth = 0.0f;

			return;
		}

		float off = (pos - Location).x - Offset;
		float w = - 0.0f;
		float g = - 0.0f;
		bool gotit = false;

		const msize textsize = Text.size();

		for (msize i = 0; i < textsize; i++)
		{
			w += Font->GetGlyphWidth(Text[i]);

			if ( (i != 0) && (i + 1) != textsize ) g = Font->GetGlyphWidth(Text[i + 1]) * 0.5f;
			else g = 0;

			if ((w > off) && (w - g) <= off) g = 0;

			if ((w - g) > off)
			{
				w -= Font->GetGlyphWidth(Text[i]);

				if (start)
				{
					CharBegin = i;
					CursorBegin = w;
					CursorEnd = w;
				}
				else
				{
					CharEnd = i;
					CursorEnd = w;
				}

				gotit = true;
				break;
			}
		}

		if (!gotit)
		{
			if (start)
			{
				CharBegin = textsize;
				CursorBegin = w;
				CursorEnd = w;
			}
			else
			{
				CharEnd = textsize ;
				CursorEnd = w;
			}
		}

		CursorChange();
	}

	void CTextBox::OnRenderBody(CVectorLayer& vl)
	{
		rectf rect = GetRegion();

		bool f = GetFocus() && (TextBoxStyle != TBS_NoSelect);

		if (DrawBody)
		{
			vl.DrawFullRectangle(rect, 1.0f, TB_Border, f?TB_HoverBackground:TB_Background);
		}

		if (f)
		{
			if ((CharBegin != CharEnd) && (TextBoxStyle != TBS_NoSelect))
			{
			//	float pt = rect.GetWidth();
			//	float g = pt - TextWidth;
			//	if (g < 0.0f) Offset = g;
			//	else Offset = 0.0f;
			//	rectf rtmp = rect;
			//	rtmp.Move( Offset, 0.0f );

				float bx;
				float ex;
				if (CursorBegin > CursorEnd) { bx = CursorEnd; ex = CursorBegin; }
				else  { bx = CursorBegin; ex = CursorEnd; }
				bx += TB_TextOffset;
				ex += TB_TextOffset;

				rectf selectrect( Location + vect2f( bx + Offset, 3 ), ex - bx , Size.y - 6 );
				rectf bodyrect = GetRegion();
				bodyrect.ExpandH(-1.0f);

				bool left = selectrect.left < bodyrect.left;
				bool right = selectrect.right > bodyrect.right;
				selectrect = IntersectRect(selectrect, bodyrect);

				{
					const rectf& r = selectrect;
					const colorf& bcolor = TB_SelectBorder;
					const colorf& fill = TB_SelectColor;
					const int bsize = 1;

					rectf rtab[5];

					// left
					rtab[0].Set( r.left,			r.top,				r.left + bsize,		r.bottom );
					// right
					rtab[1].Set( r.right,			r.top,				r.right - bsize,	r.bottom );
					// bottom
					rtab[2].Set( r.left + bsize,	r.top,				r.right - bsize,	r.top + bsize );
					// top
					rtab[3].Set( r.left + bsize,	r.bottom - bsize,	r.right - bsize,	r.bottom );
					// center
					rtab[4].Set( r.left + bsize,	r.top + bsize,		r.right - bsize,	r.bottom - bsize );

					if (!left) vl.DrawFillRectangle(rtab[0], bcolor);
					if (!right) vl.DrawFillRectangle(rtab[1], bcolor);
					vl.DrawFillRectangle(rtab[2], bcolor);
					vl.DrawFillRectangle(rtab[3], bcolor);
					vl.DrawFillRectangle(rtab[4], fill);
				}

				// vl.DrawFullRectangle(selectrect, 1, TB_SelectBorder, TB_SelectColor );
			}
			else if (TextBoxStyle == TBS_Normal)
			{
				vect2f pos;
				pos.Set(Location.x + CursorEnd + TB_TextOffset + Offset, Location.y + 3);
				vl.DrawFillRectangle( rectf(pos, 1, Size.y-6), colorf(1.0f, 0.0f, 0.0f, Cursor) );
			}
		}
	}

	void CTextBox::OnRenderText(CFontManager& fm)
	{
		rectf rect = GetRegion();

		rect.Move( Offset, 0.0f );

		rect.ExpandH( -TB_TextOffset );

		bool f = GetFocus() && (TextBoxStyle != TBS_NoSelect);

		glEnable(GL_SCISSOR_TEST);
		glScissor(static_cast<GLint>(Location.x + 2), 
				  static_cast<GLint>(GetScreenY() - Location.y - Size.y), 
				  static_cast<GLint>(Size.x - 4), 
				  static_cast<GLint>(Size.y) );

		fm.SelectFont(Font);
		if (Text.empty() && !f)
		{
			fm.DrawStaticText(Label, rect, TB_WhiteText, TB_WhiteText, FM_V_CENTER );
		}
		else
		{
			fm.DrawStaticText(Encode(Text), rect, TB_Text, TB_Text, FM_V_CENTER );
		}

		glDisable(GL_SCISSOR_TEST);
	}

	void CTextBox::OnUpdate()
	{
		if (GetFocus())
		{
			if (UpDown) Cursor += GetDeltaTime() * 2.0f;
			else Cursor -= GetDeltaTime() * 2.0f;

			if (Cursor>1.0f) UpDown = false;
			if (Cursor<0.0f) UpDown = true;
		}

		if (GetMouseCapture())
		{
			vect2f pos = GetClientCursorPos();
			UpdateCursor(pos, false);
		}

		if ( (GetHover() || GetMouseCapture()) && (TextBoxStyle != TBS_NoSelect) )
		{
			ChangeSysCursor( SC_IBEAM );
			LockSysCursor(this);
		}
		else
		{
			UnlockSysCursor(this);
		}

		IControl::OnUpdate();
	}

	void CTextBox::CursorChange()
	{
		float base = FloatIs(CursorBegin, CursorEnd) ? CursorBegin : CursorEnd;

		while ( base >= (Size.x + -Offset) )
		{
			Offset -= 50.0f;
		}

		while ( base < -Offset )
		{
			Offset += 50.0f;
		}
	}

	void CTextBox::SignalMouse(const vect2f& pos, u32 flags)
	{
		if (flags&GM_LEFT)
		{
			if (flags&GM_DBCLICK)
			{
				ChangeSelect(0, Text.size());
			}

			if  (flags&GM_DOWN)
			{
				Tools::GetGuiManager()->MouseCapture = this;
				UpdateCursor(pos, true);
			}
			else if (flags&GM_UP && GetMouseCapture())
			{
				UnlockSysCursor(this);
				Tools::GetGuiManager()->MouseCapture = nullptr;
			}
		}
	}

	void CTextBox::SignalChar(char32_t c)
	{
		if (TextBoxStyle == TBS_Normal)
		{
			if (c>=0x20) // char
			{
				if (CharBegin != CharEnd) EraseSelect();

				if (Text.size() < MaxLength)
				{
					if (CharEnd == Text.size())
					{
						Text += c;
						CharBegin++;
						CharEnd++;
						CursorEnd += Font->GetGlyphWidth(c);
						CursorBegin = CursorEnd;
					}
					else
					{
						Text.insert(CharEnd, 1, c);
						ChangeSelect(CharEnd+1, CharEnd+1);
					}
				}
			}
			else if (c == 0x16) // paste
			{
				tstring result = GetFromClipboard();
				result = NormalizeWhitespace(result);
				InsertText( result.substr(0, MaxLength - Text.size()) );
			}
			else if (c == 0x18) // cut
			{
				tstring out_result;
				CopySelect(out_result);
				CopyToClipboard(out_result);

				EraseSelect();
			}
			else if (c == 0x08) // backspace
			{
				if (CharBegin != CharEnd) EraseSelect();
				else
				{
					if ((CharEnd == Text.size()) && (!Text.empty()))
					{
						CharBegin--;
						CharEnd--;
						CursorEnd -= Font->GetGlyphWidth(Text[Text.size()-1]);
						CursorBegin = CursorEnd;
						Text.erase(Text.size()-1);
					}
					else
					{
						int v = CharEnd - 1;
						if (v>=0)
						{
							Text.erase(v, 1);
							ChangeSelect(CharEnd-1, CharEnd-1);
						}
					}
				} // if (CharBegin != CharEnd)
			}

			if (OnTextChange) OnTextChange(this, static_cast<int>(c));
			CheckTextWidth();
		} // if (TextBoxStyle == TBS_Normal)

		if ((TextBoxStyle == TBS_Normal) || (TextBoxStyle == TBS_ReadOnly))
		{
			if (c == 0x03) // copy
			{
				tstring out_result;
				CopySelect(out_result);
				CopyToClipboard(out_result);
			}
		}

		CursorChange();
	}

	void CTextBox::SignalKeys(int key, u32 flags)
	{
		if (flags&GM_SHIFT)
		{
			if (flags&GM_DOWN)
			{
				switch (key)
				{
					case sf::Keyboard::Left: ChangeSelect(CharBegin, CharEnd-1); break;
					case sf::Keyboard::Right: ChangeSelect(CharBegin, CharEnd+1); break;

					case sf::Keyboard::Delete: break;
					case sf::Keyboard::Home: ChangeSelect(CharBegin, 0); break;
					case sf::Keyboard::End: ChangeSelect(CharBegin, Text.size()); break;
				}
			}
		}
		else
		{
			if (flags&GM_DOWN)
			{
				switch (key)
				{
					case sf::Keyboard::Left: ChangeSelect(CharEnd-1, CharEnd-1); break;
					case sf::Keyboard::Right: ChangeSelect(CharEnd+1, CharEnd+1); break;

					case sf::Keyboard::Delete: EraseSelect(); break;
					case sf::Keyboard::Home: ChangeSelect(0, 0); break;
					case sf::Keyboard::End: ChangeSelect(Text.size(), Text.size()); break;
				}
			}
		}
	}

	// ======================================== WINDOW =========================== //

	const colorf W_Border = MakeColorf(0x455e8aa1);
	const colorf W_Body = MakeColorf(0x2d445eff);
	const colorf W_TextA(1.00f, 1.00f, 1.00f, 1.00f);
	const colorf W_TextB(0.90f, 0.90f, 0.90f, 1.00f);
	const colorf W_Cross(0.80f, 0.80f, 0.80f, 1.00f);

	CWindow::CWindow()
	{
		RectLabel.Zero();
		RectCenter.Zero();

		MinimumSize.Set(300.0f, 200.0f);
		Resize = true;

		MouseOffset.Set(0.0f, 0.0f);
		BoundMouseOffset.Set(0.0f, 0.0f);

		LabelHeight = 25.0f;

		HoverBound = BOUND_NONE;

		for (int i = 0; i < RECTBOUND_COUNT; i++)
		{
			RectBounds[i].Zero();
		}

		MouseButtonDownX = false;

		IsMoving = false;
		NoMove = false;
	}

	CWindow::~CWindow()
	{
	}

	void CWindow::UpdateRects()
	{
		rectf rect = GetRegion();

		if (!Equal(RectCenter, rect))
		{
			const float bx = 3.0f;
			const float bbx = 20.0f;
			const float bby = 20.0f;

			RectBounds[RB_TOP].Set		(rect.left+bbx	, rect.top			, rect.right-bbx	, rect.top+bx		);
			RectBounds[RB_BOTTOM].Set	(rect.left+bbx	, rect.bottom-bx	, rect.right-bbx	, rect.bottom		);
			RectBounds[RB_LEFT].Set		(rect.left		, rect.top+bby		, rect.left+bx		, rect.bottom-bby	);
			RectBounds[RB_RIGHT].Set	(rect.right-bx	, rect.top+bby		, rect.right		, rect.bottom-bby	);
			RectBounds[RB_LT1].Set		(rect.left		, rect.top			, rect.left+bbx		, rect.top+bx		);
			RectBounds[RB_LT2].Set		(rect.left		, rect.top			, rect.left+bx		, rect.top+bby		);
			RectBounds[RB_RT1].Set		(rect.right-bbx	, rect.top			, rect.right		, rect.top+bx		);
			RectBounds[RB_RT2].Set		(rect.right-bx	, rect.top			, rect.right		, rect.top+bby		);
			RectBounds[RB_LB1].Set		(rect.left		, rect.bottom-bx	, rect.left+bbx		, rect.bottom		);
			RectBounds[RB_LB2].Set		(rect.left		, rect.bottom-bby	, rect.left+bx		, rect.bottom		);
			RectBounds[RB_RB1].Set		(rect.right-bbx	, rect.bottom-bx	, rect.right		, rect.bottom		);
			RectBounds[RB_RB2].Set		(rect.right-bx	, rect.bottom-bby	, rect.right		, rect.bottom		);
/*
			RectBounds[RB_RBM].Set();
*/

			const float lableh = LabelHeight; // (25.0f);
			const float borderX = (6.0f);
			const float borderY = (6.0f);

			RectLabel.Set(rect.left+borderX, rect.top+bx, rect.right-borderX, rect.top+lableh);

			RectBody.Set(rect.left + borderX, rect.top+lableh, rect.right-borderX, rect.bottom -borderY);

			float g = 16.0f;
			CrossRect.Set(RectLabel.RightTop() - vect2f(g, -1.0f), g, g);

			RectCenter = rect;
		}
	}

	void CWindow::OnRenderBody(CVectorLayer& vl)
	{
//		vl.DrawFillRoundRectangle( RectCenter, 10, 16, W_Border );
		vl.DrawFillRectangle(RectCenter, W_Border);
		vl.DrawFillRectangle(RectBody, W_Body);

		if (OnX) vl.DrawCross(CrossRect, W_Cross);

		OverDrawWindow(vl);

		IControl::OnRenderBody(vl);
	}

	void CWindow::OnRenderText(CFontManager& fm)
	{
		fm.SelectFont(Font);
		fm.DrawStaticText(Label, RectLabel, W_TextA,  W_TextA, FM_V_CENTER | FM_H_LEFT | FM_H_CLIP);

	}

	const ESysCursor GSysCursorLookupTable[] = { SC_SIZENS, SC_SIZENS,
												 SC_SIZEWE, SC_SIZEWE,
												 SC_SIZENWSE, SC_SIZENWSE,
												 SC_SIZENESW, SC_SIZENESW, SC_SIZENESW, SC_SIZENESW,
												 SC_SIZENWSE, SC_SIZENWSE, SC_SIZENWSE };

	inline void CalcBound(float minimum, float rectsize, float& location, float& size, float rupper, float rlower)
	{
		if (rectsize > minimum)
		{
			location = rupper;
			size = rectsize;
		}
		else
		{
			location = rlower - minimum;
			size = minimum;
		}
	}

	void CWindow::ResetMove()
	{
		UnlockSysCursor(this);
		Tools::GetGuiManager()->MouseCapture = nullptr;
		IsMoving = false;
	}

	void CWindow::OnUpdate()
	{
		vect2f pos = GetClientCursorPos();

		if (IsMoving && HoverBound == BOUND_NONE)
		{
			SetLocation(pos - MouseOffset);
			UpdateRectangles();
		}

		if (GetHover() == false)
		{
			HoverBound = BOUND_NONE;
		}

		if (Resize && !NoMove)
		{
			if (!IsMoving && GetHover())
			{
				HoverBound = BOUND_NONE;

				for (int i = 0; i < RECTBOUND_COUNT; i++)
				{
					if (RectBounds[i].IsPointInside(pos))
					{
						UnlockSysCursor(this);
						ChangeSysCursor(GSysCursorLookupTable[i]);
						LockSysCursor(this);
						HoverBound = i;
						break;
					}
				}

				if (HoverBound == BOUND_NONE) UnlockSysCursor(this);
			}

			if (!GetHover()) UnlockSysCursor(this);

			if (IsMoving && HoverBound != BOUND_NONE)
			{
				vect2f opos =  pos - BoundMouseOffset;
				ChangeSysCursor(GSysCursorLookupTable[HoverBound]);
				rectf r = GetRegion();

				switch (HoverBound)
				{
					case RB_TOP:
						r.top = opos.y;
						CalcBound(MinimumSize.y, r.GetHeight(), Location.y, Size.y, r.top, r.bottom);
					break;

					case RB_BOTTOM:
						r.bottom = opos.y;
						Size.y = (r.GetHeight() > MinimumSize.y) ? r.GetHeight() : MinimumSize.y;
					break;

					case RB_LEFT:
						r.left = opos.x;
						CalcBound(MinimumSize.x, r.GetWidth(), Location.x, Size.x, r.left, r.right);
					break;

					case RB_RIGHT:
						r.right = opos.x;
						Size.x = (r.GetWidth() > MinimumSize.x) ? r.GetWidth() : MinimumSize.x;
					break;

					case RB_LT1:
					case RB_LT2:
						r.left = opos.x;
						r.top = opos.y;
						CalcBound(MinimumSize.x, r.GetWidth(), Location.x, Size.x, r.left, r.right);
						CalcBound(MinimumSize.y, r.GetHeight(), Location.y, Size.y, r.top, r.bottom);
					break;

					case RB_RT1:
					case RB_RT2:
						r.right = opos.x;
						r.top = opos.y;
						Size.x = (r.GetWidth() > MinimumSize.x) ? r.GetWidth() : MinimumSize.x;
						CalcBound(MinimumSize.y, r.GetHeight(), Location.y, Size.y, r.top, r.bottom);
					break;

					case RB_LB1:
					case RB_LB2:
						r.left = opos.x;
						r.bottom = opos.y;
						CalcBound(MinimumSize.x, r.GetWidth(), Location.x, Size.x, r.left, r.right);
						Size.y = (r.GetHeight() > MinimumSize.y) ? r.GetHeight() : MinimumSize.y;
					break;

					case RB_RB1:
					case RB_RB2:
						r.right = opos.x;
						r.bottom = opos.y;
						Size.x = (r.GetWidth() > MinimumSize.x) ? r.GetWidth() : MinimumSize.x;
						Size.y = (r.GetHeight() > MinimumSize.y) ? r.GetHeight() : MinimumSize.y;
					break;
				}
			}
		}

		UpdateRectangles();
		UpdateRects();
		IControl::OnUpdate();
	}

	void CWindow::SignalMouse(const vect2f& pos, u32 flags)
	{
		if (OnX && IsMoving == false)
		{
			if ( flags&GM_LEFT )
			{
				if  ( ( flags&GM_DOWN ) && CrossRect.IsPointInside( pos ) )
				{
					Tools::GetGuiManager()->MouseCapture = this;
					MouseButtonDownX = true;
				}
				else if ( ( flags&GM_UP ) && ( MouseButtonDownX == true ) )
				{
					Tools::GetGuiManager()->MouseCapture = nullptr;
					MouseButtonDownX = false;
					if ( CrossRect.IsPointInside( pos ) )
					{
						OnX( this );
					}
				}
			}
		}

		if (NoMove) return;

		if (flags&GM_LEFT && MouseButtonDownX == false)
		{
			if  ((flags&GM_DOWN) && (RectLabel.IsPointInside(pos) || ( HoverBound != BOUND_NONE )) )
			{
				if (Resize)
				{
					if ( HoverBound == RB_LB1 || HoverBound == RB_LB2 ) BoundMouseOffset = pos - RectBounds[HoverBound].LeftBottom();
					else if ( HoverBound == RB_RT1 || HoverBound == RB_RT2 ) BoundMouseOffset = pos - RectBounds[HoverBound].RightTop();
					else if ( HoverBound == RB_TOP || HoverBound == RB_LEFT || HoverBound == RB_LT1 || HoverBound == RB_LT2 ) BoundMouseOffset = pos - RectBounds[HoverBound].LeftTop();
					else if ( HoverBound != BOUND_NONE ) BoundMouseOffset = pos - RectBounds[HoverBound].RightBottom();
				}

				Tools::GetGuiManager()->MouseCapture = this;
				IsMoving = true;
				MouseOffset = pos - Location;
			}

			if (flags&GM_UP /*&& IsMoving == true*/)
			{
				ResetMove();
			}
		}
	}

	void CWindow::SignalChar(char32_t c)
	{
		IControl::SignalChar(c);
	}

	void CWindow::SignalKeys(int key, u32 flags)
	{
	}

	// ============================== GUI MANAGER =========================== //

	CGuiManager::CGuiManager()
	{
		MainPlane.Parent = &MainPlane;
		FocusControl = nullptr;
		FocusRequest = nullptr;
		ResetFocus();
	}

	CGuiManager::~CGuiManager()
	{
		ResetFocus();
	}

	void CGuiManager::ResetFocus()
	{
		if (FocusControl != nullptr)
		{
			if (FocusControl->OnFocusLost) FocusControl->OnFocusLost(FocusControl);
			FocusControl = nullptr;
		}

		MouseCapture = nullptr;
		HoverControl = nullptr;
		FocusControl = &MainPlane;
	}

	void CGuiManager::CheckFocusRequest()
	{
		if (FocusRequest)
		{
			IControl* control = FocusRequest;

			if (FocusControl != control)
			{
				if (FocusControl->OnFocusLost) FocusControl->OnFocusLost(FocusControl);
				if (control->OnFocusGot) control->OnFocusGot(control->Parent);

				FocusControl = control;
			}

			FocusRequest = nullptr;
			MouseCapture = nullptr;
			HoverControl = nullptr;
		}
	}

	void CGuiManager::Update()
	{
		MainPlane.OnUpdate();

	}

	void CGuiManager::Render()
	{
		CVectorLayer vl;
		MainPlane.OnRenderBody( vl );

	//	vl.DrawFullRectangle( FocusControl->GetRegion(), 1, N_COLOR, colorf(1,0,0,0.5) );
		if (HoverControl)
		{
	//	vl.DrawFullRectangle( HoverControl->GetRegion(), 1, N_COLOR, colorf(0,1,0,0.5) );
		}
		Tools::GetVectorManager()->FlushLayer( vl );
	}

	void CGuiManager::CheckHover(const vect2f& pos)
	{
		if (MouseCapture) return;
		else
		{
			IControl* control = MainPlane.CheckHover(pos);

			if (HoverControl != control)
			{
				if (HoverControl == nullptr) HoverControl = control;
				else
				{
					if (HoverControl->OnLeave) HoverControl->OnLeave(HoverControl->Parent);
					if (control->OnEnter) control->OnEnter(control);

					HoverControl = control;
				}
			}
		}
	}

	void CGuiManager::HitTest(const vect2f& pos, u32 flags)
	{
		if (MouseCapture) MouseCapture->SignalMouse(pos, flags);
		else
		{
			IControl* control = MainPlane.HitTest(pos, flags);

			if (FocusRequest) control = FocusRequest;

			if (FocusControl != control)
			{
				if (FocusControl->OnFocusLost) FocusControl->OnFocusLost(FocusControl);
				if (control->OnFocusGot) control->OnFocusGot(control->Parent);

				FocusControl = control;
			}

			FocusRequest = nullptr;
		}
	}

	void CGuiManager::SignalChar(char32_t c)
	{
		FocusControl->SignalChar(c);
	}

	void CGuiManager::SignalKeys(int key, u32 flags)
	{
		FocusControl->SignalKeys(key, flags);
	}

} // End oo
