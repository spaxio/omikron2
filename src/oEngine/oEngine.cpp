﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oEngine.hpp"

namespace oo
{

	tstring GetEngineVersion() { return "v 1.0.0." + ToString(GAME_BUILD); }

	CFontManager*		Tools::FontManager		= nullptr;
	CGuiManager*		Tools::GuiManager		= nullptr;
	CVectorManager*		Tools::VectorManager	= nullptr;
	CGraphManager*		Tools::GraphManager		= nullptr;

	void Tools::SetUpTools()
	{
		FontManager = new CFontManager();
		GuiManager = new CGuiManager();
		VectorManager = new CVectorManager();
		GraphManager = new CGraphManager();
	}

	void Tools::ShutDownTools()
	{
		delete GraphManager; GraphManager = nullptr;
		delete VectorManager; VectorManager = nullptr;
		delete GuiManager; GuiManager = nullptr;
		delete FontManager; FontManager = nullptr;
	}

} // End oo
