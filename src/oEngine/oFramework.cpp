﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oFramework.hpp"
#include "oGraph.hpp"
#include "oDownload.hpp"

#if defined O2_SYS_WINDOWS
	#include "GL/wglext.h"
#elif defined O2_SYS_LINUX
    #include "GL/glx.h"
#endif

PFNGLBINDBUFFERARBPROC				glBindBuffer			= nullptr;
PFNGLDELETEBUFFERSARBPROC			glDeleteBuffers			= nullptr;
PFNGLGENBUFFERSARBPROC				glGenBuffers			= nullptr;
PFNGLBUFFERDATAARBPROC				glBufferData			= nullptr;
PFNGLBUFFERSUBDATAARBPROC			glBufferSubData			= nullptr;
PFNGLMAPBUFFERARBPROC				glMapBuffer				= nullptr;
PFNGLUNMAPBUFFERARBPROC				glUnmapBuffer			= nullptr;

#ifdef O2_SYS_WINDOWS
extern int main(int argc, char** argv);
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	main(__argc, __argv);
}
#endif

namespace oo
{

	std::unique_ptr<sf::Window> GAppWindow;

	// Szkielet aplikacji
	struct SAppFramework
	{
		CDisplayMode SysDisplayMode;	// Systemowy tryb graficzny
		CDisplayMode DisplayMode;		// Aktualnie ustawiony tryb graficzny
		EFullScreenMode Fullscreen;		// Pelny ekran? (Ustawiony dla aplkacji)

		float FloatScreenX;
		float FloatScreenY;

		bool MultisampleSupport;
		int MultisampleFormat;

		bool NonPower2Support;
	};
	SAppFramework GAppFramework;

	// Stan aplikacji
	struct SAppState
	{
		bool Active;
		bool LoadCompleted;
		bool Vsync;

		u32 RandomSeed;
		u32 RandomHelp;
		vect2f CursorPos;
		bool IsRequestingExit;

		u8 StateC[256];
	};
	SAppState GAppState;

	CDisplayMode GetSysDisplayMode()
	{
		return GAppFramework.SysDisplayMode;
	}

	u32 GetRandomSeed()
	{
		GAppState.RandomHelp++;
		GAppState.RandomHelp &= static_cast<u32>( time(nullptr) );
		GenCRC((u8*)&GAppState.RandomHelp, sizeof(u32), GAppState.RandomSeed);

		return GAppState.RandomSeed;
	}

	bool TextureNonPower2Support()
	{
		return GAppFramework.NonPower2Support;
	}

	bool CheckGLExt(const char* name)
	{
		if(strstr((const char*)glGetString(GL_EXTENSIONS), name) == nullptr) return false;
		else return true;
	}

	float GetScreenX() { return static_cast<float>(GAppFramework.DisplayMode.Width); }
	float GetScreenY() { return static_cast<float>(GAppFramework.DisplayMode.Height); }

	float GetFloatScreenX() { return GAppFramework.FloatScreenX; }
	float GetFloatScreenY() { return GAppFramework.FloatScreenY; }

	vect2f GetScreenXY() { return vect2f((float)GAppFramework.DisplayMode.Width, (float)GAppFramework.DisplayMode.Height); }
	vect2f GetFloatScreenXY() { return vect2f(GAppFramework.FloatScreenX, GAppFramework.FloatScreenY); }

	vect2f GetClientCursorPos()
	{
		auto pos = sf::Mouse::getPosition(*GAppWindow);
		return vect2f(static_cast<float>(pos.x), static_cast<float>(pos.y));
	}

	void CopyToClipboard(const tstring& text)
	{
#ifdef O2_SYS_WINDOWS
		if (OpenClipboard(nullptr))
		{
			HGLOBAL hGlobal = nullptr;
			char *p = nullptr;
			hGlobal = GlobalAlloc (GHND, (text.size() + 1) * sizeof(char) );

			if ( hGlobal )
			{
				p = (char*) GlobalLock(hGlobal);
				if (p)
				{
					CopyMemory(p,text.c_str(), (text.size() + 1) * sizeof(char) );

					EmptyClipboard ();
					SetClipboardData ( CF_UNICODETEXT , hGlobal);
					GlobalUnlock(hGlobal);
				}
			}
			CloseClipboard();
		}
#endif
	}

	tstring GetFromClipboard()
	{
		tstring rs;

#ifdef O2_SYS_WINDOWS
		if (OpenClipboard(nullptr))
		{
			if (IsClipboardFormatAvailable ( CF_UNICODETEXT ))
			{
				char *p = nullptr;

				HGLOBAL hGlobal = GetClipboardData( CF_UNICODETEXT );

				p = (char *) GlobalLock(hGlobal);
				rs += p;
				GlobalUnlock(hGlobal);
			}

			CloseClipboard();
		}
#endif

		return rs;
	}


	// Pokazuje kursor
	void ShowSysCursor()
	{
		GAppWindow->setMouseCursorVisible(true);
	}

	// Ukrywa kursor
	void HideSysCursor()
	{
		GAppWindow->setMouseCursorVisible(false);
	}

#ifdef O2_SYS_WINDOWS
	LPCSTR GCursorNames [] = {
		IDC_ARROW,
		IDC_IBEAM,
		IDC_SIZENWSE,
		IDC_SIZENESW,
		IDC_SIZEWE,
		IDC_SIZENS,
		IDC_SIZEALL
	};
	HCURSOR GCursors[] = { 0, 0, 0, 0, 0, 0, 0 };
#endif

	ESysCursor GActiveCursor = SC_ARROW;

	bool GSysCursorLock = false;
	void* GWhoLockedSysCursor = nullptr;

	void SetSysCursor(ESysCursor sc)
	{
#ifdef O2_SYS_WINDOWS
		int i = sc;
		if (GCursors[i] == nullptr)
		{
			GCursors[i] = LoadCursorA(nullptr, GCursorNames[i]);
		}

		SetCursor(GCursors[i]);
#endif
	}

	void ChangeSysCursor(ESysCursor sc)
	{
		if (GSysCursorLock) return;
		SetSysCursor(sc);
		GActiveCursor = sc;
	}

	void LockSysCursor(void* who)
	{
		if (GSysCursorLock) return;

		GWhoLockedSysCursor = who;
		GSysCursorLock = true;
	}

	void UnlockSysCursor(void* who)
	{
		if (GWhoLockedSysCursor == who)
		{
			GSysCursorLock = false;
		}
	}

	void ForceUnlockSysCursor()
	{
		GSysCursorLock = false;
		GWhoLockedSysCursor = nullptr;
		ChangeSysCursor(SC_ARROW);
	}

	extern const char* CLASSNAME;
	extern const char* APPNAME;
	extern const char* APPDIR;
	extern const char* CONSOLECLASSNAME;
	extern const char* CONSOLENAME;

	tstring GAppDataDirectory;

	void LogSysInfo()
	{
		tstring ss = format("[SYS] System info: \r\n"
							 "-------------------------------- \r\n"
							 "| App data dir: %s\r\n"
							 "| Game directory: %s \r\n"
							 "--------------------------------",
							 GAppDataDirectory,
							 GetExeDirectory() );

		LogMsg(ss);
	}

	bool IsAppActive() { return GAppState.Active; }

	void ProcessEvent(const sf::Event& event)
	{
		switch (event.type)
		{
			case sf::Event::Resized:
				OnResize();
			break;

			case sf::Event::GainedFocus:
				GAppState.Active = true;
			break;

			case sf::Event::LostFocus:
				GAppState.Active = false;
			break;

			case sf::Event::Closed:
				CloseApp();
			break;
		}
	}

	const char* GAspectRatioStrTab[] = { "Unknown", "16:9", "16:10", "3:2", "4:3", "5:4" };

	const float GAspectRatioVal[] = {          1.0f,
									  16.0f	/  9.0f,
									  16.0f	/ 10.0f,
									   3.0f	/  2.0f,
									   4.0f	/  3.0f,
									   5.0f	/  4.0f };

	const char* AspectRatioToString(EAspectRatio ar) { return GAspectRatioStrTab[ar]; }

	EAspectRatio CDisplayMode::GetAspectRatio() const
	{
		int ar = (int)( ( (float)Width / (float)Height ) * 10.0f );
		switch (ar)
		{
			case 17: return AR_16_9;
			case 16: return AR_16_10;
			case 15: return AR_3_2;
			case 13: return AR_4_3;
			case 12: return AR_5_4;
			default: return AR_UNKNOWN;
		}
	}

	std::vector<CDisplayMode> EnumDisplayModes(int bits, EAspectRatio aspectratio)
	{
		std::vector<CDisplayMode> v;

		auto modes = sf::VideoMode::getFullscreenModes();

		EAspectRatio ar;
		CDisplayMode s;

		for (const auto& mode : modes)
		{
			CDisplayMode newdis(mode.width, mode.height, mode.bitsPerPixel);
			if (newdis != s && ( newdis.Bits == bits || bits < 0 ) && (newdis.Width > 640))
			{
				ar = newdis.GetAspectRatio();
				if (aspectratio == AR_MAX || ar == aspectratio)
				{
					v.push_back(newdis);
					s = newdis;
				}
			}
		}

		return v;
	}

	bool CheckDisplayMode(const CDisplayMode& mode)
	{
		std::vector<CDisplayMode> sysmode = EnumDisplayModes();

		for (auto& sm : sysmode)
		{
			if (sm == mode) return true;
		}

		return false;
	}

	void SetProgramIco()
	{
#ifdef O2_SYS_WINDOWS
		auto handle = GAppWindow->getSystemHandle();

	#ifdef O2_NUWEN_MINGW
		LPCWSTR iname = MAKEINTRESOURCE(0);
	#else
		LPCWSTR iname = L"0";
	#endif

		auto ico = LoadIconW((HINSTANCE)GetModuleHandleW(nullptr), iname);
		SetClassLongW(handle, GCL_HICON, (LONG)ico);
#endif
	}

	void ResetViewport(int width, int height)
	{
		glViewport(0, 0, width, height);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_POLYGON_SMOOTH);
		glDisable(GL_CULL_FACE);
		glDisable(GL_ALPHA_TEST);

		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		GBlendMode = BM_NORMAL;

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		GTextureEnable = false;
		GBlendEnable = false;

		GUseVertex = true;
		GUseTexCoord = true;
		GUseColor = true;
	}

	void SetFloatScreen(const vect2f& offset)
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, (GLdouble)GAppFramework.FloatScreenX, 0.0, GAppFramework.FloatScreenY, -1.0, 1.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(offset.x, offset.y, 0.0f);
	}

	void SetFloatScreenFlip(const vect2f& offset)
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, (GLdouble)GAppFramework.FloatScreenX, 0.0, GAppFramework.FloatScreenY, -1.0, 1.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, (GLfloat)GAppFramework.FloatScreenY, 0.0f);
		glScalef (1.0f, -1.0f, 1.0f);
		glTranslatef(offset.x, offset.y, 0.0f);
	}

	void SetFixedScreen()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, (GLdouble)GAppFramework.DisplayMode.Width, 0.0, (GLdouble)GAppFramework.DisplayMode.Height, -1.0, 1.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, (GLfloat)GAppFramework.DisplayMode.Height, 0.0f);
		glScalef (1.0f, -1.0f, 1.0f);
	}

	void InitOpenGL()
	{
		LogMsg("[OGL] Driver: OpenGL %s : %s", (const char*)glGetString(GL_VERSION), (const char*)glGetString(GL_VENDOR));
		LogMsg("[OGL] Renderer: %s", (const char*)glGetString(GL_RENDERER));

		int max_texture_size = 0;
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_texture_size);

		LogMsg("[OGL] Max texture size: %s", max_texture_size);

		if (!CheckGLExt("GL_ARB_vertex_buffer_object"))
		{
			const char* msg = "You graphic card does not support OpenGL 1.5, which is required for this game to run. "
							   "Please make sure that your graphics card drivers are up-to date. "
							   "Keep in mind that the default Microsoft OpenGL drivers will not work. "
							   "You will need drivers directly from your graphics card manufacturer.";

			LogMsg(msg);
			ThrowCriticalError("Missing GL_ARB_vertex_buffer_object");
		}

#if defined O2_SYS_WINDOWS
		glBindBuffer =				(PFNGLBINDBUFFERARBPROC)			wglGetProcAddress("glBindBufferARB");
		glDeleteBuffers =			(PFNGLDELETEBUFFERSARBPROC)			wglGetProcAddress("glDeleteBuffersARB");
		glGenBuffers =				(PFNGLGENBUFFERSARBPROC)			wglGetProcAddress("glGenBuffersARB");
		glBufferData =				(PFNGLBUFFERDATAARBPROC)			wglGetProcAddress("glBufferDataARB");
		glBufferSubData =			(PFNGLBUFFERSUBDATAARBPROC)			wglGetProcAddress("glBufferSubDataARB");
		glMapBuffer =				(PFNGLMAPBUFFERARBPROC)				wglGetProcAddress("glMapBufferARB");
		glUnmapBuffer =				(PFNGLUNMAPBUFFERARBPROC)			wglGetProcAddress("glUnmapBufferARB");
#elif defined O2_SYS_LINUX
		glBindBuffer =				(PFNGLBINDBUFFERARBPROC)			glXGetProcAddressARB((const GLubyte*)"glBindBufferARB");
		glDeleteBuffers =			(PFNGLDELETEBUFFERSARBPROC)			glXGetProcAddressARB((const GLubyte*)"glDeleteBuffersARB");
		glGenBuffers =				(PFNGLGENBUFFERSARBPROC)			glXGetProcAddressARB((const GLubyte*)"glGenBuffersARB");
		glBufferData =				(PFNGLBUFFERDATAARBPROC)			glXGetProcAddressARB((const GLubyte*)"glBufferDataARB");
		glBufferSubData =			(PFNGLBUFFERSUBDATAARBPROC)			glXGetProcAddressARB((const GLubyte*)"glBufferSubDataARB");
		glMapBuffer =				(PFNGLMAPBUFFERARBPROC)				glXGetProcAddressARB((const GLubyte*)"glMapBufferARB");
		glUnmapBuffer =				(PFNGLUNMAPBUFFERARBPROC)			glXGetProcAddressARB((const GLubyte*)"glUnmapBufferARB");
#endif

	}

	// Pomocznicza funkcja :) przy SetDisplayMode
	void UpdateFloatScreenSize()
	{
		EAspectRatio ar = GAppFramework.DisplayMode.GetAspectRatio();

		if (ar == AR_UNKNOWN)
		{
			GAppFramework.FloatScreenX = (float)GAppFramework.DisplayMode.Width;
			GAppFramework.FloatScreenY = (float)GAppFramework.DisplayMode.Height;
		}
		else
		{
			GAppFramework.FloatScreenX = BASE_SCREEN_Y * GAspectRatioVal[ar];
			GAppFramework.FloatScreenY = BASE_SCREEN_Y;
		}
	}

	void SetDisplayMode(const CDisplayMode& mode, EFullScreenMode full)
	{
		if (GAppFramework.DisplayMode == mode && GAppFramework.Fullscreen == full) return;

		sf::Uint32 style;

		if (full == FSM_STANDARD) style = sf::Style::Fullscreen;
		else if (full == FSM_NOBORDER) style = 0; // sf::Style::None
		else style = sf::Style::Close;

		sf::ContextSettings settings;
		settings.depthBits = 0;
		settings.stencilBits = 0;
		settings.antialiasingLevel = 0;
		settings.majorVersion = 1;
		settings.minorVersion = 5;

		GAppWindow->create( sf::VideoMode(mode.Width, mode.Height, mode.Bits), APPNAME, style, settings );
		GAppWindow->setVerticalSyncEnabled( GAppState.Vsync );

		GAppFramework.DisplayMode = mode;
		GAppFramework.Fullscreen = full;

		SetProgramIco();

		ResetViewport(mode.Width, mode.Height);
		UpdateFloatScreenSize();
		OnResize();
	}

	void GetDisplayMode(CDisplayMode& mode, EFullScreenMode& full)
	{
		mode = GAppFramework.DisplayMode;
		full = GAppFramework.Fullscreen;
	}

	void SetVsync(bool enable)
	{
		GAppState.Vsync = enable;
		GAppWindow->setVerticalSyncEnabled( GAppState.Vsync );
	}

	u32 GameFrameForInput = 0;
	long long Accumulator = 0LL;
	const long long MaxFrameTime = 250LL * 1000LL;

	void GameCycle()
	{
		ChangeSysCursor(SC_ARROW);
		SetSysCursor(GActiveCursor);

		long long ft = timGetFrameTime();

		// Ucinamy za duze przedzialy, aby zachowac wzgledna stabilnosc
		if (ft > MaxFrameTime) ft = MaxFrameTime;

		Accumulator += ft;
		while (Accumulator >= PHISICSTEP_FIXED)
		{
			OnPhysic();
			GameFrameForInput++;
			Accumulator -= PHISICSTEP_FIXED;
		}

		Core::GetEngineScheduler()->Update(static_cast<float>(timGetDeltaTime()));
		OnUpdate();
		OnInput();

		glLoadIdentity();
		OnRender();

		GAppWindow->display();

		UpdateAudioSystem();

		Tools::GetGraphManager()->FrameCheckForUnusedTextures();

		UpdateNetworkSystem();
    }

	void EnterMessageLoop()
	{
		LogMsg( "[ENG] Rozpoczynam glowna petle gry" );
		GAppState.Active = true;

		timResume();

		while (GAppWindow->isOpen() && !GAppState.IsRequestingExit)
		{
			sf::Event event;
			while (GAppWindow->pollEvent(event))
			{
				ProcessEvent(event);
				OnEvent(event);
			}

			if (GAppState.Active == false)
			{
				std::this_thread::sleep_for( std::chrono::milliseconds(10) );
			}

			timBeginFrame();
			GameCycle();
			timEndFrame();
		}

		GAppState.Active = false;
		LogMsg( "[ENG] Wychodze z glownej petli gry" );
	}

	DEFINE_FUNC_SYS(unpack, "kk", "mix_file,folder")
	{
		tstring dir = GetExeDirectory() + PATH_SEP;
		tstring fullpath_input = dir + GetS(arg[0]) + ".mix";
		tstring fullpath_output = dir + GetS(arg[1]);

		if (sysDirectoryExists(fullpath_output))
		{
			out.Writeln("folder o takiej nazwie juz istnieje, podaj inna nazwe");
		}

		fullpath_output += PATH_SEP;

		if (Vfs_Unpack(fullpath_input, fullpath_output)) out.Writeln("Rozpakowywanie archiwum zakonczone sukcesem");
		else out.Writeln("Wystapil blad podczas rozpakowywania archiwum");
	};

	DEFINE_FUNC_SYS(pack, "kkss", "folder,mix_file,omit,compress")
	{
		tstring dir = GetExeDirectory() + PATH_SEP;
		tstring fullpath_input = dir + GetS(arg[0]) + PATH_SEP;
		tstring fullpath_output = dir + GetS(arg[1]) + ".mix";

		if (sysFileExists(fullpath_output))
		{
			out.Writeln("plik_mix o takiej nazwie juz istnieje, podaj inna nazwe");
		}
		else
		{
			if (Vfs_Pack(fullpath_input, fullpath_output, GetS(arg[2]), GetS(arg[3]))) out.Writeln("Tworzenie archiwum zakonczone sukcesem");
			else out.Writeln("Wystapil blad podczas tworzenia archiwum");
		}
	};

	DEFINE_FUNC_SYS(listfiles, "k", "mix_file")
	{
		tstring fullpath_input = GetExeDirectory() + PATH_SEP + GetS(arg[0]) + ".mix";

		std::vector<tstring> files;
		if (Vfs_ListFiles(fullpath_input, files))
		{
			for (auto& name : files) out.Writeln(name);
		}
	};

	DEFINE_FUNC_SYS(set, "kv", "varible_name,value")
	{
		CEngineVarTable* vt = Core::GetEngineVarTable();
		tstring key = arg[0].GetAsString();

		ESetResult result;
		switch ( arg[1].GetType() )
		{
			case VT_STRING: result = vt->SetStringValue(key, GetS(arg[1])); break;
			case VT_FLOAT:  result = vt->SetFloatValue(key, GetF(arg[1])); break;
			case VT_INT:    result = vt->SetIntValue(key, GetI(arg[1])); break;
			default: assert(0);
		}
	};

	DEFINE_FUNC_SYS(get, "k", "varible_name")
	{
		CEngineVarTable* vt = Core::GetEngineVarTable();
		tstring str = arg[0].GetAsString();
		out.Writeln( format("%s = %s", str, vt->GetValue(str) ) );
	};

	DEFINE_FUNC_SYS(viewlog, NOARGS, NOHELP)
	{
		sysOpen(GLogFilename);
	};

	DEFINE_FUNC_SYS(viewconfig, NOARGS, NOHELP)
	{
		sysOpen(GConfigFilename);
	};

	DEFINE_FUNC_SYS(restart, NOARGS, NOHELP)
	{
		// todo
	};

	DEFINE_FUNC_SYS(listvar, NOARGS, NOHELP)
	{
		CEngineVarTable* vt = Core::GetEngineVarTable();
		vt->WriteStat(out);
	};

	DEFINE_FUNC_SYS(close, NOARGS, NOHELP)
	{
		CloseApp();
	};

	DEFINE_FUNC_SYS(getexedir, NOARGS, NOHELP)
	{
		out.Writeln(GetExeDirectory());
	};

#ifndef OMIKRON2_DEDICATED_SERVER
	DEFINE_FUNC_SYS(fullscreen, NOARGS, NOHELP)
	{
		if (GAppFramework.Fullscreen == FSM_STANDARD || GAppFramework.Fullscreen == FSM_NOBORDER)
		{
			CDisplayMode mode;
			mode.Width =  Core::GetEngineConfig()->GetValue<int>("app.size", 800, 0);
			mode.Height = Core::GetEngineConfig()->GetValue<int>("app.size", 600, 1);
			mode.Bits = GAppFramework.SysDisplayMode.Bits;

			SetDisplayMode(mode, FSM_NONE);
		}
		else
		{
			SetDisplayMode(GetSysDisplayMode(), FSM_NOBORDER);
		}
	};

	DEFINE_FUNC_SYS(setdisplay, "iii", "width,height,window_mode")
	{
		CDisplayMode mode;

		mode.Width = GetI(arg[0]);
		mode.Height = GetI(arg[1]);
		mode.Bits = GAppFramework.SysDisplayMode.Bits;
		bool full = GetI(arg[2]) != 0;

		EFullScreenMode flag = full ? FSM_STANDARD : FSM_NONE;
		SetDisplayMode(mode, flag);
	};

	DEFINE_FUNC_SYS(getsysres, NOARGS, NOHELP)
	{
		out.Writeln(format("%i x %i (%s)", GAppFramework.SysDisplayMode.Width
										   , GAppFramework.SysDisplayMode.Height
										   , AspectRatioToString(GAppFramework.SysDisplayMode.GetAspectRatio())) );
	};

	DEFINE_FUNC_SYS(vsync, "i", "enable")
	{
		SetVsync( GetI(arg[0]) != 0 );
	};

	DEFINE_FUNC_SYS(listres, NOARGS, NOHELP)
	{
		std::vector<CDisplayMode> v;

		int i = AR_MAX - 1;

		while ( i > 0 )
		{
			EAspectRatio ar = (EAspectRatio)i;
			v = EnumDisplayModes(GAppFramework.SysDisplayMode.Bits, ar);
			out.Writeln( format("[%s]", AspectRatioToString(ar) ) );
			for (auto& m : v)
			{
				out.Writeln(m.ResolutionToString());
			}

			i--;
		}
	};
#endif

	void StartApp()
	{
		memset( &GAppFramework, 0, sizeof(GAppFramework) );
		memset( &GAppState, 0, sizeof(GAppState) );

		auto desktop = sf::VideoMode::getDesktopMode();

		GAppFramework.SysDisplayMode.Change(desktop.width, desktop.height, desktop.bitsPerPixel);

		auto pcfg = Core::GetEngineConfig();
		int full = pcfg->GetValue("app.fullscreen",  0, 0);
		int width = pcfg->GetValue("app.size", 800, 0);
		int height = pcfg->GetValue("app.size", 600, 1);
		int bits = desktop.bitsPerPixel;

		EFullScreenMode flag;

		if (full != 0)
		{
			width = desktop.width;
			height = desktop.height;
			flag = FSM_NOBORDER;
		}
		else
		{
			flag = FSM_NONE;
		}

		LogSysInfo();

		int aa = pcfg->GetValue("app.aa", 0, 0);

		auto clip01 = [](float v) { return ClipValue(v, 0.0f, 1.0f); };
		float svol = pcfg->CheckAndGetValue<float>("app.soundvol", clip01, 1.0f, 0);
		float mvol = pcfg->CheckAndGetValue<float>("app.musicvol", clip01, 1.0f, 0);

		GAppState.RandomSeed = static_cast<u32>( time(nullptr) );

		GAppWindow.reset( new sf::Window() );
		SetDisplayMode( CDisplayMode(width, height, bits), flag );

		InitOpenGL();

		CreateAudioSystem();

		SetSoundVolume(svol);
		SetMusicVolume(mvol);

		int vsync = pcfg->GetValue("app.vsync", 1, 0);
		SetVsync( vsync != 0 );

		// Main Loop
			OnLoad(); GAppState.LoadCompleted = true;
				EnterMessageLoop();
			OnUnload();
		// End Main Loop

		DeleteAudioSystem();

		DownloadDrop();

		// Sprzatamy... ;p
		GAppWindow->close();
	}

	void CloseApp()
	{
		GAppState.IsRequestingExit = true;
	}

	sf::Mouse::Button MouseInputCodeToSFCode(u32 code)
	{
		assert(code <= 2);
		const sf::Mouse::Button buttons[] = { sf::Mouse::Left, sf::Mouse::Middle, sf::Mouse::Right };
		return buttons[code];
	}

	sf::Keyboard::Key KeyInputCodeToSFCode(u32 code)
	{
		if (Key2Str[code].type == 2) return static_cast<sf::Keyboard::Key>(Key2Str[code].code);
		else return sf::Keyboard::Unknown;
	}

	bool CheckKey(u32 button, bool once, bool state)
	{
		if (once)
		{
			if (!state) GAppState.StateC[button] = 1;
			if (state && GAppState.StateC[button]) { GAppState.StateC[button] = 0; return true; }
			else return false;
		}
		else return state;
	}

	bool MouseState(u32 button, bool once)
	{
		bool state = sf::Mouse::isButtonPressed( MouseInputCodeToSFCode(button) );
		return CheckKey(button, once, state);
	}

	bool KeyState(u32 key, bool once)
	{
		bool state = sf::Keyboard::isKeyPressed( KeyInputCodeToSFCode(key) );
		return CheckKey(key, once, state);
	}

	bool JoyState(u32 button, bool once)
	{
		bool state = false;

		if (InRange<u32>(button, J_P1_BUTTON1, J_P1_BUTTON10))
		{
			state = sf::Joystick::isButtonPressed(0, button - J_P1_BUTTON1);
		}
		else if (InRange<u32>(button, J_P2_BUTTON1, J_P2_BUTTON10))
		{
			state = sf::Joystick::isButtonPressed(1, button - J_P2_BUTTON1);
		}
		else if (InRange<u32>(button, J_P1_LEFT, J_P1_LEFT+3))
		{
			vect2f axis = GetAxis(0);

			if (button == J_P1_LEFT && axis.x < -0.5f) state = true;
			else if (button == J_P1_RIGHT && axis.x > 0.5f) state = true;
			else if (button == J_P1_UP && axis.y < -0.5f) state = true;
			else if (button == J_P1_DOWN && axis.y > 0.5f) state = true;
		}
		else if (InRange<u32>(button, J_P2_LEFT, J_P2_LEFT+3))
		{
			vect2f axis = GetAxis(1);

			if (button == J_P2_LEFT && axis.x < -0.5f) state = true;
			else if (button == J_P2_RIGHT && axis.x > 0.5f) state = true;
			else if (button == J_P2_UP && axis.y < -0.5f) state = true;
			else if (button == J_P2_DOWN && axis.y > 0.5f) state = true;
		}
		else return false;

		return CheckKey(button, once, state);
	}

	vect2f GetAxis(int joystick)
	{
		return vect2f(
			sf::Joystick::getAxisPosition(joystick, sf::Joystick::X) * 0.01f,
			sf::Joystick::getAxisPosition(joystick, sf::Joystick::Y) * 0.01f
		);
	}

} // End oo
