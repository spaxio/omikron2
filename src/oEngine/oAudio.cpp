﻿/*
 * Copyright © 2013 Karol Pałka
 */

#include "stdafx.h"
#include "oAudio.hpp"

#ifdef OMIKRON2_DEDICATED_SERVER
namespace oo
{
	CComposition::CComposition(msize max) {}
	CComposition::~CComposition() {}
	void CComposition::SetPos(msize channel, const vect2f& p) {}
	vect2f CComposition::GetPos(msize channel) { return vect2f(0.0f, 0.0f); }
	void CComposition::SetGain(msize channel, float g) {}
	float CComposition::GetGain(msize channel) { return 0.0f; }
	void CComposition::Update() {}
	void CComposition::Play(msize channel, const tstring& filename, bool loop, float gain, const vect2f& pos) {}
	void CComposition::Stop(msize channel) {}
	void CComposition::Hush() {}

	void SetSoundVolume(float v) {}
	float GetSoundVolume() { return 0.0f; }
	void SetListenerPos(const vect2f& v) { }
	vect2f GetListenerPos() { return vect2f(0.0f, 0.0f); }
	void Scream(const tstring& filename) {}
	void Hush() {}
	bool PlayMusicFile(const tstring& path, const std::function<void()>& on_end) { return false; }
	void ReplayLastMusicFile() {}
	void StopMusicFile() {}
	void PauseMusicFile() {}
	void ResumeMusicFile() {}
	tstring GetMusicComment(const char* str) { return N_STRING; }
	void SetMusicVolume(float v) {}
	float GetMusicVolume() { return 0.0f; }
	bool UpdateMusicStream() { return false; }
	bool CreateAudioSystem() { return false; }
	void DeleteAudioSystem() {}
	void UpdateAudioSystem() {}
}

#else

#include <AL/al.h>
#include <AL/alc.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

namespace oo
{

	enum { RIFF_tag = 0x46464952 };
	enum { WAVE_tag = 0x45564157 };
	enum { FMT__tag = 0x20746D66 };
	enum { DATA_tag = 0x61746164 };

	enum { WAVE_FORMAT_TAG_PCM = 1 };

	enum { OGG_BUFFER_SIZE = 4096 * 8 };

	#pragma pack(1)
	struct WAVEFILEHEADER
	{
		// riff
		u32	RiffTag;		// Lancuch znakow "RIFF"
		u32	RiffSize;		// Rozmiar naszych danych dzwiekowych
		u32	WaveTag;		// Lancuch znakow "WAVE"
		u32	FmtTag;			// Lancuch znakow "fmt "
		u32	FmtSize;		// Rozmiar strukturki "fmt " (musi byc == 16)

		// fmt
		u16	FmtFormatTag;		// Format danych (wspieramy tylko WAVE_FORMAT_PCM)
		u16	FmtChannels;		// Liczba kanalow (mono/stereo)
		u32	FmtSamplesPerSec;	// Czestotliwosc
		u32	FmtAvgBytesPerSec;	// Liczba bajtow na sekunde
		u16	FmtBlockAlign;		// Liczba bajtow na cykl
		u16 FmtBitsPerSample;	// Liczba bitow na probke

		// data
		u32	DataTag;		// lancuch znakow "data"
		u32	DataSize;		// Rozmiar naszych danych dzwiekowych

	};
	#pragma pack()

	struct SMusicStuff
	{
		RefFile			File;			// Uchwyt na plik
		OggVorbis_File  Stream;			// Naglowek pliku ogg

		ALuint Buffers[2]; // Przedni i tylni bufor
		ALuint Source;     // Zrodlo OpenAL
		ALenum Format;     // Format OpenAL (mono/stereo)

		bool IsMusic;	// Czy mamy muzyke do odtwarzania
		bool IsPause;	// Pauza?

		tstring ActiveMusicFilename;
		std::function<void()> OnMusicEnd;

		SMusicStuff()
		{
			ClearOnStopMusic();
		}

		void ClearOnStopMusic()
		{
			ov_clear(&Stream);

			Buffers[0] = std::numeric_limits<ALuint>::max();
			Buffers[1] = std::numeric_limits<ALuint>::max();
			Source = std::numeric_limits<ALuint>::max();
			Format = AL_FORMAT_STEREO16;
			IsMusic = false;
			IsPause = false;

			File.reset();
		}
	};
	SMusicStuff GMusicStuff;	// Do muzyczki...

	ALCdevice* Device = nullptr;
	ALCcontext* Context = nullptr;

	ov_callbacks GOggCallbacks;					// A to obsluga CFile

	float GMusicGain = 1.0f;			// Wzmocnienie muzyki
	float GSoundGain = 1.0f;			// Wzmocnienie dzwieku

	inline bool IsALError(const char* str)
	{
		auto e = alGetError();

		if (e != AL_NO_ERROR)
		{
			LogMsg("[ERR] [OAL] %s : %s", str, e);
			assert(!"IsALError");
			return true;
		}

		return false;
	}

	//=======================================================================
	// Callbacki ktore pozwola funkcji ov_read czytac z naszej klasy CFile
	//=======================================================================

	size_t o_read_func(void *ptr, size_t size, size_t nmemb, void *datasource)
	{
		IStream* s = static_cast<IStream*>(datasource);
		assert(s->CanRead());

		return s->Read(ptr, size*nmemb);
	}

	int o_seek_func(void *datasource, ogg_int64_t offset, int whence)
	{
		return -1;	// Nie pozwalamy na seek'owanie
	}

	int o_close_func(void *datasource)
	{
		return 0;
	}

	long o_tell_func(void *datasource)
	{
		IStream* s = static_cast<IStream*>(datasource);
		return long(s->GetPos());
	}

	//=======================================================================
	// Dzwieki
	//=======================================================================

	bool LoadOggFile(const tstring& filename, ALuint& buffer, float& time)
	{
		time = 0.0f;

		if (buffer != AL_NONE && alIsBuffer(buffer)) { assert(0); return false; }

		RefFile file = vfsGetFile(filename);
		if(!file->IsOK())
		{
			LogMsg( "[ERR] [AUD] Nie mozna otworzyc pliku OGG : " + filename );
			return false;
		}

		OggVorbis_File  stream;			// Naglowek pliku ogg

		int result;
		if((result = ov_open_callbacks(file.get(), &stream, nullptr, 0, GOggCallbacks)) < 0)
		{
			LogMsg( "[ERR] [OAL] To nie jest prawidlowy plik OGG vorbis : " + filename );
			return false;
		}

		// Format
		ALenum format;
		if(stream.vi->channels == 1) format = AL_FORMAT_MONO16;
		else format = AL_FORMAT_STEREO16;

		std::vector<char> pdata;
		pdata.reserve(1024 * 100);
		std::vector<char> buff(1024 * 4);
		int  section;

		// Rozpakowanie calego pliku ogg do pamieci
		result = 1;
		while (result != 0)
		{
			result = ov_read(&stream, &buff[0], buff.size(), 0, 2, 1, &section);

			if (result > 0)										// Wszystko ok czytamy dalej
			{
				pdata.insert(pdata.end(), buff.begin(), buff.begin() + result);
			}
			else if (result < 0) { assert(0); return false; }	// Jakis blad w strumieniu - przerywamy czytanie
		}

		// Tworzymy buforek OpenAL
		alGenBuffers(1, &buffer);

		// Wypelniamy go danymi
		alBufferData(buffer, format, &pdata[0], pdata.size(), stream.vi->rate);

		if (IsALError("LoadOggFile::alBufferData"))
		{
			LogMsg("[ERR] [OAL] OpenAL nie moze utworzyc bufora z danych : " + filename);
			return false;
		}

		return true;
	}

	bool LoadWavFile(const tstring& filename, ALuint& buffer, float& time)
	{
		if (buffer != AL_NONE && alIsBuffer(buffer)) { assert(0); return false; }

		RefFile file = vfsGetFile(filename);
		if(!file->IsOK())
		{
			LogMsg( "[ERR] [AUD] Nie mozna otworzyc pliku WAV : " + filename );
			return false;
		}

		WAVEFILEHEADER waveheader;
		memset(&waveheader, 0, sizeof(WAVEFILEHEADER));
		file->Read(&waveheader, sizeof(WAVEFILEHEADER));

		if ( (waveheader.RiffTag != RIFF_tag) ||
			 (waveheader.WaveTag != WAVE_tag) ||
			 (waveheader.FmtTag != FMT__tag)  ||
			 (waveheader.DataTag != DATA_tag) ||
			 (waveheader.FmtFormatTag!=WAVE_FORMAT_TAG_PCM) ||
			 ((waveheader.FmtBitsPerSample!=16) && (waveheader.FmtBitsPerSample!=8)) )
		{
			LogMsg( "[ERR] [AUD] Jest to nierozpoznawalny plik wav'e (wspieramy tylko nieskompresowane sample PCM 8/16, mono/stereo) : " + filename );
			return false;
		}

		int bps = waveheader.FmtSamplesPerSec * (waveheader.FmtBitsPerSample/8);
		time = (float)waveheader.DataSize / (float)bps;

		// Tworzymy tymczasowy buforek na dane, i wypelniamy je danymi z pliku
		std::vector<u8> tmpdata(waveheader.DataSize);
		if (file->Read(&tmpdata[0], waveheader.DataSize) != waveheader.DataSize)
		{
			LogMsg( "[ERR] [AUD] Niespodziewany koniec pliku przy odczytywaniu sampli : " + filename );
			return false;
		}

		ALenum format;
		if (waveheader.FmtBitsPerSample == 16) format = ((waveheader.FmtChannels==2)?AL_FORMAT_STEREO16:AL_FORMAT_MONO16);
		else format = ((waveheader.FmtChannels==2)?AL_FORMAT_STEREO8:AL_FORMAT_MONO8);

		// Tworzymy buforek OpenAL
		alGenBuffers(1, &buffer);

		// Wypelniamy go danymi
		alBufferData(buffer, format, &tmpdata[0], tmpdata.size(), waveheader.FmtSamplesPerSec);

		if (IsALError("LoadWavFile::alBufferData"))
		{
			LogMsg("[ERR] [OAL] OpenAL nie moze utworzyc bufora z danych : " + filename);
			return false;
		}

		return true;
	}

	bool LoadSoundFile(const tstring& filename, ALuint& buffer, float& time)
	{
		tstring ext = GetFileExtension(filename);
		if (ext == ".wav") return LoadWavFile(filename, buffer, time);
		else if (ext == ".ogg") return LoadOggFile(filename, buffer, time);
		else return false;
	}

	// Klasa dzwieku
	class CSound
	{
		private: // Obiekty CSound nie sa do kopiowania
			CSound( const CSound& );
			CSound& operator=( const CSound& );

		public:
			tstring Filename;
			float Time;
			ALuint Buffer;

			CSound(const tstring& filename);
			~CSound();

			bool IsValid() { return Buffer != AL_NONE; }
	};

	typedef std::shared_ptr<CSound> RefSound;

	CSound::CSound(const tstring& filename) : Filename(filename), Time(0.0f)
	{
		if (!LoadSoundFile(filename, Buffer, Time))
		{
			LogMsg( "[ERR] [AUD] Nie mozna bylo wczytac dzwieku : " + filename );
			Buffer = AL_NONE;
		}
	}

	CSound::~CSound()
	{
		if (Buffer != AL_NONE && alIsBuffer(Buffer))
		{
			alDeleteBuffers(1, &Buffer);
			Buffer = AL_NONE;
		}
	}

	CDataManager<CSound> GSoundDataBase;

	void ClearSoundDataBase() { GSoundDataBase.Clear(); }

	RefSound LoadSound(const tstring& filename)
	{
		RefSound sound = GSoundDataBase.GetExist(filename);
		if (sound) return sound;

		sound = std::make_shared<CSound>(filename);
		if (sound->IsValid()) LogMsg("[MGR] Wczytano dzwiek " + filename);
		else LogMsg("[MGR] Brak dzwieku " + filename);

		GSoundDataBase.InsertResource(filename, sound);
		return sound;
	}

	// Zrodlo dzwieku dla dowolnego obiektu
	class CSource
	{
		private:
			typedef std::vector<ALuint> SrcList_t;

		private:
			SrcList_t SrcList;	// Lista zrodel dzwieku
			vect2f Pos;			// Pozycja zrodla w przestrzeni
			float Gain;			// Wzmocnienie dzwieku

			bool EmptyList;		// Czy lista jest pusta?

		public:
			CSource() : Pos(0.0f, 0.0f), Gain(GSoundGain) {}
			~CSource() { Hush(); }

			bool IsEmpty() { return EmptyList; }

			void SetPos(const vect2f& p);
			vect2f GetPos() { return Pos; }

			void SetGain(float g);
			float GetGain() { return Gain; }

			void Update();

			// Odtwarza dzwiek
			void Scream(const RefSound& sound);
			void Scream(const tstring& filename);

			// Ucisza
			void Hush();

	};

	CSource GSource;
	std::vector<std::future<RefSound>> GSoundLoadProcessList;

	void CSource::SetPos(const vect2f& p)
	{
		Pos = p;
		for (auto it = SrcList.begin(); it != SrcList.end(); ++it)
		{
			alSource3f(*it, AL_POSITION, p.x, p.y, 0.0f);
		}
	}

	void CSource::SetGain(float g)
	{
		Gain = ClipValue(g, 0.0f, GSoundGain);
		for (auto it  = SrcList.begin(); it != SrcList.end(); ++it)
		{
			alSourcef(*it, AL_GAIN, (ALfloat)Gain);
		}
	}

	void CSource::Update()
	{
		ALenum state;
		ALuint tmp;

		EmptyList = true;
		for (auto it  = SrcList.begin(); it != SrcList.end(); ++it)
		{
			alGetSourcei(*it, AL_SOURCE_STATE, &state);
			if (state != AL_PLAYING)
			{
				tmp = *it;
				alDeleteSources(1, &tmp);
				it = SrcList.erase(it);
				if (it == SrcList.end()) break;
			}
			EmptyList = false;
		}
	}

	void CSource::Scream(const RefSound& sound)
	{
		ALuint buffer = sound->Buffer;
		if (buffer == AL_NONE) return;

		ALuint source = 0;

		alGenSources(1, &source);

		alSourcei (source, AL_BUFFER, buffer);
		alSource3f(source, AL_POSITION,        Pos.x, Pos.y, 0.0f);
		alSource3f(source, AL_VELOCITY,         0.0f,  0.0f, 0.0f);
		alSource3f(source, AL_DIRECTION,        0.0f,  0.0f, 0.0f);
		alSourcef (source, AL_GAIN,             Gain			 );
		alSourcei (source, AL_SOURCE_RELATIVE,  AL_TRUE          );

		if (IsALError("CSource::Scream"))
		{
			if (alIsSource(source))
			{
				alDeleteSources(1, &source);
			}
			return;
		}

		alSourcePlay(source);
		SrcList.push_back(source);
	}

	void CSource::Scream(const tstring& filename)
	{
		RefSound sound = GSoundDataBase.GetExist(filename);
		if (sound) this->Scream(sound);
		else
		{
#ifndef O2_NUWEN_MINGW
			GSoundLoadProcessList.push_back( std::async( [filename]() { return std::make_shared<CSound>(filename); } ) );
#endif
		}
	}

	void CSource::Hush()
	{
		ALuint tmp;
		for (auto it = SrcList.begin(); it != SrcList.end(); ++it)
		{
			tmp = *it;
			alSourceStop(tmp);
			alDeleteSources(1, &tmp);
		}
		SrcList.clear();
	}

	CComposition::CComposition(msize max)
	{
		assert(max != 0);

		Channel.resize(max, BAD_CHANNEL);
		Pos.resize(max, vect2f(0.0f, 0.0f));
		Gain.resize(max, 1.0f);
	}

	CComposition::~CComposition()
	{
		Hush();
	}

	void CComposition::SetPos(msize channel, const vect2f& p)
	{
		assert(Pos.size() >= channel);
		Pos[channel] = p;
		if (Channel[channel] != BAD_CHANNEL)
		{
			alSource3f(Channel[channel], AL_POSITION, p.x, p.y, 0.0f);
		}
	}

	vect2f CComposition::GetPos(msize channel)
	{
		assert(Pos.size() >= channel);
		return Pos[channel];
	}

	void CComposition::SetGain(msize channel, float g)
	{
		assert(Gain.size() >= channel);
		Gain[channel] = ClipValue(g, 0.0f, GSoundGain);
		if (Channel[channel] != BAD_CHANNEL)
		{
			alSourcef(Channel[channel], AL_GAIN, (ALfloat)Gain[channel]);
		}
	}

	float CComposition::GetGain(msize channel)
	{
		assert(Gain.size() >= channel);
		return Gain[channel];
	}

	void CComposition::Update()
	{
		ALenum state;
		ALuint tmp;

		for (msize i = 0; i < Channel.size(); i++)
		{
			if (Channel[i] != BAD_CHANNEL)
			{
				alGetSourcei(Channel[i], AL_SOURCE_STATE, &state);
				if (state != AL_PLAYING)
				{
					tmp = Channel[i];
					alDeleteSources(1, &tmp);
					Channel[i] = BAD_CHANNEL;
				}
			}
		}
	}

	void CComposition::Play(msize channel, const tstring& filename, bool loop, float gain, const vect2f& pos)
	{
		assert(Gain.size() >= channel);

		Stop(channel);

		ALuint buffer = LoadSound(filename)->Buffer;
		ALuint source = 0;

		alGenSources(1, &source);

		alSourcei (source, AL_BUFFER, buffer);
		alSource3f(source, AL_POSITION,        pos.x, pos.y, 0.0f);
		alSource3f(source, AL_VELOCITY,         0.0f,  0.0f, 0.0f);
		alSource3f(source, AL_DIRECTION,        0.0f,  0.0f, 0.0f);
		alSourcef (source, AL_GAIN,             gain			 );
		alSourcei (source, AL_SOURCE_RELATIVE,  AL_TRUE          );
		alSourcei (source, AL_LOOPING,	    loop?AL_TRUE:AL_FALSE);

		if (IsALError("CComposition::Play"))
		{
			if (alIsSource(source))
			{
				alDeleteSources(1, &source);
				IsALError("CComposition::alDeleteSources");
			}
			LogMsg( "[ERR] [AUD] Nie mozna odtworzyc dzwieku : " + filename );
			return;
		}

		alSourcePlay(source);

		Channel[channel] = source;
		Pos[channel] = pos;
		Gain[channel] = gain;
	}

	void CComposition::Stop(msize channel)
	{
		assert(Channel.size() >= channel);

		ALuint tmp = BAD_CHANNEL;

		if (Channel[channel] != BAD_CHANNEL)
		{
			tmp = Channel[channel];
			alSourceStop(tmp);
			alDeleteSources(1, &tmp);
			Channel[channel] = BAD_CHANNEL;
		}
	}

	void CComposition::Hush()
	{
		for (msize i = 0; i < Channel.size(); i++)
		{
			Stop(i);
		}
	}

	void SetSoundVolume(float v)
	{
		GSoundGain = ClipValue(v, 0.0f, 1.0f);
		GSource.SetGain(v);
	}

	float GetSoundVolume()
	{
		return GSource.GetGain();
	}

	void Scream(const tstring& filename)
	{
		GSource.Scream(filename);
	}

	void Hush()
	{
		GSource.Hush();
	}

	void SetListenerPos(const vect2f& v)
	{
		alListener3f(AL_POSITION, (ALfloat)v.x, (ALfloat)v.y, 0.0f);
	}

	vect2f GetListenerPos()
	{
		ALfloat fx, fy, fz;
		alGetListener3f(AL_POSITION, &fx, &fy, &fz);
		return vect2f((float)fx, (float)fy);
	}

	//=======================================================================
	// Muzyka
	//=======================================================================

	bool IsMusicPlaying()
	{
		ALenum state;
		alGetSourcei(GMusicStuff.Source, AL_SOURCE_STATE, &state);
		return (state == AL_PLAYING);
	}

	char data[OGG_BUFFER_SIZE];

	bool MusicStream(ALuint buffer)
	{
		int  size = 0;
		int  section;
		int  result;

		while(size < OGG_BUFFER_SIZE)
		{
			result = ov_read(&GMusicStuff.Stream, data+size, OGG_BUFFER_SIZE-size, 0, 2, 1, &section);

			if(result > 0) size += result;						// Wszystko ok czytamy dalej
			else if(result < 0) { assert(0); return false; }	// Jakis blad w strumieniu - przerywamy czytanie
				 else break;									// Koniec strumienia
		}

		if(size == 0) return false;

		alBufferData(buffer, GMusicStuff.Format, data, size, GMusicStuff.Stream.vi->rate);
		IsALError("MusicStream::alBufferData");

		return true;
	}

	bool MusicPlayback()
	{
		if (alIsSource(GMusicStuff.Source) == AL_FALSE) return false;
		if(IsMusicPlaying()) return true;

		// Odkolejkowanie
		// Czasami (bardzo rzadko) w czasie odtwarzania, bufor moze sie skonczyc zbyt szybko
		// W takich chwilach ta funkcja jest wywolywana na nowo wywalajac buforki z kolejki
		// wypelniajac je na nowo i wprowadzajac zpowrotem do kolejki
		// Gdyby nie to, to funkcja alBufferData zwracala by blad AL_INVALID_VALUE (nie mozna wypelniac buforka w kolejce!)
		int queued;
		alSourceStop(GMusicStuff.Source);
		alGetSourcei(GMusicStuff.Source, AL_BUFFERS_QUEUED, &queued);
		while(queued--)
		{
			ALuint buffer;
			alSourceUnqueueBuffers(GMusicStuff.Source, 1, &buffer);
			IsALError("MusicPlayback::alSourceUnqueueBuffers");
		}

		if(!MusicStream(GMusicStuff.Buffers[0])) return false;
		if(!MusicStream(GMusicStuff.Buffers[1])) return false;

		alSourceQueueBuffers(GMusicStuff.Source, 2, GMusicStuff.Buffers);
		alSourcePlay(GMusicStuff.Source);

		return true;
	}

	bool PlayMusicFile(const tstring& path, const std::function<void()>& on_end)
	{
		// Jesli jakis plik jest odtwarzany to zatrzymujemy go
		StopMusicFile();

		GMusicStuff.File = vfsGetFile(path);

		if (GMusicStuff.File->IsOK() == false)
		{
			LogMsg( "[ERR] [OAL] Nie mozna otworzyc pliku z muzyka : " + path );
			return false;
		}

		int result;
		if((result = ov_open_callbacks(GMusicStuff.File.get(), &GMusicStuff.Stream, nullptr, 0, GOggCallbacks)) < 0)
		{
			LogMsg( "[ERR] [OAL] To nie jest prawidlowy plik OGG vorbis : " + path );
			return false;
		}

		// Format
		if(GMusicStuff.Stream.vi->channels == 1) GMusicStuff.Format = AL_FORMAT_MONO16;
		else GMusicStuff.Format = AL_FORMAT_STEREO16;

		// Generujemy zrodelko
		alGenSources(1, &GMusicStuff.Source);
		if (IsALError("PlayMusicFile::alGenSources"))
		{
			ov_clear(&GMusicStuff.Stream);
			LogMsg( "[ERR] [OAL] Nie mozna wygenerowac zrodla : " + path );
			return false;
		}

		// Generujemy buforki
		alGenBuffers(2, GMusicStuff.Buffers);
		if (IsALError("PlayMusicFile::alGenBuffers"))
		{
			alDeleteSources(1, &GMusicStuff.Source);
			IsALError("PlayMusicFile::alDeleteSources");
			ov_clear(&GMusicStuff.Stream);
			LogMsg( "[ERR] [OAL] Nie mozna wygenerowac buforow : " + path );
			return false;
		}

		alSource3f(GMusicStuff.Source, AL_POSITION,        0.0f, 0.0f, 0.0f);
		alSource3f(GMusicStuff.Source, AL_VELOCITY,        0.0f, 0.0f, 0.0f);
		alSource3f(GMusicStuff.Source, AL_DIRECTION,       0.0f, 0.0f, 0.0f);
		alSourcef (GMusicStuff.Source, AL_GAIN,         (ALfloat)GMusicGain);
		alSourcef (GMusicStuff.Source, AL_ROLLOFF_FACTOR,  0.0f            );
		alSourcei (GMusicStuff.Source, AL_SOURCE_RELATIVE, AL_TRUE         );

		bool r = MusicPlayback();
		GMusicStuff.IsPause = false;
		GMusicStuff.IsMusic = r;
		if (r)
		{
			GMusicStuff.OnMusicEnd = on_end;
			GMusicStuff.ActiveMusicFilename = path;
		}

		return r;
	}

	void ReplayLastMusicFile()
	{
		if (GMusicStuff.ActiveMusicFilename.empty()) return;

		PlayMusicFile(GMusicStuff.ActiveMusicFilename, GMusicStuff.OnMusicEnd);
	}

	void StopMusicFile()
	{
		if (alIsSource(GMusicStuff.Source) == AL_FALSE) return;

		// Zwolenienie danych wszelakich
		int queued;
		alGetSourcei(GMusicStuff.Source, AL_BUFFERS_QUEUED, &queued);

		// Zatrzymujemy zrodelko
		alSourceStop(GMusicStuff.Source);

		while(queued--)
		{
			ALuint buffer;
			alSourceUnqueueBuffers(GMusicStuff.Source, 1, &buffer);
			IsALError("StopMusicFile::alSourceUnqueueBuffers");
		}

		alDeleteSources(1, &GMusicStuff.Source); IsALError("StopMusicFile::alDeleteSources");
		alDeleteBuffers(2, GMusicStuff.Buffers); IsALError("StopMusicFile::alDeleteBuffers");

		// W tym miejscu plik jest zamykany
		GMusicStuff.ClearOnStopMusic();
	}

	void PauseMusicFile()
	{
		GMusicStuff.IsPause = true;
	}

	void ResumeMusicFile()
	{
		GMusicStuff.IsPause = false;
	}

	tstring GetMusicComment(const char* str)
	{
		vorbis_comment* vc = ov_comment(&GMusicStuff.Stream, -1);

		if (vc!=nullptr)
		{
			for (int i=0; i < (vc->comments); i++)
			{
				const char* pch = strstr(vc->user_comments[i], str);
				if (pch != nullptr)
				{
					pch = strstr(pch, "=");
					if (pch != nullptr)
					{
						pch++;
						return pch;
					}
				}
			}
		}

		return tstring("Unknown");
	}

	void SetMusicVolume(float v)
	{
		GMusicGain = v;
		if (alIsSource(GMusicStuff.Source) == AL_FALSE) return;
		alSourcef(GMusicStuff.Source, AL_GAIN, (ALfloat)GMusicGain);
	}

	float GetMusicVolume()
	{
		return GMusicGain;
	}

	bool UpdateMusicStream()
	{
		if (alIsSource(GMusicStuff.Source)==AL_FALSE) return false;

		int processed;
		bool active = true;

		alGetSourcei(GMusicStuff.Source, AL_BUFFERS_PROCESSED, &processed);

		while(processed--)
		{
			ALuint buffer;

			alSourceUnqueueBuffers(GMusicStuff.Source, 1, &buffer);
			active = MusicStream(buffer);
			alSourceQueueBuffers(GMusicStuff.Source, 1, &buffer);
		}

		return active;
	}

	bool CreateAudioSystem()
	{
		assert((Device == nullptr) && (Context == nullptr));

		GOggCallbacks.read_func = o_read_func;
		GOggCallbacks.seek_func = o_seek_func;
		GOggCallbacks.close_func = o_close_func;
		GOggCallbacks.tell_func = o_tell_func;

		Device = alcOpenDevice(nullptr);
		if (Device != nullptr)
		{
			Context = alcCreateContext(Device, nullptr);
			if (Context != nullptr)
			{
				alcMakeContextCurrent(Context);

				auto e = alcGetError(Device);
				if (e != ALC_NO_ERROR)
				{
					LogMsg( "[ERR] [OAL] CreateAudioSystem::alcMakeContextCurrent : " + ToString(e) );
					assert(!"IsALCError");
					return false;
				}
			}
			else
			{
				LogMsg( "[ERR] [OAL] Nie mozna stworzyc kontekstu karty dzwiekowej" );
				return false;
			}
		}
		else
		{
			LogMsg( "[ERR] [OAL] Nie mozna zainicalizowac sterownika dzwieku" );
			return false;
		}

		ALfloat ListenerPos[] = { 0.0f, 0.0f, 0.0f };
		ALfloat ListenerVel[] = { 0.0f, 0.0f, 0.0f };
		ALfloat ListenerOri[] = { 0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f };

		alListenerfv(AL_POSITION,    ListenerPos);
		alListenerfv(AL_VELOCITY,    ListenerVel);
		alListenerfv(AL_ORIENTATION, ListenerOri);

		GMusicGain = 1.0f;
		GSoundGain = 1.0f;

		LogMsg("[OAL] Driver: %s - %s", alGetString(AL_VERSION), alGetString(AL_VENDOR));
		LogMsg("[OAL] Renderer: %s", alGetString(AL_RENDERER));

		return true;
	}

	void DeleteAudioSystem()
	{
		GSoundLoadProcessList.clear();

		StopMusicFile();

		ClearSoundDataBase();

		Context = alcGetCurrentContext();
		Device = alcGetContextsDevice(Context);
		alcMakeContextCurrent(nullptr);

		if (Context) { alcDestroyContext(Context); Context = nullptr; }
		if (Device) { alcCloseDevice(Device); Device = nullptr; }
	}

	void UpdateAudioSystem()
	{
		remove_erase_if( GSoundLoadProcessList, [](std::future<RefSound>& sound_process) -> bool
		{
			if (is_ready(sound_process))
			{
				auto sound = sound_process.get();

				if (GSoundDataBase.Exist(sound->Filename) == false)
				{
					GSource.Scream(sound);
					GSoundDataBase.InsertResource(sound->Filename, sound);
				}

				return true;
			}

			return false;
		});

		// Dzwieki
		GSource.Update();

		// Czy jest jakas muza do odtwarzania?
		if ((GMusicStuff.IsMusic) && (!GMusicStuff.IsPause))
		{
			if (!UpdateMusicStream())
			{
				// Muzyka sie skonczyla
				StopMusicFile();
				if (GMusicStuff.OnMusicEnd) GMusicStuff.OnMusicEnd();
				return;
			}

			// W przypadku przerwania odnawiamy strumien - jak sie nie uda konczymy muze
			if (!IsMusicPlaying())
			{
				if (!MusicPlayback())
				{
					StopMusicFile();
					if (GMusicStuff.OnMusicEnd) GMusicStuff.OnMusicEnd();
				}
			}
		}
	}

} // End oo

#endif // OMIKRON2_DEDICATED_SERVER
