﻿/*
 * Copyright © 2013 Karol Pałka
 */

// C
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cwchar>
#include <cstdint>
#include <cmath>

// C++
#include <string>
#include <algorithm>
#include <vector>
#include <list>
#include <limits>
#include <map>

// C++11
#include <initializer_list>
#include <future>
#include <thread>
#include <chrono>
#include <atomic>

// C++14
//#include <filesystem>

#ifdef _MSC_VER
	#pragma warning(disable : 4267)  // size_t to long/dword/ulong
	#pragma warning(disable : 4100)  // 'identifier' : unreferenced formal parameter
	#pragma warning(disable : 4201)	 // nonstandard extension used : nameless struct/union

	#define O2_VISUAL_CPP
#elif __GNUG__
    #define O2_GCC
#else
	#error "Unknown compiler"
#endif

// Systemowe headery
#ifdef _WIN32 // Windows
	#ifdef __GNUG__
		#define PFD_SUPPORT_COMPOSITION 0x00008000
	#endif

	#define WINVER 0x0501
	#define _WIN32_WINNT 0x0501
	#define _WIN32_IE 0x0700

	#ifndef NOMINMAX
		#define NOMINMAX
	#endif

	#define WIN32_LEAN_AND_MEAN
	#include <winsock2.h>
	#include <windows.h>
	#include <mmsystem.h>
	#include <shellapi.h>

	#define O2_LINE_TERMINATOR "\r\n"
	#define O2_PATH_SEPARATOR '\\'

	#define O2_SYS_WINDOWS

	#include <gl/gl.h>
	#include <gl/glext.h>

#elif __APPLE__
	#define O2_LINE_TERMINATOR "\n"
	#define O2_PATH_SEPARATOR '/'

	#define O2_SYS_MAC

#elif __linux__
	#define O2_LINE_TERMINATOR "\n"
	#define O2_PATH_SEPARATOR '/'

	#define O2_SYS_LINUX

	#include <GL/gl.h>
	#include <GL/glext.h>
#else
	#error "Unknown OS"
#endif

#define BOOST_BIND_NO_PLACEHOLDERS
#define BOOST_DATE_TIME_NO_LIB
#define BOOST_REGEX_NO_LIB

#include <utf8.h>

#include <boost/asio.hpp>

#include <boost/tuple/tuple.hpp>
#include <boost/range/as_literal.hpp>
#include <boost/circular_buffer.hpp>

#ifdef OMIKRON2_ADDITIONAL_PCH_FILES
#include "additional.hpp"
#endif

#include <SFML/Window.hpp>
